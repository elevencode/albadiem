<?php
/*
Template Name: Single page template
*/
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package albadiem
 */


get_header(); 

get_template_part( 'inc/layouts/header' ); ?>



	<!--[if lt IE 8]>

		<p class="browserupgrade">Koristite <strong>zastarjeli</strong> preglednik. Molimo <a href="http://browsehappy.com/">nadogradite preglednik</a> kako biste mogli koristiti stranicu.</p>

	<![endif]-->



	<!-- Wrapper -->

	<div id="wrapper">

		<?php require_once 'inc/advertising/top-bar.php'; ?>
        <?php
            
            // Start the loop.
            while ( have_posts() ) : the_post(); 
            
            require_once 'inc/layouts/banner-blog-single.php'; ?>

		<!-- Main Content

		================================================== -->
        
		<main id="content" role="main" class="single-page">

           <div id="single-page-intro" class="container-fluid single-blog-post">

                <div class="container">
                    <div class="row">
                    <?php require_once 'inc/layouts/breadcrumb-blog.php'; ?>
                        <div class="single-page-content">
                            
                            

                            <div class="col-md-12">

                                <div class="pos-relative">
                                <?php

                                    $featuredBlogPostImg = get_the_post_thumbnail_url();

                                    echo '<img class="blog-lazy-load-img lazy-load-trigger-img" data-width="600" data-height="400" data-ratio="1.5" data-desktop-src="'.$featuredBlogPostImg.'"	data-tablet-src="'.$featuredBlogPostImg.'"	data-mobile-src="'.$featuredBlogPostImg.'" />';

                                ?>
                                </div>

                                <noscript>
                                <img src="<?php echo $featuredBlogPostImg; ?>" alt="<?php the_title(); ?>" />
                                </noscript>

                                <?php echo the_content(); ?> 
                            </div>

                        </div>
                    </div>
                </div>
        
            </div>

        </main>
		<!-- Main Content / End -->
        
        <?php endwhile;?>

		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>
    
  </div>  
  
<?php

get_template_part( 'inc/layouts/footer' ); 
