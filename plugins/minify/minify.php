<?php 
	
	require_once("../../../../../wp-load.php");
	
	/* ACCESS GLOBAL SITE VARIABLE */
	global $site;
		
	/* LOAD MINIFY */
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/Minify.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/CSS.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/JS.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/Exception.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/Exceptions/BasicException.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/Exceptions/FileImportException.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/Exceptions/IOException.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/path-converter-master/src/ConverterInterface.php');
	require_once($site["path"].'wp-content/themes/ranklio/inc/minify/src/path-converter-master/src/Converter.php');
	use MatthiasMullie\Minify;

	global $minifiedLibs;	
	$minifiedLibs = [];

	function getMinified($sourceName,$sourcePath,$minifiedPath,$minifyType){
		global $minifiedLibs, $site;	
		if($minifyType=="css"){
			$minifier = new Minify\CSS($site["path"]."/".$sourcePath);
		}else if($minifyType=="js"){
			$minifier = new Minify\JS($site["path"]."/".$sourcePath);
		}

		// we can even add another file, they'll then be
		// joined in 1 output file
		//$sourcePath2 = '/path/to/second/source/css/file.js';
		//$minifier->add($sourcePath2);

		// or we can just add plain js
		//$js = 'var test = 1';
		//$minifier->add($js);

		// save minified file to disk
		//$minifier->minify($minifiedPath);
		$minified = $minifier->minify();
		file_put_contents($site["path"]."/".$minifiedPath, $minified);
		$minifiedContent = $minifier->minify();

		
		$minifiedLibs[] = array("name"=>$sourceName,"sourcePath"=>$sourcePath,"minifiedPath"=>$minifiedPath,"minifiedContent"=>$minifiedContent);
	}
	
	getMinified("ionicIconsCss",$GLOBALS['site']['templateDirectoryRelPath']."/"."css/ionic/ionicons.css",$GLOBALS['site']['templateDirectoryRelPath']."/"."css/ionic/ionicons.min.css","css");
	getMinified("styleCss",$GLOBALS['site']['templateDirectoryRelPath']."/"."css/style.css",$GLOBALS['site']['templateDirectoryRelPath']."/"."css/style.min.css","css");
	getMinified("flickityJs",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/flickity/flickity.js",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/flickity/flickity.min.js","js");
	getMinified("flickityBgLazyLoadJs",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/flickity/bg-lazyload.js",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/flickity/bg-lazyload.min.js","js");
	getMinified("mainJs",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/main.js",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/main.min.js","js");
	getMinified("loadAsyncJs",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/loadAsync.js",$GLOBALS['site']['templateDirectoryRelPath']."/"."js/loadAsync.min.js","js");
	
	$response["success"] = true;
	echo json_encode($response);
