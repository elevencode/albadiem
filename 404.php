<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package albadiem
 */

get_header(); 

get_template_part( 'inc/layouts/header' ); ?>



	<!--[if lt IE 8]>

		<p class="browserupgrade">Koristite <strong>zastarjeli</strong> preglednik. Molimo <a href="http://browsehappy.com/">nadogradite preglednik</a> kako biste mogli koristiti stranicu.</p>

	<![endif]-->



	<!-- Wrapper -->

	<div id="wrapper">

		<?php require_once 'inc/advertising/top-bar.php'; ?>
		
		<?php require_once 'inc/layouts/banner-404.php'; ?>

		<!-- Main Content

		================================================== -->
        
		<main id="content" role="main" class="single-page">


            <div id="single-page-intro" class="container-fluid">

                <div class="container">
                    <div class="row">

											<div class="col-md-12">

                        <div class="single-page-content" style="margin: 50px 0;">

														<section class="error-404 not-found">

															<div class="page-content">
																<p><?php _e( 'Izgleda da na ovoj lokaciji nije pronađeno ništa. Možda isprobajte neku od donjih poveznica?', 'albadiem' ); ?></p>

																<p>&nbsp;</p>
																
																<?php
																	the_widget( 'WP_Widget_Recent_Posts' );
																?>

																<?php if(false): ?>	
																<div class="widget widget_categories">
																	<h2 class="widget-title"><?php esc_html_e( 'Najčešće korištene kategorije', 'albadiem' ); ?></h2>
																	<ul>
																		<?php
																		wp_list_categories( array(
																			'orderby'    => 'count',
																			'order'      => 'DESC',
																			'show_count' => 1,
																			'title_li'   => '',
																			'number'     => 10,
																		) );
																		?>
																	</ul>
																</div><!-- .widget -->
																<?php endif; ?>
															
															</div><!-- .page-content -->
														</section><!-- .error-404 -->

                        </div>

											</div>

                    </div>
                </div>
        
            </div>

        </main>
		<!-- Main Content / End -->

		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>
<?php

get_template_part( 'inc/layouts/footer' ); 

