/* GET WIDTH */

function getWindowWidth() {
	return (
		window.innerWidth ||
		document.documentElement.clientWidth ||
		document.body.clientWidth
	);
}

/* GET HEIGHT */

function getWindowHeight() {
	return (
		window.innerHeight ||
		document.documentElement.clientHeight ||
		document.body.clientHeight
	);
}

/* Framework var */

var customSelector;
var Selector;
var customEvent;
var cEvent;
var szvAdvertisting;
var advertisingOn = true;

/* Content var */

var cookieNotiTitle = '<span class="cookieNotiTitle">Privatnost</span>';
var cookieNotiText =	'<span class="cookieNotiText">Koristimo kolačiće i slažeš se da ih skupljamo? <a href="https://en.wikipedia.org/wiki/HTTP_cookie">Kolačiće</a> možeš isključiti, ali stranica onda neće biti u potpunosti funkcionalna. Pročitaj kako brinemo o tvojim podacima <a href="">ovdje</a>.';

/* Timer */

var resizeTimer;
var scrollTimer;

/* Width */

var onloadWidth = getWindowWidth();

/* Main Nav */

var mainNav;

/* Cookie */

var jsCookie;

/* Sticky */

var stickyMMenu;

var stickyHeader;

/* Main search */

var mainSearch;

//DOM READY

var domReady = function(fn) {
	// Sanity check

	if (typeof fn !== "function") return;

	// If document is already loaded, run method

	if (document.readyState === "complete") {
		return fn();
	}

	// Otherwise, wait until document is loaded

	document.addEventListener("DOMContentLoaded", fn, false);
};

/* ROUND NUMBER */

function roundNum(num, dec) {
	return +(Math.round(num + "e+" + dec) + "e-" + dec);
}

//GET WINDOW SCROLL OFFSET

function getWindowScrollOffest(type) {
	if (type == "x") {
		return window.pageXOffset || document.documentElement.scrollLeft;
	} else if (type == "y") {
		return window.pageYOffset || document.documentElement.scrollTop;
	}
}

function dw_getScrollOffsets() {
	var doc = document,
		w = window;

	var x, y, docEl;

	if (typeof w.pageYOffset === "number") {
		x = w.pageXOffset;

		y = w.pageYOffset;
	} else {
		docEl =
			doc.compatMode && doc.compatMode === "CSS1Compat"
				? doc.documentElement
				: doc.body;

		x = docEl.scrollLeft;

		y = docEl.scrollTop;
	}

	return { x: x, y: y };
}

//CLEAN STRING

function cleanString(str, type) {
	var cleanStr;

	switch (type) {
		case "trimLeadTrail":
			cleanStr = str.replace(/(^\s+|\s+$)/g, " ");

			break;

		case "trimAll":
			cleanStr = str.replace(/ +/g, " ");

			break;

		default:
			cleanStr = "";
	}

	return cleanStr;
}

//GET FORM FIELD VALUE

function getFormFiedValue(name, type) {
	var fieldValue = "";

	var name = name.replace(/\s+/g, "");

	switch (type) {
		case "text":
			fieldValue = cleanString(
				document.getElementsByName(name)[0].value,
				"trimAll"
			);

			break;

		case "radio":
			fieldValue = document.getElementsByName(name)[0].checked ? 1 : 0;

			break;

		case "checkbox":
			fieldValue = document.getElementsByName(name)[0].checked ? 1 : 0;

			break;

		case "select":
			fieldValue = cleanString(
				document.getElementsByName(name)[0].value,
				"trimAll"
			);

			break;

		case "textarea":
			fieldValue = cleanString(
				document.getElementsByName(name)[0].value,
				"trimAll"
			);

			break;

		default:
			fieldValue = cleanString(
				document.getElementsByName(name)[0].value,
				"trimAll"
			);
	}

	return fieldValue;
}

//OPEN URL

function openUrl(url, name) {
	window.open(url, name);
}

//CUSTOM SELECTOR

function cleanSelector(selector) {
	var cleanSelector = selector.replace(".", "");

	cleanSelector = cleanSelector.replace("#", "");

	return cleanSelector;
}

//IS STRING

function isString(selector) {
	if (typeof selector == "string" || selector instanceof String) {
		return true;
	} else {
		return false;
	}
}

//SHUFFLE

function shuffle(nmbrOfElements, containerId, shuffleClass) {
	// Prepare array with max number of elements

	var array = [];

	var i,
		start = 0,
		end;

	for (i = start; i < nmbrOfElements; i++) {
		array[i] = i + 1;
	}

	// Shuffle array

	var currentIndex = array.length;

	var temporaryValue, randomIndex;

	while (0 !== currentIndex) {
		// Pick a remaining element...

		randomIndex = Math.floor(Math.random() * currentIndex);

		currentIndex -= 1;

		// And swap it with the current element.

		temporaryValue = array[currentIndex];

		array[currentIndex] = array[randomIndex];

		array[randomIndex] = temporaryValue;
	}

	// Collect initial html and order

	var initialArray = [];

	Selector.Each(containerId + " " + shuffleClass, function(e) {
		var id = e.getAttribute("id");

		initialArray[id] = e.innerHTML;
	});

	// Set new shuffled order

	var tmpId = "",
		tmpHTML = "",
		j = 0;

	for (i = 0; i < array.length; i++) {
		j = i + 1;

		tmpId = "shuffle-" + array[i];

		tmpHTML = initialArray[tmpId];

		Selector.Html("#shuffle-" + j, tmpHTML);
	}
}

//SCROLL TO

function scrollToElement(element, speed, customOffset) {
	var defaultDuration = 777; // ms

	var edgeOffset = 180; // px

	if (Selector.HasClass("#header", ".is-sticky")) {
		if (getWindowWidth() > 991) {
			edgeOffset = 100;
		} else {
			edgeOffset = 100;
		}
	} else if (Selector.ExistsBool("#single-affix-cta-mobile")) {
		if (getWindowWidth() > 991) {
			edgeOffset = 20;
		} else {
			edgeOffset = 90;
		}
	}

	edgeOffset += customOffset;

	//console.log('\nedgeOffset: '+edgeOffset);

	zenscroll.setup(defaultDuration, edgeOffset);

	zenscroll.to(element, speed);
}

function scrollToY(y, speed) {
	/*

	var defaultDuration = 777; // ms

	var edgeOffset = 42; // px

	zenscroll.setup(defaultDuration, edgeOffset);

	*/

	zenscroll.toY(y, speed);
}

// Document ready

domReady(function() {
	/* var customSelector = function () { }; */

	customSelector = function() {};

	customSelector.prototype = {
		cleanSelector: null,

		cleanSelectorName: null,

		setSelector: function(selector, selectorName) {
			this.cleanSelector = cleanSelector(selector);

			this.cleanSelectorName = cleanSelector(selectorName);
		},

		setSelectorName: function(selectorName) {
			this.cleanSelectorName = cleanSelector(selectorName);
		},

		AddClass: function(selector, selectorName) {
			if (isString(selector)) {
				this.setSelector(selector, selectorName);

				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].classList.add(this.cleanSelectorName);
				}
			} else {
				this.setSelectorName(selectorName);

				selector.classList.add(this.cleanSelectorName);
			}
		},

		RemoveClass: function(selector, selectorName) {
			if (isString(selector)) {
				this.setSelector(selector, selectorName);

				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].classList.remove(this.cleanSelectorName);
				}
			} else {
				this.setSelectorName(selectorName);

				selector.classList.remove(this.cleanSelectorName);
			}
		},

		ToggleClass: function(selector, selectorName) {
			if (isString(selector)) {
				this.setSelector(selector, selectorName);

				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].classList.toggle(this.cleanSelectorName);
				}
			} else {
				this.setSelectorName(selectorName);

				selector.classList.toggle(this.cleanSelectorName);
			}
		},

		HasClass: function(el, className) {
			className = cleanSelector(className);

			if (isString(el)) {
				var elements = document.querySelectorAll(el);

				var hasClass = false;

				for (var i = 0; i < elements.length; i++) {
					if (
						elements[i].classList
							? elements[i].classList.contains(className)
							: new RegExp("\\b" + className + "\\b").test(
									elements[i].className
							  )
					) {
						hasClass = true;
					}
				}

				return hasClass;
			} else {
				return el.classList
					? el.classList.contains(className)
					: new RegExp("\\b" + className + "\\b").test(el.className);
			}
		},

		ClearAllClasses: function(selector) {
			if (isString(selector)) {
				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].setAttribute("class", "");
				}
			} else {
				this.setSelectorName(selectorName);

				selector.setAttribute("class", "");
			}
		},

		SetAttribute: function(selector, type, selectorName) {
			if (isString(selector)) {
				this.setSelector(selector, selectorName);

				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].setAttribute(type, this.cleanSelectorName);
				}
			} else {
				this.setSelectorName(selectorName);

				selector.setAttribute(type, this.cleanSelectorName);
			}
		},

		RemoveAttribute: function(selector, type, selectorName) {
			if (isString(selector)) {
				var elements = document.querySelectorAll(selector);

				for (var i = 0; i < elements.length; i++) {
					elements[i].setAttribute(type, "");
				}
			} else {
				this.setSelectorName(selectorName);

				selector.setAttribute(type, "");
			}
		},

		GetClosest: function(elem, selector) {
			if (!Element.prototype.matches) {
				Element.prototype.matches =
					Element.prototype.matchesSelector ||
					Element.prototype.mozMatchesSelector ||
					Element.prototype.msMatchesSelector ||
					Element.prototype.oMatchesSelector ||
					Element.prototype.webkitMatchesSelector ||
					function(s) {
						var matches = (
								this.document || this.ownerDocument
							).querySelectorAll(s),
							i = matches.length;

						while (--i >= 0 && matches.item(i) !== this) {}

						return i > -1;
					};
			}

			for (; elem && elem !== document; elem = elem.parentNode) {
				if (elem.matches(selector)) return elem;
			}

			return null;
		},

		FadeOut: function(element, speed, callback) {
			if (!element.style.opacity) {
				element.style.opacity = 1;
			}

			var start = null;

			window.requestAnimationFrame(function animate(timestamp) {
				start = start || timestamp;

				var progress = timestamp - start;

				element.style.opacity = 1 - progress / speed;

				if (progress >= speed) {
					if (callback && typeof callback === "function") {
						element.setAttribute("style", "display:none !important");

						callback();
					}
				} else {
					window.requestAnimationFrame(animate);
				}
			});
		},

		FadeIn: function(element, speed, callback) {
			if (!element.style.opacity) {
				element.style.opacity = 0;
			}

			var start = null;

			window.requestAnimationFrame(function animate(timestamp) {
				start = start || timestamp;

				var progress = timestamp - start;

				element.style.opacity = progress / speed;

				if (progress >= speed) {
					if (callback && typeof callback === "function") {
						element.setAttribute("style", "display:block !important");

						callback();
					}
				} else {
					window.requestAnimationFrame(animate);
				}
			});
		},

		ExistsBool: function(element) {
			var elements = document.querySelectorAll(element);

			if (elements.length > 0) {
				return true;
			} else {
				return false;
			}
		},

		Exists: function(element, callback) {
			var elements = document.querySelectorAll(element);

			if (elements.length > 0) {
				callback();
			}
		},

		Each: function(element, callback) {
			var elements = document.querySelectorAll(element);

			for (var i = 0; i < elements.length; i++) {
				callback(elements[i]);
			}
		},

		Html: function(element, html) {
			if (isString(element)) {
				var elements = document.querySelectorAll(element);

				for (var i = 0; i < elements.length; i++) {
					elements[i].innerHTML = html;
				}
			} else {
				element.innerHTML = html;
			}
		},

		GetText: function(element) {
			var text = "";

			if (isString(element)) {
				var elements = document.querySelectorAll(element);

				for (var i = 0; i < elements.length; i++) {
					text = elements[i].textContent || elements[i].innerText;
				}
			} else {
				text = element.textContent || element.innerText;
			}

			return text;
		}
	};

	/* var Selector = new customSelector(); */

	Selector = new customSelector();

	/* CUSTOM EVENT */

	/* var customEvent = function () { }; */

	customEvent = function() {};

	customEvent.prototype = {
		Trigger: function(elem, eventType, callback) {
			if ("createEvent" in document) {
				// modern browsers, IE9+

				var e = document.createEvent("HTMLEvents");

				e.initEvent(eventType, false, true);

				if (!isString(elem)) {
					elem.dispatchEvent(e);
				} else {
					var elements = document.querySelectorAll(elem);

					for (var i = 0; i < elements.length; i++) {
						elements[i].dispatchEvent(e);
					}
				}
			} else {
				// IE 8

				var e = document.createEventObject();

				e.eventType = eventType;

				if (!isString(elem)) {
					elem.fireEvent("on" + e.eventType, e);
				} else {
					var elements = document.querySelectorAll(elem);

					for (var i = 0; i < elements.length; i++) {
						elements[i].fireEvent("on" + e.eventType, e);
					}
				}
			}

			return callback(e);
		},

		Remove: function(elem, eventType, handler) {
			if (!isString(elem)) {
				if (elem.removeEventListener)
					elem.removeEventListener(eventType, handler, false);

				if (elem.detachEvent) elem.detachEvent("on" + eventType, handler);
			} else {
				var elements = document.querySelectorAll(elem);

				for (var i = 0; i < elements.length; i++) {
					if (elements[i].removeEventListener)
						elements[i].removeEventListener(eventType, handler, false);

					if (elements[i].detachEvent)
						elements[i].detachEvent("on" + eventType, handler);
				}
			}
		},

		Attach: function(selector, type, callback) {
			var laThis = this;

			if (window.attachEvent) {
				if (!isString(selector)) {
					selector.attachEvent(type, function(e) {
						// remove event

						//laThis.Remove(e.target,type,callback);

						// call handler

						return callback(e);
					});
				} else {
					var elements = document.querySelectorAll(selector);

					for (var i = 0; i < elements.length; i++) {
						elements[i].attachEvent(type, function(e) {
							// remove event

							//laThis.Remove(e.target,type,callback);

							// call handler

							return callback(e);
						});
					}
				}
			} else {
				if (!isString(selector)) {
					selector.addEventListener(type, function(e) {
						// remove event

						//laThis.Remove(e.target,type,callback);

						// call handler

						return callback(e);
					});
				} else {
					var elements = document.querySelectorAll(selector);

					for (var i = 0; i < elements.length; i++) {
						elements[i].addEventListener(type, function(e) {
							// remove event

							//laThis.Remove(e.target,type,callback);

							// call handler

							return callback(e);
						});
					}
				}
			}
		},

		ClickWithOutsideTrack: function(selector, type, callback) {
			var laThis = this;

			if (window.attachEvent) {
				if (!isString(selector)) {
					selector.attachEvent(type, function(e) {
						// remove event

						//laThis.Remove(e.target,type,callback);

						// call handler

						return callback(e);
					});
				} else {
					var elements = document.querySelectorAll(selector);

					for (var i = 0; i < elements.length; i++) {
						elements[i].attachEvent(type, function(e) {
							// remove event

							//laThis.Remove(e.target,type,callback);

							// call handler

							return callback(e);
						});
					}
				}
			} else {
				if (!isString(selector)) {
					selector.addEventListener(type, function(e) {
						// remove event

						//laThis.Remove(e.target,type,callback);

						// call handler

						return callback(e);
					});
				} else {
					var elements = document.querySelectorAll(selector);

					for (var i = 0; i < elements.length; i++) {
						elements[i].addEventListener(type, function(e) {
							// remove event

							//laThis.Remove(e.target,type,callback);

							// call handler

							return callback(e);
						});
					}
				}
			}
		}
	};

	/* var cEvent = new customEvent(); */

	cEvent = new customEvent();

	/* VALIDATION */

	var forms = document.querySelectorAll("form");

	for (var i = 0; i < forms.length; i++) {
		forms[i].setAttribute("novalidate", true);
	}

	var datetimeout;

	var dateInputs = document.querySelectorAll("input.date");

	for (var i = 0; i < dateInputs.length; i++) {
		cEvent.Attach(dateInputs[i], "keydown", function(e) {
			clearTimeout(datetimeout);

			datetimeout = setTimeout(function() {
				e.target.value = "";
			}, 30);
		});
	}

	var customValidatio = function() {};

	customValidatio.prototype = {
		ValidateEmail: function(selector, value) {
			var flag = true;

			if (Selector.HasClass(selector, ".required")) {
				flag = this.IsEmail(value) ? true : false;
			} else {
				if (value != "") {
					flag = this.IsEmail(value) ? true : false;
				} else {
					flag = true;
				}
			}

			return flag;
		},

		ValidateText: function(selector, value) {
			var flag = true;

			if (Selector.HasClass(selector, ".required")) {
				flag = value.length > 0 ? true : false;
			}

			return flag;
		},

		ValidateTextarea: function(selector, value) {
			var flag = true;

			if (Selector.HasClass(selector, ".required")) {
				flag = value.length > 0 ? true : false;
			}

			return flag;
		},

		ValidateSelect: function(selector, value) {
			var flag = true;

			var selectId = Selector.GetClosest(selector, "select").getAttribute("id");

			var elements = document.querySelectorAll("#" + selectId + " option");

			var br = 0;

			if (Selector.HasClass(selector, ".required")) {
				for (var i = 0; i < elements.length; i++) {
					if (elements[i].selected == true && elements[i].value != "-----") {
						br++;
					}
				}

				if (br > 0) {
					flag = true;
				} else {
					flag = false;
				}
			}

			return flag;
		},

		SetStatus: function(status, element, message) {
			if (status == "ok") {
				Selector.AddClass(element, ".validatio-valid");

				Selector.RemoveClass(element, ".validatio-error");

				var closestElement = Selector.GetClosest(element, ".validatio-holder");

				var validatioHolder = closestElement.querySelectorAll(
					".validatio-messages"
				);

				for (var i = 0; i < validatioHolder.length; i++) {
					validatioHolder[i].innerHTML = "";

					Selector.RemoveClass(validatioHolder[i], ".validatio-padding");
				}
			} else if (status == "error") {
				Selector.RemoveClass(element, ".validatio-valid");

				Selector.AddClass(element, ".validatio-error");

				var closestElement = Selector.GetClosest(element, ".validatio-holder");

				var validatioHolder = closestElement.querySelectorAll(
					".validatio-messages"
				);

				for (var i = 0; i < validatioHolder.length; i++) {
					validatioHolder[i].innerHTML = message;

					Selector.AddClass(validatioHolder[i], ".validatio-padding");
				}
			}
		},

		SetSelectifyStatus: function(status, element, message) {
			if (status == "ok") {
				var closestElement = Selector.GetClosest(element, ".validatio-holder");

				var selectifyElements = closestElement.querySelectorAll(
					".selectify-option-selected"
				);

				for (var i = 0; i < selectifyElements.length; i++) {
					Selector.AddClass(selectifyElements[i], ".validatio-valid");

					Selector.RemoveClass(selectifyElements[i], ".validatio-error");
				}
			} else if (status == "error") {
				var closestElement = Selector.GetClosest(element, ".validatio-holder");

				var selectifyElements = closestElement.querySelectorAll(
					".selectify-option-selected"
				);

				for (var i = 0; i < selectifyElements.length; i++) {
					Selector.RemoveClass(selectifyElements[i], ".validatio-valid");

					Selector.AddClass(selectifyElements[i], ".validatio-error");
				}
			}
		},

		ValidateCheckBox: function(selector) {
			var flag = true;

			if (Selector.HasClass(selector, ".required")) {
				if (selector.checked == true) {
					flag = true;
				} else {
					flag = false;
				}
			}

			return flag;
		},

		ValidateGroupCheckBox: function(selector) {
			var flag = true;

			var groupCheckboxId = selector.getAttribute("id");

			var elements = document.querySelectorAll(
				"#" + groupCheckboxId + " .checkbox"
			);

			var br = 0;

			if (Selector.HasClass(selector, ".required")) {
				for (var i = 0; i < elements.length; i++) {
					if (elements[i].checked == true) {
						br++;
					}
				}
			}

			if (br > 0) {
				flag = true;
			} else {
				flag = false;
			}

			return flag;
		},

		ValidateRadio: function(selector) {
			var flag = true;

			var selectorName = selector.getAttribute("name");

			var elements = document.querySelectorAll(
				"input[name=" + selectorName + "]"
			);

			var br = 0;

			if (Selector.HasClass(selector, ".required")) {
				for (var i = 0; i < elements.length; i++) {
					if (elements[i].checked == true) {
						br++;
					}
				}

				if (br > 0) {
					flag = true;
				} else {
					flag = false;
				}
			}

			return flag;
		},

		Bind: function(formId) {
			formId = "#" + cleanSelector(formId);

			var elements = document.querySelectorAll(formId + " .validatio");

			var cThis = this;

			for (var i = 0; i < elements.length; i++) {
				var dataType = elements[i].getAttribute("data-valid-type");

				if (Selector.HasClass(elements[i], ".validatio") && dataType != "") {
					switch (dataType) {
						case "email":
							cEvent.Attach(elements[i], "blur", function(e) {
								if (cThis.ValidateEmail(e.target, e.target.value)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "text":
							cEvent.Attach(elements[i], "blur", function(e) {
								if (cThis.ValidateText(e.target, e.target.value)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "date":
							cEvent.Attach(elements[i], "blur", function(e) {
								if (cThis.ValidateText(e.target, e.target.value)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "select":
							cEvent.Attach(elements[i], "change", function(e) {
								if (cThis.ValidateSelect(e.target, e.target.value)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);

									cThis.SetSelectifyStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);

									cThis.SetSelectifyStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "checkbox":
							cEvent.Attach(elements[i], "change", function(e) {
								if (cThis.ValidateCheckBox(e.target)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "radio":
							cEvent.Attach(elements[i], "change", function(e) {
								if (cThis.ValidateRadio(e.target)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "textarea":
							cEvent.Attach(elements[i], "blur", function(e) {
								if (cThis.ValidateTextarea(e.target, e.target.value)) {
									cThis.SetStatus(
										"ok",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								} else {
									cThis.SetStatus(
										"error",
										e.target,
										cThis.Data.fieldRequiredMessage
									);
								}
							});

							break;

						case "groupCheckbox":
							var elemThis = elements[i];

							var groupCheckboxId = elemThis.getAttribute("id");

							var groupCheckboxes = document.querySelectorAll(
								"#" + groupCheckboxId + " .checkbox"
							);

							for (var gCi = 0; gCi < groupCheckboxes.length; gCi++) {
								cEvent.Attach(groupCheckboxes[gCi], "change", function(e) {
									var groupCheckboxBox = Selector.GetClosest(
										e.target,
										".validatio"
									);

									if (cThis.ValidateGroupCheckBox(groupCheckboxBox)) {
										cThis.SetStatus(
											"ok",
											groupCheckboxBox,
											cThis.Data.fieldRequiredMessage
										);
									} else {
										cThis.SetStatus(
											"error",
											groupCheckboxBox,
											cThis.Data.fieldRequiredMessage
										);
									}
								});
							}

							break;

						default:

						//text = "Looking forward to the Weekend";
					}
				}
			}
		},

		IsValid: function(formId) {
			formId = "#" + cleanSelector(formId);

			var elements = document.querySelectorAll(formId + " .validatio");

			var cThis = this;

			var notValid = 0;

			for (var i = 0; i < elements.length; i++) {
				var dataType = elements[i].getAttribute("data-valid-type");

				if (Selector.HasClass(elements[i], ".validatio") && dataType != "") {
					switch (dataType) {
						case "email":
							if (cThis.ValidateEmail(elements[i], elements[i].value)) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "text":
							if (cThis.ValidateText(elements[i], elements[i].value)) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "date":
							if (cThis.ValidateText(elements[i], elements[i].value)) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "select":
							if (cThis.ValidateSelect(elements[i], elements[i].value)) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								cThis.SetSelectifyStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								cThis.SetSelectifyStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "checkbox":
							if (cThis.ValidateCheckBox(elements[i])) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "radio":
							if (cThis.ValidateRadio(elements[i])) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "textarea":
							if (cThis.ValidateTextarea(elements[i], elements[i].value)) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						case "groupCheckbox":
							if (cThis.ValidateGroupCheckBox(elements[i])) {
								cThis.SetStatus(
									"ok",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);
							} else {
								cThis.SetStatus(
									"error",
									elements[i],
									cThis.Data.fieldRequiredMessage
								);

								notValid++;
							}

							break;

						default:

						//text = "Looking forward to the Weekend";
					}
				}
			}

			if (notValid > 0) {
				return false;
			} else {
				return true;
			}
		},

		IsEmail: function(cString) {
			var atpos = cString.indexOf("@");

			var dotpos = cString.lastIndexOf(".");

			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= cString.length) {
				return false;
			} else {
				return true;
			}
		},

		Data: {
			// fieldRequiredMessage: fieldNotValidMsg,

			// setInit: function(fieldRequiredMessage) {
			// 	this.fieldRequiredMessage = fieldRequiredMessage;
			// }
		}
	};

	/* var cValidatio = new customValidatio(); */

	cValidatio = new customValidatio();

	/* GET DIMENSIONS */

	function getDimension(selector, type) {
		var result = 0;

		var elements = document.querySelectorAll(selector);

		for (var i = 0; i < elements.length; i++) {
			if (type == "width") {
				/* Including padding and border */

				result = elements[i].offsetWidth;
			} else if (type == "height") {
				/* Including padding and border */

				result = elements[i].offsetHeight;
			} else if (type == "inner-width") {
				/* Including padding no border */

				result = elements[i].clientWidth;
			} else if (type == "inner-height") {
				/* Including padding no border */

				result = elements[i].clientHeight;
			}
		}

		return result;
	}

	/* Window click */

	cEvent.Attach(window, "click", function(e) {
		//console.log('\nWindow Click className: '+e.target.className);

		if (Selector.HasClass(e.target, ".cookie-confirm")) {
			//console.log('\nCookie notification close!');
		} else if (Selector.HasClass(e.target, ".follow-href")) {
			//console.log('\nCookie notification close! ' +e.target.getAttribute('data-href'));

			openUrl(e.target.getAttribute("data-href"), "_self");
		} else {
			//Click outside
		}
	});
});
