var domReady = function(fn) {
	// Sanity check

	if (typeof fn !== "function") return;

	// If document is already loaded, run method

	if (window.readyState === "complete") {
		return fn();
	}

	// Otherwise, wait until document is loaded

	window.addEventListener("DOMContentLoaded", fn, false);
};

var domLoad = function(fn) {
	// Sanity check

	if (typeof fn !== "function") return;

	// If document is already loaded, run method

	if (window.readyState === "complete") {
		return fn();
	}

	// Otherwise, wait until document is loaded

	window.addEventListener("load", fn, false);
};

// Custom vars

var flickitySingleProviderDiscounts,
	flickitySingleProviderLocations,
	stickySingleProviderCtaMobile,
	stickySingleProviderCta;

// Document ready

domReady(function() {
	// // Cookies

	// /* jsCookie */

	// /* var jsCookie = jsCookie || {}; */

	// jsCookie = jsCookie || {};

	// jsCookie = {
	// 	Init: function() {},

	// 	SetExpireDate: function(cDays, cHours, cMinutes) {
	// 		var expireDate = new Date(
	// 			new Date().getTime() +
	// 				cDays * 24 * 60 * 60 * 1000 +
	// 				cHours * 60 * 60 * 1000 +
	// 				cMinutes * 60 * 1000
	// 		);

	// 		return expireDate;
	// 	},

	// 	API: {
	// 		Set: function(jsCookieHelper) {
	// 			//console.log('\nSet cookie: '+jsCookieHelper.name);

	// 			var cookieExpires = jsCookie.SetExpireDate(
	// 				jsCookieHelper.expiresDays,
	// 				jsCookieHelper.expiresHours,
	// 				jsCookieHelper.expiresMinutes
	// 			);

	// 			Cookies.set(jsCookieHelper.name, jsCookieHelper.value, {
	// 				path: jsCookieHelper.path,
	// 				domain: jsCookieHelper.domain,
	// 				expires: cookieExpires,
	// 				secure: jsCookieHelper.secure
	// 			});
	// 		},

	// 		Update: function(jsCookieHelper) {
	// 			//console.log('\nUpdate cookie: '+jsCookieHelper.name);

	// 			var cookieExpires = jsCookie.SetExpireDate(
	// 				jsCookieHelper.expiresDays,
	// 				jsCookieHelper.expiresHours,
	// 				jsCookieHelper.expiresMinutes
	// 			);

	// 			Cookies.set(jsCookieHelper.name, jsCookieHelper.value, {
	// 				path: jsCookieHelper.path,
	// 				domain: jsCookieHelper.domain,
	// 				secure: jsCookieHelper.secure
	// 			});
	// 		},

	// 		UpdateWithExpire: function(jsCookieHelper) {
	// 			//console.log('\nUpdateWithExpire cookie: '+jsCookieHelper.name);

	// 			var cookieExpires = jsCookie.SetExpireDate(
	// 				jsCookieHelper.expiresDays,
	// 				jsCookieHelper.expiresHours,
	// 				jsCookieHelper.expiresMinutes
	// 			);

	// 			Cookies.set(jsCookieHelper.name, jsCookieHelper.value, {
	// 				path: jsCookieHelper.path,
	// 				domain: jsCookieHelper.domain,
	// 				expires: cookieExpires,
	// 				secure: jsCookieHelper.secure
	// 			});
	// 		},

	// 		Get: function(jsCookieHelper) {
	// 			//console.log('\nGet cookie: '+jsCookieHelper.name);

	// 			return Cookies.get(jsCookieHelper.name, {
	// 				path: jsCookieHelper.path,
	// 				domain: jsCookieHelper.domain,
	// 				secure: jsCookieHelper.secure
	// 			});
	// 		},

	// 		Remove: function(jsCookieHelper) {
	// 			//console.log('\nRemove cookie: '+jsCookieHelper.name);

	// 			Cookies.remove(jsCookieHelper.name, {
	// 				path: jsCookieHelper.path,
	// 				domain: jsCookieHelper.domain,
	// 				secure: jsCookieHelper.secure
	// 			});
	// 		}
	// 	}
	// };

	// jsCookie.Init();

	// /* Show Cookie Notification Pop Up */

	// function showCookieNotificationPopUp() {
	// 	// Show cookie accept pop up

	// 	Swal.fire({
	// 		position: "bottom-start", //'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'.

	// 		type: "info",

	// 		//heightAuto: false,

	// 		timer: 10000,

	// 		customClass: "cookieBox",

	// 		showCloseButton: false,

	// 		showCancelButton: false,

	// 		//allowOutsideClick: false,

	// 		//focusConfirm: true,

	// 		customContainerClass: "cookie-confirm-container",

	// 		confirmButtonClass: "cookie-confirm",

	// 		confirmButtonText: "Prihvaćam",

	// 		confirmButtonAriaLabel: "Prihvaćam",

	// 		title: cookieNotiTitle,

	// 		html: cookieNotiText,

	// 		toast: true,

	// 		animation: false, // Important. If set to true, Flickity and Sticky break apart on window resize.

	// 		onBeforeOpen: function() {},

	// 		onClose: function() {
	// 			//Store a cookie and don't show pop-up again

	// 			var tmpNotificationCookieHelper = {
	// 				name: "cookieAcceptBox",

	// 				value: "1",

	// 				domain: oSZV.domain,

	// 				path: "/",

	// 				secure: true,

	// 				expiresDays: 0,

	// 				expiresHours: 0,

	// 				expiresMinutes: 1
	// 			};

	// 			jsCookie.API.UpdateWithExpire(tmpNotificationCookieHelper);
	// 		}
	// 	});
	// }

	// //Show Cookie Notification Pop Up

	// var cookieNotificationPopUpHelper = {
	// 	name: "cookieAcceptBox",

	// 	value: "0",

	// 	domain: oSZV.domain,

	// 	path: "/",

	// 	secure: true,

	// 	expiresDays: 0,

	// 	expiresHours: 0,

	// 	expiresMinutes: 1
	// };

	// if (!jsCookie.API.Get(cookieNotificationPopUpHelper)) {
	// 	jsCookie.API.Set(cookieNotificationPopUpHelper);

	// 	showCookieNotificationPopUp();
	// } else {
	// 	var tmpCookieAccept = jsCookie.API.Get(cookieNotificationPopUpHelper);

	// 	if (tmpCookieAccept == 0) {
	// 		showCookieNotificationPopUp();
	// 	}
	// }

	// //Favorites Cookie

	// var favoritesCookieHelper = {
	// 	name: "favorites",

	// 	value: "0",

	// 	domain: oSZV.domain,

	// 	path: "/",

	// 	secure: true,

	// 	expiresDays: 0,

	// 	expiresHours: 0,

	// 	expiresMinutes: 1
	// };

	// if (!jsCookie.API.Get(favoritesCookieHelper)) {
	// } else {
	// }

	// /* Advertisting */

	// /* var szvAdvertisting = szvAdvertisting || {}; */

	// szvAdvertisting = szvAdvertisting || {};

	// szvAdvertisting = {
	// 	Init: function() {
	// 		szvAdvertisting.Data.Parameters.SetInit(
	// 			".advertisting-banner",
	// 			".hide",
	// 			"click"
	// 		);
	// 	},

	// 	Attach: function(event, jsCookieHelper) {
	// 		if (event == "click") {
	// 			cEvent.Attach(
	// 				szvAdvertisting.Data.Parameters.selector + " .advertising-close",
	// 				szvAdvertisting.Data.Parameters.event,
	// 				function(e) {
	// 					var finalSelector = Selector.GetClosest(
	// 						e.target,
	// 						".advertising-banner"
	// 					).getAttribute("id");

	// 					Selector.AddClass(
	// 						"#" + finalSelector,
	// 						szvAdvertisting.Data.Parameters.hideClass
	// 					);

	// 					//console.log('\nUpdateCookie: '+ jsCookieHelper.name);

	// 					if (jsCookie.API.Get(jsCookieHelper)) {
	// 						//console.log('\nUpdateCookie: '+ jsCookieHelper.value);

	// 						jsCookie.API.UpdateWithExpire(jsCookieHelper);
	// 					}
	// 				}
	// 			);
	// 		}
	// 	},

	// 	API: {
	// 		Hide: function(selector, jsCookieHelper) {
	// 			szvAdvertisting.Data.Parameters.SetSelector(selector);

	// 			Selector.AddClass(selector, szvAdvertisting.Data.Parameters.hideClass);

	// 			szvAdvertisting.Attach("click", jsCookieHelper);
	// 		},

	// 		Show: function(selector, jsCookieHelper) {
	// 			szvAdvertisting.Data.Parameters.SetSelector(selector);

	// 			Selector.RemoveClass(
	// 				selector,
	// 				szvAdvertisting.Data.Parameters.hideClass
	// 			);

	// 			szvAdvertisting.Attach("click", jsCookieHelper);
	// 		},

	// 		Toggle: function(selector, jsCookieHelper) {
	// 			szvAdvertisting.Data.Parameters.SetSelector(selector);

	// 			Selector.ToggleClass(
	// 				selector,
	// 				szvAdvertisting.Data.Parameters.hideClass
	// 			);

	// 			szvAdvertisting.Attach("click", jsCookieHelper);
	// 		}
	// 	},

	// 	Data: {
	// 		Parameters: {
	// 			verificationClass: "",

	// 			hideClass: "",

	// 			selector: "",

	// 			event: "",

	// 			SetInit: function(verificationClass, hideClass, event) {
	// 				this.verificationClass = verificationClass;

	// 				this.hideClass = hideClass;

	// 				this.event = event;
	// 			},

	// 			SetSelector: function(selector) {
	// 				this.selector = selector;
	// 			}
	// 		}
	// 	}
	// };

	// if (advertisingOn) {
	// 	szvAdvertisting.Init();

	// 	//Show Advertising Top Bar

	// 	var topBarAdCookieHelper = {
	// 		name: "advertisingTopBar",

	// 		value: "1",

	// 		domain: oSZV.domain,

	// 		path: "/",

	// 		secure: true,

	// 		expiresDays: 3650,

	// 		expiresHours: 0,

	// 		expiresMinutes: 0
	// 	};

	// 	if (!jsCookie.API.Get(topBarAdCookieHelper)) {
	// 		jsCookie.API.Set(topBarAdCookieHelper);

	// 		//Set Cookie Value to 0 if pop up banner closed

	// 		topBarAdCookieHelper.value = "0";

	// 		topBarAdCookieHelper.expiresDays = 0;

	// 		topBarAdCookieHelper.expiresMinutes = 1;

	// 		szvAdvertisting.API.Show("#advertising-top-bar", topBarAdCookieHelper);
	// 	} else {
	// 		advertisingTopBar = jsCookie.API.Get(topBarAdCookieHelper);

	// 		if (advertisingTopBar == 1) {
	// 			//Set Cookie Value to 0 if pop up banner closed

	// 			topBarAdCookieHelper.value = "0";

	// 			topBarAdCookieHelper.expiresDays = 0;

	// 			topBarAdCookieHelper.expiresMinutes = 1;

	// 			szvAdvertisting.API.Show("#advertising-top-bar", topBarAdCookieHelper);
	// 		}
	// 	}

	// 	//Show Advertising Footer Pop Up

	// 	var footerPopUpCookieHelper = {
	// 		name: "advertisingFooterPopUp",

	// 		value: "1",

	// 		domain: oSZV.domain,

	// 		path: "/",

	// 		secure: true,

	// 		expiresDays: 3650,

	// 		expiresHours: 0,

	// 		expiresMinutes: 0
	// 	};

	// 	if (!jsCookie.API.Get(footerPopUpCookieHelper)) {
	// 		jsCookie.API.Set(footerPopUpCookieHelper);

	// 		//Set Cookie Value to 0 if pop up banner closed

	// 		footerPopUpCookieHelper.value = "0";

	// 		footerPopUpCookieHelper.expiresDays = 0;

	// 		footerPopUpCookieHelper.expiresMinutes = 1;

	// 		szvAdvertisting.API.Show(
	// 			"#advertising-footer-pop-up",
	// 			footerPopUpCookieHelper
	// 		);
	// 	} else {
	// 		advertisingTopBar = jsCookie.API.Get(footerPopUpCookieHelper);

	// 		if (advertisingTopBar == 1) {
	// 			//Set Cookie Value to 0 if pop up banner closed

	// 			footerPopUpCookieHelper.value = "0";

	// 			footerPopUpCookieHelper.expiresDays = 0;

	// 			footerPopUpCookieHelper.expiresMinutes = 1;

	// 			szvAdvertisting.API.Show(
	// 				"#advertising-footer-pop-up",
	// 				footerPopUpCookieHelper
	// 			);
	// 		}
	// 	}
	// }

	/* Main Nav */

	/* var mainNav = mainNav || {}; */

	mainNav = mainNav || {};

	mainNav = {
		Init: function() {
			mainNav.API.BindMainNavTrigger();
		},

		Attach: function(
			selectorAttach,
			selectorAttachEvent,
			cparentBox,
			childElement,
			showMobileMenuClass
		) {
			cEvent.Attach(selectorAttach, selectorAttachEvent, function(e) {
				var parentBox = Selector.GetClosest(e.target, cparentBox);

				var elements = parentBox.querySelectorAll(childElement);

				for (var i = 0; i < elements.length; i++) {
					Selector.ToggleClass(elements[i], showMobileMenuClass);
				}
			});
		},

		API: {
			BindMainNavTrigger: function() {
				cEvent.Attach("#mmenu-trigger", "click", function(e) {
					Selector.ToggleClass("#nav-lang-nav-container", ".display-block");

					Selector.ToggleClass("#mmenu-icon", ".fa-bars");

					Selector.ToggleClass("#mmenu-icon", ".fa-close");

					Selector.ToggleClass("#header", ".override-sticky");

					scrollToY(0, 200);
				});
			},

			CloseMainNav: function() {
				//Reset mobile menu icon

				Selector.RemoveClass("#nav-lang-nav-container", ".display-block");

				Selector.AddClass("#mmenu-icon", ".fa-bars");

				Selector.RemoveClass("#mmenu-icon", ".fa-close");
			},

			BindDropDownTrigger: function(
				selectorAttach,
				selectorAttachEvent,
				parentBox,
				childElement,
				showMobileMenuClass
			) {
				mainNav.Attach(
					selectorAttach,
					selectorAttachEvent,
					parentBox,
					childElement,
					showMobileMenuClass
				);
			},

			SetActive: function() {
				mainNav.API.BindDropDownTrigger(
					"#nav .nav-first-level",
					"click",
					".mmenu-expand-dropdown",
					".mmenu-expand-dropdown ul",
					".show-mobile-menu"
				);

				mainNav.API.BindDropDownTrigger(
					"#nav .dropdown-fullwidth-container .nav-first-level",
					"click",
					".dropdown-fullwidth-container",
					"#nav .dropdown-fullwidth",
					".show-mobile-menu"
				);

				mainNav.API.BindDropDownTrigger(
					"#lang-nav-container .nav-first-level",
					"click",
					".mmenu-expand-dropdown",
					"#lang-nav-container ul",
					".show-mobile-menu"
				);
			}
		}
	};

	mainNav.Init();

	mainNav.API.SetActive();

	/* Breadcrumb */

	cEvent.Attach("#breadcrumb-expand", "click", function(e) {
		Selector.ToggleClass("#breadcrumb", ".hide-breadcrumb");

		Selector.ToggleClass("#breadcrumb", ".expanded-breadcrumb");

		Selector.ToggleClass("#breadcrumb-expand .fa", ".fa-plus-circle");

		Selector.ToggleClass("#breadcrumb-expand .fa", ".fa-minus-circle");
	});

	/* Sticky mobile menu trigger icon */

	/*

	if(getWindowWidth() < 992){

		stickyMMenu = new Sticky('#mmenu');

	}else{

		stickyHeader = new Sticky('#header');

	}

	*/

	Selector.Exists("#header", function(e) {
		if (!Selector.HasClass("body", ".single-provider")) {
			stickyHeader = new Sticky("#header");
		}
	});

	if (getWindowWidth() < 992) {
		Selector.Exists("#single-info-cta-holder", function(e) {
			stickySingleProviderCtaMobile = new Sticky("#single-affix-cta-mobile");
		});
	} else {
		Selector.Exists("#single-info-cta-holder", function(e) {
			stickySingleProviderCta = new Sticky("#single-info-cta-holder");
		});
	}

	//Search logic for scroll or redirect

	mainSearch = mainSearch || {};

	mainSearch = {
		Init: function() {
			if (Selector.ExistsBool(".banner-search-trigger-home")) {
				mainSearch.Data.Parameters.SetInit("home", "#banner-search-trigger");
			} else if (Selector.ExistsBool(".banner-search-trigger-category")) {
				mainSearch.Data.Parameters.SetInit(
					"category",
					"#banner-search-trigger"
				);
			}
		},

		API: {
			Scroll: function(id, speed) {
				var element = document.getElementById(id);

				scrollToElement(element, 200, -100);
			},

			Redirect: function(url, name) {
				openUrl(url, name);
			},

			Attach: function() {
				cEvent.Attach(mainSearch.Data.Parameters.searchBtnId, 'click', function(e) {
                    let selectedCountry = getFormFiedValue('select-country', 'select');
					let selectedCity = getFormFiedValue('select-city', 'select');
					let selectedCategory = getFormFiedValue('select-category', 'select');

                    let currentUrl = window.location.pathname;
                    let url = '';

                    let isCitySelected = selectedCity !== 'all';
					let isCountrySelected = selectedCountry !== 'all';

					if (isCitySelected) {
                        url = [selectedCategory + "-" + selectedCity].join('/');
                    } else {
						if (isCountrySelected) {
							url = [selectedCategory + "-" + selectedCountry].join('/');
						} else {
                            url = selectedCategory;
						}
					}

					let isAlreadyOnSelectedCategory = "/".concat(url) === currentUrl;
					if (isAlreadyOnSelectedCategory) {
                        mainSearch.API.Scroll('listing', 500);
                    } else {
                        mainSearch.API.Redirect(url, '_self');
                    }
				});
			}
		},

		Data: {
			Parameters: {
				searchType: "",

				searchBtnId: "",

				SetInit: function(searchType, searchBtnId) {
					this.searchType = searchType;

					this.searchBtnId = searchBtnId;
				}
			}
		}
	};

	Selector.Exists("#banner-search-fields", function(e) {
		mainSearch.Init();

		mainSearch.API.Attach();
	});

	// Shuffle category paid advertising

	Selector.Exists("#category-top-adv", function(e) {
		shuffle(12, "#category-top-adv", ".shuffle");
	});

	Selector.Exists("#category-shuffle-listing", function(e) {
		shuffle(10, "#category-shuffle-listing", ".shuffle");
	});

	//Expand box

	cEvent.Attach(".expand-box-trigger", "click", function(e) {
		var expandBox = Selector.GetClosest(e.target, ".expand-box");

		var expandBoxOverlay = Selector.GetClosest(e.target, ".expand-box-overlay");

		var expandBoxOverlayTrigger = e.target.querySelectorAll(".fa");

		Selector.ToggleClass(expandBox, ".height-auto");

		Selector.ToggleClass(expandBoxOverlay, ".height-auto");

		for (var i = 0; i < expandBoxOverlayTrigger.length; i++) {
			Selector.ToggleClass(expandBoxOverlayTrigger[i], ".fa-angle-down");

			Selector.ToggleClass(expandBoxOverlayTrigger[i], ".fa-angle-up");
		}
	});

	// Window events

	cEvent.Attach(window, "click", function(e) {
		if (Selector.HasClass(e.target, ".cookie-confirm")) {
			//Update Cookie Accept

			var tmpAcceptCookieHelper = {
				name: "cookieAcceptBox",

				value: "1",

				domain: oSZV.domain,

				path: "/",

				secure: true,

				expiresDays: 0,

				expiresHours: 0,

				expiresMinutes: 1
			};

			jsCookie.API.UpdateWithExpire(tmpAcceptCookieHelper);
		} else {
			//Click outside
		}
	});

	// Flickity

	Selector.Exists("#best-reviews", function(e) {
		var flickityTop3Comments = new Flickity("#best-reviews", {
			cellSelector: ".review-container-item",

			draggable: true,

			cellAlign: "center",

			freeScroll: true,

			contain: true,

			prevNextButtons: true,

			pageDots: false,

			adaptiveHeight: false,

			dragThreshold: 10,

			accessibility: false,

			groupCells: true,

			resize: true
		});
	});

	Selector.Exists("#single-provider-reviews", function(e) {
		var flickitySingleProviderComments = new Flickity(
			"#single-provider-reviews",
			{
				cellSelector: ".review-container-item",

				draggable: true,

				cellAlign: "center",

				freeScroll: true,

				contain: true,

				prevNextButtons: true,

				pageDots: false,

				adaptiveHeight: false,

				dragThreshold: 10,

				accessibility: false,

				groupCells: true,

				resize: true
			}
		);

		var singleProviderCommentsTimer;

		flickitySingleProviderComments.on("change", function(index) {
			//console.log( 'Slide changed to' + index );
		});

		flickitySingleProviderComments.on("scroll", function(progress) {
			/*

			window.clearTimeout(singleProviderCommentsTimer);

			singleProviderCommentsTimer = setTimeout(function() {

				if((progress*100) >= 85){

					alert('Show next 10 reviews');

				}

				console.log( 'Flickity scrolled ' + progress * 100 + '%' )

			}, 25);

			*/
		});
	});

	Selector.Exists("#single-provider-discounts", function(e) {
		flickitySingleProviderDiscounts = new Flickity(
			"#single-provider-discounts",
			{
				cellSelector: ".single-provider-discount-item",

				draggable: true,

				cellAlign: "left",

				freeScroll: true,

				contain: true,

				prevNextButtons: true,

				pageDots: false,

				adaptiveHeight: false,

				dragThreshold: 10,

				accessibility: false,

				groupCells: true,

				resize: true
			}
		);

		setTimeout(function() {
			//flickitySingleProviderDiscounts.resize();
		}, 1);
	});

	Selector.Exists("#single-provider-locations", function(e) {
		flickitySingleProviderLocations = new Flickity(
			"#single-provider-locations",
			{
				cellSelector: ".single-provider-location-item",

				draggable: true,

				cellAlign: "left",

				freeScroll: true,

				contain: true,

				prevNextButtons: true,

				pageDots: false,

				adaptiveHeight: false,

				dragThreshold: 10,

				accessibility: false,

				groupCells: true,

				resize: true
			}
		);

		setTimeout(function() {
			//flickitySingleProviderLocations.resize();
		}, 1);
	});

	// Show All Single Provider Reviews On Click

	cEvent.Attach("#showAllProviderReviews", "click", function(e) {
		var dataProviderId = e.target.getAttribute("data-selected");

		var pTitle = "Komentari i ocjene";

		var pHTML =
			"Izvuc sve komentare - sve jezike. Tekst komentara mora bit prikazan u cijelosti.";

		Swal.fire({
			position: "center", //'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'.

			type: "info",

			heightAuto: true,

			//timer: 10000,

			customClass: "single-provider-all-reviews-popup",

			showCloseButton: true,

			showCancelButton: false,

			showConfirmButton: false,

			//allowOutsideClick: false,

			//focusConfirm: true,

			customContainerClass: "single-provider-all-reviews-container",

			title: pTitle,

			html: pHTML,

			toast: false,

			animation: false, // Important. If set to true, Flickity and Sticky break apart on window resize.

			onBeforeOpen: function() {},

			onClose: function() {}
		});
	});

	// Reviews

	function showFullReviewTestimonialPopUp(pTitle, pHTML) {
		Swal.fire({
			position: "center", //'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'.

			type: "info",

			heightAuto: true,

			//timer: 10000,

			customClass: "full-review-testimonial-popup",

			showCloseButton: true,

			showCancelButton: false,

			showConfirmButton: false,

			//allowOutsideClick: false,

			//focusConfirm: true,

			customContainerClass: "full-review-testimonial-popup-container",

			title: pTitle,

			html: pHTML,

			toast: false,

			animation: false, // Important. If set to true, Flickity and Sticky break apart on window resize.

			onBeforeOpen: function() {},

			onClose: function() {}
		});
	}

	cEvent.Attach(".review-expand-full-trigger", "click", function(e) {
		var reviewContainerBox = Selector.GetClosest(e.target, ".review-container");

		// Pop up review title

		var rating = "",
			ratingBox = reviewContainerBox.querySelectorAll(".rating-box");

		for (var i = 0; i < ratingBox.length; i++) {
			rating +=
				'<span class="pos-relative"><span class="rating-box">' +
				ratingBox[i].innerHTML +
				'</span><span class="cf"></span></span>';
		}

		var reviewer = "",
			reviewerProviderBox = reviewContainerBox.querySelectorAll(".reviewer");

		for (var i = 0; i < reviewerProviderBox.length; i++) {
			reviewer +=
				'<span class="reviewer-pop-up">' +
				reviewerProviderBox[i].innerHTML +
				"</span>";
		}

		var reviewTestimonialTitle = rating + reviewer;

		// Pop up review content

		var reviewDate = "",
			reviewDateBox = reviewContainerBox.querySelectorAll(".review-date");

		for (var i = 0; i < reviewDateBox.length; i++) {
			reviewDate +=
				'<p class="review-date-pop-up">' + reviewDateBox[i].innerHTML + "</p>";
		}

		var reviewCommentPositive = "",
			reviewCommentPositivePartialBox = reviewContainerBox.querySelectorAll(
				".review-testimonial-pos .review-testimonial-partial"
			);

		reviewCommentPositiveFullBox = reviewContainerBox.querySelectorAll(
			".review-testimonial-pos .review-testimonial-full"
		);

		for (var i = 0; i < reviewCommentPositivePartialBox.length; i++) {
			reviewCommentPositive += Selector.GetText(
				reviewCommentPositivePartialBox[i]
			);
		}

		for (var i = 0; i < reviewCommentPositiveFullBox.length; i++) {
			reviewCommentPositive += Selector.GetText(
				reviewCommentPositiveFullBox[i]
			);
		}

		reviewCommentPositive =
			'<p class="review-comment-positive-pop-up review-testimonial-pos">' +
			reviewCommentPositive +
			"</p>";

		var reviewCommentNegative = "",
			reviewCommentNegativePartialBox = reviewContainerBox.querySelectorAll(
				".review-testimonial-neg .review-testimonial-partial"
			);

		reviewCommentNegativeFullBox = reviewContainerBox.querySelectorAll(
			".review-testimonial-neg .review-testimonial-full"
		);

		for (var i = 0; i < reviewCommentNegativePartialBox.length; i++) {
			reviewCommentNegative += Selector.GetText(
				reviewCommentNegativePartialBox[i]
			);
		}

		for (var i = 0; i < reviewCommentNegativeFullBox.length; i++) {
			reviewCommentNegative += Selector.GetText(
				reviewCommentNegativeFullBox[i]
			);
		}

		reviewCommentNegative =
			'<p class="review-comment-negative-pop-up review-testimonial-neg">' +
			reviewCommentNegative +
			"</p>";

		var reviewTestimonial =
			reviewDate + reviewCommentPositive + reviewCommentNegative;

		// Show full review in a pop up

		showFullReviewTestimonialPopUp(reviewTestimonialTitle, reviewTestimonial);
	});

	// Category sliders

	function resizeCategorySliderCells(selectorBox) {
		var screenWidth = getWindowWidth();

		var imgHeight = 320;

		if (screenWidth > 1199) {
			imgHeight = 320;
		} else if (screenWidth > 991 && screenWidth < 1200) {
			imgHeight = 320;
		} else if (screenWidth > 767 && screenWidth < 992) {
			imgHeight = 210;
		} else if (screenWidth > 575 && screenWidth < 768) {
			imgHeight = 210;
		} else if (screenWidth > 479 && screenWidth < 576) {
			imgHeight = 210;
		} else if (screenWidth < 480) {
			imgHeight = 200;
		}

		Selector.Each(selectorBox + " .carousel-cell", function(e) {
			var width = e.offsetWidth;

			var height = e.offsetHeight;

			var ratio = roundNum(width / height, 2);

			//console.log('\nwidth: '+width+' | height: '+height+' | ratio: '+ratio);

			var imgWidth = Math.round(imgHeight * ratio);

			//console.log('\nimgWidth: '+imgWidth+' | imgHeight: '+imgHeight);

			e.style.width = imgWidth + "px";

			e.style.height = imgHeight + "px";
		});
	}

	function resizeCategorySliders(selectorBox) {
		Selector.Each(selectorBox, function(e) {
			var categorySliderId = "#" + e.getAttribute("id");

			var flkty = new Flickity(categorySliderId);

			flkty.resize();
		});
	}

	Selector.Each(".category-listing-slider", function(e) {
		resizeCategorySliderCells(".category-listing-slider");

		var categorySliderId = "#" + e.getAttribute("id");

		var categorySlider = new Flickity(categorySliderId, {
			lazyLoad: true,

			wrapAround: true,

			bgLazyLoad: 2,

			dragThreshold: 50,

			resize: true,

			pageDots: false,

			cellAlign: "center"
		});

		categorySlider.on("lazyLoad", function(event) {
			var img = event.target;

			//console.log(event.type,img.src);
		});

		categorySlider.on("bgLazyLoad", function(event, element) {
			var img = event.target;

			//console.log(event.type,img.src);
		});
	});

	// Home categories advertising slider

	Selector.Exists("#home-categories-slider", function(e) {
		var categoryPaidAdvertisingSlider = new Flickity(
			"#home-categories-slider",
			{
				lazyLoad: true,

				wrapAround: true,

				bgLazyLoad: 3,

				dragThreshold: 50,

				resize: true,

				pageDots: false,

				cellAlign: "center"
			}
		);
	});

	// Category paid advertising slider

	Selector.Exists("#category-top-adv", function(e) {
		var categoryPaidAdvertisingSlider = new Flickity("#category-top-adv", {
			lazyLoad: true,

			wrapAround: true,

			bgLazyLoad: 3,

			dragThreshold: 50,

			resize: true,

			pageDots: false,

			cellAlign: "center"
		});
	});

	// Resize single post slider

	function resizeSingleSliderCells(sliderId) {
		var screenWidth = getWindowWidth();

		var imgHeight = 320;

		if (screenWidth > 1199) {
			imgHeight = 520;
		} else if (screenWidth > 991 && screenWidth < 1200) {
			imgHeight = 470;
		} else if (screenWidth > 767 && screenWidth < 992) {
			imgHeight = 350;
		} else if (screenWidth > 575 && screenWidth < 768) {
			imgHeight = 300;
		} else if (screenWidth > 479 && screenWidth < 576) {
			imgHeight = 280;
		} else if (screenWidth < 480) {
			imgHeight = 250;
		}

		Selector.Each(sliderId + " .carousel-cell", function(e) {
			var width = e.offsetWidth;

			var height = e.offsetHeight;

			var ratio = roundNum(width / height, 2);

			//console.log('\nwidth: '+width+' | height: '+height+' | ratio: '+ratio);

			var imgWidth = Math.round(imgHeight * ratio);

			//console.log('\nimgWidth: '+imgWidth+' | imgHeight: '+imgHeight);

			e.style.width = imgWidth + "px";

			e.style.height = imgHeight + "px";
		});
	}

	function resizeSingleSlider(sliderId) {
		var flkty = new Flickity(sliderId);

		flkty.resize();
	}

	// Single post slider

	Selector.Exists("#single-slider", function(e) {
		resizeSingleSliderCells("#single-slider");

		var indexSlider = new Flickity("#single-slider", {
			lazyLoad: true,

			wrapAround: true,

			bgLazyLoad: 2,

			dragThreshold: 50,

			resize: true,

			pageDots: false,

			fullscreen: true
		});

		indexSlider.on("lazyLoad", function(event) {
			var img = event.target;

			//console.log(event.type,img.src);
		});

		indexSlider.on("bgLazyLoad", function(event, element) {
			var img = event.target;

			//console.log(event.type,img.src);
		});

		//resizeSlider('#islider',getWindowWidth());
	});

	// Expand category text ellipsis boxes

	cEvent.Attach(".expand-category-listing", "click", function(e) {
		var parentBox = Selector.GetClosest(e.target, ".cat-listing-ellipsis");

		Selector.ToggleClass(parentBox, ".hide-catlisting-ellipsis");

		var elements = parentBox.querySelectorAll(".expand-category-listing .fa");

		for (var i = 0; i < elements.length; i++) {
			Selector.ToggleClass(elements[i], ".fa-plus-circle");

			Selector.ToggleClass(elements[i], ".fa-minus-circle");
		}
	});

	// Open review in popup on category listing-ellipsis

	cEvent.Attach(".popup-category-listing-review", "click", function(e) {
		var parentBox = Selector.GetClosest(e.target, ".cat-listing-reviews-box");

		var elements = parentBox.querySelectorAll(".cat-listing-reviews-comment");

		var title = '<span class="fa fa-comment"></span>';

		var commentHTML = "";

		for (var i = 0; i < elements.length; i++) {
			commentHTML += elements[i].innerHTML;
		}

		showFullReviewTestimonialPopUp(title, commentHTML);
	});

	// Scroll To Contact Form

	cEvent.Attach(".scrollToContactForm", "click", function(e) {
		var element = document.getElementById("contact");

		scrollToElement(element, 200, -20);
	});

	// Scroll To Single Provider Reviews

	cEvent.Attach(".scrollToSingleProviderReviews", "click", function(e) {
		var element = document.getElementById("reviews");

		scrollToElement(element, 200, 0);
	});

	// Show Map Pop Pup

	cEvent.Attach(".showMapPopPup", "click", function(e) {
		var pTitle = e.target.getAttribute("data-location-name");

		var pHTML =
			'<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBuNXmPtCbpPpllszu59b5ZpmeYPru7g7s&q=Space+Needle,Seattle+WA" allowfullscreen></iframe>';

		Swal.fire({
			position: "center", //'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'.

			heightAuto: true,

			//timer: 10000,

			customClass: "single-provider-all-reviews-popup",

			showCloseButton: true,

			showCancelButton: false,

			showConfirmButton: false,

			//allowOutsideClick: false,

			//focusConfirm: true,

			customContainerClass: "single-provider-all-reviews-container",

			title: pTitle,

			html: pHTML,

			toast: false,

			animation: false, // Important. If set to true, Flickity and Sticky break apart on window resize.

			onBeforeOpen: function() {},

			onClose: function() {}
		});
	});

	// Show Iframe Single Provider Video Pop Up

	cEvent.Attach(".openIframeSingleProviderVideoPopUp", "click", function(e) {
		var pTitle = Selector.GetClosest(
			e.target,
			".iframeSingleProviderVideoPopUp"
		).getAttribute("data-title");

		var iframeSrc = Selector.GetClosest(
			e.target,
			".iframeSingleProviderVideoPopUp"
		).getAttribute("data-src");

		var pHTML =
			'<iframe src="' +
			iframeSrc +
			'?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%; height: 300px;"></iframe>';

		Swal.fire({
			position: "center", //'top', 'top-start', 'top-end', 'center', 'center-start', 'center-end', 'bottom', 'bottom-start', or 'bottom-end'.

			heightAuto: true,

			//timer: 10000,

			customClass: "single-provider-iframe-video-popup",

			showCloseButton: true,

			showCancelButton: false,

			showConfirmButton: false,

			//allowOutsideClick: false,

			//focusConfirm: true,

			customContainerClass: "single-provider-iframe-video-container",

			title: pTitle,

			html: pHTML,

			toast: false,

			animation: false, // Important. If set to true, Flickity and Sticky break apart on window resize.

			onBeforeOpen: function() {},

			onClose: function() {}
		});
	});

	/* Window resize */

	cEvent.Attach(window, "resize", function(e) {
		/* On horizontal resize finished */

		window.clearTimeout(resizeTimer);

		resizeTimer = setTimeout(function() {
			if (onloadWidth != getWindowWidth()) {
				onloadWidth = getWindowWidth();

				/* Close open swal2 alerts */

				if (Swal.isVisible()) {
					Swal.close();

					//console.log('\nSwal Visibile!');
				}

				//console.log('\nresizeTimer Fired!');

				window.clearTimeout(stickyResizeTimer);

				stickyResizeTimer = setTimeout(function() {
					//console.log('\nstickyResizeTimer Fired!');

					/* Update sticky */

					Selector.Exists("#header", function(e) {
						if (!Selector.HasClass("body", ".single-provider")) {
							stickyHeader.update();
						}
					});

					if (getWindowWidth() < 992) {
						Selector.Exists("#single-info-cta-holder", function(e) {
							//Selector.AddClass('#single-info-cta-holder','.hide');

							Selector.RemoveClass("#single-info-cta-holder", "is-sticky");

							Selector.AddClass(
								"#single-affix-cta-mobile-placeholder",
								".hide"
							);

							Selector.RemoveClass("#single-affix-cta-mobile", ".hide");

							stickySingleProviderCtaMobile = new Sticky(
								"#single-affix-cta-mobile"
							);

							stickySingleProviderCtaMobile.update();

							if (stickySingleProviderCta) {
								/*

								console.log('\nstickySingleProviderCta: ' + stickySingleProviderCta);

								stickySingleProviderCta.destroy();

								Selector.RemoveAttribute('#single-info-cta-holder','style','');

								Selector.RemoveClass('#single-info-cta-holder','is-sticky');

								*/
							}
						});
					} else {
						Selector.Exists("#single-info-cta-holder", function(e) {
							/*	

							if(stickySingleProviderCtaMobile) {

								console.log('\nstickySingleProviderCta: ' + stickySingleProviderCtaMobile);

								stickySingleProviderCtaMobile.destroy();

								Selector.RemoveAttribute('#single-affix-cta-mobile','style','');

								Selector.RemoveClass('#single-affix-cta-mobile','is-sticky');

							}

							*/

							Selector.AddClass("#single-affix-cta-mobile", ".hide");

							Selector.RemoveClass(
								"#single-affix-cta-mobile-placeholder",
								".hide"
							);

							//Selector.RemoveClass('#single-info-cta-holder','.hide');

							stickySingleProviderCta = new Sticky("#single-info-cta-holder");

							stickySingleProviderCta.update();
						});
					}
				}, 100);

				/* Remove override-sticky class for sticky to work on desktop nav */

				if (getWindowWidth() < 992) {
				} else {
					Selector.RemoveClass("#header", ".override-sticky");
				}

				/* Reset mobile nav */

				/* Lazy load */

				/* Reset sliders */

				Selector.Exists("#single-slider", function(e) {
					resizeSingleSliderCells("#single-slider");

					resizeSingleSlider("#single-slider");
				});

				Selector.Exists(".category-listing-slider", function(e) {
					resizeCategorySliderCells(".category-listing-slider");

					resizeCategorySliders(".category-listing-slider");
				});

				/* Trigger scroll */

				cEvent.Trigger(window, "scroll", function(e) {
					//console.log('\nResize - Scroll!');
				});
			}
		}, 75);
	});

	/* Bind form validation */

	Selector.Exists("#contactInquiryForm", function(e) {
		cValidatio.Bind("#contactInquiryForm");

		cEvent.Attach("#sendContactInquiryForm", "click", function(e) {
			if (cValidatio.IsValid("#contactInquiryForm")) {
				alert("ok");
			} else {
				alert("error");
			}
		});
	});

	/* Window scroll */

	cEvent.Attach(window, "scroll", function(e) {
		/* On scroll finished */

		window.clearTimeout(scrollTimer);

		scrollTimer = setTimeout(function() {
			//console.log('\nScroll finished!');
		}, 75);
	});
});

// Document load

domLoad(function() {
	// Resize slider to fix height

	Selector.Exists("#single-provider-discounts", function(e) {
		flickitySingleProviderDiscounts.resize();
	});

	// Resize slider to fix height

	Selector.Exists("#single-provider-locations", function(e) {
		flickitySingleProviderLocations.resize();
	});
});
