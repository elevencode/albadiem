jQuery(document).ready(function($) {
	// use $ here safely
	$(".dropdown-fullwidth-trigger, .dropdown-fullwidth").hover(
		function() {
			$(".dropdown-fullwidth").addClass("active");
		},
		function() {
			$(".dropdown-fullwidth").removeClass("active");
		}
	);
});
