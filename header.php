<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package albadiem
 */

?>
<?php
	global $szvData; 

	$szvData['budleJs'] = false;

	$szvData['loaderIconUrl'] = 'https://www.static.szv.com.hr/img/svg-loaders/oval.svg';
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">

	<!-- Basic Page Tags -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  
	<!-- Prefetch -->
	<link href="https://www.static.szv.com.hr" rel="preconnect" crossorigin>
	<link href="https://www.img.szv.com.hr" rel="preconnect" crossorigin>
	<link href="https://fonts.googleapis.com" rel="preconnect" crossorigin>

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="https://www.static.szv.com.hr/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="https://www.static.szv.com.hr/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="https://www.static.szv.com.hr/favicon/favicon-16x16.png">
	<link rel="manifest" href="https://www.static.szv.com.hr/favicon/site.webmanifest">
	<link rel="mask-icon" href="https://www.static.szv.com.hr/favicon/safari-pinned-tab.svg" color="#c37d92">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<!-- Relationships meta data profile -->
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php 
	
		wp_head(); 

		// Get canonical and store it into global $szvData
		$szvData['canonical'] = get_canonical();
		$szvData['noindex'] = get_noindex();

		//var_dump($szvData);

	?>

	<link href="https://fonts.googleapis.com/css?family=Varela+Round&amp;subset=latin-ext" rel="stylesheet"> 
	<link rel="stylesheet" href="https://www.static.szv.com.hr/css/styles.9.min.css">
	<?php if(false): ?>
	<link rel="stylesheet" href="https://www.static.szv.com.hr/js/phonevalidation/intl-tel-input-16.0.0/build/css/intlTelInput.min.css">
	<?php endif; ?>
	
	
 	<!-- JS var -->
 	<script type="application/json" id="jsonCustomVar">
    {
			"environment":  					"<?php echo WP_SZV_ENVIRONMENT; ?>",
			"domain":  								"<?php echo WP_SZV_DOMAIN; ?>",
			"path":  									"<?php echo WP_SZV_PATH; ?>",
			"fullPath":  							"<?php echo WP_SZV_FULLPATH; ?>",
			"imgCdnUrl":  						"<?php echo $szvData['imgCdnUrl']; ?>",
			"staticCdnUrl":  					"<?php echo $szvData['staticCdnUrl']; ?>",
			"onlineSZVUrl":  					"<?php echo $szvData['onlineSZVUrl']; ?>",
			"timestamp":  						"<?php echo $szvData['timestamp']; ?>",
			"wpLangId":  							"<?php echo $szvData['wpLangId']; ?>",
			"wpLangCode":  						"<?php echo $szvData['wpLangCode']; ?>",
			"onlineLangId":  					"<?php echo $szvData['onlineLangId']; ?>",
			"onlineLangCode":  				"<?php echo $szvData['onlineLangCode']; ?>",
			"postId":  								"<?php echo $szvData['postId']; ?>",
			"canonical":  						"<?php echo $szvData['canonical']; ?>",
			"noindex":  							"<?php echo $szvData['noindex']; ?>",
			"cookieNotiTitle":  			<?php echo $szvData['siteSettings']['cookieNotiTitle']; ?>,
			"cookieNotiText":  				<?php echo $szvData['siteSettings']['cookieNotiText']; ?>,
			"iAcceptText": 						"<?php echo $szvData['siteSettings']['iAcceptText']; ?>",
			"hasSearch":							<?php echo $szvData['hasSearch'] ? 'true' : 'false'; ?>,
			"isCategoryListing":			<?php echo $szvData['isCategoryListing'] ? 'true' : 'false'; ?>,
			"isProvider":							<?php echo $szvData['isProvider'] ? 'true' : 'false'; ?>,
			"libsVersion":						"1",
			"loaderIconUrl":					"<?php echo $szvData['loaderIconUrl']; ?>",
			"textAddedToFav":					"<?php echo $szvData['translations']['hasBeenAddedToFavorites'][$szvData['wpLangCode']]; ?>",
			"textRemovedFromFav":			"<?php echo $szvData['translations']['hasBeenRemovedFromFavorites'][$szvData['wpLangCode']]; ?>",
			"fieldIsRequired":				"<?php echo $szvData['translations']['fieldIsRequired'][$szvData['wpLangCode']]; ?>",
			"defaultErrorMsg":				"<?php echo $szvData['translations']['defaultErrorMsg'][$szvData['wpLangCode']]; ?>",
			"enterAllRequiredFields":	"<?php echo $szvData['translations']['enterAllRequiredFields'][$szvData['wpLangCode']]; ?>",
			"durationInquiryProcessing":	"<?php echo $szvData['translations']['durationInquiryProcessing'][$szvData['wpLangCode']]; ?>",
			"enterAllRequiredFieldsMultiInquiry":	"<?php echo $szvData['translations']['enterAllRequiredFieldsMultiInquiry'][$szvData['wpLangCode']]; ?>",
			"mobilePhone":				"<?php echo $szvData['translations']['mobilePhone'][$szvData['wpLangCode']]; ?>",
			"pleaseEnterMobilePhone":				"<?php echo $szvData['translations']['pleaseEnterMobilePhone'][$szvData['wpLangCode']]; ?>",
			"showPhoneNumber":				"<?php echo $szvData['translations']['showPhoneNumber'][$szvData['wpLangCode']]; ?>",
			"callUsOnMobilePhone":				"<?php echo $szvData['translations']['callUsOnMobilePhone'][$szvData['wpLangCode']]; ?>",
			"forExample":				"<?php echo $szvData['translations']['forExample'][$szvData['wpLangCode']]; ?>",
			"enterMobilePhone":				"<?php echo $szvData['translations']['enterMobilePhone'][$szvData['wpLangCode']]; ?>"
		}
	</script>

	<!-- Load JS -->

	<?php

		/* LOAD MINIFY */
		require_once(get_template_directory().'/plugins/minify-master/src/Minify.php');
		require_once(get_template_directory().'/plugins/minify-master/src/CSS.php');
		require_once(get_template_directory().'/plugins/minify-master/src/JS.php');
		require_once(get_template_directory().'/plugins/minify-master/src/Exception.php');
		require_once(get_template_directory().'/plugins/minify-master/src/Exceptions/BasicException.php');
		require_once(get_template_directory().'/plugins/minify-master/src/Exceptions/FileImportException.php');
		require_once(get_template_directory().'/plugins/minify-master/src/Exceptions/IOException.php');
		require_once(get_template_directory().'/plugins/minify-master/src/path-converter-master/src/ConverterInterface.php');
		require_once(get_template_directory().'/plugins/minify-master/src/path-converter-master/src/Converter.php');
		use MatthiasMullie\Minify;
	
		$sourcePath = STATIC_CDN_PATH.'js/inviewport/inviewport.js';
		$minifier = new Minify\JS($sourcePath);
		$minifier->add(STATIC_CDN_PATH.'js/zenscroll/zenscroll.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/main.js');
		$minifier->add(STATIC_CDN_PATH.'js/skip-link-focus-fix.js');
		$minifier->add(STATIC_CDN_PATH.'js/cookiejs/cookie.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/flickity/flickity.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/flickity/bg-lazyload.js');
		$minifier->add(STATIC_CDN_PATH.'js/sweetalert2/sweetalert2.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/sweetalert2/promise.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/sticky/sticky.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/phonevalidation/intl-tel-input-16.0.0/build/js/intlTelInput.min.js');
		$minifier->add(STATIC_CDN_PATH.'js/phonevalidation/intl-tel-input-16.0.0/build/js/utils.js');
		$minifier->add(STATIC_CDN_PATH.'js/async.js');

		// or we can just add plain js
		$js = '';
		$minifier->add($js);

		// save minified file to disk
		$minifiedPath = STATIC_CDN_PATH.'js/bundle.js';
		$minifier->minify($minifiedPath);


	?>

	<?php if($szvData['budleJs']): ?>
	<script defer src="<?php echo STATIC_CDN_URL.'/js/bundle.js'; ?>"></script>
	<?php endif; ?>
	<script defer src="<?php echo STATIC_CDN_URL.'/js/loadCSS.js'; ?>"></script>

	<?php if(true): ?>
	<script type="text/javascript">
		var dataLayer = [];
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P8ZKJFM');</script>
	<!-- End Google Tag Manager -->
	<?php endif; ?>

</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

	<?php if(true): ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8ZKJFM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php endif; ?>

	<div id="page" class="site">

		<!--[if lt IE 8]>
		<?php echo $szvData['siteSettings']['upgradeBrowserText']; ?>
		<![endif]-->

		<?php get_template_part( 'inc/advertising/top-bar' ); ?>
