<?php
/*
Template Name: Single page template
*/
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package albadiem
 */


get_header(); 

get_template_part( 'inc/layouts/header' ); ?>



	<!--[if lt IE 8]>

		<p class="browserupgrade">Koristite <strong>zastarjeli</strong> preglednik. Molimo <a href="http://browsehappy.com/">nadogradite preglednik</a> kako biste mogli koristiti stranicu.</p>

	<![endif]-->



	<!-- Wrapper -->

	<div id="wrapper">

		<?php require_once 'inc/advertising/top-bar.php'; ?>
        <?php
                // Start the loop.
                while ( have_posts() ) : the_post(); ?>

		
		<?php require_once 'inc/layouts/banner.php'; ?>

		<!-- Main Content

		================================================== -->
        
		<main id="content" role="main" class="single-page">


            <div id="single-page-intro" class="container-fluid">

                <div class="container">
                    <div class="row">
                        <div class="single-page-content" style="margin: 100px 0;">
                            <h1><?php echo get_the_title(); ?></h1>
                            <?php echo the_content(); ?> 
                        </div>
                    </div>
                </div>
        
            </div>

        </main>
		<!-- Main Content / End -->
        <?php endwhile;?>

		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>
    
  </div>    
   
<?php

get_template_part( 'inc/layouts/footer' ); 
