<?php

class Custom_Fields_Controller extends WP_REST_Controller
{
    public function __construct()
    {
        $this->namespace = 'acf';
    }

    /*
     * Due to ACF not having option to get custom fields for a category, it is needed to have initial post
     * for each of the categories and then get the acf field settings for that initial post.
     */
    public function get_items($request)
    {
        $category_posts = get_posts(array(
            'category' => $request['id'],
            'post_status' => 'draft',
            'numberposts' => -1));

        foreach ($category_posts as $category_post)
        {
            if ($category_post->post_title == 'initial')
                $initial_category_post = $category_post;
        }

        if (!isset($initial_category_post))
            return new WP_Error('custom-fields-error','Category has no initial post.', array('status' => 500 ));

        $field_objects = get_field_objects($initial_category_post->ID, true, false);
        return new WP_REST_Response($field_objects, 200);
    }

    public function get_item_hr($request)
    {
        /*
        $post = get_post($request['id']);
        if (empty($post))
            return new WP_Error('invalid_post_id', 'Post does not exist.', array( 'status' => 404 ));
        */

        switch_to_blog(1);
        $post = get_post($request['id']);
        $locale = array('language' => 'hr');
        if(empty($post)){
            return new WP_Error('invalid_post_id', 'Post does not exist.', array( 'status' => 404 ));
        }

        //$locale = array('language' => get_locale());
        $post_acf_values = get_field_objects($request['id'], true, true);

        return (object)array_merge($locale, (array)$post, (array)$post_acf_values);
    }

    public function get_item_en($request)
    {
        /*
        $post = get_post($request['id']);
        if (empty($post))
            return new WP_Error('invalid_post_id', 'Post does not exist.', array( 'status' => 404 ));
        */

        switch_to_blog(3);
        $post = get_post($request['id']);
        $locale = array('language' => 'en');
        if(empty($post)){
            return new WP_Error('invalid_post_id', 'Post does not exist.', array( 'status' => 404 ));
        }

        //$locale = array('language' => get_locale());
        $post_acf_values = get_field_objects($request['id'], true, true);

        return (object)array_merge($locale, (array)$post, (array)$post_acf_values);
    }

    public function register_routes()
    {
        $path = '(?P<id>\d+)';

        register_rest_route($this->namespace, '/category/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_items'),
                'permission_callback' => array($this, 'permissions_check')),
        ]);

        register_rest_route($this->namespace, '/post/hr/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_item_hr'),
                'permission_callback' => array($this, 'permissions_check')),
        ]);

        register_rest_route($this->namespace, '/post/en/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_item_en'),
                'permission_callback' => array($this, 'permissions_check')),
        ]);
    }

    /**
     * @return bool
     */
    public function permissions_check()
    {
        require_once ('Authorization_Helper.php');
        return (new Authorization_Helper())->Is_User_Authorized();
    }
}