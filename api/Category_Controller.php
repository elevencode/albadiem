<?php
class Category_Controller extends WP_REST_Controller
{
    public function __construct()
    {
        $this->namespace = 'acf';
    }

    public function get_item($request)
    {
        $category_posts = get_posts(array(
            'category_name' => $request['slug'],
            'post_status' => 'draft',
            'numberposts' => -1));

        foreach ($category_posts as $category_post)
        {
            if ($category_post->post_title == 'initial')
                $initial_category_post = $category_post;
        }

        if (!isset($initial_category_post))
            return new WP_Error('custom-fields-error','Category has no initial post.', array('status' => 500 ));

        $field_objects = get_field_objects($initial_category_post->ID, true, false);
        return new WP_REST_Response($field_objects, 200);
    }

    public function register_routes()
    {
        $path = '(?P<slug>[^/]+)';

        register_rest_route($this->namespace, '/category-slug/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_item'),
                'permission_callback' => array($this, 'permissions_check')),
        ]);
    }

    /**
     * @return bool
     */
    public function permissions_check()
    {
        require_once ('Authorization_Helper.php');
        return (new Authorization_Helper())->Is_User_Authorized();
    }
}