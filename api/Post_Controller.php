<?php
class Post_Controller extends WP_REST_Controller
{
    public function __construct()
    {
        $this->namespace = 'posts';
    }

    public function create_item($request)
    {
        $request_params = $request->get_json_params();

        foreach ($request_params['post_category'] as $category_id)
        {
            $category_posts = get_posts(array(
                'category' => $category_id,
                'post_status' => 'any',
                'numberposts' => -1));

            foreach ($category_posts as $category_post)
            {
                if($category_post->post_title == $request_params['post_title'])
                    return new WP_Error('post_creation_fail', 'Post with same title in this category already exist.',
                        array('status' => 500));
            }
        }

        $post = array
        (
            'post_title' => $request_params['post_title'],
            'post_content' => $request_params['post_content'],
            'post_status' => $request_params['post_status'],
            'post_author' => $request_params['post_author'],
            'post_category' => $request_params['post_category'],

            'post_date' => $request_params['post_date'],
            'post_date_gmt' => $request_params['post_date_gmt'],
            'post_excerpt' => $request_params['post_excerpt'],
            'comment_status' => $request_params['comment_status'],
            'ping_status' => $request_params['ping_status'],
            'post_password' => $request_params['post_password'],
            'post_name' => $request_params['post_name'],
            'to_ping' => $request_params['to_ping'],
            'pinged' => $request_params['pinged'],
            'post_modified' => $request_params['post_modified'],
            'post_modified_gmt' => $request_params['post_modified_gmt'],
            'post_content_filtered' => $request_params['post_content_filtered'],
            'post_parent' => $request_params['post_parent'],
            'guid' => $request_params['guid'],
            'menu_order' => $request_params['menu_order'],
            'post_type' => $request_params['post_type'],
            'post_mime_type' => $request_params['post_mime_type']
        );
        $post_id = wp_insert_post($post, true);

        if (is_wp_error($post_id))
            return new WP_Error('post_creation_fail', $post_id->get_error_message(), array('status' => 500));

        return $this->add_or_update_post_meta_fields($request_params, $post_id);
    }

    public function update_item($request)
    {
        $request_params = $request->get_json_params();

        foreach ($request_params['post_category'] as $category_id)
        {
            $category_posts = get_posts(array(
                'category' => $category_id,
                'post_status' => 'any',
                'numberposts' => -1));

            foreach ($category_posts as $category_post)
            {
                if($category_post->post_title == $request_params['post_title'] && $category_post->ID != $request_params['ID'])
                    return new WP_Error('post_update_fail', 'Post with same title in this category already exist.',
                        array('status' => 500));
            }
        }

        $post = array
        (
            'ID' => $request_params['ID'],
            'post_title' => $request_params['post_title'],
            'post_content' => $request_params['post_content'],
            'post_status' => $request_params['post_status'],
            'post_author' => $request_params['post_author'],
            'post_category' => $request_params['post_category'],

            'post_date' => $request_params['post_date'],
            'post_date_gmt' => $request_params['post_date_gmt'],
            'post_excerpt' => $request_params['post_excerpt'],
            'comment_status' => $request_params['comment_status'],
            'ping_status' => $request_params['ping_status'],
            'post_password' => $request_params['post_password'],
            'post_name' => $request_params['post_name'],
            'to_ping' => $request_params['to_ping'],
            'pinged' => $request_params['pinged'],
            'post_modified' => $request_params['post_modified'],
            'post_modified_gmt' => $request_params['post_modified_gmt'],
            'post_content_filtered' => $request_params['post_content_filtered'],
            'post_parent' => $request_params['post_parent'],
            'guid' => $request_params['guid'],
            'menu_order' => $request_params['menu_order'],
            'post_type' => $request_params['post_type'],
            'post_mime_type' => $request_params['post_mime_type']
        );
        $post_id = wp_update_post($post, true);

        if (is_wp_error($post_id))
            return new WP_Error('post_update_fail', $post_id->get_error_message(), array('status' => 500));

        return $this->add_or_update_post_meta_fields($request_params, $post_id);
    }

    public function delete_item($request)
    {
        $post = get_post($request['post_id']);

        if (empty($post))
            return new WP_Error('invalid_post_id', 'post does not exist', array( 'status' => 404 ));

        $post = array( 'ID' => $post->ID, 'post_status' => 'draft' );
        $post_update = wp_update_post($post, true);

        if (is_wp_error($post_update))
            return new WP_Error('post_creation_fail', $post_update->get_error_message(), array( 'status' => 500 ));

        return new WP_REST_Response("Post successfully deleted.", 200);
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, '/create', [
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'create_item' ),
                'permission_callback' => array( $this, 'permissions_check' )),
        ]);

        register_rest_route($this->namespace, '/delete', [
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'delete_item' ),
                'permission_callback' => array( $this, 'permissions_check' )),
        ]);

        register_rest_route($this->namespace, '/update', [
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'update_item' ),
                'permission_callback' => array( $this, 'permissions_check' )),
        ]);
    }

    /**
     * @return bool
     */
    public function permissions_check()
    {
        require_once ('Authorization_Helper.php');
        return (new Authorization_Helper())->Is_User_Authorized();
    }

    private function add_or_update_post_meta_fields($request_params, $post_id)
    {
        $top_parent_category = $this->get_top_category($post_id);
        $category_posts = get_posts(array(
            'category' => array($top_parent_category->term_id),
            'post_status' => 'draft',
            'numberposts' => -1));

        foreach ($category_posts as $category_post)
        {
            if ($category_post->post_title == 'initial')
            {
                $initial_category_post = $category_post;
                break;
            }
        }

        if (!isset($initial_category_post))
            return new WP_Error('custom-fields-error','Category has no initial post.', array('status' => 500 ));

        $category_acf_settings = get_field_objects($initial_category_post->ID, true, false);
        $category_custom_field_names = array_keys($category_acf_settings);

        foreach ($request_params as $key => $request_param)
        {
            if (in_array($key, $category_custom_field_names))
                $this->add_or_update_child_custom_field($request_params, $key, $post_id);
        }

        return new WP_REST_Response('Post created/updated.', 200);
    }

    private function add_or_update_child_custom_field($request_params, $key, $post_id)
    {
        $is_leaf = isset($request_params[$key]['sub_fields']);
        if ($is_leaf)
        {
            update_field($request_params[$key]['key'], $request_params[$key]['value'], $post_id);
            return;
        }

        $is_value_an_object = $request_params[$key]['type'] == 'group';
        $value = $is_value_an_object
            ? (array)$request_params[$key]['value']
            : $request_params[$key]['value'];

        update_field($request_params[$key]['key'], $value, $post_id);

        foreach ($request_params[$key]['sub_fields'] as $key => $request_param)
        {
            $this->add_or_update_child_custom_field($request_params[$key]['sub_fields'], $key, $post_id);
        }
    }

    /**
     * @param $post_id
     * @return WP_Term
     */
    private function get_top_category($post_id)
    {
        $categories = get_the_category($post_id);
        $top_categories_array = array();

        foreach($categories as $category) {
            if ($category->parent == 0) {
                $top_categories_array[] = $category;
            }
        }
        return $top_categories_array[0];
    }
}