<?php
class Authorization_Helper
{
    /**
     * return bool
     */
    public function Is_User_Authorized()
    {
        $ip_addresses_whitelist = array('185.58.73.126');
        
        //var_dump($_SERVER);

        //if (!in_array($_SERVER['REMOTE_ADDR'], $ip_addresses_whitelist))
        if(!in_array($_SERVER['SERVER_ADDR'], $ip_addresses_whitelist))
          return false;

        $request_header = apache_request_headers()['Authorization'];
        if(empty($request_header))
          return false;

        $credentials = explode(':', base64_decode(preg_replace('/^Basic /', '', $request_header)));

        $username = $credentials[0];
        $password = $credentials[1];

        $authentication = wp_authenticate($username, $password);

        if (is_wp_error($authentication))
          return false;

        return true;
    }
}