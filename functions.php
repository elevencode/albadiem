<?php
/**
 * albadiem functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package albadiem
 */

if ( version_compare($GLOBALS['wp_version'], '5.0-beta', '>') ) {
    // WP > 5 beta
    add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );
} else {
    // WP < 5 beta
    add_filter( 'gutenberg_can_edit_post_type', '__return_false' );
}

if ( ! function_exists( 'albadiem_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function albadiem_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on albadiem, use a find and replace
		 * to change 'albadiem' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'albadiem', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one grad.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'albadiem' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'albadiem_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'albadiem_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function albadiem_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Language widget', 'albadiem' ),
		'id'            => 'language-widget',
		'description'   => esc_html__( 'Add widgets here.', 'albadiem' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'albadiem_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function albadiem_scripts() {
	//wp_enqueue_style( 'albadiem-style', get_stylesheet_uri() );
		//wp_enqueue_style( 'szv-style-global', get_template_directory_uri() . '/css/styles.min.css');
	//wp_enqueue_style( 'szv-style-global', 'https://www.static.szv.com.hr/css/styles.min.css');

	//wp_enqueue_script('jquery');
	//wp_enqueue_script( 'albadiem-main', get_template_directory_uri() . '/assets/js/main.js', array(), true );
	//wp_enqueue_script( 'albadiem-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), true );
	//wp_enqueue_script( 'albadiem-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), true );
	//wp_enqueue_script( 'albadiem-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array(), true );
	//wp_enqueue_script( 'albadiem-zenscroll', get_template_directory_uri() . '/assets/js/zenscroll/zenscroll.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-cookiejs', get_template_directory_uri() . '/assets/js/cookiejs/cookie.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-sweetalert2', get_template_directory_uri() . '/assets/js/sweetalert2/sweetalert2.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-sweetalert2-promise', get_template_directory_uri() . '/assets/js/sweetalert2/promise.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-sticky', get_template_directory_uri() . '/assets/js/sticky/sticky.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-noti', get_template_directory_uri() . '/assets/js/noti/noti.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-flickity-lazyload', get_template_directory_uri() . '/assets/js/flickity/flickity.min.js', array(), true );
	//wp_enqueue_script( 'albadiem-flickity-', get_template_directory_uri() . '/assets/js/flickity/bg-lazyload.js', array(), true );

	//wp_enqueue_script( 'albadiem-async', get_template_directory_uri() . '/assets/js/async.js', array(), true );
	//wp_enqueue_script( 'albadiem-global', get_template_directory_uri() . '/assets/js/global.js', array(), true );

	/*
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	*/
}
add_action( 'wp_enqueue_scripts', 'albadiem_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* 
	============================================
	Disable wordpress comments
	============================================
*/

require get_template_directory() . '/inc/disable-comments.php';

/* 
	============================================
	Custom post types
	============================================
*/

require get_template_directory() . '/inc/custom-post-types/blog-cpt.php';

/* 
	============================================
	Include Walker file
	============================================
*/

require get_template_directory() . '/inc/nav/main-walker-menu.php';
require get_template_directory() . '/inc/nav/category-walker-menu.php';
require get_template_directory() . '/inc/nav/footer-walker-menu.php';

add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'Category', __( 'Category Menu', 'theme-slug' ) );
}

add_filter('body_class', 'custom_body_class');
function custom_body_class($classes) {
  $post_type = get_post_type();
  //var_dump($post_type);
    if (is_single() && $post_type != 'blog')
        $classes[] = 'single-provider ';
    return $classes;
}

function add_link_atts($atts) {
  $atts['class'] = "nav-first-level prev-default";
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_link_atts');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Global Options',
		'menu_title'	=> 'Global Options',
		'menu_slug' 	=> 'global-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Global Dynamic Descriptions',
		'menu_title'	=> 'Global Dynamic Descriptions',
		'parent_slug'	=> 'global-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Search Bar City and Country List',
		'menu_title'	=> 'Search Bar Settings',
		'parent_slug'	=> 'global-options',
	));
}

// Remove tags support from posts
function unregister_tags() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'unregister_tags');

/* Register template redirect action callback */
add_action('template_redirect', 'meks_remove_wp_archives');
 
/* Remove archives */
function meks_remove_wp_archives(){
  //If we are on category or tag or date or author archive
  if( is_tag() || is_date() || is_author() ) {
    global $wp_query;
    $wp_query->set_404(); //set to 404 not found page
  }
}

add_action( 'after_setup_theme', 'register_custom_nav_menus' );
function register_custom_nav_menus() {
	register_nav_menus( array(
		'footer_widget_1' => 'Footer Widget 1',
		'footer_widget_2' => 'Footer Widget 2',
		'footer_widget_3' => 'Footer Widget 3',
	) );
}

/*
add_filter('locale','sitename_locale');
function sitename_locale($locale){  

	global $current_blog;
	var_dump($current_blog); 

	if (is_admin ()) // if this is the admin back end don't change the locale 
		return $locale; 
	
	if (1 == $current_blog->blog_id){  // 7 is the site id for real men drink milk
		return 'hr';
	}else if ('3' == $current_blog->blog_id){ // 15 is the site id for doug lawrence UAE
		return 'en';
	}

	return $locale;
}
*/

function albadiem_pagination(
	$numpages = '', 
	$pagerange = '', 
	$paged='') 
	{

	if (empty($pagerange)) {
	  $pagerange = 2;
	}
	global $paged;
	if (empty($paged)) {
	  $paged = 1;
	}
	if ($numpages == '') {
	  global $wp_query;
	  $numpages = $wp_query->max_num_pages;
	  if(!$numpages) {
		  $numpages = 1;
	  }
	}
  
	$big = 999999999; // need an unlikely integer

	$pagination_args = array(
 	  'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	  'format'          => '/page/%#%',
	  'total'           => $numpages,
	  'current'         => $paged,
	  'show_all'        => False,
	  'end_size'        => 1,
	  'mid_size'        => $pagerange,
	  'prev_next'       => True,
	  'prev_text'    	  => sprintf( '<span class="fa fa-chevron-left"></span> %1$s', __( '', 'albadiem' ) ),
	  'next_text'    	  => sprintf( '%1$s <span class="fa fa-chevron-right"></span>', __( '', 'albadiem' ) ),
	  'type'            => 'array',
	  'add_args'        => false,
	  'add_fragment'    => '#listing'
	);
  
  $paginate_links = paginate_links($pagination_args);
  
  //var_dump($paginate_links);
  
  if (is_array($paginate_links)) {
    //  echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
    foreach ( $paginate_links as $page ) {
            
      $finalPage = $page;
      
      if(strpos($page, '/page/1"') !== false) {
        $finalPage = str_replace('/page/1"', '"', $page);
      }
      
      if(strpos($page, '/page/1#listing') !== false) {
        $finalPage = str_replace('/page/1#listing', '#listing', $page);
      }
      
      echo $finalPage;
      
    }
  }

}

// removing yoast seo breadcrumb separator
function filter_wpseo_breadcrumb_separator($this_options_breadcrumbs_sep) {
	return '';
};

// add the filter for breadcrum separator
add_filter('wpseo_breadcrumb_separator', 'filter_wpseo_breadcrumb_separator', 10, 1);

//add extra fields to category edit form hook
add_action ( 'edit_category_form_fields', 'extra_category_fields');

//add extra fields to category edit form callback function
function extra_category_fields( $tag ) {    //check for existing featured ID
    $t_id = $tag->term_id;
    $cat_meta = get_option( "category_$t_id");
?>
	<tr class="form-field">
		<th scope="row" valign="top"><label for="extra"><?php _e('Advertised field'); ?></label></th>
		<td>
			<input type="text" name="Cat_meta[extra]" id="Cat_meta[extra]" size="25" style="width:60%;" value="<?php echo $cat_meta['extra'] ? $cat_meta['extra'] : ''; ?>"><br />
			<span class="description"><?php _e('Advertised field (Insert up to 10 ID for specific category which will be filtered in Advertised query. Separate them with comma (1,2,3,4...)'); ?></span>
		</td>
	</tr>
<?php
}

// save extra category extra fields hook
add_action ( 'edited_category', 'save_extra_category_fileds');
// save extra category extra fields callback function
function save_extra_category_fileds( $term_id ) {
    if ( isset( $_POST['Cat_meta'] ) ) {
        $t_id = $term_id;
        $cat_meta = get_option( "category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
            foreach ($cat_keys as $key){
            if (isset($_POST['Cat_meta'][$key])){
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
        //save the option array
        update_option( "category_$t_id", $cat_meta );
    }
}

// register custom api controllers
require_once('api/Post_Controller.php');
require_once('api/Custom_Fields_Controller.php');
require_once('api/Category_Controller.php');
add_action('rest_api_init', function () {
    (new Post_Controller())->register_routes();
    (new Custom_Fields_Controller())->register_routes();
    (new Category_Controller())->register_routes();
});

// /*
//  * Intercepts main query before going to database to exclude searching by children categories
//  * which was causing slower query.
//  */
// add_action( 'pre_get_posts', 'exclude_children_categories' );
// function exclude_children_categories( $query ) {
//     if ( !is_admin() && $query->is_main_query() ) {
//         $tax_query = array(
//             'taxonomy' => $query->query_vars['category_name'],
//             'field' => 'slug',
//             'terms' => $query->query_vars['category_name'],
//             'operator'=> 'AND',
//             'include_children' => false);
//         $query->tax_query->queries[] = $tax_query;
// 		$query->query_vars['tax_query'] = $query->tax_query->queries;
// 		$query->set('paged', get_query_var('paged'));
//     }
// }

/**
 * Set a Category Vjencanje on post creation and save
 * 
 * Read more: {@link https://developer.wordpress.org/reference/functions/get_post_type/}
 *            {@link https://codex.wordpress.org/Function_Reference/get_category_by_slug}
 *            {@link https://codex.wordpress.org/Function_Reference/wp_set_object_terms}
 *            
 */
add_action( 'save_post', 'set_vjencanje_cat_on_save' );
function set_vjencanje_cat_on_save( $post_id )
{
    global $wpdb;

    // Check for correct post type
    if ( get_post_type() == 'post' && get_locale() == 'hr') {  
		
		// Find the Category by slug
		$field = get_field_object('providerBigCityName', $post_id);
		$value = $field['value'];
		$label = $field['choices'][ $value ];
		$bigCityID = 'vjencanje-' . strtolower($label);
		$idObj = get_category_by_slug( 'vjencanje' ); // Set your Category here
		$idObjCity = get_category_by_slug($bigCityID); // Set your Category here
	
		// Get the Category ID
		$id = $idObj->term_id;
		$idCity = $idObjCity->term_id;

		$categoryArray = array($id, $idCity);


       // Set now the Category for this CPT
       wp_set_object_terms( $post_id, $categoryArray, 'category', true );
	
	} else if ( get_post_type() == 'post' && get_locale() == 'en_US') {
      
       // Find the Category by slug
		$field = get_field_object('providerBigCityName', $post_id);
		$value = $field['value'];
		$label = $field['choices'][ $value ];
		$bigCityID = 'wedding-' . strtolower($label);
		$idObj = get_category_by_slug( 'wedding' ); // Set your Category here
		$idObjCity = get_category_by_slug($bigCityID); // Set your Category here
	
		// Get the Category ID
		$id = $idObj->term_id;
		$idCity = $idObjCity->term_id;

		$categoryArray = array($id, $idCity);


       // Set now the Category for this CPT
       wp_set_object_terms( $post_id, $categoryArray, 'category', true );
    }

	
	
}// end function


// Remove URL category hierarchy function
add_action( 'init', 'build_taxonomies', 0 );
function build_taxonomies() {

register_taxonomy( 'category', 'post', array(
	'hierarchical' => true,
	'update_count_callback' => '_update_post_term_count',
	'query_var' => 'category_name',
	'rewrite' => did_action( 'init' ) ? array(
	'hierarchical' => false,
	'slug' => get_option('category_base') ? get_option('category_base') : 'category',
	'with_front' => false) : false,
	'public' => true,
	'show_ui' => true,
	'_builtin' => true,
	) );
}

//hook into the init action and call create_topics_nonhierarchical_taxonomy when it fires
 
add_action( 'init', 'create_gradovi_nonhierarchical_taxonomy', 0 );
add_action( 'init', 'create_drzave_nonhierarchical_taxonomy', 1 );
 
function create_gradovi_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Gradovi', 'taxonomy general name' ),
    'singular_name' => _x( 'Grad', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Gradovi' ),
    'popular_items' => __( 'Popular Gradovi' ),
    'all_items' => __( 'All Gradovi' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Grad' ), 
    'update_item' => __( 'Update Grad' ),
    'add_new_item' => __( 'Add New Grad' ),
    'new_item_name' => __( 'New Grad Name' ),
    'separate_items_with_commas' => __( 'Separate topics with commas' ),
    'add_or_remove_items' => __( 'Add or remove topics' ),
    'choose_from_most_used' => __( 'Choose from the most used topics' ),
    'menu_name' => __( 'Gradovi' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('gradovi','post',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'grad' ),
  ));
  
}

function create_drzave_nonhierarchical_taxonomy() {
 
	// Labels part for the GUI
	 
	  $labels = array(
		'name' => _x( 'Drzave', 'taxonomy general name' ),
		'singular_name' => _x( 'Drzava', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Drzave' ),
		'popular_items' => __( 'Popular Drzave' ),
		'all_items' => __( 'All Drzave' ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __( 'Edit Drzava' ), 
		'update_item' => __( 'Update Drzava' ),
		'add_new_item' => __( 'Add New Drzava' ),
		'new_item_name' => __( 'New Drzava Name' ),
		'separate_items_with_commas' => __( 'Separate drzave with commas' ),
		'add_or_remove_items' => __( 'Add or remove drzave' ),
		'choose_from_most_used' => __( 'Choose from the most used drzave' ),
		'menu_name' => __( 'Drzave' ),
	  ); 
	 
	// Now register the non-hierarchical taxonomy like tag
	 
	  register_taxonomy('drzave','post',array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'drzava' ),
	  ));
	  
	}

	// Remove emoji 
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
	remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Disable embeds
	function my_deregister_scripts(){
		wp_dequeue_script( 'wp-embed' );
	}
	add_action( 'wp_footer', 'my_deregister_scripts' );

	// Disable wp generator WP version
	remove_action( 'wp_head', 'wp_generator' );

	// Disable RSS feed
	function itsme_disable_feed() {
		wp_die( __( 'No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' ) );
	}
	add_action('do_feed', 'itsme_disable_feed', 1);
	add_action('do_feed_rdf', 'itsme_disable_feed', 1);
	add_action('do_feed_rss', 'itsme_disable_feed', 1);
	add_action('do_feed_rss2', 'itsme_disable_feed', 1);
	add_action('do_feed_atom', 'itsme_disable_feed', 1);
	add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
	add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);

	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

	// Remove JSON API from head
	remove_action('template_redirect', 'rest_output_link_header', 11, 0);
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links');

	// Remove dns-prefetch
	remove_action( 'wp_head', 'wp_resource_hints', 2 );

	// Remove wp-block-library
	add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
	function wps_deregister_styles() {
		wp_dequeue_style( 'wp-block-library' );
	}

	// Remove shortlink
	add_filter('after_setup_theme', 'remove_redundant_shortlink');
	function remove_redundant_shortlink() {
		// remove HTML meta tag
		// <link rel='shortlink' href='http://example.com/?p=25' />
		remove_action('wp_head', 'wp_shortlink_wp_head', 10);

		// remove HTTP header
		// Link: <https://example.com/?p=25>; rel=shortlink
		remove_action( 'template_redirect', 'wp_shortlink_header', 11);
	}

	// Remove inline CSS
	add_action('wp_enqueue_scripts', 'remove_embedded_style');
	function remove_embedded_style() {
		global $wp_widget_factory;
		remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	}
  
  // Get all multisite sites
  function get_all_sites(){
    $sites = get_sites();
    foreach ( $sites as $site ) {
        
    }
    return $sites;
  }
  
  // Override WP_HOME & WP_SITEURL
  /*
  function _ms_config_wp_siteurl( $url = '' ) {
    if (is_multisite()):
        global $blog_id, $current_site;
        $cur_blog_id = defined('BLOG_ID_CURRENT_SITE')? BLOG_ID_CURRENT_SITE : 1;
        $key = ($blog_id!=$cur_blog_id)? $blog_id.'_' : '';
        $constant = 'WP_'.$key.'SITEURL';
        if ( defined( $constant ) )
            return untrailingslashit( constant($constant) );
    endif;
    return $url;
  }
  add_filter( 'option_siteurl', '_ms_config_wp_siteurl' );

  function _ms_config_wp_home( $url = '' ) {
    if (is_multisite()):
        global $blog_id;
        $cur_blog_id = defined('BLOG_ID_CURRENT_SITE')? BLOG_ID_CURRENT_SITE : 1;
        $key = ($blog_id!=$cur_blog_id)? $blog_id.'_' : '';
        $constant = 'WP_'.$key.'HOME';
        if ( defined( $constant ) )
            return untrailingslashit( constant($constant) );
    endif;
    return $url;
  }
  add_filter( 'option_home',    '_ms_config_wp_home'    );
  */

  /*
  function _ms_config_wp_siteurl( $url = '' ) {
    
    if(is_multisite()){
      $cur_blog_id = $GLOBALS['current_blog']->blog_id;
      
      //echo '<br> SITEURL cur_blog_id: '.$cur_blog_id;
      
      if($cur_blog_id > 1){
        $constant = 'WP_'.$cur_blog_id.'_SITEURL';
        $url = untrailingslashit(constant($constant));
      }else{
        $constant = 'WP_SITEURL';
        $url = untrailingslashit(constant($constant));
      }
      
    }else{
      
    }
    
    echo '<br>SITEURL: '.$url;
    
    return $url;
  }
  add_filter( 'option_siteurl', '_ms_config_wp_siteurl');
  */

  /*
  function _ms_config_wp_home( $url = '' ) {
    
    if(is_multisite()){
      $cur_blog_id = $GLOBALS['current_blog']->blog_id;

      //echo '<br> HOME cur_blog_id: '.$cur_blog_id;

      if($cur_blog_id > 1){
        $constant = 'WP_'.$cur_blog_id.'_HOME';
        $url = untrailingslashit(constant($constant));
      }else{
        $constant = 'WP_HOME';
        $url = untrailingslashit(constant($constant));
      }
      
    }else{
      
    }
    
    echo '<br>HOME: '.$url;
    
    return $url;
  }
  add_filter( 'option_home', '_ms_config_wp_home');
	*/

	// Get current id for post, page, cat...
	function set_wp_data(){

		global $wp_query; 
		
		$id = -1;

		if(is_home() || is_front_page()){
			$id = get_option('page_on_front');
		}
		else if(is_search()){
			$id = -2;
		}
		else{
			$id = $wp_query->get_queried_object_id();
		}

		//var_dump($id);
		global $szvData;
		$szvData['postId'] = $id;	

		//var_dump($wp_query);

	}
	add_action('wp', 'set_wp_data'); 

	// Canonical
	function design_canonical($url) {

		//Override canonical
		//$url = 'test';

		return $url;
	}
	add_filter('wpseo_canonical', 'design_canonical');

	// Custom get canonical
	function get_canonical($post_id = NULL){

		$wpseo = WPSEO_Frontend::get_instance();
		//var_dump($wpseo);

		//return wp_get_canonical_url($post_id);
		return esc_url($wpseo->canonical(false), null, 'other');
	}	

	// Custom get robots noindex
	function get_noindex($post_id = NULL){

		$wpseo = WPSEO_Frontend::get_instance();
		//var_dump($wpseo);

		$robots = '';

		ob_start();
		$robotsYoast = $wpseo->robots(false);
		$output = ob_get_contents();
		ob_end_clean();

		//var_dump($output);

		if (strpos($output, 'noindex') !== false) {
			$robots = 'noindex';
		}
		//var_dump($robots);

		return $robots;
	}

	


  
  // Override WP_HOME & WP_SITEURL
  add_action( 'after_setup_theme', 'set_custom_wp_home_siteurl' );
  function set_custom_wp_home_siteurl() {
    
    $cur_blog_id = $GLOBALS['current_blog']->blog_id;
    $cur_siteurl = get_site_url($cur_blog_id);
    $cur_home = get_home_url($cur_blog_id);
    
    $constant = 'WP_'.$cur_blog_id.'_SITEURL';
    $url = untrailingslashit(constant($constant));
    
    //echo '<br>cur_siteurl: '.$cur_siteurl;
    //echo '<br>cur_home: '.$cur_siteurl;
 
    if($cur_siteurl == $url && $cur_home == $url){
      
    }else{
      
      //echo '<br>SET NEW HOME & SITURL: '.$url;
      update_option("siteurl",$url);
      update_option("home",$url);
    
		}
		
		// Set WP postId % langId and langCode
		global $szvData;

		$szvData['wpLangId'] = untrailingslashit(constant('WP_'.$cur_blog_id.'_LANGID'));
		$szvData['wpLangCode'] = untrailingslashit(constant('WP_'.$cur_blog_id.'_LANGCODE'));;
		$szvData['onlineLangId'] = untrailingslashit(constant('WP_'.$cur_blog_id.'_ONLINELANGID'));
		$szvData['onlineLangCode'] = untrailingslashit(constant('WP_'.$cur_blog_id.'_ONLINELANGCODE'));

	}	

	// Get acf field
	function get_acf_field($field, $json_encode = false, $post_id = NULL){

		if($json_encode){
			//$value = json_encode(get_post_meta($post_id, $field, true));
			$value = json_encode(get_field($field, 'option'));
		}else{
			//$value = get_post_meta($post_id, $field, true);
			$value = get_field($field, 'option');
		}

		return $value;

	}

	// Get Site settings acf fields from db
	function get_site_settings(){

		global $szvData;

		$szvData['siteSettings'] = [];
		$szvData['siteSettings']['siteName'] = get_acf_field('siteName');
		$szvData['siteSettings']['siteDesc'] = get_acf_field('siteDesc');
		$szvData['siteSettings']['siteUrl'] = get_acf_field('siteUrl');
		$szvData['siteSettings']['logoImgSrc'] = get_acf_field('logoImgSrc');
		$szvData['siteSettings']['logoHref'] = get_acf_field('logoHref');
		$szvData['siteSettings']['szvPhone'] = get_acf_field('szvPhone');
		$szvData['siteSettings']['szvMobilePhone'] = get_acf_field('szvMobilePhone');
		$szvData['siteSettings']['szvEmail'] = get_acf_field('szvEmail');
		$szvData['siteSettings']['szvFacebookUrl'] = get_acf_field('szvFacebookUrl');
		$szvData['siteSettings']['szvInstagramUrl'] = get_acf_field('szvInstagramUrl');
		$szvData['siteSettings']['szvChatEnabled'] = get_acf_field('szvChatEnabled');
		$szvData['siteSettings']['showFreeAdvertising'] = get_acf_field('showFreeAdvertising');
		$szvData['siteSettings']['cookieNotiTitle'] = get_acf_field('cookieNotiTitle', true);
		$szvData['siteSettings']['cookieNotiText'] = get_acf_field('cookieNotiText', true);
		$szvData['siteSettings']['topBarMarketingLink'] = get_acf_field('topBarMarketingLink');
		$szvData['siteSettings']['topBarFavoritesLink'] = get_acf_field('topBarFavoritesLink');
		$szvData['siteSettings']['topBarMarketingLinkText'] = get_acf_field('topBarMarketingLinkText');
		$szvData['siteSettings']['upgradeBrowserText'] = get_acf_field('upgradeBrowserText');
		$szvData['siteSettings']['iAcceptText'] = get_acf_field('iAcceptText');
		$szvData['siteSettings']['homeCountryImgSrc'] = get_acf_field('homeCountryImgSrc');
		
		$szvData['translations']['weddingSlug']['hr'] = 'vjencanje';
		$szvData['translations']['weddingSlug']['en'] = 'wedding';

		$szvData['translations']['for']['hr'] = 'za';
		$szvData['translations']['for']['en'] = 'for';

		$szvData['translations']['wedding']['hr'] = 'Vjenčanje';
		$szvData['translations']['wedding']['en'] = 'Wedding';

		$szvData['translations']['weddings']['hr'] = 'Vjenčanja';
		$szvData['translations']['weddings']['en'] = 'Weddings';

		$szvData['translations']['croatia']['hr'] = 'Hrvatska';
		$szvData['translations']['croatia']['en'] = 'Croatia';

		$szvData['translations']['inCroatia']['hr'] = 'u Hrvatskoj';
		$szvData['translations']['inCroatia']['en'] = 'in Croatia';

		$szvData['translations']['in']['hr'] = 'u';
		$szvData['translations']['in']['en'] = 'in';

		$szvData['translations']['numberOfMembers']['hr'] = 'Broj članova';
		$szvData['translations']['numberOfMembers']['en'] = 'Members';

		$szvData['translations']['upTo']['hr'] = 'do';
		$szvData['translations']['upTo']['en'] = 'up to';

		$szvData['translations']['address']['hr'] = 'Adresa';
		$szvData['translations']['address']['en'] = 'Address';

		$szvData['translations']['maleVocals']['hr'] = 'Muških vokala';
		$szvData['translations']['maleVocals']['en'] = 'Male vocals';
		
		$szvData['translations']['femaleVocals']['hr'] = 'Ženskih vokala';
		$szvData['translations']['femaleVocals']['en'] = 'Female vocals';

		$szvData['translations']['priceIncludes']['hr'] = 'Cijena uključuje';
		$szvData['translations']['priceIncludes']['en'] = 'Price includes';

		$szvData['translations']['price']['hr'] = 'Cijena';
		$szvData['translations']['price']['en'] = 'Price';

		$szvData['translations']['soundSystem']['hr'] = 'Razglas';
		$szvData['translations']['soundSystem']['en'] = 'Sound system';

		$szvData['translations']['decoration']['hr'] = 'Dekoracije';
		$szvData['translations']['decoration']['en'] = 'Decoration';

		$szvData['translations']['bouquets']['hr'] = 'Buketi';
		$szvData['translations']['bouquets']['en'] = 'Bouquets';

		$szvData['translations']['lapel']['hr'] = 'Reveri';
		$szvData['translations']['lapel']['en'] = 'Lapel';

		$szvData['translations']['garter']['hr'] = 'Podvezice';
		$szvData['translations']['garter']['en'] = 'Garter';

		$szvData['translations']['balloons']['hr'] = 'Baloni ';
		$szvData['translations']['balloons']['en'] = 'Balloons ';

		$szvData['translations']['capacityPersons']['hr'] = 'Kapacitet osoba';
		$szvData['translations']['capacityPersons']['en'] = 'Persons';

		$szvData['translations']['rentAWaiter']['hr'] = 'Najam konobara';
		$szvData['translations']['rentAWaiter']['en'] = 'Rent A Waiter';

		$szvData['translations']['rentAChair']['hr'] = 'Najam stolica';
		$szvData['translations']['rentAChair']['en'] = 'Rent A Chair';

		$szvData['translations']['rentATent']['hr'] = 'Najam šatora';
		$szvData['translations']['rentATent']['en'] = 'Rent A Tent';

		$szvData['translations']['onInquiry']['hr'] = 'Na upit';
		$szvData['translations']['onInquiry']['en'] = 'On inquiry';

		$szvData['translations']['sendInquiry']['hr'] = 'Pošalji upit';
		$szvData['translations']['sendInquiry']['en'] = 'Send inquiry';

		$szvData['translations']['multiInquiry']['hr'] = 'Multi upit';
		$szvData['translations']['multiInquiry']['en'] = 'Multi inquiry';

		$szvData['translations']['enterData']['hr'] = 'Unesi podatke';
		$szvData['translations']['enterData']['en'] = 'Enter form data';
	
		$szvData['translations']['karaoke']['hr'] = 'Karaoke';
		$szvData['translations']['karaoke']['en'] = 'Karaoke';

		$szvData['translations']['photographyOtherServices']['hr'] = 'Fotografiranje & Ostale usluge';
		$szvData['translations']['photographyOtherServices']['en'] = 'Photography & Other services';

		$szvData['translations']['videoRecordingEditing']['hr'] = 'Video snimanje i montaža';
		$szvData['translations']['videoRecordingEditing']['en'] = 'Video recording and editing ';
		
		$szvData['translations']['parking']['hr'] = 'Parking';
		$szvData['translations']['parking']['en'] = 'Parking';
		
		$szvData['translations']['privateParking']['hr'] = 'Privatni parking';
		$szvData['translations']['privateParking']['en'] = 'Private parking';
		
		$szvData['translations']['freeParking']['hr'] = 'Besplatan parking';
		$szvData['translations']['freeParking']['en'] = 'Free parking';
		
		$szvData['translations']['wiFi']['hr'] = 'WiFi';
		$szvData['translations']['wiFi']['en'] = 'WiFi';
		
		$szvData['translations']['airCondition']['hr'] = 'Klimatizirano';
		$szvData['translations']['airCondition']['en'] = 'Air-conditioning';
		
		$szvData['translations']['suitableForDisabledPeople']['hr'] = 'Pogodno za osobe s invaliditetom';
		$szvData['translations']['suitableForDisabledPeople']['en'] = 'Suitable for people with disabilities';
		
		$szvData['translations']['wc']['hr'] = 'WC';
		$szvData['translations']['wc']['en'] = 'WC';
		
		$szvData['translations']['wcForDisabledPeople']['hr'] = 'WC za osobe s invaliditetom';
		$szvData['translations']['wcForDisabledPeople']['en'] = 'WC for people with disabilities';
		
		$szvData['translations']['floor']['hr'] = 'Kat';
		$szvData['translations']['floor']['en'] = 'Floor';
		
		$szvData['translations']['hasElevator']['hr'] = 'Lift';
		$szvData['translations']['hasElevator']['en'] = 'Elevator';
		
		$szvData['translations']['hasTerrace']['hr'] = 'Terasa';
		$szvData['translations']['hasTerrace']['en'] = 'Terrace';
		
		$szvData['translations']['hasView']['hr'] = 'Pogled';
		$szvData['translations']['hasView']['en'] = 'View';
		
		$szvData['translations']['smokingArea']['hr'] = 'Prostor za pušenje';
		$szvData['translations']['smokingArea']['en'] = 'Smoking Area';
		
		$szvData['translations']['childrenFriendly']['hr'] = 'Prilagođeno za djecu';
		$szvData['translations']['childrenFriendly']['en'] = 'Children friendly';
		
		$szvData['translations']['accommodationAvailable']['hr'] = 'Smještaj dostupan';
		$szvData['translations']['accommodationAvailable']['en'] = 'Accommodation available';

		$szvData['translations']['makeUp']['hr'] = 'Šminka - makeup';
		$szvData['translations']['makeUp']['en'] = 'Makeup';

		$szvData['translations']['eyelashes']['hr'] = 'Ekstenzije ili nadopuna trepavica';
		$szvData['translations']['eyelashes']['en'] = 'Eyelashes';

		$szvData['translations']['nails']['hr'] = 'Salon za nokte';
		$szvData['translations']['nails']['en'] = 'Nails';

		$szvData['translations']['maleSuits']['hr'] = 'Muška odijela';
		$szvData['translations']['maleSuits']['en'] = 'Men suits';

		$szvData['translations']['maleSuitsRental']['hr'] = 'Najam muških odijela';
		$szvData['translations']['maleSuitsRental']['en'] = 'Men suit rentals';

		$szvData['translations']['maleAccessories']['hr'] = 'Dodaci';
		$szvData['translations']['maleAccessories']['en'] = 'Accessories';

		$szvData['translations']['pool']['hr'] = 'Bazen';
		$szvData['translations']['pool']['en'] = 'Pool';

		$szvData['translations']['heatedPool']['hr'] = 'Grijani bazen';
		$szvData['translations']['heatedPool']['en'] = 'Heated pool';

		$szvData['translations']['billiards']['hr'] = 'Biljar';
		$szvData['translations']['billiards']['en'] = 'Billiards';

		$szvData['translations']['darts']['hr'] = 'Pikado';
		$szvData['translations']['darts']['en'] = 'Darts';

		$szvData['translations']['tableTennis']['hr'] = 'Stolni tenis';
		$szvData['translations']['tableTennis']['en'] = 'Table tennis';

		$szvData['translations']['tableFootball']['hr'] = 'Stolni nogomet';
		$szvData['translations']['tableFootball']['en'] = 'Table football';

		$szvData['translations']['cards']['hr'] = 'Karte';
		$szvData['translations']['cards']['en'] = 'Cards';

		$szvData['translations']['football']['hr'] = 'Nogomet';
		$szvData['translations']['football']['en'] = 'Football';

		$szvData['translations']['basketball']['hr'] = 'Basketball';
		$szvData['translations']['basketball']['en'] = 'Basketball';

		$szvData['translations']['tennis']['hr'] = 'Tenis';
		$szvData['translations']['tennis']['en'] = 'Tennis';

		$szvData['translations']['badminton']['hr'] = 'Badminton';
		$szvData['translations']['badminton']['en'] = 'Badminton';

		$szvData['translations']['boules']['hr'] = 'Balote';
		$szvData['translations']['boules']['en'] = 'Boules';

		$szvData['translations']['giftList']['hr'] = 'Lista poklona';
		$szvData['translations']['giftList']['en'] = 'Gift list';		
		
		$szvData['translations']['giftCoupon']['hr'] = 'Poklon bonovi';
		$szvData['translations']['giftCoupon']['en'] = 'GiftCoupon';

		$szvData['translations']['invitations']['hr'] = 'Izrada pozivnica';
		$szvData['translations']['invitations']['en'] = 'Invitations';

		$szvData['translations']['thankYouNotes']['hr'] = 'Izrada zahvalnica';
		$szvData['translations']['thankYouNotes']['en'] = 'Thank you notes';

		$szvData['translations']['guestLists']['hr'] = 'Izrada popisa uzvanika';
		$szvData['translations']['guestLists']['en'] = 'Guest lists';

		$szvData['translations']['lighting']['hr'] = 'Rasvjeta';
		$szvData['translations']['lighting']['en'] = 'Lighting & effects';

		$szvData['translations']['soundSystem']['hr'] = 'Razglas';
		$szvData['translations']['soundSystem']['en'] = 'Sound system';

		$szvData['translations']['effects']['hr'] = 'Efekti';
		$szvData['translations']['effects']['en'] = 'Effects';

		$szvData['translations']['liveMusic']['hr'] = 'Muzika uživo';
		$szvData['translations']['liveMusic']['en'] = 'Live music';

		$szvData['translations']['beautySalon']['hr'] = 'Salon ljepote';
		$szvData['translations']['beautySalon']['en'] = 'Beauty salon';

		$szvData['translations']['healthSalon']['hr'] = 'Salon zdravlja';
		$szvData['translations']['healthSalon']['en'] = 'Health salon';

		$szvData['translations']['firstDance']['hr'] = 'Prvi ples';
		$szvData['translations']['firstDance']['en'] = 'First dance';

		$szvData['translations']['danceSchool']['hr'] = 'Škola plesa';
		$szvData['translations']['danceSchool']['en'] = 'Dance school';

		$szvData['translations']['availableDances']['hr'] = 'Plesovi';
		$szvData['translations']['availableDances']['en'] = 'Available dances';

		$szvData['translations']['weddingCakes']['hr'] = 'Svadbene torte';
		$szvData['translations']['weddingCakes']['en'] = 'Wedding cakes';

		$szvData['translations']['cakes']['hr'] = 'Torte';
		$szvData['translations']['cakes']['en'] = 'Cakes';

		$szvData['translations']['cookies']['hr'] = 'Kolači';
		$szvData['translations']['cookies']['en'] = 'Cookies';

		$szvData['translations']['homeMadeCakesCookies']['hr'] = 'Domaće torte i kolači';
		$szvData['translations']['homeMadeCakesCookies']['en'] = 'Home made cakes and cookies';

		$szvData['translations']['accordion']['hr'] = 'Harmonika';
		$szvData['translations']['accordion']['en'] = 'Accordion';

		$szvData['translations']['trio']['hr'] = 'Trio';
		$szvData['translations']['trio']['en'] = 'Trio';

		$szvData['translations']['duo']['hr'] = 'Duo';
		$szvData['translations']['duo']['en'] = 'Duo';

		$szvData['translations']['weddingDresses']['hr'] = 'Veliki izbor vjenčanica';
		$szvData['translations']['weddingDresses']['en'] = 'Large selections of wedding dresses';

		$szvData['translations']['weddingDressesRentals']['hr'] = 'Najam vjenčanica';
		$szvData['translations']['weddingDressesRentals']['en'] = 'Wedding dresses rentals';

		$szvData['translations']['weddingDressesAccessories']['hr'] = 'Dodaci za vjenčanice i ostalo';
		$szvData['translations']['weddingDressesAccessories']['en'] = 'Accessories for wedding dresses and more';

		$szvData['translations']['wellness']['hr'] = 'Wellness';
		$szvData['translations']['wellness']['en'] = 'Wellness';

		$szvData['translations']['spa']['hr'] = 'Spa';
		$szvData['translations']['spa']['en'] = 'Spa';

		$szvData['translations']['engagementRings']['hr'] = 'Zaručničko prstenje';
		$szvData['translations']['engagementRings']['en'] = 'Engagement rings';

		$szvData['translations']['weddingRings']['hr'] = 'Vere - vjenčano prstenje';
		$szvData['translations']['weddingRings']['en'] = 'Wedding rings';

		$szvData['translations']['diamondsGemstones']['hr'] = 'Dijamanti & Drago kamenje';
		$szvData['translations']['diamondsGemstones']['en'] = 'Diamonds & Gemstones';

		$szvData['translations']['silverJewelry']['hr'] = 'Srebrni nakit';
		$szvData['translations']['silverJewelry']['en'] = 'Silver jewelry';

		$szvData['translations']['goldJewelry']['hr'] = 'Zlatni nakit';
		$szvData['translations']['goldJewelry']['en'] = 'Gold jewelry';

		$szvData['translations']['mostSearched']['hr'] = 'Najčešće tražene';
		$szvData['translations']['mostSearched']['en'] = 'Most searched';

		$szvData['translations']['other']['hr'] = 'Ostalo';
		$szvData['translations']['other']['en'] = 'Other';

		$szvData['translations']['page']['hr'] = 'Stranica';
		$szvData['translations']['page']['en'] = 'Page';

		$szvData['translations']['fromPagination']['hr'] = 'od';
		$szvData['translations']['fromPagination']['en'] = 'of';

		$szvData['translations']['from']['hr'] = 'od';
		$szvData['translations']['from']['en'] = 'from';

		$szvData['translations']['searchResultsFor']['hr'] = 'Rezultati pretrage za';
		$szvData['translations']['searchResultsFor']['en'] = 'Search results for';

		$szvData['translations']['hasBeenAddedToFavorites']['hr'] = 'se sada nalazi u favoritima :)';
		$szvData['translations']['hasBeenAddedToFavorites']['en'] = 'has been added to favorites :)';

		$szvData['translations']['hasBeenRemovedFromFavorites']['hr'] = 'se više ne nalazi u favoritima :(';
		$szvData['translations']['hasBeenRemovedFromFavorites']['en'] = 'has been removed from favorites :(';
		
		$szvData['translations']['more']['hr'] = 'Više';
		$szvData['translations']['more']['en'] = 'More';

		$szvData['translations']['findOutMore']['hr'] = 'Saznaj više';
		$szvData['translations']['findOutMore']['en'] = 'Find out more';

		$szvData['translations']['allForWedding']['hr'] = 'Sve Za Vjenčanje';
		$szvData['translations']['allForWedding']['en'] = 'All For Wedding';

		$szvData['translations']['read']['hr'] = 'Pročitaj';
		$szvData['translations']['read']['en'] = 'Read';

		$szvData['translations']['comments']['hr'] = 'komentare';
		$szvData['translations']['comments']['en'] = 'comments';

		$szvData['translations']['no']['hr'] = 'Nema';
		$szvData['translations']['no']['en'] = 'No';

		$szvData['translations']['reviews']['hr'] = 'komentara';
		$szvData['translations']['reviews']['en'] = 'reviews';

		$szvData['translations']['fieldIsRequired']['hr'] = 'Polje je obvezno.';
		$szvData['translations']['fieldIsRequired']['en'] = 'This field is required.';

		$szvData['translations']['defaultErrorMsg']['hr'] = 'Dogodila se greška. Molimo pokušajte ponovo.';
		$szvData['translations']['defaultErrorMsg']['en'] = 'An error has occurred. Please try again.';

		$szvData['translations']['enterAllRequiredFields']['hr'] = 'Greška! Molimo unesite sva obavezna polja.';
		$szvData['translations']['enterAllRequiredFields']['en'] = 'Error! Please enter all required fields.';

		$szvData['translations']['enterAllRequiredFieldsMultiInquiry']['hr'] = 'Greška! Molimo unesite sva obavezna polja. Provjerite da li ste odabrali primatelje upita.';
		$szvData['translations']['enterAllRequiredFieldsMultiInquiry']['en'] = 'Error! Please enter all required fields. Make sure you selected the recipients for the multi inquiry.';

		$szvData['translations']['durationInquiryProcessing']['hr'] = 'Pokrenuli smo slanje upita. Obrada upita može potrajati 1 - 2 minute.';
		$szvData['translations']['durationInquiryProcessing']['en'] = 'Your request is being submitted. Inquiry processing can take up to 1 - 2 minutes.';

		$szvData['translations']['mobilePhone']['hr'] = 'Mobitel';
		$szvData['translations']['mobilePhone']['en'] = 'Mobile phone';

		$szvData['translations']['enterMobilePhone']['hr'] = 'Unesi svoj broj mobitela i NAZOVI NAS ODMAH';
		$szvData['translations']['enterMobilePhone']['en'] = 'Enter your phone number and CALL US NOW';

		//$szvData['translations']['pleaseEnterMobilePhone']['hr'] = 'Molimo unesite svoj broj mobitela kako bi Vas mogli nazvati ukoliko nas ne uspijete dobiti.';
		//$szvData['translations']['pleaseEnterMobilePhone']['en'] = 'Please enter your cell phone number so we can contact you if you are unable to reach us.';
		
		$szvData['translations']['pleaseEnterMobilePhone']['hr'] = 'Hej, za prikaz kontakt telefona još nam je samo potreban tvoj broj mobitela :)';
		$szvData['translations']['pleaseEnterMobilePhone']['en'] = 'Hey, to display our contact phone number we just need your cell phone number :)';
		
		$szvData['translations']['showPhoneNumber']['hr'] = 'Prikaži kontakt telefon';
		$szvData['translations']['showPhoneNumber']['en'] = 'Show phone number';

		$szvData['translations']['callUsOnMobilePhone']['hr'] = 'Molimo nazovite nas direktno na telefon koji je prikazan ispod.';
		$szvData['translations']['callUsOnMobilePhone']['en'] = 'Please call us directly on the phone number shown below.';

		$szvData['translations']['forExample']['hr'] = 'Npr.';
		$szvData['translations']['forExample']['en'] = 'Ex.';

		$szvData['translations']['contactPerson']['hr'] = 'Kontakt osoba';
		$szvData['translations']['contactPerson']['en'] = 'Contact person';

	}
	add_action('wp', 'get_site_settings'); 

	// Customize lang menu
	function customize_lang_menu($html){

		global $szvData;

		$imgHrSrc = 'https://www.static.szv.com.hr/img/flag/hr.svg';
		$imgEnSrc = 'https://www.static.szv.com.hr/img/flag/gb.svg';

		$imgHr = '<img data-desktop-src="'.$imgHrSrc.'" data-tablet-src="'.$imgHrSrc.'" data-mobile-src="'.$imgHrSrc.'" data-width="26" data-height="26" data-ratio="1" class="lazy-load-trigger-img" alt="HR" />';
		$imgEn = '<img data-desktop-src="'.$imgEnSrc.'" data-tablet-src="'.$imgEnSrc.'" data-mobile-src="'.$imgEnSrc.'" data-width="26" data-height="26" data-ratio="1" class="lazy-load-trigger-img" alt="EN" />';

		preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $html, $matches);

		//var_dump($matches[2][0]);
		//var_dump();
		if($szvData['wpLangCode'] == 'hr'){
			$html = '<a href="'.$matches[2][0].'" title="EN">'.$imgEn.'</a>';
		}else if($szvData['wpLangCode'] == 'en'){
			$html = '<a href="'.$matches[2][0].'" title="HR">'.$imgHr.'</a>';
		}

		/*
		$html = str_replace("https://www.svezavjencanje.local/wp-content/plugins/multisite-language-switcher/flags/us.png","https://www.static.szv.com.hr/img/flag/gb.svg", $html);
		$html = str_replace("https://www.svezavjencanje.local/en/wp-content/plugins/multisite-language-switcher/flags/hr.png","https://www.static.szv.com.hr/img/flag/hr.svg", $html);
		
		$html = str_replace("https://www.test.svezavjencanje.com.hr/wp-content/plugins/multisite-language-switcher/flags/us.png","https://www.static.szv.com.hr/img/flag/gb.svg", $html);
		$html = str_replace("https://www.test.svezavjencanje.com.hr/en/wp-content/plugins/multisite-language-switcher/flags/hr.png","https://www.static.szv.com.hr/img/flag/hr.svg", $html);
		
		$html = str_replace("https://www.svezavjencanje.com.hr/wp-content/plugins/multisite-language-switcher/flags/us.png","https://www.static.szv.com.hr/img/flag/gb.svg", $html);
		$html = str_replace("https://www.svezavjencanje.com.hr/en/wp-content/plugins/multisite-language-switcher/flags/hr.png","https://www.static.szv.com.hr/img/flag/hr.svg", $html);
		*/

		return $html;
	}

		
function filter_wp_seo_get_bc_ancestors( $ancestors ) { 
    echo "123455";
    return $ancestors; 
}; 
         
// add the filter 
add_filter( 'wp_seo_get_bc_ancestors', 'filter_wp_seo_get_bc_ancestors', 10, 1 ); 

// Get primary category by post id
function getPrimaryCategoryByPostId($postId, $type = 'slug'){

	// SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
	$category = get_the_category($postId);
	//var_dump($category);
	$useCatLink = true;
	// If post has a category assigned.
	if($category){

		$category_display = '';
		$category_link = '';
		$primary_category_id = '';

		if(class_exists('WPSEO_Primary_Term')){

			// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
			$wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_id());
			$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
			$term = get_term($wpseo_primary_term);

			if(is_wp_error($term)) { 
				// Default to first category (not Yoast) if an error is returned
				$category_display = $category[0]->name;
				$category_link = get_category_link($category[0]->term_id);
				$primary_category_id = $category[0]->term_id;
				$category_slug = $category[0]->slug;
			}else{ 
				// Yoast Primary category
				$category_display = $term->name;
				$category_link = get_category_link($term->term_id);
				$primary_category_id = $term->term_id;
				$category_slug = $term->slug;
			}

		} 
		else{

			// Default, display the first category in WP's list of assigned categories
			$category_display = $category[0]->name;
			$category_link = get_category_link($category[0]->term_id);
			$primary_category_id = $category[0]->term_id;
			$category_slug = $category[0]->slug;

		}

		// Display category
		/*
		if(!empty($category_display)){
			if ($useCatLink == true && !empty($category_link)){
				echo '<span class="post-category">';
				echo '<a href="'.$category_link.'">'.htmlspecialchars($category_display).'</a>';
				echo '</span>';
			}else{
				echo '<span class="post-category">'.htmlspecialchars($category_display).'</span>';
			}
		}
		*/

		if($type == 'name'){
			$result = $category_display;
		}else if($type == 'slug'){
			$result = $category_slug;
		}else if($type == 'id'){
			$result = $primary_category_id;
		}

		return $result;

	}

	

}

// CUSTOM BREADCRUMB 
function generateBreadcrumb(){

	global $szvData;
	$breadcrumb['arr'] = [];
	$breadcrumb['arrBr'] = 0;
	$breadcrumb['ldjson'] = '';
	$breadcrumb['html'] = '';
	//var_dump($szvData);


	// DISPLAY NO BREADCRUMB
	if($szvData['isCategoryListing']){
		$qobj = get_queried_object();
		$currentCategoryName = esc_html($qobj->name);
		$term_ids = get_ancestors(get_queried_object_id(), 'category', 'taxonomy');
    foreach($term_ids as $term_id ){
        $term = get_term($term_id, 'category');
        if($term && !is_wp_error($term)){
						$categoryUrl = esc_html(esc_url(get_term_link($term)));
						$categoryName = esc_html($term->name);
						array_unshift($breadcrumb['arr'], array('url' => $categoryUrl, 'name' => $categoryName));
						$breadcrumb['arrBr']++;
        }
    }

	}
	//var_dump($breadcrumb['arr']);

	//if($breadcrumb['arrBr']){
	if(true){
		$breadcrumb['html'] .= '<div id="breadcrumb-container">';
		$breadcrumb['html'] .= '<div id="breadcrumb" class="hide-breadcrumb">';
		$breadcrumb['html'] .= '<a href="'.get_home_url().'/" class="breadcrumb-first"><span class="fa fa-home"></span></a> ';

		foreach($breadcrumb['arr'] as $breadcrumbItem) {
				$breadcrumb['html'] .= '<a href="'.$breadcrumbItem['url'].'">'.$breadcrumbItem['name'].'</a> ';
		}

		$breadcrumb['html'] .= '<span class="breadcrumb-last">'.$currentCategoryName.'</span>';
		$breadcrumb['html'] .= '<span id="breadcrumb-expand"><span class="fa fa-plus-circle"></span></span>';
		$breadcrumb['html'] .= '</div>';		
		$breadcrumb['html'] .= '</div>';
	}
	
	echo $breadcrumb['html'];
}

// CUSTOM BREADCRUMB BLOG
function generateBreadcrumbBlog(){

	global $szvData;
	$breadcrumb['arr'] = [];
	$breadcrumb['arrBr'] = 0;
	$breadcrumb['ldjson'] = '';
	$breadcrumb['html'] = '';
	//var_dump($szvData);


	// DISPLAY NO BREADCRUMB
	if($szvData['isCategoryListing']){
		$qobj = get_queried_object();
		$currentCategoryName = esc_html($qobj->name);
		$term_ids = get_ancestors(get_queried_object_id(), 'category', 'taxonomy');
    foreach($term_ids as $term_id ){
        $term = get_term($term_id, 'category');
        if($term && !is_wp_error($term)){
						$categoryUrl = esc_html(esc_url(get_term_link($term)));
						$categoryName = esc_html($term->name);
						array_unshift($breadcrumb['arr'], array('url' => $categoryUrl, 'name' => $categoryName));
						$breadcrumb['arrBr']++;
        }
    }

	}
	//var_dump($breadcrumb['arr']);

	//if($breadcrumb['arrBr']){
	if(true){
		$breadcrumb['html'] .= '<div id="breadcrumb-container" class="blog-breadcrumb">';
		$breadcrumb['html'] .= '<div id="breadcrumb" class="hide-breadcrumb">';
		$breadcrumb['html'] .= '<a href="'.get_home_url().'/" class="breadcrumb-first"><span class="fa fa-home"></span></a> ';

		$breadcrumb['html'] .= '<a href="'.get_post_type_archive_link('blog').'">Blog</a> ';

		$breadcrumb['html'] .= '<span class="breadcrumb-last">'.get_the_title().'</span>';
		$breadcrumb['html'] .= '<span id="breadcrumb-expand"><span class="fa fa-plus-circle"></span></span>';
		$breadcrumb['html'] .= '</div>';		
		$breadcrumb['html'] .= '</div>';
	}
	
	echo $breadcrumb['html'];
}

// CUSTOM BREADCRUMB SINGLE PROVIDER
function generateBreadcrumbSingleProvider(){

	global $szvData;
	$breadcrumb['arr'] = [];
	$breadcrumb['arrBr'] = 0;
	$breadcrumb['ldjson'] = '';
	$breadcrumb['html'] = '';
	//var_dump($szvData);


	// DISPLAY NO BREADCRUMB
	if($szvData['isProvider']){
		$categories = get_the_category();
		$category_primary_name = getPrimaryCategoryByPostId(get_the_ID(), 'name');
		$category_primary_id = getPrimaryCategoryByPostId(get_the_ID(), 'id');
	}
	//var_dump($breadcrumb['arr']);

	//if($breadcrumb['arrBr']){
	if(true){
		$breadcrumb['html'] .= '<div id="breadcrumb-container" class="single-provider-breadcrumb">';
		$breadcrumb['html'] .= '<div id="breadcrumb" class="hide-breadcrumb">';
		$breadcrumb['html'] .= '<a href="'.get_home_url().'/" class="breadcrumb-first"><span class="fa fa-home"></span></a> ';

		$breadcrumb['html'] .= '<a href="'.get_category_link($category_primary_id).'">'.$category_primary_name.'</a> ';

		$breadcrumb['html'] .= '<span class="breadcrumb-last">'.get_the_title().'</span>';
		$breadcrumb['html'] .= '<span id="breadcrumb-expand"><span class="fa fa-plus-circle"></span></span>';
		$breadcrumb['html'] .= '</div>';		
		$breadcrumb['html'] .= '</div>';
	}
	
	echo $breadcrumb['html'];
}

// Get all reviews for post
function getAllSingleProviderReviews($postId){

	$providerReviews = array();

	$providerReviews['allReviews'] = array();
	$providerReviews['ratingSum'] = 0;
	$providerReviews['ratingNmbr'] = 0;
	$providerReviews['avgRating'] = 0;
	
	if(have_rows('reviews_providerReviews', $postId)){

		while(have_rows('reviews_providerReviews', $postId)) : the_row();

			if(get_sub_field('providerReviewPublished') && get_sub_field('providerReviewApproved') && !get_sub_field('providerReviewMarkedAsSpam')) {

				$providerReviews['allReviews'][] = array(
				'providerReviewOrder' => get_sub_field('providerReviewOrder'),
				'providerReviewApproved' => get_sub_field('providerReviewApproved'),
				'providerReviewMarkedAsSpam' => get_sub_field('providerReviewMarkedAsSpam'),
				'providerReviewDate' => get_sub_field('providerReviewDate'),
				'providerReviewConsumationBeginDate' => get_sub_field('providerReviewConsumationBeginDate'),
				'providerReviewConsumationEndDate' => get_sub_field('providerReviewConsumationEndDate'),
				'providerReviewRaterName' => get_sub_field('providerReviewRaterName'),
				'providerReviewRaterLastName' => get_sub_field('providerReviewRaterLastName'),
				'providerReviewRaterproviderReviewLangName' => get_sub_field('providerReviewLang'),
				'providerReviewPositiveComment' => get_sub_field('providerReviewPositiveComment'),
				'providerReviewNegativeComment' => get_sub_field('providerReviewNegativeComment'),
				'providerReviewRating' => get_sub_field('providerReviewRating')
				);

				$providerReviews['ratingSum'] += get_sub_field('providerReviewRating');
				$providerReviews['ratingNmbr']++;
			
			}

		endwhile;

	}

	if($providerReviews['ratingNmbr'] > 0 && $providerReviews['ratingSum'] > 0){
		$providerReviews['avgRating'] = round(($providerReviews['ratingSum'] / $providerReviews['ratingNmbr']), 1);
	}

	//var_dump($providerReviews);

	return $providerReviews;

}


/*
// Remove hreflang
add_action( 'wp_head', 'disable_for_page', 0 );
function disable_for_page() {
  if(is_404() || is_search()) {
    add_filter( 'wpml_head_langs', '__return_empty');
  }
}
*/


// Edit hreflang
function my_msls_head_hreflang( $language ) {
	
	if('en_US' == $language) {
		$language = 'en';
	}
	
	return $language;
}
add_filter( 'msls_head_hreflang', 'my_msls_head_hreflang' );


// Check hreflang status
add_action( 'wp_head', 'check_hreflang', 1 );
function check_hreflang(){

	global $szvData;
	if($szvData['hreflangMultisiteLanguageSwitcherValid']){
		
	}else{
		exit("Multisite Language Switcher plugin update overwrote code for hreflang!");
	}

	// Code that is in /wp-content/plugins/multisite-language-switcher/includes/MslsPlugin.php

	// On top of the page
	// global $szvData;
	// $szvData['hreflangMultisiteLanguageSwitcherValid'] = true;		

	// Commented out line that prints hreflang to head
	// add_action( 'wp_head', [ $obj, 'print_alternate_links' ] );

}

// Set custom hreflang
add_action( 'wp_head', 'custom_hreflang', 2);
function custom_hreflang(){

	$options 	= lloc\Msls\MslsOptions::instance();
	$obj     = new lloc\Msls\MslsPlugin( $options );
	if(!is_search() && !is_404() && !is_paged()){
		add_action( 'wp_head', [ $obj, 'print_alternate_links' ] );
	}

}

// Remove Yoast rel prev next
add_filter( 'wpseo_next_rel_link', 'custom_remove_wpseo_next', 10 );
add_filter( 'wpseo_prev_rel_link', 'custom_remove_wpseo_prev', 10 );
function custom_remove_wpseo_next( $link ) {
	/* Make the magic happen here
	* Example below removes the rel=”next” link
	*/
	if(is_search() || is_404() || is_paged()){
		return false;
	} else { 
		return $link;
	}
}
function custom_remove_wpseo_prev( $link ) {
	/* Make the magic happen here
	* Example below removes the rel=”next” link
	*/
	if(is_search() || is_404() || is_paged()){
		return false;
	} else { 
		return $link;
	}
}

// Disabel WP Search
function disable_search( $query, $error = true ) {
  if ( is_search() && !is_admin()) {
    $query->is_search = false;
    $query->query_vars[s] = false;
    $query->query[s] = false;
    // to error
    if ( $error == true )
    $query->is_404 = true;
  }
}
add_action( 'parse_query', 'disable_search' );
add_filter( 'get_search_form', create_function( '$a', "return null;" ) );

// Yoast LDJSON customization
add_filter( 'disable_wpseo_json_ld_search', '__return_true' );
add_filter( 'wpseo_schema_needs_author', '__return_false' );

add_filter( 'wpseo_schema_article', 'change_article_author' );
function change_article_author( $data ) {
  $data['author'] = 'Sve Za Vjenčanje';
  return $data;
}

add_filter( 'wpseo_schema_article', 'yoast_modify_schema_graph_pieces' );
add_filter( 'wpseo_schema_webpage', 'yoast_modify_schema_graph_pieces' );
function yoast_modify_schema_graph_pieces( $data ) { 

	unset($data['datePublished']);
	unset($data['dateModified']);

	return $data;
}

// Set custom pagination for paginated pages
add_filter('wpseo_title', 'filter_product_wpseo_title');
function filter_product_wpseo_title($title) {

	global $wp_query;
	global $szvData;

	if(is_paged()) {
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$title = $title.' | '.$szvData['translations']['page'][$szvData['wpLangCode']].' '.$paged.' '.$szvData['translations']['fromPagination'][$szvData['wpLangCode']].' '.$wp_query->max_num_pages;
	}

  return $title;
}

// Redirect category pages that have parent
add_action('template_redirect', 'redirect_category_pages_with_parent'); 
function redirect_category_pages_with_parent(){

	if(is_archive()){

		$actualCategory = get_category( get_query_var('cat') );
		//var_dump($actualCategory);

		$qobj = get_queried_object();
		$currentCategoryName = esc_html($qobj->name);
		$term_ids = get_ancestors(get_queried_object_id(), 'category', 'taxonomy');

		$redirectUrl = WP_1_HOME;
		$parentBr = 0;
    foreach($term_ids as $term_id){
        $term = get_term($term_id, 'category');
        if($term && !is_wp_error($term)){
						$redirectUrl = esc_html(esc_url(get_term_link($term)));
						//$categoryName = esc_html($term->name);
						//echo '<br><br>categoryName: '.$categoryName;
						$parentBr++;
        }
		}
		
		//echo '<br>redirectUrl: '.$redirectUrl;

		if($parentBr > 0)
		header('Location: '.$redirectUrl , true, 302);

	}

}

// Check if is mobile phone number
function isMobilePhoneNumber($phoneNmbr){
	$response = [];

	$phoneNmbr = str_replace(" ", "", $phoneNmbr);
	$phoneNmbr = str_replace("-", "", $phoneNmbr);
	
	$phoneNmbr = str_replace("00385", "385", $phoneNmbr);
	$phoneNmbr = str_replace("+385", "385", $phoneNmbr);

	if($phoneNmbr[0] == "0" && $phoneNmbr[1] == "9" && ($phoneNmbr[2] == "1" || $phoneNmbr[2] == "2" || $phoneNmbr[2] == "5" || $phoneNmbr[2] == "7" || $phoneNmbr[2] == "8" || $phoneNmbr[2] == "9")){
		$phoneNmbr = "385".$phoneNmbr;
	}
	
	$countryPrefix = $phoneNmbr[0].$phoneNmbr[1].$phoneNmbr[2];
	$phoneNmbr = str_replace("3850", "385", $phoneNmbr);
	
	if(strpos($phoneNmbr, "38591") !== false || strpos($phoneNmbr, "38592") !== false || strpos($phoneNmbr, "38595") !== false || strpos($phoneNmbr, "38597") !== false || strpos($phoneNmbr, "38598") !== false || strpos($phoneNmbr, "38599") !== false){
		$response["isMobilePhone"] = true;
		$response["phoneNumber"] = '+'.$phoneNmbr;
	}else{
		$response["isMobilePhone"] = false;
		$response["phoneNumber"] = "";
	}

	return $response;
}

// Get provider contacts
function getProviderContacts($postId){

	$providerContact = [];
	$providerContact['sendToEmail'] = null;
	$providerContact['callPhone'] = null;
	$providerContact['callMobilePhone'] = null;
	$providerContact['email'] = null;
	$providerContact['phone'] = null;
	$providerContact['mobilePhone'] = null;
	$providerContact['showContactPerson'] = null;
	$providerContact['contactPersonName'] = null;
	$providerContact['contactPersonSurname'] = null;
	$providerContact['contactPersonEmail'] = null;
	$providerContact['contactPersonPhone'] = null;

	$tmp = get_field('providerEmail', $postId);
	if($tmp)
	$providerContact['email'] = get_field('providerEmail', $postId);

	$tmp = get_field('providerPhone', $postId);
	if($tmp){
		$providerContact['phone'] = get_field('providerPhone', $postId);
		$providerContact['callPhone'] = get_field('providerPhone', $postId);
	}

	$tmp = get_field('providerMobilePhone', $postId);
	if($tmp)
	$providerContact['mobilePhone'] = get_field('providerMobilePhone', $postId);


	$tmp = get_field('providerShowContactPerson', $postId);
	if($tmp)
	$providerContact['showContactPerson'] = true;
	else
	$providerContact['showContactPerson'] = false;

	$tmp = get_field('providerContactPersonName', $postId);
	if($tmp)
	$providerContact['contactPersonName'] = get_field('providerContactPersonName', $postId);

	$tmp = get_field('providerContactPersonSurname', $postId);
	if($tmp)
	$providerContact['contactPersonSurname'] = get_field('providerContactPersonSurname', $postId);

	$tmp = get_field('providerContactPersonEmail', $postId);
	if($tmp)
	$providerContact['contactPersonEmail'] = get_field('providerContactPersonEmail', $postId);

	$tmp = get_field('providerContactPersonPhone', $postId);
	if($tmp)
	$providerContact['contactPersonPhone'] = get_field('providerContactPersonPhone', $postId);


	//if((isset($providerContact['showContactPerson']) && $providerContact['showContactPerson']) && isset($providerContact['providerContactPersonEmail'])){
	if(isset($providerContact['providerContactPersonEmail'])){	
		$providerContact['sendToEmail'] = $providerContact['providerContactPersonEmail'];
	}else {
		$providerContact['sendToEmail'] = $providerContact['email'];
	}

	$contactPersonPhoneIsMobile = isMobilePhoneNumber($providerContact['contactPersonPhone']);
	$mobilePhoneIsMobile = isMobilePhoneNumber($providerContact['mobilePhone']);

	if($contactPersonPhoneIsMobile["isMobilePhone"]){
		$providerContact['callMobilePhone'] = $contactPersonPhoneIsMobile["phoneNumber"];
	}else if($mobilePhoneIsMobile["isMobilePhone"]){
		$providerContact['callMobilePhone'] = $mobilePhoneIsMobile["phoneNumber"];
	}

	return $providerContact;														

}


// Force img environment urls
function force_img_environment_urls($imageUrl) {

	/*
	if(WP_SZV_ENVIRONMENT == 'development'){ // production testing 
	}
	*/

	//var_dump(WP_SZV_ENVIRONMENT);
	//var_dump(WP_SZV_FULLPATH);

	$imageUrl = str_replace('"','', $imageUrl);
	$imageUrl = str_replace('=','', $imageUrl);
	$imageUrl = str_replace('src','', $imageUrl);

	$finalImgUrl = $imageUrl;

	// CHECK IMG HAS SZV URLS 
	if(strpos($imageUrl, 'svezavjencanje.') !== false) {
		

		$breakImgUrl = parse_url($imageUrl);
		$imgPath = $breakImgUrl['path'];

		$finalImgUrl = WP_SZV_FULLPATH.$imgPath;

	}

	return $finalImgUrl;
}

// Lazy load images
function filter_lazyload($content) {
 
return preg_replace_callback('/(<\s*img[^>]+)(src\s*=\s*"[^"]+")([^>]+>)/i', 'preg_lazyload', $content);
	
}
add_filter('the_content', 'filter_lazyload');

function preg_lazyload($img_match) {

	/*
	$img_replace = $img_match[1] . ' data-original' . substr($img_match[2], 3) . $img_match[3];

	$img_replace = preg_replace('/class\s*=\s*"/i', 'class="lazy ', $img_replace);

	$img_replace .= '<noscript>' . $img_match[0] . '</noscript>';
	*/

	//var_dump($img_match);
	// class="lazy-load-trigger-img"	

	if(is_page()){
		$class = ' page-lazy-load-img ';
	}else{
		$post_type = get_post_type();
    if($post_type == 'blog'){
			$class = ' blog-lazy-load-img ';
		}
	}

	$finalImgUrl = force_img_environment_urls($img_match[2]);

	$img_replace = '<span class="wp-lazy-load-img-parent"><img class="'.$class.' lazy-load-trigger-img" data-width="600" data-height="400" data-ratio="1.5" data-desktop-src="'.$finalImgUrl.'"	data-tablet-src="'.$finalImgUrl.'"	data-mobile-src="'.$finalImgUrl.'" /></span>';

	$img_replace .= '<noscript><img '.$finalImgUrl.' alt="'.get_the_title().'" /></noscript>';

	return $img_replace;
}

// Change excerpt [...] 
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Get excerpt with max chars
function get_excerpt($maxChars){
	//$excerpt = strip_tags(get_the_content());
	$excerpt = strip_tags(get_the_excerpt());
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $maxChars);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	//$excerpt = $excerpt.'... <a href="'.get_the_permalink().'">more</a>';
	$excerpt = $excerpt.'...';
	return $excerpt;
}

// Get n words from string
function truncWords($phrase, $max_words) {
	$phrase_array = explode(' ',$phrase);
	if(count($phrase_array) > $max_words && $max_words > 0)
		 $phrase = implode(' ',array_slice($phrase_array, 0, $max_words));
	return $phrase;
}

// Get provider images
function getProviderImagesByPostId($postId){
	$images = array();
	if(have_rows('images_providerImages', $postId)){
		while(have_rows('images_providerImages', $postId)) : the_row() ?><?php

				$img = array();
				$img['providerImgOriginalUrl'] = get_sub_field('providerImgOriginalUrl');
				$img['providerImgDesktopUrl'] = get_sub_field('providerImgDesktopUrl');
				$img['providerImgTabletUrl'] = get_sub_field('providerImgTabletUrl');
				$img['providerImgMobileUrl'] = get_sub_field('providerImgMobileUrl');
				$img['providerImgWidth'] = get_sub_field('providerImgWidth');
				$img['providerImgHeight'] = get_sub_field('providerImgHeight');
				$img['providerImgAlt'] = get_sub_field('providerImgAlt');
				$img['providerImgDesc'] = get_sub_field('providerImgDesc');
				$img['providerImgOriginalUrl'] = get_sub_field('providerImgOriginalUrl');
				$img['providerImgTargetContainer'] = get_sub_field('providerImgTargetContainer');
				$img['providerImgRatio'] = get_sub_field('providerImgRatio');
				$img['providerImgClass'] = get_sub_field('providerImgClass');
				$img['providerImgQueryStringVersion'] = get_sub_field('providerImgQueryStringVersion');

				$images[] = $img;

			?><?php endwhile; ?><?php	
	}
	return $images;
}

// Change Yoast social image
add_filter( 'wpseo_opengraph_image', 'change_image' );
function change_image($image) {
 
	if(is_single()){
		$postImages = getProviderImagesByPostId(get_the_id());
		$image = $postImages[0]['providerImgOriginalUrl'];
		//var_dump($image);
	}

	/*
	if( $image == 'http://url_for_image_to_change' ) {
		$image = 'http://url_to_our_new_image';
	}
	*/
 
 return $image;
}


// Get hashed string
function get_hashed_string($plain_text){
	return password_hash($plain_text, PASSWORD_DEFAULT);
}

// Verify hashed string
function verify_hashed_string($plain_text, $hashed_text){
	return password_verify($plain_text, $hashed_text) ? true : false;
}
