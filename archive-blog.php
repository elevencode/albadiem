<?php

get_header(); 

get_template_part( 'inc/layouts/header' ); ?>

<?php

    global $szvData;

?>


	<!--[if lt IE 8]>

		<p class="browserupgrade">Koristite <strong>zastarjeli</strong> preglednik. Molimo <a href="http://browsehappy.com/">nadogradite preglednik</a> kako biste mogli koristiti stranicu.</p>

	<![endif]-->



	<!-- Wrapper -->

	<div id="wrapper">

    <?php require_once 'inc/advertising/top-bar.php'; ?>
		
		<?php require_once 'inc/layouts/banner-blog-listing.php'; ?>

            <section id="category-main-listing-container" class="category-shuffle-listing category-blog-listing">
                <div id="category-shuffle-listing">
                    <div id="category-main-listing">

                            <div class="col-md-12">

                                <?php   
                                if ( have_posts() ) : 
                                while ( have_posts() ) : the_post(); ?>

                                <div id="shuffle-<?php echo $i; ?>" class="shuffle category-main-listing-box">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-lg-5 col-xl-5">
                                            <div class="blog-main-listing-img">
                                                <div class="blog-listing-slider-container pos-relative">

                                                    <?php

                                                        $newBlogPostImg = get_the_post_thumbnail_url();

                                                    ?>

                                                    <div class="new-blog-posts-img follow-href lazy-load-trigger-bg-img" 
                                                    data-href="<?php the_permalink(); ?>"
                                                    data-desktop-src="<?php echo $newBlogPostImg; ?>"  
                                                    data-tablet-src="<?php echo $newBlogPostImg; ?>" 
                                                    data-mobile-src="<?php echo $newBlogPostImg; ?>">
                                                    </div>

                                                    <noscript>
                                                        <img src="<?php echo $newBlogPostImg; ?>" alt="<?php the_title(); ?>" />
                                                    </noscript>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-lg-7 col-xl-7">
                                                
                                            <div class="category-main-listing-heading">	
                                                <h3><a href="<?php echo the_permalink(); ?>"><?php echo $i; ?> <?php the_title(); ?></a></h3>
                                            </div>
                                            
                        
                                            <div class="category-main-listing-content">
                                                <div class="category-main-listing-desc">
                                                    <?php
                                                        //$blogPostExcerpt = strip_tags(get_the_excerpt());
                                                        $blogPostExcerpt = get_excerpt(200);
                                                    ?>
                                                    <?php echo $blogPostExcerpt; ?>
                                                </div>
                                            </div>
                                        
                                        
                                            <div class="category-main-listing-cta">
                                                <div class="category-main-listing-cta-box">
                                                    <div class="row no-gutters">
                                                        <div class="col-2">
                                                        </div>
                                                        <div class="col-10 text-right">
                                                            <span class="category-main-listing-cta-info">
                                                                <a href="<?php echo the_permalink(); ?>"><?php echo $szvData['translations']['findOutMore'][$szvData['wpLangCode']]; ?></a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                        </div>
                                    </div>
                                </div>
                                 <?php   
                                endwhile; 
                             endif; 
                            
                             ?>
                          </div>
                             <!-- Pagination -->

                             <div class="pagination-container">
             
                                 <div class="container">
             
                                     <div class="row">			
             
                                         <div class="pagination">
                                     
             
                                             <div class="pagination-box">
             
                                                 <?php 
                                                     
                                                     if(function_exists(albadiem_pagination())):
                                                         albadiem_pagination($wp_query->max_num_pages, 2, $paged);
                                                     endif;
                                                         wp_reset_query();
                                                     
                                                 ?>
                                                     
                                             </div>
             
                                         </div>
             
                                     </div>
             
                                 </div>
             
                             </div>
             
                             <!-- Pagination / End -->
                        </div>
                    </div>
            </section>
            <!-- Section Category Main Listing / End -->
            </div>
      
		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>

<?php get_template_part( 'inc/layouts/footer' );  ?>



