<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package albadiem
 */
 
global $szvData;

$szvData['hasSearch'] = true;
$szvData['isCategoryListing'] = false;
$szvData['isProvider'] = false; 

get_header();

get_template_part( 'inc/layouts/header' ); ?>

<?php require_once 'inc/search/search-home.php'; ?>
<?php require_once 'inc/why-us.php'; ?>


    <main id="content" role="main" class="homepage">

			<div id="homepage-intro" class="container-fluid">

				<div class="row">

					<div class="home-text-intro">

						<h2><?php the_field('h1'); ?></h2>

						<hr />

					    <?php the_field('desc'); ?>

					</div>	

				</div>

			</div>
			
			<?php require_once 'inc/advertising/home-728-210.php'; ?>

			<?php require_once 'inc/home-categories.php'; ?>

			<?php require_once 'inc/weddings-country-offer.php'; ?>

			<?php require_once 'inc/advertising/home-1140-250.php'; ?>

			<?php require_once 'inc/home-best-reviews.php'; ?>

			<?php require_once 'inc/new-published-blog-posts-and-providers.php'; ?>		

		</main>

		<!-- Main Content / End -->

<?php
get_template_part( 'inc/partners' ); 
get_template_part( 'inc/cta/cta-create-profile-fw' ); 
get_template_part( 'inc/layouts/footer' );