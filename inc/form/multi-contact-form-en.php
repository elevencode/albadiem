<!-- Contact form

================================================== -->

<form id="contactInquiryForm" name="contactInquiryForm" method="post" class="contactForm">

	<div class="row">

		<div class="col-md-6">

			<div class="validatio-holder">
				<p><label for="name">Name *:</label></p>
				<input name="CIFname" id="CIFname" class="validatio required" type="text" placeholder="Ex.: Ivana" data-placeholder="Ex.: Ivana" data-valid-type="text">
				<div class="validatio-messages"></div>
			</div>

			<div class="validatio-holder">
				<p><label for="surname">Last name *:</label></p>
				<input name="CIFsurname" id="CIFsurname" class="validatio required" type="text" placeholder="Ex.: Horvat" data-placeholder="Ex.: Horvat" data-valid-type="text">
				<div class="validatio-messages"></div>
			</div>

			<div class="validatio-holder">
				<p><label for="phone">Mobile Phone *:</label></p>
				<input name="CIFphone" id="CIFphone" class="validatio required custom-tel" type="tel" placeholder="Ex.: +385 98 100 200" data-placeholder="Ex.: +385 98 100 200" data-valid-type="text" data-number-type="MOBILE">
				<div id="validation-message-mobile_phone" class="validatio-messages"></div>
			</div>

		</div>

		<div class="col-md-6">

			<div class="validatio-holder">
				<p><label for="CIFemail">E-mail *:</label></p>
				<input name="CIFemail" id="CIFemail" class="validatio required" type="text" placeholder="Ex.: ivana.horvat@example.com" data-placeholder="Ex.: ivana.horvat@example.com" data-valid-type="email">
				<div class="validatio-messages"></div>

			</div>

			<div class="validatio-holder">
				<p><label for="CIFnote">Message *:</label></p>
				<textarea name="CIFnote" id="CIFnote" class="validatio required" placeholder="Enter a message" data-placeholder="Enter a message" data-valid-type="textarea"></textarea> 
				<div class="validatio-messages"></div>
			</div>
		</div>
	</div>

	<div class="row no-gutters">

		<div class="col-12">

			<div id="CIFacceptTermsHolder" class="validatio-holder light">
				<p class="no-padding-top"><input name="CIFacceptTerms" id="CIFacceptTerms" class="checkbox validatio required" data-valid-type="checkbox" type="checkbox" />
				<label for="CIFacceptTerms"></label> <span class="formLabelText">I have read and accept <a id="showTOS" href="<?php echo get_page_link(532); ?>">terms and conditions</a> and <a id="showPP" href="<?php echo get_page_link(536); ?>">privacy policy</a>. *</span></p>
				<div class="validatio-messages"></div>
			</div>

			<div id="CIFacceptGDPRHolder" class="validatio-holder light">
				<p class="no-margin-top"><input name="CIFacceptGDPR" id="CIFacceptGDPR" class="checkbox validatio" data-valid-type="checkbox" type="checkbox" />
				<label for="CIFacceptGDPR"></label> <span class="formLabelText">Want to receive exclusive special offers and news from time to time?
				Click the consent for your data being stored in accordance with
				<span id="showPP" href="<?php echo get_page_link(536); ?>">privacy policy</span>. 
				We promise to send ultra interesting special offers, great stuff to help you with your organization, new trends ...
				And we won't fill your inbox with boring and irrelevant emails :)</span></p>
				<div class="validatio-messages"></div>
			</div>		

			<button name="sendMultiContactInquiryForm" id="sendMultiContactInquiryForm" class="btn" type="button">Send info inquiry</button>

		</div>

	</div>

</form>	

<!-- Contact Form / End -->