<!-- Contact form

================================================== -->

<form id="contactInquiryForm" name="contactInquiryForm" method="post" class="contactForm">

	

	<div class="row">

		<div class="col-md-6">

			<div class="validatio-holder">

				<p><label for="name">Ime *:</label></p>

				<input name="CIFname" id="CIFname" class="validatio required" type="text" placeholder="Unesi ime" data-placeholder="Unesi ime" data-valid-type="text">

				<div class="validatio-messages"></div>

			</div>

			

			<div class="validatio-holder">

				<p><label for="surname">Prezime *:</label></p>

				<input name="CIFsurname" id="CIFsurname" class="validatio required" type="text" placeholder="Unesi prezime" data-placeholder="Unesi prezime" data-valid-type="text">

				<div class="validatio-messages"></div>

			</div>

			

			<div class="validatio-holder">

				<p><label for="phone">Telefon *:</label></p>

				<input name="CIFphone" id="CIFphone" class="validatio required" type="text" placeholder="Unesi broj" data-placeholder="Unesi ime" data-valid-type="text">

				<div class="validatio-messages"></div>

			</div>

			

		</div>

		<div class="col-md-6">

			

			<div class="validatio-holder">

				<p><label for="CIFemail">E-mail *:</label></p>

				<input name="CIFemail" id="CIFemail" class="validatio required" type="text" placeholder="Unesi e-mail" data-placeholder="Unesi e-mail" data-valid-type="email">

				<div class="validatio-messages"></div>

			</div>



			<div class="validatio-holder">

				<p><label for="CIFnote">Poruka *:</label></p>

				<textarea name="CIFnote" id="CIFnote" class="validatio required" placeholder="Unesi poruku" data-placeholder="Unesi poruku" data-valid-type="textarea"></textarea> 

				<div class="validatio-messages"></div>

			</div>

		</div>

	</div>

	

	<div class="row no-gutters">

		<div class="col-12">

			<div id="CIFacceptTermsHolder" class="validatio-holder light">

				<p class="no-padding-top"><input name="CIFacceptTerms" id="CIFacceptTerms" class="checkbox validatio required" data-valid-type="checkbox" type="checkbox" />

				<label for="CIFacceptTerms"></label> <span class="formLabelText">Pročitao/la sam i prihvaćam <span id="showTOS" data-href="">Uvjete korištenja</span> i <span id="showPP" data-href="">Izjavu o privatnosti</span>. *</span></p>

				<div class="validatio-messages"></div>

			</div>

			

			<div id="CIFacceptGDPRHolder" class="validatio-holder light">

				<p class="no-margin-top"><input name="CIFacceptGDPR" id="CIFacceptGDPR" class="checkbox validatio" data-valid-type="checkbox" type="checkbox" />

				<label for="CIFacceptGDPR"></label> <span class="formLabelText">Želiš povremeno primati ekskluzivne posebne ponude i novosti? Klikni i daj privolu da se tvoji podaci pohranjuju u skladu sa <span id="showPP" data-href="">Izjavom o privatnosti</span>. Obećavamo da ćemo slati ultra zanimljive posebne ponude, novosti koje će ti pomoći pri organizaciji, nove trendove... I da, nećemo ti napuniti inbox sa dosadnim i nebitnim mailovima :)</span></p>

				<div class="validatio-messages"></div>

			</div>		

			<button name="sendContactInquiryForm" id="sendContactInquiryForm" class="btn" type="button">Pošalji informativni upit</button>

		</div>

	</div>

	

</form>	

<!-- Contact Form / End -->