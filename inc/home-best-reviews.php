<!-- Section Top 4 Latest Reviews

================================================== -->

<section id="best-reviews-container">

	<div class="container-fluid">

		<div class="container">

			<h2 class="text-center"><?php _e('Klijenti u Hrvatskoj posebno preporučuju', 'albadiem'); ?></h2>

			<hr />

			<div id="best-reviews">

				<div class="row justify-content-md-center">

					<?php 

						$args = array(
              'post_status' => array('publish'),
							'post_type'					=> 'post',
							'posts_per_page'		=> 3,
							'meta_key'					=> 'latest_review_date',
							'orderby'						=> 'meta_value',
							'order'							=> 'DESC'
							);	

							$the_query = new WP_Query($args);
								if($the_query->have_posts()):
									while($the_query->have_posts()):
										$the_query->the_post();

										if(have_rows('reviews', get_the_ID())):
											while(have_rows('reviews')) : the_row();
												if(have_rows('providerReviews')):
													while(have_rows('providerReviews')) : the_row();
						
													$providerReviewOrderLatest = get_sub_field('providerReviewOrder');
													$providerReviewApprovedLatest = get_sub_field('providerReviewApproved');
													$providerReviewMarkedAsSpamLatest = get_sub_field('providerReviewMarkedAsSpam');

													$tmp_dt = new DateTime(get_sub_field('providerReviewDate'));
													$providerReviewDateLatest = $tmp_dt->format("d.m.Y.");	

													$providerReviewConsumationBeginDateLatest = get_sub_field('providerReviewConsumationBeginDate');
													$providerReviewConsumationEndDateLatest = get_sub_field('providerReviewConsumationEndDate');
													$providerReviewRaterNameLatest = ucwords(mb_strtolower(get_sub_field('providerReviewRaterName')))." ";
													$providerReviewRaterLastNameLatest = mb_strtoupper(mb_substr(get_sub_field('providerReviewRaterLastName'),0,1)).". "; 
													$providerReviewLang = get_sub_field('providerReviewLang');

													$length = strlen(get_sub_field('providerReviewPositiveComment'));
													/*
													$providerReviewPositiveCommentLatestFull = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 100, $length - 100));
													$providerReviewNegativeCommentLatestFull = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 100, $length - 100));

													$providerReviewPositiveCommentLatestPartial = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 0, 100));
													$providerReviewNegativeCommentLatestPartial = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 0, 100));
													*/
													$providerReviewPositiveCommentLatestFull = trim(get_sub_field('providerReviewPositiveComment'));
													$providerReviewNegativeCommentLatestFull = trim(get_sub_field('providerReviewNegativeComment'));
								
													$providerReviewPositiveCommentLatestPartial = truncWords(trim(get_sub_field('providerReviewPositiveComment')), 20);
													$providerReviewNegativeCommentLatestPartial = truncWords(trim(get_sub_field('providerReviewNegativeComment')), 20);
													
													$providerReviewRatingLatest = get_sub_field('providerReviewRating');


													if(get_sub_field('providerReviewPublished') 
													&& get_sub_field('providerReviewApproved')
													&& !get_sub_field('providerReviewMarkedAsSpam')):
								
												?>
							

													<div class="review-container review-container-item">

														<div class="review">

															<div class="review-testimonial">

																<div class="review-testimonial-pos">

																	<span class="review-testimonial-partial">

																		<?php echo $providerReviewPositiveCommentLatestPartial; ?>

																	</span>

																	<span class="3dots">...</span>

																	<span class="review-testimonial-full hide">

																		<?php echo $providerReviewPositiveCommentLatestFull; ?>

																	</span>

																</div>

																<div class="review-testimonial-neg">

																	<span class="review-testimonial-partial">

																		<?php echo $providerReviewNegativeCommentLatestPartial; ?>

																	</span>

																	<span class="3dots">...</span>

																	<span class="review-testimonial-full hide">

																		<?php echo $providerReviewNegativeCommentLatestFull; ?>

																	</span>

																</div>

																<div class="review-date"><?php echo $providerReviewDateLatest; ?></div>

																<div class="review-expand-full"><span class="review-expand-full-trigger"><?php _e('više', 'albadiem'); ?> <span class="fa fa-expand"></span></span></div>

																

															</div>

															<div class="reviewer-provider-box">

																<div class="rating-box pos-relative text-center">

																	<span class="center-v-h"><span class="rating"><?php echo $providerReviewRatingLatest; ?></span> <small>/5</small></span>

																</div>

																<div class="reviewer-provider">

																	<div class="reviewer"><?php echo $providerReviewRaterNameLatest . ' ' . $providerReviewRaterLastNameLatest; ?></div>
																	
																	<div class="provider"><a href="<?php the_permalink(); ?>"><?php echo get_field('shortName'); ?></a></div>
																	
																</div>

																<div class="cf"></div>

															</div>

														</div>

													</div>
						
													<?php
												endif;
												break;
											endwhile;
										endif;
									endwhile;
								endif;
							endwhile;
						wp_reset_postdata();
					endif;
					?>
				</div>


			</div>

		</div>

	</div>

</section>

<!-- Section Top 4 Latest Reviews / End -->