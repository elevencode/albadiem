<!-- Section Category Main Listing

================================================== -->



<?php

//var_dump($qobj);

?>

<section id="category-main-listing-container" class="category-shuffle-listing">

	<div id="category-shuffle-listing">

		<div>
			
			<div id="category-main-listing">
				

				<h2 class="text-center">

					<?php

						global $wp_query;
						global $szvData;

						$categoryListingTitle = $szvData['translations']['searchResultsFor'][$szvData['wpLangCode']].': <strong>'.$qobj->name.'</strong>';

						if(is_paged()) {
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$categoryListingTitle = $szvData['translations']['searchResultsFor'][$szvData['wpLangCode']].': <strong>'.$qobj->name.'</strong><br /><small>('.strtolower ($szvData['translations']['page'][$szvData['wpLangCode']]).' '.$paged.' '.$szvData['translations']['fromPagination'][$szvData['wpLangCode']].' '.$wp_query->max_num_pages.')</small>';
						}

						echo $categoryListingTitle;
					
					?>

				</h2>

				<hr />

				<?php 
						
					//var_dump($qobj);
					//var_dump($qobj_id);
					
				?>

				<?php 

					$br = 1;

					// Main category query
					$paged = get_query_var('paged');
					$args = array(
            'post_status' => array('publish'),
						'post_type' => 'post',
						'posts_per_page' => 10,
						'post__not_in' => $postIdsToAdvertise,
						'paged' => $paged,
						'tax_query' => array(
							array(
								'taxonomy' => $qobj->taxonomy,
								'terms' => array($qobj->term_id), //$qobj->name,
								'field' => 'term_id', //slug
								'include_children' => false,
										'operator' => 'AND'
								)
							)
						);

						// $wp_query = new WP_Query($args);
						query_posts( $args );
						
						//var_dump($args);
					
						// if($wp_query->have_posts()):
						// 	while($wp_query->have_posts()):
						// 		$wp_query->the_post();
						while ( have_posts() ) : the_post();
									// Query check if provider is published and approved in which case it will show in main query
									$providerPublished = get_field('providerPublished');
									$providerApproved = get_field('providerApproved');
									if($providerPublished && $providerApproved): ?>

									<div id="shuffle-<?php echo $br; ?>" class="shuffle category-main-listing-box">

									<?php 

										$br++;

										$online_szv_user_id =  get_field('online_szv_user_id');
										$online_szv_service_id =  get_field('online_szv_service_id');
										$online_szv_service_lang_id =  get_field('online_szv_service_lang_id');

									?>

									<div class="row no-gutters">

										<div class="col-md-5 col-lg-5 col-xl-5">

											<div class="category-main-listing-img">

												<div class="category-listing-slider-container pos-relative">

													<div id="category-listing-slider-<?php echo get_the_ID(); ?>" class="category-listing-slider carousel pos-relative lazy-load-slider">

														<?php 

															$imgNoScriptBr = 0;
															$imgNoScript = '';

														// Provider images carousel
															if(have_rows('images')):
																while(have_rows('images')) : the_row();
																	if(get_sub_field('showProviderImages')):
																		if(have_rows('providerImages')):
																			while(have_rows('providerImages')) : the_row();

																				$imgNoScriptBr++;
																				
																				$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
																				$providerImgDesktopUrl = get_sub_field('providerImgDesktopUrl');
																				$providerImgTabletUrl = get_sub_field('providerImgTabletUrl');
																				$providerImgMobileUrl = get_sub_field('providerImgMobileUrl');
																				$providerImgWidth = get_sub_field('providerImgWidth');
																				$providerImgHeight = get_sub_field('providerImgHeight');
																				$providerImgAlt = get_sub_field('providerImgAlt');
																				$providerImgDesc = get_sub_field('providerImgDesc');
																				$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
																				$providerImgTargetContainer = get_sub_field('providerImgTargetContainer');
																				$providerImgRatio = get_sub_field('providerImgRatio');
																				$providerImgClass = get_sub_field('providerImgClass');
																				$providerImgQueryStringVersion = get_sub_field('providerImgQueryStringVersion');

																				//var_dump($providerImgWidth);
																				//var_dump($providerImgHeight);

																				$providerImgRatioHeight400 = round(($providerImgRatio * 400), 0);

																				/*
																				echo "<br><br>providerImgWidth: ".$providerImgWidth;
																				echo "<br>providerImgHeight: ".$providerImgHeight;
																				echo "<br>providerImgRatio: ".$providerImgRatio;
																				echo "<br>providerImgRatioHeight400: ".$providerImgRatioHeight400;
																				*/
																			
																				//var_dump($providerImgRatioHeight400);

																					if(get_sub_field('providerImgPublished') && $providerImgRatio < 1.6): ?>
																			
																							<div class="carousel-cell" data-flickity-bg-lazyload="<?php echo $providerImgMobileUrl; ?>" style="width:<?php echo $providerImgRatioHeight400; ?>px; height:400px;"></div>

																							<?php if($imgNoScriptBr == 1): ?>
																								<?php $imgNoScript = '<noscript><img src="'.$providerImgDesktopUrl.'" alt="'.get_the_title().'" /></noscript>'; ?>
																							<?php endif; ?>

															<?php 
																					endif;
																			endwhile;
																		endif;
																	endif;
																endwhile;
															endif;
															?>

															

													</div>

													<?php
														echo $imgNoScript; 
													?>

													<div class="category-listing-logo-box">

														<?php 

															// Main logo display
															if( have_rows('logo') ):
																while( have_rows('logo') ): the_row(); 
																	
																	$providerLogoOriginalUrl = get_sub_field('providerLogoOriginalUrl');
																	$providerLogoOriginalDesktopUrl = get_sub_field('providerLogoOriginalDesktopUrl');
																	$providerLogoOriginalTabletUrl = get_sub_field('providerLogoOriginalTabletUrl');
																	$providerLogoOriginalMobileUrl = get_sub_field('providerLogoOriginalMobileUrl');
																	$providerLogoAlt = get_sub_field('providerLogoAlt');
																	$providerLogoDesc = get_sub_field('providerLogoDesc');
																	$providerLogoWidth = get_sub_field('providerLogoWidth');
																	$providerLogoHeight = get_sub_field('providerLogoHeight');
																	$providerLogoRatio = get_sub_field('providerLogoRatio');
																	$providerLogoClass = get_sub_field('providerLogoClass');
														
														?>

																	<div class="category-listing-logo follow-href lazy-load-trigger-bg-img" 
																	data-href="<?php the_permalink(); ?>"
																	data-desktop-src="<?php echo $providerLogoOriginalDesktopUrl; ?>"  
																	data-tablet-src="<?php echo $providerLogoOriginalTabletUrl; ?>" 
																	data-mobile-src="<?php echo $providerLogoOriginalMobileUrl; ?>"></div>

																	<noscript>
																		<img src="<?php echo $providerLogoOriginalDesktopUrl; ?>" alt="<?php the_title(); ?> Logo" />
																	</noscript>
															
														<?php 
																endwhile;
															endif;
														?>
													</div>	

													<?php 
														//if(have_rows('avarageRatingGroup')):
														//	while(have_rows('avarageRatingGroup')): the_row();
														//		$avarageRating = get_sub_field('avarageRating');
														//		$totalNumberReviews = get_sub_field('totalNumberReviews');
														//		$avarageRatingLink = get_sub_field('avarageRatingLink');
													?>
														
													<?php
														//	endwhile;
														// endif;
													?>

													<?php
														$providerReviews = getAllSingleProviderReviews(get_the_id());
													?>

													<?php if($providerReviews['ratingNmbr'] > 0): ?>
													<div class="cat-listing-avg-rating-box">

														<div class="cat-listing-avg-rating">

															<a href="<?php the_permalink(); ?>#reviews">
																<span class="rating">
																	<?php echo $providerReviews['avgRating'] ?>
																	</span> 
																	<small>/5</small>&nbsp;&nbsp;
																<?php _e('Pogledaj sve komentare', 'albadiem') ?> 
																<?php echo '('.$providerReviews['ratingNmbr'].')'; ?>
															</a>
															
														</div>

													</div>
													<?php endif; ?>


													<?php if(false): ?>
													<div class="badges">

														<?php
															
															// If have special offers in single provider show the special offer badge
															if(have_rows('specialOffers')):
																while( have_rows('specialOffers') ): the_row(); 
																	if(get_sub_field('showProviderSpecialOffers')): 
														?>
																		
																		<div class="discount-badge">
																			<a href="#discounts">% 
																				<?php _e('Akcija', 'albadiem'); ?>
																			</a>
																		</div>
														<?php 
																	endif; 
																endwhile;
															endif;
														?>

													</div>
														<?php endif; ?>

												</div>

											</div>

										</div>

										<div class="col-md-7 col-lg-7 col-xl-7">

											<div class="category-main-listing-heading">	
												
												<?php $shortName = get_field('shortName'); ?>

												<div class="add-to-favorites">
													<div class="add-to-favorites-trigger" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortName; ?>">												
													</div>
													<span class="fa fa-plus favorites-change-icon" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortName; ?>"></span>
												</div>
															
												<h3><a href="<?php the_permalink(); ?>"><?php echo $shortName ?></a></h3>

												<div class="category-main-listing-cat-loc">

													<span class="category-main-listing-cat">
														<?php 
														// Display primary category
														echo getPrimaryCategoryByPostId(get_the_ID(), 'name'); ?>
													</span> <span class="category-main-listing-location">
													<?php
														// Display ACF city name 
														$fieldCityName = get_field_object('providerCityName');
														$valueCityName = $fieldCityName['value'];
														$labelCityName = $fieldCityName['choices'][ $valueCityName ];
												
														// Display ACF Big city name 
														$fieldBigCityName = get_field_object('providerBigCityName');
														$valueBigCityName = $fieldBigCityName['value'];
														$labelBigCityName = $fieldBigCityName['choices'][ $valueBigCityName ];

														if($labelCityName == $labelBigCityName){
															echo $labelBigCityName.', '.$szvData['translations']['croatia'][$szvData['wpLangCode']];
														}else {
															echo $labelCityName.' ('.$labelBigCityName.'), '.$szvData['translations']['croatia'][$szvData['wpLangCode']];;
														}
													?>
													</span>

												</div>							

											</div>

											
											<div class="category-main-listing-content">

												<div class="category-main-listing-desc cat-listing-ellipsis hide-catlisting-ellipsis">

													<?php 
														

														if(get_field('shortDesc')){
															if(strlen(get_field('shortDesc'))<100) {
																echo get_field('shortDesc', false, false).' '.get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);
															}else{
																echo get_field('shortDesc', false, false);
															}
														}else{
															echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);
														}

															/* 
														else: 
															echo the_field('dynamicDesc', 'options', false, false);
														endif; 
														*/
													
													?>

													<span class="expand-category-listing">
														<span class="fa fa-plus-circle"></span>
													</span>

												</div>

												<div class="category-main-listing-add-fields cat-listing-ellipsis hide-catlisting-ellipsis">
													<?php include(dirname(__FILE__) . '/archive-category.php'); ?>
													<span class="expand-category-listing"><span class="fa fa-plus-circle"></span></span>
												</div>
												
												<?php 
													if(have_rows('reviews')):
														while(have_rows('reviews')) : the_row();
															if(have_rows('providerReviews')):
																while(have_rows('providerReviews')) : the_row();
																	if(get_sub_field('providerReviewPublished') 
																	&& get_sub_field('providerReviewApproved') 
																	&& !get_sub_field('providerReviewMarkedAsSpam')): 
												?>

																		<div class="cat-listing-reviews-box">
																			
																			<div class="cat-listing-reviews-comment cat-listing-ellipsis hide-catlisting-ellipsis" 
																			data-title="<?php _e('Komentar usluge', 'albadiem'); ?>
																			<?php echo $shortName ?>">

																				<?php 
																					
																					if(get_sub_field('providerReviewPositiveComment')): 
																					//if(true): 
																					
																				?>
																				<span class="catlist-b catlist-b-pos catlist-b-text">
																					<?php echo get_sub_field('providerReviewPositiveComment'); ?>
																				</span>
																				<?php endif; ?>

																				<?php 
																				
																					if(get_sub_field('providerReviewNegativeComment')): 
																					//if(true): 
																					
																				?>
																				<span class="catlist-b catlist-b-neg catlist-b-text">
																					<?php echo get_sub_field('providerReviewNegativeComment'); ?>
																				</span>
																				<?php endif; ?>

																				<span class="catlist-b">
																					<?php
																						$tmp_dt = new DateTime(get_sub_field('providerReviewDate'));
																						$providerReviewDateLatest = $tmp_dt->format("d.m.Y.");	
																					?>
																					<em><?php echo $providerReviewDateLatest; ?></em>
																				</span>

																				<span class="catlist-b">
																					<?php	
																						$providerReviewRaterNameLatest = ucwords(mb_strtolower(get_sub_field('providerReviewRaterName')))." ";
																						$providerReviewRaterLastNameLatest = mb_strtoupper(mb_substr(get_sub_field('providerReviewRaterLastName'),0,1)).". "; 
																					?>
																					<strong><?php echo $providerReviewRaterNameLatest . ' ' 
																					. $providerReviewRaterLastNameLatest ?>
																					</strong>
																				</span>

																			</div>

																			<span class="popup-category-listing-review">
																				<?php _e('više', 'albadiem'); ?>
																			</span>

																		</div>

												<?php
																		break;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												?>

											</div>


											<div class="category-main-listing-cta">

												<div class="category-main-listing-cta-box">

													<div class="row no-gutters">		

													<?php 

														$providerContact = getProviderContacts(get_the_ID());
														//var_dump($providerContact);

													?>

													
														<div class="col-4">
															<?php if(isset($providerContact['callPhone']) && !isset($providerContact['callMobilePhone'])): ?>	
																<span class="category-main-listing-cta-phone">
																	<a data-href="<?php echo $providerContact['callPhone']; ?>" class="category-cta-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortName; ?>" data-wp-id="<?php echo get_the_id(); ?>">
																		<span class="fa fa-plus-circle"></span> <?php if(false): ?>	<span class="fa fa-phone"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callPhone']); ?>
																	</a>
																</span>
															<?php endif; ?> 
															<?php if(isset($providerContact['callMobilePhone'])): ?>	
																<span class="category-main-listing-cta-phone">
																	<a data-href="<?php echo $providerContact['callMobilePhone']; ?>" class="category-cta-mobile-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortName; ?>" data-wp-id="<?php echo get_the_id(); ?>">
																		<span class="fa fa-plus-circle"></span> <?php if(false): ?>	<span class="fa fa-mobile"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callMobilePhone']); ?>
																	</a>
																</span>
															<?php endif; ?>
														</div>							

														<div class="col-8 text-right">

															<span class="category-main-listing-cta-info">

																<a href="<?php the_permalink(); ?>">
																	<span class="find-out-more-full"><?php echo $szvData['translations']['findOutMore'][$szvData['wpLangCode']]; ?></span>
																	<span class="find-out-more-short"><?php echo $szvData['translations']['more'][$szvData['wpLangCode']]; ?></span>
																</a>

															</span>

															<span class="category-main-listing-cta-contact">

																<a href="<?php the_permalink(); ?>#contact"><?php _e('Pošalji upit', 'albadiem'); ?></a>

															</span>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>
									
						<?php	
									endif;
								endwhile; 
							
						?>
								<!-- Pagination -->

								<div class="pagination-container">
				
									<div class="container">
				
										<div class="row">			
				
											<div class="pagination">
										
				
												<div class="pagination-box">
				
													<?php 
														
														if(function_exists(albadiem_pagination())):
															albadiem_pagination($wp_query->max_num_pages, 2, $paged);
														endif;
															wp_reset_query();
														
													?>
														
												</div>
				
											</div>
				
										</div>
				
									</div>
				
								</div>
				
								<!-- Pagination / End -->
				<?php
				
					// endif;
				
				?>
					
			</div>
		</div>

	</div>

</section>

<!-- Section Category Main Listing / End -->