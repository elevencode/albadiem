
<?php

//$parent_category_slug = $qobj->slug;

global $szvData;
$translations = $szvData['translations'];
$tLang = $szvData['wpLangCode'];

$parent_category_slug = getPrimaryCategoryByPostId(get_the_ID(), 'slug');

// Category specific - BENDOVI
 if($parent_category_slug == 'bendovi' || $parent_category_slug == 'bands'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom', get_the_ID());
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo', get_the_ID());
	$providerMalePersons = get_field('providerMalePersons', get_the_ID());
	$providerFemalePersons = get_field('providerFemalePersons', get_the_ID());
    $providerInstrumentsObjects = get_field_object('providerInstruments', get_the_ID());
    $providerInstruments = get_field('providerInstruments', get_the_ID());
	$providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded', get_the_ID());
?>

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['numberOfMembers'][$tLang]; ?>: 
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
        <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </span>
    <?php if($providerMalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['maleVocals'][$tLang]; ?>: <?php  echo $providerMalePersons ?></span>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></span>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . implode(', ',  $providerInstrumentsLabels) . '</span>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</span>'; ?>

<?php endif; ?>

<?php //Category specific - BENDOVI ?>

<?php //Category specific - BRAČNO PUTOVANJE ?>
<?php if($parent_category_slug == 'bracno-putovanje' || $parent_category_slug == 'honeymoon'): ?>
<?php 
    $providerDestinations = get_field('providerDestinations');
	$providerDestinationsDesc = get_field('providerDestinationsDesc');
?>

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $providerDestinations ?></span>
    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $providerDestinationsDesc ?></span>

<?php endif; ?>
<?php //Category specific - BRAČNO PUTOVANJE ?>

<?php //Category specific - CATERING ?>
<?php if($parent_category_slug == 'catering-usluge' || $parent_category_slug == 'catering'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$rentAWaiter = get_field_object('rentAWaiter');
	$rentAChair = get_field_object('rentAChair');
	$rentATent = get_field_object('rentATent');
?> 

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>
    <?php if($rentAWaiter['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['rentAWaiter'][$tLang] . '</span>'; ?>
    <?php if($rentAChair['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['rentAChair'][$tLang] . '</span>'; ?>
    <?php if($rentATent['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['rentATent'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - CATERING ?>

<?php //Category specific - DEKORACIJE, BUKETI, REVERI, PODVEZICE ?>
<?php if($parent_category_slug == 'dekoracije-buketi-reveri-podvezice-baloni' || $parent_category_slug == 'decoration-bouquets-lapel-garter-balloons'): ?>
<?php 
    $providerDecorations = get_field_object('providerDecorations');
	$providerBouquets = get_field_object('providerBouquets');
	$providerBoutonnieres = get_field_object('providerBoutonnieres');
	$providerGarters = get_field_object('providerGarters');
	$providerBalloons = get_field_object('providerBalloons');
?>

    <?php if($providerDecorations['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['decoration'][$tLang] . '</span>'; ?>
    <?php if($providerBouquets['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['bouquets'][$tLang] . '</span>'; ?>
    <?php if($providerBoutonnieres['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['lapel'][$tLang] . '</span>'; ?>
    <?php if($providerGarters['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['garter'][$tLang] . '</span>'; ?>
    <?php if($providerBalloons['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['balloons'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - DEKORACIJE, BUKETI, REVERI, PODVEZICE ?>

<?php //Category specific - DJ ?>
<?php if($parent_category_slug == 'dj-izvodaci' || $parent_category_slug == 'dj'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$providerMalePersons = get_field('providerMalePersons');
	$providerFemalePersons = get_field('providerFemalePersons');
	$providerInstruments = get_field_object('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
    $providerKaraoke = get_field_object('providerKaraoke');
?>

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['numberOfMembers'][$tLang]; ?>: 
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
        <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </span>
    <?php if($providerMalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['maleVocals'][$tLang]; ?>: <?php  echo $providerMalePersons ?></span>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></span>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        } 
        echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . implode(', ',  $providerInstrumentsLabels) . '</span>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</span>'; ?>
    <?php if($providerKaraoke['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['karaoke'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - DJ ?>

<?php //Category specific - FOTO KABINE ?>
<?php if($parent_category_slug == 'foto-kabine' || $parent_category_slug == 'photo-cabins'): ?>

<?php endif; ?>

<?php //Category specific - FOTO KABINE ?>

<?php //Category specific - FOTO, VIDEO ?>
<?php if($parent_category_slug == 'foto-video' || $parent_category_slug == 'photo-video'): ?>
<?php 
    $providerFoto = get_field_object('providerFoto');
	$providerVideo = get_field_object('providerVideo');
?>

    <?php if($providerFoto['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['photographyOtherServices'][$tLang] . '</span>'; ?>
    <?php if($providerVideo['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['videoRecordingEditing'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - FOTO, VIDEO ?>

<?php //Category specific - GLAZBA ZA CRKVU ?>
<?php if($parent_category_slug == 'glazba-za-crkvu' || $parent_category_slug == 'church-music'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom', get_the_ID());
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo', get_the_ID());
	$providerMalePersons = get_field('providerMalePersons', get_the_ID());
	$providerFemalePersons = get_field('providerFemalePersons', get_the_ID());
    $providerInstrumentsObjects = get_field_object('providerInstruments', get_the_ID());
    $providerInstruments = get_field('providerInstruments', get_the_ID());
	$providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded', get_the_ID());
?>

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['numberOfMembers'][$tLang]; ?>: 
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
        <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </span>
    <?php if($providerMalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['maleVocals'][$tLang]; ?>: <?php  echo $providerMalePersons ?></span>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></span>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . implode(', ',  $providerInstrumentsLabels) . '</span>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - GLAZBA ZA CRKVU ?>

<?php //Category specific - HOTELI ?>
<?php if($parent_category_slug == 'hoteli' || $parent_category_slug == 'hotels'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

    $providerParking = get_field_object('providerParking');
    $providerPrivateParking = get_field_object('providerPrivateParking');
    $providerFreeParking = get_field_object('providerFreeParking');
    $providerWiFi = get_field_object('providerWiFi');
    $providerAirCondition = get_field_object('providerAirCondition');
    $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
    $providerWC = get_field_object('providerWC');
    $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
    $providerFloor = get_field_object('providerFloor');
    $providerHasElevator = get_field_object('providerHasElevator');
    $providerView = get_field_object('providerView');
    $providerTerrace = get_field_object('providerTerrace');
    $providerSmokingArea = get_field_object('providerSmokingArea');
    $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
    $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');

?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>

    <?php if($providerParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['parking'][$tLang] . '</span>'; ?>
    <?php if($providerPrivateParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['privateParking'][$tLang] . '</span>'; ?>
    <?php if($providerFreeParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['freeParking'][$tLang] . '</span>'; ?>
    <?php if($providerWiFi['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wiFi'][$tLang] . '</span>'; ?>
    <?php if($providerAirCondition['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['airCondition'][$tLang] . '</span>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['suitableForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerWC['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wc'][$tLang] . '</span>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wcForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php //if($providerFloor['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['floor'][$tLang] . '</span>'; ?>
    <?php if($providerHasElevator['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasElevator'][$tLang] . '</span>'; ?>
    <?php if($providerView['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasView'][$tLang] . '</span>'; ?>
    <?php if($providerTerrace['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasTerrace'][$tLang] . '</span>'; ?>
    <?php if($providerSmokingArea['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['smokingArea'][$tLang] . '</span>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['childrenFriendly'][$tLang] . '</span>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['accommodationAvailable'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - HOTELI ?>

<?php //Category specific - IZDVAJAMO ?>
<?php if($parent_category_slug == 'izdvajamo' || $parent_category_slug == 'interesting'): ?>

<?php endif; ?>
<?php //Category specific - IZDVAJAMO ?>

<?php //Category specific - MAKEUP NOKTI I TREPAVICE ?>
<?php if($parent_category_slug == 'makeup-nokti-trepavice' || $parent_category_slug == 'makeup-nails-eyelashes'): ?>
<?php 
    $providerMakeUp = get_field_object('providerMakeUp');
	$providerNails = get_field_object('providerNails');
	$providerEyelashesTrue = get_field_object('providerEyelashes');
?>

    <?php if($providerMakeUp['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['makeUp'][$tLang] . '</span>'; ?>
    <?php if($providerNails['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['nails'][$tLang] . '</span>'; ?>
    <?php if($providerEyelashesTrue['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['eyelashes'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - MAKEUP NOKTI I TREPAVICE ?>

<?php //Category specific - MALI RESTORANI ?>
<?php if($parent_category_slug == 'mali-restorani' || $parent_category_slug == 'small-restaurants'): ?>
<?php 
   $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
   $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

   $providerParking = get_field_object('providerParking');
   $providerPrivateParking = get_field_object('providerPrivateParking');
   $providerFreeParking = get_field_object('providerFreeParking');
   $providerWiFi = get_field_object('providerWiFi');
   $providerAirCondition = get_field_object('providerAirCondition');
   $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
   $providerWC = get_field_object('providerWC');
   $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
   $providerFloor = get_field_object('providerFloor');
   $providerHasElevator = get_field_object('providerHasElevator');
   $providerView = get_field_object('providerView');
   $providerTerrace = get_field_object('providerTerrace');
   $providerSmokingArea = get_field_object('providerSmokingArea');
   $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
   $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>

    <?php if($providerParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['parking'][$tLang] . '</span>'; ?>
    <?php if($providerPrivateParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['privateParking'][$tLang] . '</span>'; ?>
    <?php if($providerFreeParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['freeParking'][$tLang] . '</span>'; ?>
    <?php if($providerWiFi['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wiFi'][$tLang] . '</span>'; ?>
    <?php if($providerAirCondition['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['airCondition'][$tLang] . '</span>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['suitableForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerWC['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wc'][$tLang] . '</span>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wcForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php //if($providerFloor['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['floor'][$tLang] . '</span>'; ?>
    <?php if($providerHasElevator['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasElevator'][$tLang] . '</span>'; ?>
    <?php if($providerView['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasView'][$tLang] . '</span>'; ?>
    <?php if($providerTerrace['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasTerrace'][$tLang] . '</span>'; ?>
    <?php if($providerSmokingArea['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['smokingArea'][$tLang] . '</span>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['childrenFriendly'][$tLang] . '</span>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['accommodationAvailable'][$tLang] . '</span>'; ?>


<?php endif; ?>
<?php //Category specific - MALI RESTORANI ?>

<?php //Category specific - MUŠKA ODIJELA I DODACI ?>
<?php if($parent_category_slug == 'muska-odijela-i-dodaci' || $parent_category_slug == 'mens-suits-and-accessories'): ?>
<?php 
    $providerMaleSuits = get_field_object('providerMaleSuits');
	$providerMaleSuitsRental = get_field_object('providerMaleSuitsRental');
	$providerMaleAcessories = get_field_object('providerMaleAcessories');
?>

    <?php if($providerMaleSuits['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['maleSuits'][$tLang] . '</span>'; ?>
    <?php if($providerMaleSuitsRental['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['maleSuitsRental'][$tLang] . '</span>'; ?>
    <?php if($providerMaleAcessories['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['maleAccessories'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - MUŠKA ODIJELA I DODACI ?>

<?php //Category specific - NAJAM KUĆE ZA PROSLAVE ?>
<?php if($parent_category_slug == 'najam-kuce-za-proslave' || $parent_category_slug == 'rent-celebration-houses'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

    $providerParking = get_field_object('providerParking');
    $providerPrivateParking = get_field_object('providerPrivateParking');
    $providerFreeParking = get_field_object('providerFreeParking');
    $providerWiFi = get_field_object('providerWiFi');
    $providerAirCondition = get_field_object('providerAirCondition');
    $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
    $providerWC = get_field_object('providerWC');
    $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');

    $providerPool = get_field_object('providerPool');
    $providerHeatedPool = get_field_object('providerHeatedPool');
    $providerBilliards = get_field_object('providerBilliards');
    $providerDarts = get_field_object('providerDarts');
    $providerTableTennis = get_field_object('providerTableTennis');
    $providerTableSoccer = get_field_object('providerTableSoccer');
    $providerCards = get_field_object('providerCards');
    $providerFootball = get_field_object('providerFootball');
    $providerBasketball = get_field_object('providerBasketball');
    $providerTennis = get_field_object('providerTennis');
    $providerBadminton = get_field_object('providerBadminton');
    $providerBalote = get_field_object('providerBalote');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>

    <?php if($providerParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['parking'][$tLang] . '</span>'; ?>
    <?php if($providerPrivateParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['privateParking'][$tLang] . '</span>'; ?>
    <?php if($providerFreeParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['freeParking'][$tLang] . '</span>'; ?>
    <?php if($providerWiFi['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wiFi'][$tLang] . '</span>'; ?>
    <?php if($providerAirCondition['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['airCondition'][$tLang] . '</span>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['suitableForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerWC['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wc'][$tLang] . '</span>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wcForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerPool['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['pool'][$tLang] . '</span>'; ?>
    <?php if($providerHeatedPool['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['heatedPool'][$tLang] . '</span>'; ?>
    <?php if($providerBilliards['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['billiards'][$tLang] . '</span>'; ?>
    <?php if($providerDarts['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['darts'][$tLang] . '</span>'; ?>
    <?php if($providerTableTennis['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['tableTennis'][$tLang] . '</span>'; ?>
    <?php if($providerTableSoccer['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['tableFootball'][$tLang] . '</span>'; ?>
    <?php if($providerCards['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['cards'][$tLang] . '</span>'; ?>
    <?php if($providerFootball['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['football'][$tLang] . '</span>'; ?>
    <?php if($providerBasketball['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['basketball'][$tLang] . '</span>'; ?>
    <?php if($providerTennis['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['tennis'][$tLang] . '</span>'; ?>
    <?php if($providerBadminton['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['badminton'][$tLang] . '</span>'; ?>
    <?php if($providerBalote['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['boules'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - NAJAM KUĆE ZA PROSLAVE ?>

<?php //Category specific - NAJAM ŠATORA ?>
<?php if($parent_category_slug == 'najam-satora' || $parent_category_slug == 'rent-a-tent'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>
<?php endif; ?>
<?php //Category specific - NAJAM ŠATORA ?>

<?php //Category specific - POKLONI ?>
<?php if($parent_category_slug == 'pokloni' || $parent_category_slug == 'gifts'): ?>
<?php 
    $providerGiftList = get_field_object('providerGiftList');
	$providerGiftCoupon = get_field_object('providerGiftCoupon');
?>

    <?php if($providerGiftList['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['giftList'][$tLang] . '</span>'; ?>
    <?php if($providerGiftCoupon['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' .  $translations['giftCoupon'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - POKLONI ?>

<?php //Category specific - POZIVNICE, ZAHVALNICE, POPIS GOSTIJU ?>
<?php if($parent_category_slug == 'pozivnice-zahvalnice-popis-gostiju' || $parent_category_slug == 'invitations-letter-of-thanks-and-guest-lists'): ?>
<?php 
    $providerInvitations = get_field_object('providerInvitations');
	$providerThankYouNotes = get_field_object('providerThankYouNotes');
	$providerGuestLists = get_field_object('providerGuestLists');
?>

    <?php if($providerInvitations['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['invitations'][$tLang] . '</span>'; ?>
    <?php if($providerThankYouNotes['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['thankYouNotes'][$tLang] . '</span>'; ?>
    <?php if($providerGuestLists['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['guestLists'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - POZIVNICE, ZAHVALNICE, POPIS GOSTIJU ?>

<?php //Category specific - RASVJETA, RAZGLAS, EFEKTI ?>
<?php if($parent_category_slug == 'rasvjeta-razglas-efekti' || $parent_category_slug == 'lighting-soundsystem-effects'): ?>
<?php 
    $providerLighting = get_field_object('providerLighting');
	$providerSoundSystem = get_field_object('providerSoundSystem');
	$providerEffects = get_field_object('providerEffects');
?>

    <?php if($providerLighting['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['lighting'][$tLang] . '</span>'; ?>
    <?php if($providerSoundSystem['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['soundSystem'][$tLang] . '</span>'; ?>
    <?php if($providerEffects['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['effects'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - RASVJETA, RAZGLAS, EFEKTI ?>

<?php //Category specific - RENT A LIMO ?>
<?php if($parent_category_slug == 'najam-limuzina' || $parent_category_slug == 'rent-a-limo'): ?>
<?php endif; ?>
<?php //Category specific - RENT A LIMO ?>

<?php //Category specific - RENT A OLDTIMER ?>
<?php if($parent_category_slug == 'najam-oldtimera' || $parent_category_slug == 'rent-a-oldtimer'): ?>
<?php endif; ?>
<?php //Category specific - RENT A OLDTIMER ?>

<?php //Category specific - RESTORANI I SALE ?>
<?php if($parent_category_slug == 'restorani-sale' || $parent_category_slug == 'restaurants-and-wedding-venues'): ?>
<?php 
     $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
     $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
  
     $providerParking = get_field_object('providerParking');
     $providerPrivateParking = get_field_object('providerPrivateParking');
     $providerFreeParking = get_field_object('providerFreeParking');
     $providerWiFi = get_field_object('providerWiFi');
     $providerAirCondition = get_field_object('providerAirCondition');
     $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
     $providerWC = get_field_object('providerWC');
     $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
     $providerFloor = get_field_object('providerFloor');
     $providerHasElevator = get_field_object('providerHasElevator');
     $providerView = get_field_object('providerView');
     $providerTerrace = get_field_object('providerTerrace');
     $providerSmokingArea = get_field_object('providerSmokingArea');
     $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
     $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>

    <?php if($providerParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['parking'][$tLang] . '</span>'; ?>
    <?php if($providerPrivateParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['privateParking'][$tLang] . '</span>'; ?>
    <?php if($providerFreeParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['freeParking'][$tLang] . '</span>'; ?>
    <?php if($providerWiFi['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wiFi'][$tLang] . '</span>'; ?>
    <?php if($providerAirCondition['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['airCondition'][$tLang] . '</span>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['suitableForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerWC['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wc'][$tLang] . '</span>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wcForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php //if($providerFloor['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['floor'][$tLang] . '</span>'; ?>
    <?php if($providerHasElevator['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasElevator'][$tLang] . '</span>'; ?>
    <?php if($providerView['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasView'][$tLang] . '</span>'; ?>
    <?php if($providerTerrace['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasTerrace'][$tLang] . '</span>'; ?>
    <?php if($providerSmokingArea['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['smokingArea'][$tLang] . '</span>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['childrenFriendly'][$tLang] . '</span>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['accommodationAvailable'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - RESTORANI I SALE ?>

<?php //Category specific - RESTORANI KONOBE DJEVOJAČKE MOMAČKE VEČERI ?>
<?php if($parent_category_slug == 'restorani-konobe-djevojacke-momacke-veceri' || $parent_category_slug == 'restaurants-taverns-bachelorette-and-bachelor-party'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$providerLiveMusic = get_field_object('providerLiveMusic');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>
    <?php if($providerLiveMusic['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['liveMusic'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - RESTORANI KONOBE DJEVOJAČKE MOMAČKE VEČERI ?>

<?php //Category specific - SALONI LJEPOTE I ZDRAVLJA ?>
<?php if($parent_category_slug == 'saloni-ljepote-zdravlja' || $parent_category_slug == 'beauty-and-health-salons'): ?>
<?php 

	$providerHealthServices = get_field_object('providerHealthServices');
	$providerBeautyServices = get_field_object('providerBeautyServices');
?>

    <?php if($providerBeautyServices['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['beautySalon'][$tLang] . '</span>'; ?>
    <?php if($providerHealthServices['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['healthSalon'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - SALONI LJEPOTE I ZDRAVLJA ?>

<?php //Category specific - SELFIE MIRROR ?>
<?php if($parent_category_slug == 'najam-selfie-mirror' || $parent_category_slug == 'selfie-mirror'): ?>
<?php endif; ?>
<?php //Category specific - SELFIE MIRROR ?>

<?php //Category specific - SMJEŠTAJ ?>
<?php if($parent_category_slug == 'smjestaj' || $parent_category_slug == 'accommodation'): ?>
<?php 
     $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
     $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
  
     $providerParking = get_field_object('providerParking');
     $providerPrivateParking = get_field_object('providerPrivateParking');
     $providerFreeParking = get_field_object('providerFreeParking');
     $providerWiFi = get_field_object('providerWiFi');
     $providerAirCondition = get_field_object('providerAirCondition');
     $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
     $providerWC = get_field_object('providerWC');
     $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
     $providerFloor = get_field_object('providerFloor');
     $providerHasElevator = get_field_object('providerHasElevator');
     $providerView = get_field_object('providerView');
     $providerTerrace = get_field_object('providerTerrace');
     $providerSmokingArea = get_field_object('providerSmokingArea');
     $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
     $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>

<span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </span>

    <?php if($providerParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['parking'][$tLang] . '</span>'; ?>
    <?php if($providerPrivateParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['privateParking'][$tLang] . '</span>'; ?>
    <?php if($providerFreeParking['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['freeParking'][$tLang] . '</span>'; ?>
    <?php if($providerWiFi['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wiFi'][$tLang] . '</span>'; ?>
    <?php if($providerAirCondition['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['airCondition'][$tLang] . '</span>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['suitableForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php if($providerWC['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wc'][$tLang] . '</span>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wcForDisabledPeople'][$tLang] . '</span>'; ?>
    <?php //if($providerFloor['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['floor'][$tLang] . '</span>'; ?>
    <?php if($providerHasElevator['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasElevator'][$tLang] . '</span>'; ?>
    <?php if($providerView['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasView'][$tLang] . '</span>'; ?>
    <?php if($providerTerrace['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['hasTerrace'][$tLang] . '</span>'; ?>
    <?php if($providerSmokingArea['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['smokingArea'][$tLang] . '</span>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['childrenFriendly'][$tLang] . '</span>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['accommodationAvailable'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - SMJEŠTAJ ?>

<?php //Category specific - TAMBURAŠI ?>
<?php if($parent_category_slug == 'tamburasi' || $parent_category_slug == 'tambouras'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom', get_the_ID());
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo', get_the_ID());
	$providerMalePersons = get_field('providerMalePersons', get_the_ID());
	$providerFemalePersons = get_field('providerFemalePersons', get_the_ID());
    $providerInstrumentsObjects = get_field_object('providerInstruments', get_the_ID());
    $providerInstruments = get_field('providerInstruments', get_the_ID());
	$providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded', get_the_ID());
?>

    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['numberOfMembers'][$tLang]; ?>: 
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
        <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </span>
    <?php if($providerMalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['maleVocals'][$tLang]; ?>: <?php  echo $providerMalePersons ?></span>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></span>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . implode(', ',  $providerInstrumentsLabels) . '</span>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - TAMBURAŠI ?>

<?php //Category specific - TAXI TRANSFERI ?>
<?php if($parent_category_slug == 'taxi-transferi' || $parent_category_slug == 'taxi-transfers'): ?>
<?php endif; ?>
<?php //Category specific - TAXI TRANSFERI ?>

<?php //Category specific - TEČAJ PLESA ?>
<?php if($parent_category_slug == 'skola-plesa' || $parent_category_slug == 'dance-lessons'): ?>
<?php 
   $providerFirstDance = get_field_object('providerFirstDance');
   $providerDanceSchool = get_field_object('providerDanceSchool');
   $providerAvailableDances = get_field_object('providerAvailableDances');
?>

    <?php if($providerFirstDance['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['firstDance'][$tLang] . '</span>'; ?>
    <?php if($providerDanceSchool['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['danceSchool'][$tLang] . '</span>'; ?>
    <?php if($providerAvailableDances['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['availableDances'][$tLang] . ': '. $providerAvailableDances['value'] .'</span>'; ?>

<?php endif; ?>
<?php //Category specific - TEČAJ PLESA ?>

<?php //Category specific - TORTE, KOLAČI ?>
<?php if($parent_category_slug == 'torte-kolaci' || $parent_category_slug == 'cakes'): ?>
<?php 
   $providerWeddingCakes = get_field_object('providerWeddingCakes');
   $providerCakes = get_field_object('providerCakes');
   $providerCookies = get_field_object('providerCookies');
   $providerHomeMadeCakesCookies = get_field_object('providerHomeMadeCakesCookies');
?>

    <?php if($providerWeddingCakes['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['weddingCakes'][$tLang] . '</span>'; ?>
    <?php if($providerCakes['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['cakes'][$tLang] . '</span>'; ?>
    <?php if($providerCookies['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['cookies'][$tLang] . '</span>'; ?>
    <?php if($providerHomeMadeCakesCookies['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['homeMadeCakesCookies'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - TORTE, KOLAČI ?>

<?php //Category specific - TRIO, DUO HARMONIKA ?>
<?php if($parent_category_slug == 'trio-duo-harmonika' || $parent_category_slug == 'trio-duo-accordion'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom', get_the_ID());
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo', get_the_ID());
	$providerMalePersons = get_field('providerMalePersons', get_the_ID());
	$providerFemalePersons = get_field('providerFemalePersons', get_the_ID());
    $providerInstrumentsObjects = get_field_object('providerInstruments', get_the_ID());
    $providerInstruments = get_field('providerInstruments', get_the_ID());
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded', get_the_ID());
    $providerTrio = get_field_object('providerTrio');
	$providerDuo = get_field_object('providerDuo');
	$providerAccordion = get_field_object('providerAccordion');
?>
    <?php if($providerTrio['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['trio'][$tLang] . '</span>'; ?>
    <?php if($providerDuo['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['duo'][$tLang] . '</span>'; ?>
    <?php if($providerAccordion['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['accordion'][$tLang] . '</span>'; ?>
    <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['numberOfMembers'][$tLang]; ?>: 
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
        <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </span>
    <?php if($providerMalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['maleVocals'][$tLang]; ?>: <?php  echo $providerMalePersons ?></span>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <span class="cml-add-fields"><span class="fa fa-check"></span><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></span>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . implode(', ',  $providerInstrumentsLabels) . '</span>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>'. $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</span>'; ?>
<?php endif; ?>
<?php //Category specific - TRIO, DUO HARMONIKA ?>

<?php //Category specific - VJENČANICE I DODACI ?>
<?php if($parent_category_slug == 'vjencanice-i-dodaci' || $parent_category_slug == 'wedding-dresses-and-accessories'): ?>
<?php 
   $providerWeddingDresses = get_field_object('providerWeddingDresses');
   $providerWeddingDressesRental = get_field_object('providerWeddingDressesRental');
   $providerWeddingAccessories = get_field_object('providerWeddingAccessories');
?>

    <?php if($providerWeddingDresses['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['weddingDresses'][$tLang] . '</span>'; ?>
    <?php if($providerWeddingDressesRental['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['weddingDressesRentals'][$tLang] . '</span>'; ?>
    <?php if($providerWeddingAccessories['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['weddingDressesAccessories'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - VJENČANICE I DODACI  ?>

<?php //Category specific - WELLNESS SPA ?>
<?php if($parent_category_slug == 'wellness-spa-usluge' || $parent_category_slug == 'wellness-spa'): ?>
<?php 
   $providerWellness = get_field_object('providerWellness');
   $providerSpa = get_field_object('providerSpa');
?>

    <?php if($providerWellness['value']) echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['wellness'][$tLang] . '</span>'; ?>
    <?php if($providerSpa['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['spa'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - WELLNESS SPA ?>

<?php //Category specific - ZARUČNIČKI TEČAJ ?>
<?php if($parent_category_slug == 'zarucnicki-tecajevi' || $parent_category_slug == 'engagement-courses'): ?>
<?php endif; ?>
<?php //Category specific - ZARUČNIČKI TEČAJ ?>

<?php //Category specific - Zaručničo prstenje, vere ?>
<?php if($parent_category_slug == 'zarucnicko-prstenje-vere' || $parent_category_slug == 'jewelry-engagement-rings'): ?>
<?php 
   $providerEngagementRings = get_field_object('providerEngagementRings');
   $providerWeddingRings = get_field_object('providerWeddingRings');
   $providerDiamondsGemstones = get_field_object('providerDiamondsGemstones');
   $providerSilverJewelry = get_field_object('providerSilverJewelry');
   $providerGoldJewelry = get_field_object('providerGoldJewelry');
?>

    <?php if($providerEngagementRings['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['engagementRings'][$tLang] . '</span>'; ?>
    <?php if($providerWeddingRings['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['weddingRings'][$tLang] . '</span>'; ?>
    <?php if($providerDiamondsGemstones['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['diamondsGemstones'][$tLang] . '</span>'; ?>
    <?php if($providerGoldJewelry['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['goldJewelry'][$tLang] . '</span>'; ?>
    <?php if($providerSilverJewelry['value'])	echo '<span class="cml-add-fields"><span class="fa fa-check"></span>' . $translations['silverJewelry'][$tLang] . '</span>'; ?>

<?php endif; ?>
<?php //Category specific - Zaručničo prstenje, vere ?>

