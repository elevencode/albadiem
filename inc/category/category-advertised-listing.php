<!-- Section Category Main Listing

================================================== -->
<?php

    $categoryId = $qobj->cat_ID;
    $categoryOption = get_option('category_'.$categoryId);
    $postIdsString = $categoryOption['extra'];

    $postIdsToAdvertise = !empty($postIdsString)
        ? explode(',', $postIdsString)
        : array(-1);
        
    //var_dump($postIdsToAdvertise);    
        
if(empty($postIdsString)):

else:
?>
<section id="category-main-listing-container" class="category-shuffle-listing">

	<div id="category-shuffle-listing">

		<div id="category-main-listing">

			<h2 class="text-center">
				<?php _e('Sponzorirani', 'albadiem') ?>
				<?php echo $qobj->name ?> 
				<?php _e('za vjenčanje Hrvatska', 'albadiem') ?>
			</h2>

			<hr />

			<?php
					$args = array(
            'post_status' => array('publish'),
						'post_type' => 'post',
						'posts_per_page' => 10,
						'post__in' => $postIdsToAdvertise,
						'no_found_rows' => true, 
						'update_post_meta_cache' => false, 
						'update_post_term_cache' => false, 
						'tax_query' => array(
								array(
                  'taxonomy' => $qobj->taxonomy,
                  'terms' => array($qobj->term_id), //$qobj->name,
                  'field' => 'term_id', //slug
									'include_children' => false,
        							'operator' => 'AND'
								)
							)
					);

					$the_query = new WP_Query($args);
					if($the_query->have_posts()):
						while($the_query->have_posts()):
							$the_query->the_post();
							
								// Query check if provider is published and approved in which case it will show in main query
								$providerPublished = get_field('providerPublished');
								$providerApproved = get_field('providerApproved');
								if($providerPublished && $providerApproved): ?>

								<div id="shuffle-" class="shuffle category-main-listing-box">

								<div class="row no-gutters">

									<div class="col-md-5 col-lg-5 col-xl-5">

										<div class="category-main-listing-img">

											<div class="category-listing-slider-container pos-relative">

												<div id="category-listing-slider-<?php echo get_the_ID(); ?>" class="category-listing-slider carousel pos-relative">

													<?php 

													// Provider images carousel
														if(have_rows('images')):
															while(have_rows('images')) : the_row();
																if(get_sub_field('showProviderImages')):
																	if(have_rows('providerImages')):
																		while(have_rows('providerImages')) : the_row();
																			$image = get_sub_field('providerImgOriginalUrl');
																				if(get_sub_field('providerImgPublished')): ?>
																		
																						<div class="carousel-cell" 
																						data-flickity-bg-lazyload="<?php echo $image; ?>" 
																						style="width:400px;"></div>
														<?php 
																				endif;
																		endwhile;
																	endif;
																endif;
															endwhile;
														endif;
														?>
												</div>

												<div class="category-listing-logo-box">

													<?php 

														// Main logo display
														if( have_rows('logo') ):
															while( have_rows('logo') ): the_row(); 
																
																$providerLogoOriginalUrl = get_sub_field('providerLogoOriginalUrl');
																$providerLogoOriginalDesktopUrl = get_sub_field('providerLogoOriginalDesktopUrl');
																$providerLogoOriginalTabletUrl = get_sub_field('providerLogoOriginalTabletUrl');
																$providerLogoOriginalMobileUrl = get_sub_field('providerLogoOriginalMobileUrl');
																$providerLogoAlt = get_sub_field('providerLogoAlt');
																$providerLogoDesc = get_sub_field('providerLogoDesc');
																$providerLogoWidth = get_sub_field('providerLogoWidth');
																$providerLogoHeight = get_sub_field('providerLogoHeight');
																$providerLogoRatio = get_sub_field('providerLogoRatio');
																$providerLogoClass = get_sub_field('providerLogoClass');
													
													?>
																<div style="background-image: url(<?php echo $providerLogoOriginalUrl ?>);" 
																class="category-listing-logo follow-href" data-href="https://www.cdn385.com.hr" 
																data-mobile-src="<?php echo $providerLogoOriginalMobileUrl; ?>" 
																data-tablet-src="<?php echo $providerLogoOriginalTabletUrl; ?>" 
																data-desktop-src="<?php echo $providerLogoOriginalDesktopUrl; ?>" 
																data-width="1200" data-height="1200" data-ratio="1" data-class="" 
																data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
														
													<?php 
															endwhile;
														endif;
													?>
												</div>	

												<?php 
													if(have_rows('avarageRatingGroup')):
														while(have_rows('avarageRatingGroup')): the_row();
															$avarageRating = get_sub_field('avarageRating');
															$totalNumberReviews = get_sub_field('totalNumberReviews');
															$avarageRatingLink = get_sub_field('avarageRatingLink');
												?>
													<div class="cat-listing-avg-rating-box">

														<div class="cat-listing-avg-rating">
														
															<a href="<?php echo $avarageRatingLink; ?> ">
																<span class="rating">
																	<?php echo $avarageRating ?>
																	</span> 
																	<small>/10</small>&nbsp;&nbsp;
																<?php _e('Pogledaj sve komentare', 'albadiem') ?>
															</a>
															
														</div>

													</div>
												<?php
														endwhile;
													endif;
												?>

												<div class="badges">

													<?php
														
														// If have special offers in single provider show the special offer badge
														if(have_rows('specialOffers')):
															while( have_rows('specialOffers') ): the_row(); 
																if(get_sub_field('showProviderSpecialOffers')): 
													?>
																	
																	<div class="discount-badge">
																		<a href="#discounts">% 
																			<?php _e('Akcija', 'albadiem'); ?>
																		</a>
																	</div>
													<?php 
																endif; 
															endwhile;
														endif;
													?>

												</div>

											</div>

										</div>

									</div>

									<div class="col-md-7 col-lg-7 col-xl-7">

										<div class="category-main-listing-heading">	
											
											<?php $shortName = get_field('shortName'); ?>

											<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
														
											<h3><a href="<?php the_permalink(); ?>"><?php echo $shortName ?></a></h3>

											<div class="category-main-listing-cat-loc">

												<span class="category-main-listing-cat">
													<?php 
													// Display top parent category
													// Fetching just first category from the list
													$category = get_the_category();
													$parent_category = $category[0]->cat_name;
													echo $parent_category; ?>
												</span> - <span class="category-main-listing-location">
												<?php
													// Display ACF city name 
													$fieldCityName = get_field_object('providerCityName');
													$valueCityName = $fieldCityName['value'];
													$labelCityName = $fieldCityName['choices'][ $valueCityName ];
													echo $labelCityName;
												?>, 
												<?php
													// Display ACF Big city name 
													$fieldBigCityName = get_field_object('providerBigCityName');
													$valueBigCityName = $fieldBigCityName['value'];
													$labelBigCityName = $fieldBigCityName['choices'][ $valueBigCityName ];
													echo $labelBigCityName;
												?>
												</span>

											</div>							

										</div>

										
										<div class="category-main-listing-content">

											<div class="category-main-listing-desc cat-listing-ellipsis hide-catlisting-ellipsis">

												<?php 
													
													if(get_field('shortDesc')): 
														echo get_field('shortDesc', false, false); 
													else: 
														echo the_field('dynamicDesc', 'options', false, false);
													endif; 
												
												?>

												<span class="expand-category-listing">
													<span class="fa fa-plus-circle"></span>
												</span>

											</div>

											<div class="category-main-listing-add-fields cat-listing-ellipsis hide-catlisting-ellipsis">
												<?php include(dirname(__FILE__) . '/archive-category.php'); ?>
												<span class="expand-category-listing"><span class="fa fa-plus-circle"></span></span>
											</div>
											
											<?php 
												if(have_rows('reviews')):
													while(have_rows('reviews')) : the_row();
														if(have_rows('providerReviews')):
															while(have_rows('providerReviews')) : the_row();
																 if(get_sub_field('providerReviewPublished') 
																 && get_sub_field('providerReviewApproved') 
																 && !get_sub_field('providerReviewMarkedAsSpam')): 
											?>

																	<div class="cat-listing-reviews-box">
																		
																		<div class="cat-listing-reviews-comment cat-listing-ellipsis hide-catlisting-ellipsis" 
																		data-title="<?php _e('Komentar usluge', 'albadiem'); ?>
																		<?php echo $shortName ?>">

																			<span class="catlist-b catlist-b-text">
																				<span class="fa fa-thumbs-o-up"></span> 
																					<?php echo get_sub_field('providerReviewPositiveComment'); ?>
																			</span>

																			<span class="catlist-b catlist-b-text">
																				<span class="fa fa-thumbs-o-down"></span> 
																					<?php echo get_sub_field('providerReviewNegativeComment'); ?>
																			</span>

																			<span class="catlist-b">
																				<em><?php echo get_sub_field('providerReviewDate'); ?></em>
																			</span>

																			<span class="catlist-b">
																				<strong><?php echo get_sub_field('providerReviewRaterName') . ' ' 
																				. get_sub_field('providerReviewRaterLastName') ?>
																				</strong>
																			</span>

																		</div>

																		<span class="popup-category-listing-review">
																			<?php _e('više', 'albadiem'); ?>
																		</span>

																	</div>

											<?php
																	break;
																endif;
															endwhile;
														endif;
													endwhile;
												endif;
											?>

										</div>


										<div class="category-main-listing-cta">

											<div class="category-main-listing-cta-box">

												<div class="row no-gutters">

													<?php if(get_field('providerShowContactPerson')): ?>

														<div class="col-1">
															<span class="category-main-listing-cta-phone">
																<a href="tel:<?php echo get_field('providerContactPersonPhone'); ?>">
																	<span class="fa fa-mobile"></span>
																</a>
															</span>
														</div>

													<?php elseif(get_field('providerMobilePhone')): ?>

														<div class="col-1">
															<span class="category-main-listing-cta-phone">
																<a href="tel:<?php echo get_field('providerMobilePhone'); ?>">
																	<span class="fa fa-mobile"></span>
																</a>
															</span>
														</div>
															
													<?php endif; ?>

													<?php if(get_field('providerPhone')): ?>
														
														<div class="col-1">
															<span class="category-main-listing-cta-phone">
																<a href="tel:<?php echo get_field('providerPhone'); ?>">
																	<span class="fa fa-phone"></span>
																</a>
															</span>
														</div>

													<?php endif; ?>

													<div class="col-10 text-right">

														<span class="category-main-listing-cta-contact">

															<a href=""><?php _e('Pošalji upit', 'albadiem'); ?></a>

														</span>

														<span class="category-main-listing-cta-info">

															<a href="<?php the_permalink(); ?>"><?php _e('Saznaj više', 'albadiem'); ?></a>

														</span>

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>
								
							<?php
						endif;
					endwhile;
					wp_reset_postdata();
				endif;
			?>

		</div>

	</div>

</section>
<?php
  endif;
?>
<!-- Section Category Main Listing / End -->