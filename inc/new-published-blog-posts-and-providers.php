<div id="home-new-blog-providers">

	<div class="container-fluid">

		<div class="container">

			<div class="row">

				<div class="col-lg-6">

					<div id="new-blog-posts">

						<h2 class="text-center"><?php _e('Kutak za trenutak', 'albadiem'); ?></h2>

						<hr />

                        <div class="row">

                        <?php
                        // the query
                        $the_query_blog = new WP_Query( array(
                            'post_status' => array('publish'),
                            'post_type' => 'blog',
                            'posts_per_page' => 2,
                            'orderby' => 'publish_date',
                            'order' => 'DESC',
                            'no_found_rows' => true, 
                            'update_post_meta_cache' => false, 
                            'update_post_term_cache' => false, 
                        ));
                        ?>

                        <?php if ( $the_query_blog->have_posts() ) : ?>
                            <?php while ( $the_query_blog->have_posts() ) : $the_query_blog->the_post(); ?>

                                <div class="col-12 col-md-6">

                                    <div class="new-blog-posts-box">

                                        <?php 
                                            if( have_rows('mainImg') ):
                                                while( have_rows('mainImg') ) : the_row(); ?>

                                                    <?php

                                                        $newBlogPostImg = get_the_post_thumbnail_url();

                                                    ?>

                                                    <div class="new-blog-posts-img follow-href lazy-load-trigger-bg-img" 
                                                    data-href="<?php the_permalink(); ?>"
                                                    data-desktop-src="<?php echo $newBlogPostImg; ?>"  
                                                    data-tablet-src="<?php echo $newBlogPostImg; ?>" 
                                                    data-mobile-src="<?php echo $newBlogPostImg; ?>" 
                                                    >
                                                    </div>

                                                    <noscript>
                                                        <img src="<?php echo $newBlogPostImg; ?>" alt="<?php the_title(); ?>" />
                                                    </noscript>
                                                    
                                                    <?php 
                                                endwhile;
                                            endif;
                                        ?>

                                        <div class="new-blog-posts-title">
                                            <a href="<?php the_permalink(); ?>"> 
                                                <?php 
                                                    the_title();
                                                ?>
                                            </a>
                                        </div>

                                        <div class="new-blog-posts-desc"> 
                                            
                                           <?php 
                                                the_field('shortDesc'); 
                                            ?>
                                   
                                        </div>

                                    </div>

                                </div>

                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                            <p><?php _e('Nema zadnjih objava', 'albadiem'); ?></p>
                        <?php endif; ?>


						</div>

					</div>	

				</div>

				<div class="col-lg-6">

					<div id="new-providers">

						<h2 class="text-center"><?php _e('Zadnji objavljeni profili', 'albadiem'); ?></h2>

						<hr />

                        <div class="row">
                        <?php
                        // the query
                        $the_query = new WP_Query( array(
                            'post_type' => 'post',
                            'posts_per_page' => 2,
                            'no_found_rows' => true, 
                            'update_post_meta_cache' => false, 
                            'update_post_term_cache' => false, 
                        ));
                        ?>

                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="col-12 col-md-6">

                                    <div class="new-providers-box">

                                    <?php 
                                        if(have_rows('images')):
                                            while(have_rows('images')) : the_row();
                                        ?>
                                            <?php if(get_sub_field('showProviderImages')): ?>

                                                    <?php 
                                                        if(have_rows('providerImages')):
                                                            while(have_rows('providerImages')) : the_row();
                                                                
                                                                $providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
                                                                $providerImgDesktopUrl = get_sub_field('providerImgDesktopUrl');
                                                                $providerImgTabletUrl = get_sub_field('providerImgTabletUrl');
                                                                $providerImgMobileUrl = get_sub_field('providerImgMobileUrl');
                                                                $providerImgWidth = get_sub_field('providerImgWidth');
                                                                $providerImgHeight = get_sub_field('providerImgHeight');
                                                                $providerImgAlt = get_sub_field('providerImgAlt');
                                                                $providerImgDesc = get_sub_field('providerImgDesc');
                                                                $providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
                                                                $providerImgTargetContainer = get_sub_field('providerImgTargetContainer');
                                                                $providerImgRatio = get_sub_field('providerImgRatio');
                                                                $providerImgClass = get_sub_field('providerImgClass');
                                                                $providerImgQueryStringVersion = get_sub_field('providerImgQueryStringVersion');
                                                                    
                                                                    if(get_sub_field('providerImgPublished')): ?>

                                                                         <div class="new-blog-posts-img follow-href lazy-load-trigger-bg-img" 
                                                                        data-href="<?php the_permalink(); ?>" 
                                                                        data-desktop-src="<?php echo $providerImgDesktopUrl; ?>" 
                                                                        data-tablet-src="<?php echo $providerImgTabletUrl; ?>" 
                                                                        data-mobile-src="<?php echo $providerImgMobileUrl; ?>" 
                                                                        data-width="<?php echo $providerImgWidth; ?>" 
                                                                        data-height="<?php echo $providerImgHeight; ?>" 
                                                                        data-ratio="<?php echo $providerImgRatio; ?>">
                                                                        </div>    
                                                                        
                                                                        <noscript>
                                                                            <img src="<?php echo $providerImgDesktopUrl; ?>" alt="<?php echo $providerImgAlt; ?>" />
                                                                        </noscript>
                                                                        
                                                                        <?php

                                                                        break;
                                                                    endif;
                                                            endwhile;
                                                        endif;
                                                    ?>
                                                    
                                               
                                        <?php endif; ?>
                                        
                                        <?php endwhile; ?>




                                        <?php endif; ?>


                                        <div class="new-blog-posts-title">
                                            <a href="<?php the_permalink(); ?>"> 
                                                <?php 
                                                    the_field('shortName');
                                                ?>
                                            </a>
                                        </div>

                                        <div class="new-blog-posts-desc"> 
                                            
                                           <?php 
                                                
                                                the_field('shortDesc');
                                            ?>
                                   
                                        </div>
                                    </div>

                                </div>

                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                            <p><?php _e('Nema novih profila', 'albadiem'); ?></p>
                        <?php endif; ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>