<!-- Section Weddings Country Offer

================================================== -->

<section id="weddings-country-offer-container">

	<?php

		global $szvData;

		if($szvData['wpLangCode'] == 'hr'){
			$categoryWeddingId = 556;
		} else if($szvData['wpLangCode'] == 'en'){
			$categoryWeddingId = 1345;
		}

		$categoryWeddingLink = get_category_link($categoryWeddingId);

	?>

	<div class="container-fluid">

		<div id="weddings-country-offer">

			<div class="row">

				<div class="col-lg-6 no-padding">	

					<div id="country-bg-hr" class="country-bg pos-relative lazy-load-trigger-bg-img" data-desktop-src="<?php echo $szvData['siteSettings']['homeCountryImgSrc']; ?>" data-tablet-src="<?php echo $szvData['siteSettings']['homeCountryImgSrc']; ?>" data-mobile-src="<?php echo $szvData['siteSettings']['homeCountryImgSrc']; ?>" data-width="1200" data-height="1200" data-ratio="1">

						<noscript>
							<img src="<?php echo $szvData['siteSettings']['homeCountryImgSrc']; ?>" alt="<?php echo $szvData['translations']['wedding'][$szvData['wpLangCode']].' '.$szvData['translations']['inCroatia'][$szvData['wpLangCode']]; ?>">
						</noscript>

						<div class="center-v-h">

							<h3 class="country-bg-hr-box-title"><a href="<?php echo $categoryWeddingLink; ?>#listing"><?php _e('Vjenčanje u Hrvatskoj', 'albadiem'); ?></a></h3>
							
						</div>

					</div>

				</div>

				<div class="col-lg-6 no-padding bg-f5f5f5">	

					<div class="country-bg-hr-box">

						<h3><?php _e('Sve Za Vjenčanje u Hrvatskoj', 'albadiem'); ?></h3>

						<p><?php _e('Predivne lokacije, savršen ambijent,... Hrvatska je idealna destinacija za vjenčanje iz snova. Pronađi na jednom mjestu sve za svoje vjenčanje brzo i jednostavno.', 'albadiem'); ?></p>

						<p><a href="<?php echo $categoryWeddingLink; ?>" class="btn cta-link"><?php _e('Klikni i zaboravi stres :)', 'albadiem'); ?></a></p>

						<?php if(false): ?>
						<div class="row">
					
							<div class="col-xl-12">

									<ul class="weddings-country-offer-list">

										<?php
											
											//preparing an array to hold later info
											
												if($szvData['wpLangCode'] == 'hr'){
													$categoryWeddingId = 556;
												} else if($szvData['wpLangCode'] == 'en'){
													$categoryWeddingId = 1345;
												}

												$counter = 0;
												$grandchildren_ids = [];
												//getting the child categories of parent 116

												$args = array(
												'orderby' => 'name',
												'parent' => $categoryWeddingId,
												'taxonomy' => 'category',
												'hide_empty' => 0,
												'field' => 'slug',
												'include_children' => false,
												'operator' => 'AND'
												);
												$categories =  get_categories($args);

													foreach ( $categories as $category ) {
													//setting up the args where the parents are the child categories
													$grandchildrenargs = array(
													'parent' => $category->term_id,
													'taxonomy' => 'category',
													'hide_empty' => 0,
													'field' => 'slug',
													'include_children' => false,
													'operator' => 'AND'
													);
													
													$grandchildrencategories =  get_categories($grandchildrenargs);
												
													foreach ( $grandchildrencategories as $grandchildrencategory ) {
													//getting the grandchildren ids or whatever else is needed and populating the array
														$grandchildren_ids[] = $grandchildrencategory->name;
														$str = ltrim($grandchildrencategory->name, 'Vjenčanje');
														echo '<li><a href="' . get_category_link( $grandchildrencategory->cat_ID ) . '">' . $str . '</a></li>';
													}
												}
										?>

										</ul>

							</div>

						</div>
						<?php endif; ?>

					</div>

				</div>

			</div>

		</div>

	</div>

	

</section>

<!-- Section Weddings Country Offer / End -->