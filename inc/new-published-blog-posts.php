<!-- Section New blog posts
================================================== -->
<section id="new-blog-posts-container">
	<div class="container-fluid">
		<div class="container">
			<div id="new-blog-posts">
				<h2 class="text-center">Kutak za trenutak</h2>
				<hr />
				<div class="row">
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-blog-posts-box">
							<div class="new-blog-posts-img follow-href" data-href="https://www.cdn385.com.hr/?v=1" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-blog-posts-title"><a href="">Mali detalji čine razliku na vjenčanjima</a></div>
							<div class="new-blog-posts-desc">Male stvari čine veliku razliku Želite biti malo drugačiji, kreativniji i oduševiti svoje goste, a neznate kako? Nema problema, pomoći...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-blog-posts-box">
							<div class="new-blog-posts-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-blog-posts-title"><a href="">Iznenadite voljenu ženu buketom cvijeća za dan žena</a></div>
							<div class="new-blog-posts-desc">Bilo da se radi o majci, sestri, prijateljici, djevojci ili supruzi... sigurni smo da u životu imate žensku osobu koja zauzima posebno mjesto...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-blog-posts-box">
							<div class="new-blog-posts-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-blog-posts-title"><a href="">Mali detalji čine razliku na vjenčanjima</a></div>
							<div class="new-blog-posts-desc">Male stvari čine veliku razliku Želite biti malo drugačiji, kreativniji i oduševiti svoje goste, a neznate kako? Nema problema, pomoći...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-blog-posts-box">
							<div class="new-blog-posts-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-blog-posts-title"><a href="">Iznenadite voljenu ženu buketom cvijeća za dan žena</a></div>
							<div class="new-blog-posts-desc">Bilo da se radi o majci, sestri, prijateljici, djevojci ili supruzi... sigurni smo da u životu imate žensku osobu koja zauzima posebno mjesto...</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Section New blog posts / End -->