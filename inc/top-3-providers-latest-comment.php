<!-- Section Top 4 Latest Reviews

================================================== -->

<section id="best-reviews-container">

	<div class="container-fluid">

		<div class="container">
		
			<?php if($szvData['wpLangCode'] == 'hr'): ?>
				<h2 class="text-center"><?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - Ocjene, komentari & iskustva</h2>
			<?php elseif($szvData['wpLangCode'] == 'en'): ?>
				<h2 class="text-center"><?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - Reviews, rating & comments</h2>
			<?php endif; ?>

			<hr />



			<div id="best-reviews">

			<div class="row justify-content-md-center">




					<?php 

						$reviewsNmbr = 0;

						$args = array(
              'post_status' => array('publish'),
							'post_type' => 'post',
							'posts_per_page' => 3,
							'meta_key'					=> 'latest_review_date',
							'orderby'						=> 'meta_value',
							'order'							=> 'DESC',
							'tax_query' => array(
								array(
									'taxonomy' => $qobj->taxonomy,
									'terms' => array($qobj->term_id), //$qobj->name,
									'field' => 'term_id', //slug
									'include_children' => false,
											'operator' => 'AND'
									)
								)
							);

						/*
						$args = array(
							'post_type'					=> 'post',
							'posts_per_page'		=> 3,
							'meta_key'					=> 'latest_review_date',
							'orderby'						=> 'meta_value',
							'order'							=> 'DESC',
							'tax_query' => array(
								array(
									'taxonomy' => $qobj->taxonomy,
									'terms' => array($qobj->term_id),
									'field' => 'term_id',
									'include_children' => false,
									'operator' => 'AND'
									)
								)
						);
						*/	

						/*
						$args = array(
							'post_type' => 'support',
							'tax_query' => array(
									array(
											'taxonomy' => 'category',
											'field'    => 'term_id',
											'terms'    => $cat->cat_ID,
											),
									),
							);
						*/

						/*
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3,
							'orderby' => 'modified',
							'tax_query' => array(
								array(
									'taxonomy' => $qobj->taxonomy,
									'terms' => $qobj->name,
									'field' => 'slug',
									'include_children' => false,
									'operator' => 'AND'
									)
								)
						);
						*/


							$the_query = new WP_Query($args);
								if($the_query->have_posts()):
									while($the_query->have_posts()):
										$the_query->the_post();

										if(have_rows('reviews', get_the_ID())):
											while(have_rows('reviews')) : the_row();
												if(have_rows('providerReviews')): ?>

													<?php while(have_rows('providerReviews')) : the_row();
						
													$providerReviewOrderLatest = get_sub_field('providerReviewOrder');
													$providerReviewApprovedLatest = get_sub_field('providerReviewApproved');
													$providerReviewMarkedAsSpamLatest = get_sub_field('providerReviewMarkedAsSpam');

													$tmp_dt = new DateTime(get_sub_field('providerReviewDate'));
													$providerReviewDateLatest = $tmp_dt->format("d.m.Y.");	

													$providerReviewConsumationBeginDateLatest = get_sub_field('providerReviewConsumationBeginDate');
													$providerReviewConsumationEndDateLatest = get_sub_field('providerReviewConsumationEndDate');
													$providerReviewRaterNameLatest = ucwords(mb_strtolower(get_sub_field('providerReviewRaterName')))." ";
													$providerReviewRaterLastNameLatest = mb_strtoupper(mb_substr(get_sub_field('providerReviewRaterLastName'),0,1)).". "; 
													$providerReviewLang = get_sub_field('providerReviewLang');

													$length = strlen(get_sub_field('providerReviewPositiveComment'));
													/*
													$providerReviewPositiveCommentLatestFull = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 100, $length - 100));
													$providerReviewNegativeCommentLatestFull = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 100, $length - 100));

													$providerReviewPositiveCommentLatestPartial = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 0, 100));
													$providerReviewNegativeCommentLatestPartial = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 0, 100));
													*/
													$providerReviewPositiveCommentLatestFull = trim(get_sub_field('providerReviewPositiveComment'));
													$providerReviewNegativeCommentLatestFull = trim(get_sub_field('providerReviewNegativeComment'));
								
													$providerReviewPositiveCommentLatestPartial = truncWords(trim(get_sub_field('providerReviewPositiveComment')), 20);
													$providerReviewNegativeCommentLatestPartial = truncWords(trim(get_sub_field('providerReviewNegativeComment')), 20);

													$providerReviewRatingLatest = get_sub_field('providerReviewRating');


													if(get_sub_field('providerReviewPublished') 
													&& get_sub_field('providerReviewApproved')
													&& !get_sub_field('providerReviewMarkedAsSpam')):

													$reviewsNmbr++;
								
												?>
							

													<div class="review-container review-container-item">

														<div class="review">

															<div class="review-testimonial">

																<div class="review-testimonial-pos">

																	<span class="review-testimonial-partial">

																		<?php echo $providerReviewPositiveCommentLatestPartial; ?>

																	</span>

																	<span class="3dots">...</span>

																	<span class="review-testimonial-full hide">

																		<?php echo $providerReviewPositiveCommentLatestFull; ?>

																	</span>

																</div>

																<div class="review-testimonial-neg">

																	<span class="review-testimonial-partial">

																		<?php echo $providerReviewNegativeCommentLatestPartial; ?>

																	</span>

																	<span class="3dots">...</span>

																	<span class="review-testimonial-full hide">

																		<?php echo $providerReviewNegativeCommentLatestFull; ?>

																	</span>

																</div>

																<div class="review-date"><?php echo $providerReviewDateLatest; ?></div>

																<div class="review-expand-full"><span class="review-expand-full-trigger"><?php _e('više', 'albadiem'); ?> <span class="fa fa-expand"></span></span></div>

																

															</div>

															<div class="reviewer-provider-box">

																<div class="rating-box pos-relative text-center">

																	<span class="center-v-h"><span class="rating"><?php echo $providerReviewRatingLatest; ?></span> <small>/5</small></span>

																</div>

																<div class="reviewer-provider">

																	<div class="reviewer"><?php echo $providerReviewRaterNameLatest . ' ' . $providerReviewRaterLastNameLatest; ?></div>
																	
																	<div class="provider"><a href="<?php the_permalink(); ?>"><?php echo get_field('shortName'); ?></a></div>
																	
																</div>

																<div class="cf"></div>

															</div>

														</div>

													</div>
						
													<?php
												endif;
												break;
											endwhile; ?>

										<?php endif; ?>

									<?php 
									endwhile;
								endif;
							endwhile;
						wp_reset_postdata();
				 ?>

				

					<?php endif; ?>
				
			
					<?php if($reviewsNmbr == 0): ?>

					<?php if($szvData['wpLangCode'] == 'hr'): ?>
						<p class="text-center">Korisnici još nisu ocijenili i komentirali usluge.</p>
					<?php elseif($szvData['wpLangCode'] == 'en'): ?>
						<p class="text-center">Users have not yet reviewed and rated the services.</p>
					<?php endif; ?>

					<?php endif; ?>

				</div>

			</div>





		</div>

	</div>

</section>

<!-- Section Top 4 Latest Reviews / End -->