<!-- Search Nav CTA Container -->
<div id="search-nav-cta-container">

	<!-- Search Nav CTA -->
	<div id="search-nav-cta">
		<div id="search-nav-cta-trigger"><span class="fa fa-search"></span></div>
	</div>
	<!-- Search Nav CTA / End -->

</div>
<!-- Search Nav CTA Container / End -->