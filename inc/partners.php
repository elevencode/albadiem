<!-- Section Partners

================================================== -->

<section id="partners-container">

	<div class="container-fluid">

		<div class="container">

		

			<h2 class="text-center"><?php _e('Partneri SZV projekta', 'albadiem'); ?></h2>

			<hr />

		

			<div id="partners">

				<div class="row">

					<div class="col-md-6">

						<div class="row">

							<div class="col-6">

								<a href="https://dalmatinskiportal.hr/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/dalmatinski-portal.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/dalmatinski-portal.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/dalmatinski-portal.png" alt="Dalmatinski Portal" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>
              
              <div class="col-6">

								<a href="https://www.facebook.com/Wedding-day-Sajam-vjen%C4%8Danja-Split-200797623455599/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/sajam-vjencanja-wedding-day-split.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/sajam-vjencanja-wedding-day-split.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/sajam-vjencanja-wedding-day-split.png" alt="Wedding Day Split" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>

              <?php if(false): ?>
							<div class="col-6">

								<a href="https://www.radiosunce.com.hr/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/Radio-Sunce.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/Radio-Sunce.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/Radio-Sunce.png" alt="Radio Sunce" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>
              <?php endif; ?>
              
						</div>

					</div>

					<div class="col-md-6">

						<div class="row">

							<div class="col-6">

								<a href="http://isdeutsche.hr/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/IS-Design-IS-Deutsche.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/IS-Design-IS-Deutsche.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/IS-Design-IS-Deutsche.png" alt="IS Deutsche" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>
              
              <div class="col-6">

								<a href="https://www.podstanar.hr/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/Podstanar.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/Podstanar.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/Podstanar.png" alt="Podstanar.hr" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>

              <?php if(false): ?>
							<div class="col-4">

								<a href="https://kruznitok.hr/" target="_blank" rel="nofollow noopener noreferrer">

									<img data-desktop-src="https://www.static.szv.com.hr/img/partner/auto-skola-kruzni-tok-split.png" data-tablet-src="https://www.static.szv.com.hr/img/partner/auto-skola-kruzni-tok-split.png" data-mobile-src="https://www.static.szv.com.hr/img/partner/auto-skola-kruzni-tok-split.png" alt="Auto Škola Kružni Tok Split" data-width="300" data-height="150" data-ratio="2" class="lazy-load-trigger-img" />

								</a>

							</div>
              <?php endif; ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>



</section>

<!-- Section Partners / End -->