<!-- Section Home Categories

================================================== -->

<section id="home-categories-container">

<div class="container-fluid no-padding">		

	<div id="home-categories">

		<h2 class="text-center"><?php _e("Sve Za Vjenčanje na jednom mjestu", 'albadiem'); ?></h2>

		<hr />

		<div id="home-categories-slider" class="lazy-load-slider">

				<?php

					$argsa = array(
							'parent' => 0,
							'hide_empty' => false
					);
					/**
					 * @var $categories WP_Term[]
					 */
					$categories = get_categories($argsa);

				?>

				<?php

				$excludeCat = [
						'uncategorized', 
						'vjencanje', 
						'wedding', 
						'tamburasi', 
						'tambouras', 
						'mali-restorani', 
						'small-restaurants', 
						'izdvajamo',
						'interesting'
				];

				foreach ($categories as $category): ?>

					<?php 

							$args = array(
								'post_type' => 'post',
								'posts_per_page' => 1,
								'orderby' => 'publish_date',
								'order' => 'DESC',
								'post_status' => array('publish'),
								// 'no_found_rows' => true, 
								// 'update_post_meta_cache' => false, 
								// 'update_post_term_cache' => false, 
								'tax_query' => array(
										array(
											'taxonomy' => 'category',
											'terms' => array($category->term_id),
											'field' => 'term_id',
											'include_children' => false,
											'operator' => 'AND'
										)
									)
							);
					?>

					<?php if (!in_array($category->slug, $excludeCat)): ?>
						<?php

							//var_dump($category);
							$the_query = new WP_Query($args);

							//var_dump($the_query);

							if($the_query->have_posts()):
								while($the_query->have_posts()):
									$the_query->the_post(); 

									//var_dump($post->ID);
									
									if(have_rows('images_providerImages')):
										while(have_rows('images_providerImages')) : the_row(); ?>

											<?php

												$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
												$providerImgDesktopUrl = get_sub_field('providerImgDesktopUrl');
												$providerImgTabletUrl = get_sub_field('providerImgTabletUrl');
												$providerImgMobileUrl = get_sub_field('providerImgMobileUrl');
												$providerImgWidth = get_sub_field('providerImgWidth');
												$providerImgHeight = get_sub_field('providerImgHeight');
												$providerImgAlt = get_sub_field('providerImgAlt');
												$providerImgDesc = get_sub_field('providerImgDesc');
												$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
												$providerImgTargetContainer = get_sub_field('providerImgTargetContainer');
												$providerImgRatio = get_sub_field('providerImgRatio');
												$providerImgClass = get_sub_field('providerImgClass');
												$providerImgQueryStringVersion = get_sub_field('providerImgQueryStringVersion');

												break;

											?>

										<?php endwhile; ?>
							
									<?php endif; ?>
									
									<div class="home-categories-box bg-img carousel-cell">
										<div class="home-categories-box-in follow-href" data-flickity-bg-lazyload="<?php echo $providerImgMobileUrl; ?>" data-href="<?php echo get_category_link( $category->term_id ); ?>#listing">
											<div class="category-home-bg-box center-v-h">
												<h3><a href="<?php echo get_category_link( $category->term_id ); ?>#listing"><?php echo $category->name; ?></a></h3>
											</div>
										</div>
									</div>

								<?php endwhile; ?>

							<?php endif; ?>		

					<?php endif; ?>			

				<?php endforeach; ?>


				
				<?php if(false): ?>

				<?php 

					
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 10,
						// 'no_found_rows' => true, 
						// 'update_post_meta_cache' => false, 
						// 'update_post_term_cache' => false, 
						'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'terms' => 'vjencanje',
									'field' => 'slug',
									'include_children' => false,
        							'operator' => 'AND'
								)
							)
					);

					$the_query = new WP_Query($args);
					if($the_query->have_posts()):
						while($the_query->have_posts()):
							$the_query->the_post(); 
							
							if(have_rows('mainImg')):
								while(have_rows('mainImg')) : the_row(); 
								
									$mainImgOriginalUrl = get_sub_field('mainImgOriginalUrl');
									$mainImgWidth = get_sub_field('mainImgWidth');
									$mainImgHeight = get_sub_field('mainImgHeight');

									$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
									$providerImgDesktopUrl = get_sub_field('providerImgDesktopUrl');
									$providerImgTabletUrl = get_sub_field('providerImgTabletUrl');
									$providerImgMobileUrl = get_sub_field('providerImgMobileUrl');
									$providerImgWidth = get_sub_field('providerImgWidth');
									$providerImgWidth = get_sub_field('providerImgHeight');
									$providerImgAlt = get_sub_field('providerImgAlt');
									$providerImgDesc = get_sub_field('providerImgDesc');
									$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
									$providerImgTargetContainer = get_sub_field('providerImgTargetContainer');
									$providerImgRatio = get_sub_field('providerImgRatio');
									$providerImgClass = get_sub_field('providerImgClass');
									$providerImgQueryStringVersion = get_sub_field('providerImgQueryStringVersion');
							?>

									<div class="home-categories-box bg-img follow-href carousel-cell" 
									data-href="<?php the_permalink(); ?>" 
									style="
									width:<?php echo ($mainImgWidth) ? $mainImgWidth . 'px' : '400px'; ?>; 
									height:<?php echo ($mainImgHeight) ? $mainImgHeight . 'px' : '300px'; ?>; 
									background-image: url('<?php echo ($mainImgOriginalUrl) ? $mainImgOriginalUrl : get_template_directory_uri() . "/assets/img/provider/img/1.jpg"; ?>');
									background-size: cover;
									">
							
							<?php

								endwhile;
							endif;
							
							?>	
								<div class="category-home-bg-box center-v-h">

									<h3><a href="<?php the_permalink(); ?>"><?php echo get_field('shortName') ?></a></h3>

								</div>
							</div>
			<?php


							

						endwhile;
					endif;
			?>

				<?php endif; ?>
	
		</div>

	</div>

</div>

</section>

<!-- Section Home Categories / End -->