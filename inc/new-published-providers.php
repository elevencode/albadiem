<!-- Section New providers
================================================== -->
<section id="new-providers-container">
	<div class="container-fluid">
		<div class="container">
			<div id="new-providers">
				<h2 class="text-center">Zadnji objavljeni profili</h2>
				<hr />
				<div class="row">
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-providers-box">
							<div class="new-providers-img follow-href" data-href="https://www.cdn385.com.hr/?v=1" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-providers-title"><a href="">Restoran Gusar</a></div>
							<div class="new-providers-desc">Proslavite svoj zavjet ljubavi s Vama dragim osobama uz vrhunsku uslugu i bogatu gastronomsku ponudu u ugodnom ambijent...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-providers-box">
							<div class="new-providers-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-providers-title"><a href="">Dvorana za vjenčanja Katarina</a></div>
							<div class="new-providers-desc">Za svadbenu svečanost sa stilom u luksuznoj dvorani neposredno pokraj Splita - Hotel Katarina - Sala za vjenčanja Katarina...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-providers-box">
							<div class="new-providers-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-providers-title"><a href="">Restoran Gusar</a></div>
							<div class="new-providers-desc">Proslavite svoj zavjet ljubavi s Vama dragim osobama uz vrhunsku uslugu i bogatu gastronomsku ponudu u ugodnom ambijent...</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-xl-3">
						<div class="new-providers-box">
							<div class="new-providers-img follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="" data-tablet-src="" data-desktop-src="" data-width="" data-height="" data-ratio="1" data-class="" data-alt="" data-title=""></div>
							<div class="new-providers-title"><a href="">Dvorana za vjenčanja Katarina</a></div>
							<div class="new-providers-desc">Za svadbenu svečanost sa stilom u luksuznoj dvorani neposredno pokraj Splita - Hotel Katarina - Sala za vjenčanja Katarina...</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Section New providers / End -->