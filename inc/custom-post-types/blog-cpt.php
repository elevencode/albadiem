<?php 

/**
 * Register a custom post type called "Blog posts".
 *
 * @see get_post_type_labels() for label keys.
 */

function wpdocs_codex_blog_init() {
    $labels = array(
        'name'                  => _x( 'Blog post', 'Post type general name', 'albadiem' ),
        'singular_name'         => _x( 'Blog posts', 'Post type singular name', 'albadiem' ),
        'menu_name'             => _x( 'Blog post', 'Admin Menu text', 'albadiem' ),
        'name_admin_bar'        => _x( 'Blog posts', 'Add New on Toolbar', 'albadiem' ),
        'add_new'               => __( 'Add New', 'albadiem' ),
        'add_new_item'          => __( 'Add New Blog posts', 'albadiem' ),
        'new_item'              => __( 'New Blog posts', 'albadiem' ),
        'edit_item'             => __( 'Edit Blog posts', 'albadiem' ),
        'view_item'             => __( 'View Blog posts', 'albadiem' ),
        'all_items'             => __( 'All Blog post', 'albadiem' ),
        'search_items'          => __( 'Search Blog post', 'albadiem' ),
        'parent_item_colon'     => __( 'Parent Blog post:', 'albadiem' ),
        'not_found'             => __( 'No Blog post found.', 'albadiem' ),
        'not_found_in_trash'    => __( 'No Blog post found in Trash.', 'albadiem' ),
        'featured_image'        => _x( 'Blog posts Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'albadiem' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'albadiem' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'albadiem' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'albadiem' ),
        'archives'              => _x( 'Blog posts archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'albadiem' ),
        'insert_into_item'      => _x( 'Insert into Blog posts', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'albadiem' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Blog posts', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'albadiem' ),
        'filter_items_list'     => _x( 'Filter Blog post list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'albadiem' ),
        'items_list_navigation' => _x( 'Blog post list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'albadiem' ),
        'items_list'            => _x( 'Blog post list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'albadiem' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'blog', 'with_front' => false),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'taxonomies'         => array( 'category'),
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
    );
 
    register_post_type( 'blog', $args );
    
    //flush_rewrite_rules();
}
 
add_action( 'init', 'wpdocs_codex_blog_init' );
