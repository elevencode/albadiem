<?php

	// Set timezone
	date_default_timezone_set('Europe/Zagreb');
	
	// Define global site var
	class siteHelper {
		
		protected $site = [];
 
		public function __construct(){
			$this->setEnv();
		}

		public function setEnv(){		
			switch($_SERVER['HTTP_HOST']) {
				case 'albadiem.com':
				case 'www.albadiem.com':
					$this->site['root'] = '/home/cdn385com/albadiem.com/';
					$this->site['path'] = 'albadiem.com';
					$this->site['domain'] = 'www.albadiem.com';
					$this->site['fullPath'] = 'https://www.albadiem.com';
					$this->site['fullPathSlash'] = 'https://www.albadiem.com/';
					$this->site['isLive'] = true;
					$this->site['environment'] = 'live';
					break;
				case 'albadiemtest.cdn385.com.hr':
				case 'www.albadiemtest.cdn385.com.hr':
					$this->site['root'] = '/home/cdn385com/albadiemtest.cdn385.com.hr/';
					$this->site['path'] = 'albadiemtest.cdn385.com.hr';
					$this->site['domain'] = 'www.albadiemtest.cdn385.com.hr';
					$this->site['fullPath'] = 'https://www.albadiemtest.cdn385.com.hr';
					$this->site['fullPathSlash'] = 'https://www.albadiemtest.cdn385.com.hr/';
					$this->site['isLive'] = false;
					$this->site['environment'] = 'test';
					break;
				case 'albadiem.develop':
				case 'www.albadiem.develop':
					$this->site['root'] = 'C:/xampp/htdocs/albadiem.develop/';
					$this->site['path'] = 'albadiem.develop';
					$this->site['domain'] = 'www.albadiem.develop';
					$this->site['fullPath'] = 'https://www.albadiem.develop';
					$this->site['fullPathSlash'] = 'https://www.albadiem.develop/';
					$this->site['isLive'] = false;
					$this->site['environment'] = 'local';
					break;
				default:
					$this->site['root'] = '/home/cdn385com/albadiem.com/';
					$this->site['path'] = 'albadiem.com';
					$this->site['domain'] = 'www.albadiem.com';
					$this->site['fullPath'] = 'https://www.albadiem.com';
					$this->site['fullPathSlash'] = 'https://www.albadiem.com/';
					$this->site['isLive'] = true;
					$this->site['environment'] = 'live';
					break;
			}
		}
		
		public function getEnvironmentVar($varName){
			$response = '';
			if(isset($this->site[$varName]) && !empty($this->site[$varName])){
				$response = $this->site[$varName];
			}else{
				exit("Environment error!");
			}			
			return $response;
		}
		
	};
	
	// Define siteObj
	global $siteObj;
	$siteObj = new siteHelper;
	
	
