<!-- CTA Create Profile Contatiner -->
<?php
	global $szvData;
?>

<div id="cta-create-profile-container">



	<!-- CTA Create Profile -->

	<div class="container-fluid no-padding">

		<div id="cta-create-profile">

			<p>
				<a href="<?php echo $szvData['siteSettings']['topBarMarketingLink']; ?>">
			
					<?php _e('Kreiraj profil', 'albadiem'); ?> 
					<span class="color-secondary"><strong><?php _e('ODMAH', 'albadiem'); ?></strong></span> 
					<?php _e('i počni primati', 'albadiem'); ?> 
					<span class="color-secondary"><strong><?php _e('UPITE', 'albadiem'); ?></strong></span> <?php _e('direktno', 'albadiem'); ?>!
					<br /><span class="color-secondary"><strong><?php _e('BEZ PROVIZIJE', 'albadiem'); ?></strong></span>
				
				</a>
			</p>

		</div>

	</div>

	<!-- CTA Create Profile / End -->



</div>

<!-- CTA Create Profile Contatiner / End -->