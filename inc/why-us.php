<!-- Why Us
================================================== -->
<div id="why-us-container">

	<div class="container-fluid">
		<div class="container">
			<div id="why-us">
				<div class="row">
					<div class="col-6 col-lg-3 no-padding">
						<div class="why-us-box">
							<div class="why-us-box-icon"><span class="fa fa-filter"></span><?php if(false): ?><span class="im im-icon-Filter-2"></span><?php endif; ?></div>
							<div class="why-us-box-title"><?php _e('Brzo i jednostavno', 'albadiem'); ?></div>
							<div class="why-us-box-desc"><?php _e('Odaberi državu | grad | više od 30 kategorija.', 'albadiem'); ?></div>
						</div>
					</div>
					<div class="col-6 col-lg-3 no-padding">
						<div class="why-us-box">
							<div class="why-us-box-icon"><span class="fa fa-comment"></span><?php if(false): ?><span class="im im-icon-Checked-User"></span><?php endif; ?></div>
							<div class="why-us-box-title"><?php _e('Odaberi samo najbolje', 'albadiem'); ?></div>
							<div class="why-us-box-desc"><?php _e('Uz ocjene i komentare pronađi najbolju uslugu.', 'albadiem'); ?></div>
						</div>
					</div>
					
					<div class="col-6 col-lg-3 no-padding">
						<div class="why-us-box">
							<div class="why-us-box-icon"><span class="fa fa-envelope"></span><?php if(false): ?><span class="im im-icon-Envelope-2"></span><?php endif; ?></div>
							<div class="why-us-box-title"><?php _e('Direktni kontakt', 'albadiem'); ?></div>
							<div class="why-us-box-desc"><?php _e('Kontaktiraj pružatelje usluga direktno, BEZ PROVIZIJE.', 'albadiem'); ?></div>
						</div>
					</div>
					<div class="col-6 col-lg-3 no-padding">
						<div class="why-us-box">
							<div class="why-us-box-icon"><span class="fa fa-user-plus"></span><?php if(false): ?><span class="im im-icon-Add-UserStar"></span><?php endif; ?></div>
							<div class="why-us-box-title"><?php _e('Multi Upit', 'albadiem'); ?></div>
							<div class="why-us-box-desc"><?php _e('Pošalji multi upit odjednom više pružatelja usluga.', 'albadiem'); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
</div>
<!-- Why Us / End -->