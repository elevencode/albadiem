<!-- Advertising Home Reviews

================================================== -->

<div id="advertising-home-reviews-container">

	<div id="advertising-home-reviews" class="advertising-banner advertising-1140 close-container hide">

		<?php  $advertisementHomeBannerLarge = get_field('advertisementHomeBannerLarge', 'option'); ?>

		<a href="<?php echo $advertisementHomeBannerLarge['advertisementHomeBannerLargeURL']; ?>" target="_blank" rel="nofollow noopener noreferrer">

			<img data-mobile-src="<?php echo $advertisementHomeBannerLarge['advertisementHomeBannerLargeMobileImage']; ?>" data-tablet-src="<?php echo $advertisementHomeBannerLarge['advertisementHomeBannerLargeTabletImage']; ?>" data-desktop-src="<?php echo $advertisementHomeBannerLarge['advertisementHomeBannerLargeDesktopImage']; ?>" data-width="1140" data-height="250" data-ratio="4.56" class="lazy-load-trigger-img" />

		</a>

		<?php if(false): ?><div class="advertising-close-trigger"><div class="advertising-close"></div></div><?php endif; ?>

	</div>


</div>

<!-- Advertising Home Reviews / End -->