<!-- Category Top Adv
================================================== -->
<div id="category-top-adv-container">
	<div class="category-top-adv-title">
		<h4>Izdvojeni oglasi</h4>
	</div>
	<div id="category-top-adv" class="carousel pos-relative">
				
		<?php for($i=1; $i!=8; $i++): ?>
			<div id="shuffle-<?php echo $i; ?>" class="shuffle carousel-cell bg-eee" data-flickity-bg-lazyload="/img/provider/img/1.jpg" style="width:300px; height:200px;">
				<div class="category-top-adv-box pos-relative follow-href" data-href="https://www.cdn385.com.hr">
					<div class="category-top-adv-box-bg">
						<h3><a href=""><?php echo $i; ?> Restoran Aria</a></h3>
					</div>
					<div class="category-top-adv-logo">
						<div class="category-top-adv-provider-logo follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
					</div>	
					<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
				</div>
			</div>
		<?php endfor; ?>
		
		<?php for($i=8; $i!=16; $i++): ?>
			<div id="shuffle-<?php echo $i; ?>" class="shuffle carousel-cell bg-eee" data-flickity-bg-lazyload="/img/provider/img/3.jpg" style="width:200px; height:200px;">
				<div class="category-top-adv-box pos-relative follow-href" data-href="https://www.cdn385.com.hr">
					<div class="category-top-adv-box-bg">
						<h3><a href=""><?php echo $i; ?> Restoran Hotel Sala za vjenčanje Hotel Kamen Dicmo</a></h3>
					</div>
					<div class="category-top-adv-logo">
						<div class="category-top-adv-provider-logo follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
					</div>	
					<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
				</div>
			</div>
		<?php endfor; ?>
		
	</div>
</div>
<!-- Category Top Adv / End -->


<?php if(false): ?>
<section id="category-top-adv-container">
	<div class="container-fluid">
		<div class="container">
			<div id="category-top-adv">
				<h2 class="text-center">SZV Superiška oglasi</h2>
				<hr />

				<div class="row">
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-1" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Sala za vjenčanje Aria</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>	
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>									
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-2" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Gusar Split</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kaštel Novi - Dubrovnik</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-3" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Sala za vjenčanje Kamen Dicmo</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kamen Dicmo - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-4" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Dvorana za vjenčanja Katarina Dugopolje</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-5" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Velum</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>	
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>									
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-6" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Ino</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kaštel Novi - Dubrovnik</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-7" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Sala za vjenčanje Just Say Yes</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kamen Dicmo - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-8" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Poljud</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-9" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Velum</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>	
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>									
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-10" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Ino</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kaštel Novi - Dubrovnik</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 no-padding">
						<div class="row">
							<div id="shuffle-11" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Sala za vjenčanje Just Say Yes</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Kamen Dicmo - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
							<div id="shuffle-12" class="shuffle col-md-6 no-padding">
								<div class="category-top-adv-box container-fluid no-padding bg-img pos-relative follow-href" data-href="https://www.cdn385.com.hr" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title="">
									<div class="category-top-adv-box-bg">
										<h3><a href="">Restoran Poljud</a></h3>
									</div>
									<div class="category-top-adv-logo">
										<div class="category-top-adv-provider-logo" data-mobile-src="img/bg/country/vjencanje-hrvatska-mobile.jpg" data-tablet-src="img/bg/country/vjencanje-hrvatska-tablet.jpg" data-desktop-src="country/vjencanje-hrvatska.jpg" data-width="1200" data-height="1200" data-ratio="1" data-class="" data-alt="Vjenčanje u Hrvatskoj" data-title=""></div>
									</div>
									<div class="category-top-adv-location"><p>Dugopolje - Split</p></div>	
									<div class="add-to-favorites"><div class="add-to-favorites-trigger"></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</section>
<?php endif; ?>