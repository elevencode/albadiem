<!-- Advertising Top Bar

================================================== -->
<?php

	global $szvData;

?>

<?php if(false): ?>
<div id="advertising-top-bar-container" class="bg-szv-pattern">

	<div id="advertising-top-bar" class="advertising-banner advertising-1140 close-container hide">

		<?php  $advertisementTopBar = get_field('advertisementTopBar', 'option'); ?>

		<a href="<?php echo $advertisementTopBar['advertisementTopBarURL']; ?>" target="_blank" rel="nofollow noopener noreferrer">

			<img alt="<?php echo $advertisementTopBar['advertisementTopBarURL']; ?>" data-mobile-src="<?php echo $advertisementTopBar['advertisementTopBarMobileImage']; ?>" data-tablet-src="<?php echo $advertisementTopBar['advertisementTopBarTabletImage']; ?>" data-desktop-src="<?php echo $advertisementTopBar['advertisementTopBarDesktopImage']; ?>" data-width="1140" data-height="250" data-ratio="4.56" class="lazy-load-trigger-img" />

		</a>

		<div class="advertising-close-trigger"><div class="advertising-close"></div></div>

	</div>

</div>
<?php endif; ?>

<div id="advertising-top-bar-container" class="bg-szv-pattern">

	<div id="advertising-top-bar" class="advertising-banner advertising-728 close-container hide">

		<?php  $advertisementTopBar = get_field('advertisementTopBar', 'option'); ?>

		<a href="<?php echo $advertisementTopBar['advertisementTopBarURL']; ?>" target="_blank" rel="nofollow noopener noreferrer">

			<img alt="<?php echo $advertisementTopBar['advertisementTopBarURL']; ?>" data-mobile-src="<?php echo $advertisementTopBar['advertisementTopBarMobileImage']; ?>" data-tablet-src="<?php echo $advertisementTopBar['advertisementTopBarTabletImage']; ?>" data-desktop-src="<?php echo $advertisementTopBar['advertisementTopBarDesktopImage']; ?>" data-width="728" data-height="210" data-ratio="3.47" class="lazy-load-trigger-img" />

		</a>

		<div class="advertising-close-trigger"><div class="advertising-close"></div></div>

	</div>

</div>

<!-- Advertising Top Bar / End -->