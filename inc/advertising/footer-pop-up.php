<!-- Footer Pop Up
================================================== -->
<div id="advertising-footer-pop-up-container">
	<div id="advertising-footer-pop-up" class="advertising-banner advertising-1140 close-container hide">
		
		<?php  $advertisementFooterPopUp = get_field('advertisementFooterPopUp', 'option'); ?>

		<a href="<?php echo $advertisementFooterPopUp['advertisementFooterPopUpURL']; ?>" target="_blank" rel="nofollow noopener noreferrer">

			<img data-mobile-src="<?php echo $advertisementFooterPopUp['advertisementFooterPopUpMobileImage']; ?>" data-tablet-src="<?php echo $advertisementFooterPopUp['advertisementFooterPopUpTabletImage']; ?>" data-desktop-src="<?php echo $advertisementFooterPopUp['advertisementFooterPopUpDesktopImage']; ?>" data-width="1140" data-height="250" data-ratio="4.56" class="lazy-load-trigger-img" />

		</a>

		<div class="advertising-close-trigger"><div class="advertising-close"></div></div>

	</div>
</div>
<!-- Footer Pop Up / End -->
