<!-- Advertising Home Content

================================================== -->

<?php if(false): ?>
<div id="advertising-home-content-container">

	<div id="advertising-home-content" class="advertising-banner advertising-728 close-container hide">

		<?php  $advertisementHomeBannerSmall = get_field('advertisementHomeBannerSmall', 'option'); ?>

		<a href="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallURL']; ?>" target="_blank" rel="nofollow noopener noreferrer" class="eu-fonds">

			<img data-mobile-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallMobileImage']; ?>" data-tablet-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallTabletImage']; ?>" data-desktop-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallDesktopImage']; ?>" data-width="728" data-height="210" data-ratio="3.47" class="lazy-load-trigger-img" />

		</a>

		<?php if(false): ?><div class="advertising-close-trigger"><div class="advertising-close"></div></div><?php endif; ?>

	</div>

</div>
<?php endif; ?>

<div id="advertising-home-content-container">

	<div id="advertising-home-content" class="advertising-banner advertising-1140 close-container hide">

		<?php  $advertisementHomeBannerSmall = get_field('advertisementHomeBannerSmall', 'option'); ?>

		<a href="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallURL']; ?>" target="_blank" rel="nofollow noopener noreferrer" class="eu-fonds">

			<img data-mobile-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallMobileImage']; ?>" data-tablet-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallTabletImage']; ?>" data-desktop-src="<?php echo $advertisementHomeBannerSmall['advertisementHomeBannerSmallDesktopImage']; ?>" data-width="1140" data-height="250" data-ratio="4.56" class="lazy-load-trigger-img" />

		</a>

		<?php if(false): ?><div class="advertising-close-trigger"><div class="advertising-close"></div></div><?php endif; ?>

	</div>

</div>






<!-- Advertising Home Content / End -->