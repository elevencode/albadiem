<!-- Header Container

================================================== -->
<?php
	global $szvData; 
?>

<header id="header-container">


	<?php get_template_part( 'inc/nav/top-bar' ); ?>

	<!-- Header -->
	<div id="header" data-margin-top="0" <?php if(false): ?>data-sticky-for="1023"<?php endif; ?> data-sticky-class="is-sticky">
		<div class="container-fluid no-padding">
			<div class="row">
			
				<div id="logo-col" class="col-2">
					<!-- Logo -->
					<div id="logo">
						<a href="<?php echo get_option("siteurl"); ?>/"><img src="<?php echo $szvData['siteSettings']['logoImgSrc']; ?>" alt="<?php echo $szvData['siteSettings']['siteName']; ?>" /></a>
					</div>
				</div>
				
				<div id="change-nav-position-attr" class="col-10">
					
					<!-- Mobile Navigation -->
					<div id="mmenu-container">
						<div id="mmenu">
							<div id="mmenu-trigger"><span id="mmenu-icon" class="fa fa-bars"></span></div>
						</div>
					</div>
					
					<!-- Cta Search Nav -->
					<?php get_template_part( 'inc/nav/cta-search-nav' ); ?>
					
					<!-- Nav & Lang Nav -->
					<div id="nav-lang-nav-container">
						<div id="nav-lang-nav-container-row" class="row">
							<!-- Nav & Lang Nav -->
							<div class="col-lg-11">	
								<!-- Nav -->
								<?php get_template_part( 'inc/nav/nav-'.$szvData['wpLangCode'] ); ?>
							</div>
							<div class="col-lg-1">	
								<!-- Lang selector -->
								<?php get_template_part( 'inc/nav/lang-nav' ); ?>
							</div>
						</div>
					</div>
				</div>
		
			</div>
		</div>
	</div>
	<!-- Header / End -->

	<?php if(false): ?>
	<!-- Header -->
	<div id="header" data-margin-top="0" <?php if(false): ?>data-sticky-for="1023"<?php endif; ?> data-sticky-class="is-sticky">

		<div class="container-fluid no-padding">

			<div class="row">

			

				<div id="logo-col" class="col-2">

					<!-- Logo -->

					<div id="logo">

						<a href="<?php echo $szvData['siteSettings']['logoHref']; ?>"><img src="<?php echo $szvData['siteSettings']['logoImgSrc']; ?>" alt="<?php echo $szvData['siteSettings']['siteName']; ?>" /></a>

					</div>

				</div>

				

				<div id="change-nav-position-attr" class="col-10">

					

					<!-- Mobile Navigation -->

					<div id="mmenu-container">

						<div id="mmenu">

							<div id="mmenu-trigger"><span id="mmenu-icon" class="fa fa-bars"></span></div>

						</div>

					</div>

					<!-- Cta Search Nav -->

					<?php get_template_part( 'inc/nav/cta-search-nav' ); ?>

					<!-- Nav & Lang Nav -->

					<div id="nav-lang-nav-container">

						<div id="nav-lang-nav-container-row" class="row">

							<!-- Nav & Lang Nav -->

							<div class="col-lg-11">	

								<!-- Nav -->

								<?php get_template_part( 'inc/nav/nav' ); ?>

							</div>

							<div class="col-lg-1">	

								<!-- Lang selector -->

								<?php get_template_part( 'inc/nav/lang-nav' ); ?>

							</div>

						</div>

					</div>

				</div>

		

			</div>

		</div>

	</div>
	<!-- Header / End -->
	<?php endif; ?>
	



</header>

<!-- Header Container / End -->