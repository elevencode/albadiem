<!-- Section Banner Search

================================================== -->

<?php 
	global $szvData;
?>

<section id="banner-search-container">

	<div id="banner-search" class="container-fluid no-padding lazy-load-trigger-bg-img banner-search-404" data-bg="" data-mobile-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-tablet-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-desktop-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-width="1920" data-height="1280" data-ratio="1.5">

		<div id="errorheader">

			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<h1><?php echo get_the_title(); ?></h1>

					</div>	
				</div>
			</div>

		</div>

	</div>

	

</section>

<!-- Section Banner Search / End -->