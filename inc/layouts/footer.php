	

	<!-- Footer Container

	================================================== -->

	<?php

		global $szvData;

	?>

	<footer id="footer-containter">

		<!-- Main -->

		<div class="container">

			<div class="row">

				<div class="col-lg-5">

					<div class="footer-box footer-box-logo">

						<img data-desktop-src="<?php echo $szvData['siteSettings']['logoImgSrc']; ?>" data-tablet-src="<?php echo $szvData['siteSettings']['logoImgSrc']; ?>" data-mobile-src="<?php echo $szvData['siteSettings']['logoImgSrc']; ?>" alt="<?php echo $szvData['siteSettings']['siteName']; ?>" data-width="190" data-height="70" data-ratio="2.71" class="lazy-load-trigger-img" />

						<br>

						<p><?php _e('SZV portal, online zajednica na kojoj mladenci mogu jednostavno i brzo organizirati svoje vjenčanje iz snova.', 'albadiem'); ?></p>

					</div>

				</div>



				<div class="col-lg-7">

					<div id="scroll-x-footer-box">

						<div class="footer-box footer-box-useful">

						<h4><?php _e('Info', 'albadiem'); ?></h4>

						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer_widget_1',
								'menu_class'     => 'footer-ul',
								'walker' => new Footer_Walker()
							 ) );
						?>

						</div>



						<div class="footer-box footer-box-more">

						<?php
						
						?>

							<h4><?php _e('Saznaj&nbsp;više', 'albadiem'); ?></h4>

							<?php
								wp_nav_menu( array(
									'theme_location' => 'footer_widget_2',
									'menu_class'     => 'footer-ul',
									'walker' => new Footer_Walker()
								) );
							?>

						</div>



						<div class="footer-box footer-box-info">

						<?php
						
						?>

							<h4><?php _e('Korisno', 'albadiem'); ?></h4>

							<?php
								wp_nav_menu( array(
									'theme_location' => 'footer_widget_3',
									'menu_class'     => 'footer-ul',
									'walker' => new Footer_Walker()
								) );
							?>

						</div>

						<div class="cf"></div>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-lg-12">

					<div id="eu-fonds">

						<a href="https://strukturnifondovi.hr/" target="_blank" rel="noopener noreferrer">
							<img data-desktop-src="https://www.img.szv.com.hr/img/advertising/<?php echo $szvData['wpLangCode']; ?>/eu-white.svg"  data-tablet-src="https://www.img.szv.com.hr/img/advertising/<?php echo $szvData['wpLangCode']; ?>/eu-white.svg" data-mobile-src="https://www.img.szv.com.hr/img/advertising/<?php echo $szvData['wpLangCode']; ?>/eu-white.svg" alt="EU" data-width="728" data-height="210" data-ratio="3.47" class="lazy-load-trigger-img" />
						</a>

					</div>

				</div>

				<div class="col-lg-12">

					<ul class="footer-ul-social-icons">

						<li><a class="facebook" href="<?php echo $szvData['siteSettings']['szvFacebookUrl']; ?>"><span class="fa fa-facebook"></span></a></li>

						<li><a class="instagram" href="<?php echo $szvData['siteSettings']['szvInstagramUrl']; ?>"><span class="fa fa-instagram"></span></a></li>

					</ul>

				</div>

			</div>

			

			<!-- Copyright -->

			<div class="row">

				<div class="col-lg-12">
					<?php
						$footerDate = new DateTime();
						$footerYear = $footerDate->format('Y');
					?>
					<div id="copyright">© 2017. - <?php echo $footerYear; ?>. Sve Za Vjenčanje :: Alba Diem. <?php _e('Sva prava pridržana', 'albadiem'); ?>.</div>
				</div>
			</div>
		</div>



	</footer>

	<!-- Footer Container / End -->
	
</div>

<!-- Wrapper / End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Loader Icon -->
<?php if(false): ?>
<div id="loader-icon-preload">
							
		<!-- Loader -->
		<img src="<?php echo $szvData['loaderIconUrl']; ?>" class="invisible" />

</div>
<?php endif; ?>


<!-- Footer pop up -->
<?php get_template_part( 'inc/advertising/footer-pop-up' ); ?>

<?php wp_footer(); ?>

<?php if(!$szvData['budleJs']): ?>
<script src="https://www.static.szv.com.hr/js/inviewport/inviewport.js"></script>
<script src="https://www.static.szv.com.hr/js/zenscroll/zenscroll.min.js"></script>
<script src="https://www.static.szv.com.hr/js/main.js?v=5"></script>

<script src="https://www.static.szv.com.hr/js/skip-link-focus-fix.js"></script>
<script src="https://www.static.szv.com.hr/js/cookiejs/cookie.min.js"></script>
<script src="https://www.static.szv.com.hr/js/flickity/flickity.min.js"></script>
<script src="https://www.static.szv.com.hr/js/flickity/bg-lazyload.js"></script>
<script src="https://www.static.szv.com.hr/js/sweetalert2/sweetalert2.min.js"></script>
<script src="https://www.static.szv.com.hr/js/sweetalert2/promise.min.js"></script>
<script src="https://www.static.szv.com.hr/js/sticky/sticky.min.js"></script>
<script src="https://www.static.szv.com.hr/js/phonevalidation/intl-tel-input-16.0.0/build/js/intlTelInput.min.js"></script>
<script src="https://www.static.szv.com.hr/js/phonevalidation/intl-tel-input-16.0.0/build/js/utils.js"></script>
<script src="https://www.static.szv.com.hr/js/async.js?v=5"></script>
<?php endif; ?>

<?php if(false): ?>	
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5999ec87dbb01a218b4dd590/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<?php endif; ?>

</body>

</html>