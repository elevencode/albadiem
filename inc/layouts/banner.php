<!-- Section Banner Search

================================================== -->

<section id="banner-search-container">

	<div id="banner-search" class="container-fluid no-padding bg-img" data-mobile-src="img/bg/vjencanje-par-mobile.jpg" data-tablet-src="img/bg/vjencanje-par-tablet.jpg" data-desktop-src="img/bg/vjencanje-par.jpg" data-width="1920" data-height="1280" data-ratio="1.5" data-class="" data-alt="Pronađi sve za svoje vjenčanje iz snova na jednom mjestu." data-title="" style="height: 200px">

		<div class="container" style="height:100%;">
			<div class="row" style="height:100%;">
				<div class="col-md-12" style="align-self: flex-end;">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
 	        </div>

		</div>

	</div>

</section>

<!-- Section Banner Search / End -->