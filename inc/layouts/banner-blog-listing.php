<!-- Section Banner Search

================================================== -->

<?php 
	global $szvData;
?>

<section id="banner-search-container">

	<div id="banner-search" class="container-fluid no-padding lazy-load-trigger-bg-img banner-search-404" data-bg="" data-mobile-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-tablet-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-desktop-src="https://www.static.szv.com.hr/img/bg/szv/szv-dark.png" data-width="1920" data-height="1280" data-ratio="1.5">

		<div id="errorheader">

			<div class="container">
				<div class="row">
					<div class="col-md-12">

							<h1>

								<?php

									global $wp_query;
									global $szvData;

									$blogListingTitlePrefix_1 = 'Blog '.$szvData['translations']['allForWedding'][$szvData['wpLangCode']].' '.$szvData['translations']['croatia'][$szvData['wpLangCode']];
									$blogListingTitlePrefix_2 = 'Blog ';

									if(is_paged()) {
											$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
											$blogListingTitle = $blogListingTitlePrefix_2.' <small>('.strtolower ($szvData['translations']['page'][$szvData['wpLangCode']]).' '.$paged.' '.$szvData['translations']['fromPagination'][$szvData['wpLangCode']].' '.$wp_query->max_num_pages.')</small>';
									}else{
										$blogListingTitle = $blogListingTitlePrefix_1;
									}

									echo $blogListingTitle;

								?>

							</h1>


					</div>	
				</div>
			</div>

		</div>

	</div>

	

</section>

<!-- Section Banner Search / End -->