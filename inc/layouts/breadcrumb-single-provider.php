<!-- Breadcrumb -->

<?php
	generateBreadcrumbSingleProvider();
?>

<?php if(false): ?>
<div id="breadcrumb-container">	
	<?php
		// https://yoast.com/wordpress/plugins/seo/api/
	?>
	<?php
		
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div id="breadcrumb" class="hide-breadcrumb">','</div>');
		}
	
	?>
	<span id="breadcrumb-expand"><span class="fa fa-plus-circle"></span></span>
</div>
<?php endif; ?>

<!-- Breadcrumb / End -->