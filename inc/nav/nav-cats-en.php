<?php
	global $szvData; 
?>
<div class="row">
	<div class="col-lg-4">
		<div class="nav-cat-container">
		
			<p>WEDDING DINNER</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>hotels#listing" title="Hotels">Hotels</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>restaurants-and-wedding-venues#listing" title="Restaurants and wedding venues">Restaurants and wedding venues</a></span>
        <?php if(false): ?><span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>small-restaurants#listing" title="Small restaurants">Small restaurants</a></span><?php endif; ?>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>catering#listing" title="Catering">Catering</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>rent-a-tent#listing" title="Rent a tent">Rent a tent</a></span>
				<div class="cf"></div>
			</div>	
      
      <p>MUSIC</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>bands#listing" title="Bands">Bands</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>dj#listing" title="DJ">DJ</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>trio-duo-accordion#listing" title="Trio / Duo / Accordion">Trio / Duo / Accordion</a></span>
        <?php if(false): ?><span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>tambouras#listing" title="Tambouras">Tambouras</a></span><?php endif; ?>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>church-music#listing" title="Church music">Church music</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>lighting-soundsystem-effects#listing" title="Lighting / Soundsystem / Effects">Lighting / Soundsystem / Effects</a></span>
				<div class="cf"></div>
			</div>
      
      <p>PHOTO &amp; VIDEO &amp; BOOTHS</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>photo-video#listing" title="Photo & Video">Photo & Video</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>photo-cabins#listing" title="Photo cabins">Photo cabins</a></span>
        <span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>selfie-mirror#listing" title="Selfie Mirror">Selfie Mirror</a></span>
				<div class="cf"></div>
			</div>
      	
      <p>WEDDING DRESSES &amp; MENS SUITS &amp; ACCESSORIES</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>wedding-dresses-and-accessories#listing" title="Wedding dresses &amp; accessories">Wedding dresses &amp; accessories</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>mens-suits-and-accessories#listing" title="Muška odijela &amp; dodaci">Men's suits &amp; accessories</a></span>
				<div class="cf"></div>
			</div>
			
      <?php if(false): ?>
			<p>PSSST... INTERESTING!</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>interesting#listing" title="interesting">Handpicked interesting stuff...</a></span>
				<div class="cf"></div>
			</div>
      <?php endif; ?>
			
		</div>
	</div>

	<div class="col-lg-4">
		<div class="nav-cat-container">
    
			<p>JEWELRY & ENGAGEMENT RINGS</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>jewelry-engagement-rings#listing" title="Jewelry / Engagement rings">Jewelry / Engagement rings</a></span>
				<div class="cf"></div>
			</div>
    
      <p>CAKES</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>cakes#listing" title="Cakes">Cakes</a></span>
				<div class="cf"></div>
			</div>

			<p>BEAUTY</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>makeup-nails-eyelashes#listing" title="Makeup &amp; Nails &amp; Eyelashes">Makeup &amp; Nails &amp; Eyelashes</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>hair-salons#listing" title="Hair Salons">Hair Salons</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>beauty-and-health-salons#listing" title="Saloni Ljepote i Zdravlja">Beauty and Health Salons</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>wellness-spa#listing" title="Wellness &amp; Spa">Wellness &amp; Spa</a></span>
				<div class="cf"></div>
			</div>
      
      <p>FLOWERS &amp; DECORATIONS &amp; ACCESSORIES</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>decoration-bouquets-lapel-garter-balloons#listing" title="Decorations / Bouquets / Lapels / Garters / Balloons">Decorations / Bouquets / Lapels / Garters / Balloons</a></span>
				<div class="cf"></div>
			</div>
      
      <p>INVITATIONS &amp; THANK YOU LETTERS &amp; GUEST LISTS</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>invitations-letter-of-thanks-and-guest-lists#listing" title="Invitations / Thank you notes / Guest lists">Invitations / Thank you notes / Guest lists</a></span>
				<div class="cf"></div>
			</div>	
      
		</div>
	</div>

	<div class="col-lg-4">
		<div class="nav-cat-container">
		
      <p>RENT A OLDTIMER & RENT A LIMO & TAXI</p>
			<div class="nav-cats">
        <span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>rent-a-oldtimer#listing" title="Rent a oldtimer">Rent a oldtimer</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>rent-a-limo#listing" title="Rent a car &amp; limo">Rent a limo</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>taxi-transfers#listing" title="Taxi transfers">Taxi transfers</a></span>
				<div class="cf"></div>
			</div>
      
      <p>BACHELORETTE & BACHELOR PARTY</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>rent-celebration-houses#listing" title="Kuće">Celebration Houses Rentals</a></span>
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>restaurants-taverns-bachelorette-and-bachelor-party#listing" title="Restaurants / Taverns">Restaurants / Taverns</a></span>
				<div class="cf"></div>
			</div>	
      
      <p>HONEYMOON</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>honeymoon#listing" title="Honeymoon">Honeymoon</a></span>
				<div class="cf"></div>
			</div>
			
			<p>GIFTS FOR NEWLYWEDS</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>gifts#listing" title="Gifts">Gifts</a></span>
				<div class="cf"></div>
			</div>					
			
			<p>ACCOMMODATION</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>accommodation#listing" title="Accommodation for newlyweds and guests">Accommodation for newlyweds and guests</a></span>
				<div class="cf"></div>
			</div>	
			
			<p>COURSES</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>dance-lessons#listing" title="Tečajevi plesa">Dance school</a></span>
				<?php if(true): ?><span class="nav-cat"><a href="<?php echo WP_3_SITEURL; ?>engagement-courses-info" title="Engagement courses">Engagement courses</a></span><?php endif; ?>
				<div class="cf"></div>
			</div>	

		</div>
	</div>
</div>	