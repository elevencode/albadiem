<?php
	global $szvData;
?>

<!-- Lang navigation -->
<div id="lang-nav-container">

	<!-- Lang Nav -->
	<ul id="lang-nav" class="dropdown">

		<li id="current-lang" class="mmenu-expand-dropdown">
			<a class="nav-first-level">
				<img data-mobile-src="https://www.static.szv.com.hr/img/flag/<?php echo $szvData['wpLangCode']; ?>.svg" data-tablet-src="https://www.static.szv.com.hr/img/flag/<?php echo $szvData['wpLangCode']; ?>.svg" data-desktop-src="https://www.static.szv.com.hr/img/flag/<?php echo $szvData['wpLangCode']; ?>.svg" data-width="26" data-height="26" data-ratio="1" class="lazy-load-trigger-img" />	
			</a>
			<ul id="other-langs">
				<?php if(false): ?><li><a href="#"><img src="https://www.static.szv.com.hr/img/flag/gb.svg" alt="English - Alba Diem" /></a></li><?php endif; ?>
				<li><?php echo customize_lang_menu(get_the_msls(array())); ?></li>
			</ul>
		</li>

		<?php
			/*
			echo get_locale();
			echo get_msls_flag_url( $language );
			echo get_msls_blog_description( $language );
			echo get_msls_permalink( $locale );
			*/
		?>

	</ul>	
	<!-- Lang Nav / End -->

</div>
<!-- Lang navigation / End -->