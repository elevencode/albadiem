<!-- Topbar -->
<?php
	global $szvData; 
?>
<div id="top-bar">



	<!-- Top Bar Widget -->

	<div id="top-bar-widget">

		<div class="container-fluid">

			<div class="row">

			
				<div class="col-10 no-padding">	

					<div class="cta-create-profile">
						<a href="<?php echo $szvData['siteSettings']['topBarMarketingLink']; ?>">
							<?php echo $szvData['siteSettings']['topBarMarketingLinkText']; ?>

							<?php if($szvData['siteSettings']['showFreeAdvertising']): ?>
								<?php //_e("Kreiraj profil BESPLATNO"); ?>
							<?php else: ?>
								<?php //_e("Objavi oglas"); ?>
							<?php endif; ?>
							
						</a>
					</div>

				</div>

				<div class="col-2 no-padding">

					<div id="favorites-container"><a href="<?php echo $szvData['siteSettings']['topBarFavoritesLink']; ?>" id="favorites"><span id="favorites-nmbr">0</span></a></div>

				</div>

			</div>

		</div>

	</div>

	<!-- Top Bar Widget / End -->



</div>

<!-- Topbar / End -->