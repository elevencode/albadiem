<!-- Main Navigation -->
<?php
	global $szvData; 
?>
<nav id="nav-container" class="pos-relative">

	<ul id="nav" class="dropdown">

		<li class="mmenu-expand-dropdown"><a class="nav-first-level prev-default">Dream Wedding...</a>
			<ul>
				<li><a href="<?php echo WP_3_SITEURL; ?>wedding#listing">in Croatia</a></li>
			</ul>
		</li>
		<li class="dropdown-fullwidth-container mmenu-expand-dropdown"><a class="nav-first-level prev-default">All For Wedding</a>
			<div class="dropdown-fullwidth">	
				<div class="dropdown-fullwidth-inner">
					<?php get_template_part( '/inc/nav/nav-cats-'.$szvData['wpLangCode'] ); ?>
				</div>
			</div>	
		</li>
		<li class="mmenu-expand-dropdown"><a href="<?php echo WP_3_SITEURL; ?>blog" class="nav-first-level prev-default">Blog</a></li>
    <li class="mmenu-expand-dropdown"><a href="<?php echo WP_3_SITEURL; ?>favorites" class="nav-first-level prev-default">Favorites</a></li>
		<li class="mmenu-expand-dropdown"><a href="<?php echo WP_3_SITEURL; ?>send-multi-inquiry" class="nav-first-level prev-default">Send multi inquiry</a></li>

	</ul>
	
</nav>
<!-- Main Navigation / End -->