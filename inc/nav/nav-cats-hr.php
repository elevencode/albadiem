<?php
	global $szvData; 
?>
<div class="row">
	<div class="col-lg-4">
		<div class="nav-cat-container">
		
			<p>SVADBENA VEČERA</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/hoteli#listing" title="Hoteli">Hoteli</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/restorani-sale#listing" title="Restorani &amp; Sale">Restorani &amp; Sale</a></span>
        <?php if(false): ?><span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/mali-restorani#listing" title="Mali restorani">Mali restorani</a></span><?php endif; ?>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/catering-usluge#listing" title="Catering">Catering</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/najam-satora#listing" title="Najam šatora">Najam šatora</a></span>
				<div class="cf"></div>
			</div>	
      
      <p>GLAZBA</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/bendovi#listing" title="Bendovi">Bendovi</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/dj-izvodaci#listing" title="DJ">DJ</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/trio-duo-harmonika#listing" title="Trio / Duo / Harmonika">Trio / Duo / Harmonika</a></span>
        <?php if(false): ?><span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/tamburasi#listing" title="Tamburaši">Tamburaši</a></span><?php endif; ?>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/glazba-za-crkvu#listing" title="Glazba za crkvu">Glazba za crkvu</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/rasvjeta-razglas-efekti#listing" title="Rasvjeta / Razglas / Efekti">Rasvjeta / Razglas / Efekti</a></span>
				<div class="cf"></div>
			</div>
      
      <p>FOTO &amp; VIDEO &amp; KABINE</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/foto-video#listing" title="Fotografi & Video snimanje">Foto & Video</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/foto-kabine#listing" title="Foto kabine">Foto kabine</a></span>
        <span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/najam-selfie-mirror#listing" title="Najam Selfie Mirror">Najam Selfie Mirror</a></span>
				<div class="cf"></div>
			</div>
      	
      <p>VJENČANICE &amp; ODIJELA &amp; DODACI</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/vjencanice-i-dodaci#listing" title="Najam vjenčanica &amp; dodaci">Najam vjenčanica &amp; dodaci</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/muska-odijela-i-dodaci#listing" title="Muška odijela &amp; dodaci">Muška odijela &amp; dodaci</a></span>
				<div class="cf"></div>
			</div>
			
      <?php if(false): ?>
			<p>PSSST... IZDVAJAMO!</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/izdvajamo#listing" title="Izdvajamo">Izdvajamo zanimljive usluge i proizvode</a></span>
				<div class="cf"></div>
			</div>
      <?php endif; ?>
			
		</div>
	</div>

	<div class="col-lg-4">
		<div class="nav-cat-container">
    
			<p>ZARUČNIČKO PRSTENJE &amp; VERE</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/zarucnicko-prstenje-vere#listing" title="Zaručničko prstenje i vere">Zaručničko prstenje / Vere</a></span>
				<div class="cf"></div>
			</div>
    
      <p>TORTE &amp; KOLAČI</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/torte-kolaci#listing" title="Svadbene torte / Torte &amp; Kolači">Torte &amp; Kolači</a></span>
				<div class="cf"></div>
			</div>

			<p>LJEPOTA</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/makeup-nokti-trepavice#listing" title="Makeup &amp; Nokti &amp; Trepavice">Makeup &amp; Nokti &amp; Trepavice</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/frizerski-saloni#listing" title="Frizerski saloni">Frizerski saloni</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/saloni-ljepote-zdravlja#listing" title="Saloni Ljepote i Zdravlja">Saloni Ljepote i Zdravlja</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/wellness-spa-usluge#listing" title="Wellness &amp; spa">Wellness &amp; spa</a></span>
				<div class="cf"></div>
			</div>
      
      <p>CVIJEĆE &amp; DEKORACIJE &amp; DODACI</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/dekoracije-buketi-reveri-podvezice-baloni#listing" title="Dekoracije / Buketi / Reveri / Podvezice / Baloni">Dekoracije / Buketi / Reveri / Podvezice / Baloni</a></span>
				<div class="cf"></div>
			</div>
      
      <p>POZIVNICE &amp; ZAHVALNICE &amp; POPIS GOSTIJU</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/pozivnice-zahvalnice-popis-gostiju#listing" title="Pozivnice / Zahvalnice / Popis gostiju">Pozivnice / Zahvalnice / Popis gostiju</a></span>
				<div class="cf"></div>
			</div>	
      
		</div>
	</div>

	<div class="col-lg-4">
		<div class="nav-cat-container">
		
      <p>NAJAM VOZILA I TAXI PRIJEVOZ</p>
			<div class="nav-cats">
        <span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/najam-oldtimera#listing" title="Rent a oldtimer">Najam oldtimera</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/najam-limuzina#listing" title="Rent a car &amp; limo">Najam limuzina</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/taxi-transferi#listing" title="Taxi transferi">Taxi transferi</a></span>
				<div class="cf"></div>
			</div>
      
      <p>DJEVOJAČKE &amp; MOMAČKE VEČERI</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/najam-kuce-za-proslave#listing" title="Kuće">Kuće</a></span>
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/restorani-konobe-djevojacke-momacke-veceri#listing" title="Restorani">Restorani &amp; konobe</a></span>
				<div class="cf"></div>
			</div>	
      
      <p>BRAČNO PUTOVANJE</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/bracno-putovanje#listing" title="Bračno putovanje">Bračno putovanje</a></span>
				<div class="cf"></div>
			</div>
			
			<p>POKLONI ZA MLADENCE</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/pokloni#listing" title="Pokloni &amp; poklon bonovi">Pokloni &amp; poklon bonovi</a></span>
				<div class="cf"></div>
			</div>					
			
			<p>SMJEŠTAJ</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/smjestaj#listing" title="Smještaj za mladence i uzvanike">Smještaj za mladence i uzvanike</a></span>
				<div class="cf"></div>
			</div>	
			
			<p>TEČAJEVI</p>
			<div class="nav-cats">
				<span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/skola-plesa#listing" title="Tečajevi plesa">Škola plesa</a></span>
				<?php if(true): ?><span class="nav-cat"><a href="<?php echo WP_1_SITEURL; ?>/zarucnicki-tecajevi-info" title="Zaručnički tečajevi">Zaručnički tečajevi</a></span><?php endif; ?>
				<div class="cf"></div>
			</div>	

		</div>
	</div>
</div>	