<!-- Main Navigation -->

<nav id="nav-container" class="pos-relative">
		<?php
			wp_nav_menu( array(
				'menu' => 'Main Navigation',
				'container' => '',
				'menu_class' => 'dropdown',
				'menu_id' => 'nav',
				'theme_location' => 'Primary'
			) );
		    get_template_part( '/inc/nav/nav-cats' );
		?>
</nav>
<!-- Main Navigation / End -->