<!-- Lang navigation -->

<div id="lang-nav-container">

	<!-- Lang Nav -->
	<ul id="lang-nav" class="dropdown">

		<li id="current-lang" class="mmenu-expand-dropdown">
			<?php dynamic_sidebar( 'language-widget' ); ?>
		</li>
		
	</ul>
		
	<!-- Lang Nav / End -->

</div>

<!-- Lang navigation / End -->