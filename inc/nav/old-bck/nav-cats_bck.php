<div class="dropdown-fullwidth">
    <div class="dropdown-fullwidth-inner">
        <div class="row">
			<div class="col-lg-12">
				<div class="nav-cat-container">
					<?php
						wp_nav_menu( array(
						'container' => '',
						'menu_class' => 'nav-cats',
						'items_wrap' => '<ul class="%2$s">%3$s</ul>',
						'walker' => new Walker_Category_Menu
					) ); ?>

				</div>
			</div>
        </div>
    </div>
</div>
