<!-- Main Navigation -->
<?php
	global $szvData; 
?>
<nav id="nav-container" class="pos-relative">

	<ul id="nav" class="dropdown">

		<li class="mmenu-expand-dropdown"><a class="nav-first-level prev-default">Vjenčanje...</a>
			<ul>
				<li><a href="<?php echo WP_1_SITEURL; ?>/vjencanje#listing">iz snova u Hrvatskoj</a></li>
			</ul>
		</li>
		<li class="dropdown-fullwidth-container mmenu-expand-dropdown"><a class="nav-first-level prev-default">Sve Za Vjenčanje</a>
			<div class="dropdown-fullwidth">	
				<div class="dropdown-fullwidth-inner">
					<?php get_template_part( '/inc/nav/nav-cats-'.$szvData['wpLangCode'] ); ?>
				</div>
			</div>	
		</li>
		<li class="mmenu-expand-dropdown"><a href="<?php echo WP_1_SITEURL; ?>/blog" class="nav-first-level prev-default">Blog</a></li>
    <li class="mmenu-expand-dropdown"><a href="<?php echo WP_1_SITEURL; ?>/favoriti" class="nav-first-level prev-default">Favoriti</a></li>
		<li class="mmenu-expand-dropdown"><a href="<?php echo WP_1_SITEURL; ?>/posalji-multi-upit" class="nav-first-level prev-default">Pošalji multi upit</a></li>

	</ul>
	
</nav>
<!-- Main Navigation / End -->