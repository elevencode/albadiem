<?php
  
global $szvData;
$translations = $szvData['translations'];
$tLang = $szvData['wpLangCode'];

//var_dump($parent_category_slug);

?>

<?php /*  Category specific - BENDOVI  */ ?>
<?php 
 if($parent_category_slug == 'bendovi' || $parent_category_slug == 'bands'): ?>
 <?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
    $providerMalePersons = get_field('providerMalePersons');
    $providerFemalePersons = get_field('providerFemalePersons');
    $providerInstrumentsObjects = get_field_object('providerInstruments');
    $providerInstruments = get_field('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
?>
<ul class="single-provider-litems">
    <li><?php echo $translations['numberOfMembers'][$tLang]; ?>:
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
            <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </li>
    <?php if($providerMalePersons): ?>
        <li><?php echo $translations['maleVocals'][$tLang]; ?>: <?php echo $providerMalePersons ?></li>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <li><?php echo $translations['femaleVocals'][$tLang]; ?>: <?php echo $providerFemalePersons ?></li>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<li>' . implode(', ',  $providerInstrumentsLabels) . '</li>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<li>' . $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>

<?php /*  Category specific - BENDOVI  */ ?>

<?php /*  Category specific - BRAČNO PUTOVANJE  */ ?>
<?php if($parent_category_slug == 'bracno-putovanje' || $parent_category_slug == 'honeymoon'): ?>
<?php 
    $providerDestinations = get_field('providerDestinations');
	$providerDestinationsDesc = get_field('providerDestinationsDesc');
?>
<ul class="single-provider-litems">
    <li><?php echo $providerDestinations ?></li>
    <li><?php echo $providerDestinationsDesc ?></li>
</ul>
<?php endif; ?>
<?php /*  Category specific - BRAČNO PUTOVANJE  */ ?>

<?php /*  Category specific - CATERING  */ ?>
<?php if($parent_category_slug == 'catering-usluge' || $parent_category_slug == 'catering'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$rentAWaiter = get_field_object('rentAWaiter');
	$rentAChair = get_field_object('rentAChair');
	$rentATent = get_field_object('rentATent');
?> 
<ul class="single-provider-litems">
    <li><?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>
    <?php if($rentAWaiter['value']) echo '<li>'. $translations['rentAWaiter'][$tLang] . '</li>'; ?>
    <?php if($rentAChair['value']) echo '<li>'. $translations['rentAChair'][$tLang] . '</li>'; ?>
    <?php if($rentATent['value']) echo '<li>'. $translations['rentATent'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - CATERING  */ ?>

<?php /*  Category specific - DEKORACIJE, BUKETI, REVERI, PODVEZICE  */ ?>
<?php if($parent_category_slug == 'dekoracije-buketi-reveri-podvezice-baloni' || $parent_category_slug == 'decoration-bouquets-lapel-garter-balloons'): ?>
<?php 
    $providerDecorations = get_field_object('providerDecorations');
	$providerBouquets = get_field_object('providerBouquets');
	$providerBoutonnieres = get_field_object('providerBoutonnieres');
	$providerGarters = get_field_object('providerGarters');
	$providerBalloons = get_field_object('providerBalloons');
?>
<ul class="single-provider-litems">
    <?php if($providerDecorations['value']) echo '<li>'. $translations['decoration'][$tLang] . '</li>'; ?>
    <?php if($providerBouquets['value']) echo '<li>'. $translations['bouquets'][$tLang] . '</li>'; ?>
    <?php if($providerBoutonnieres['value']) echo '<li>'. $translations['lapel'][$tLang] . '</li>'; ?>
    <?php if($providerGarters['value']) echo '<li>'. $translations['garter'][$tLang] . '</li>'; ?>
    <?php if($providerBalloons['value']) echo '<li>'. $translations['balloons'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - DEKORACIJE, BUKETI, REVERI, PODVEZICE  */ ?>

<?php /*  Category specific - DJ  */ ?>
<?php if($parent_category_slug == 'dj-izvodaci' || $parent_category_slug == 'dj'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$providerMalePersons = get_field('providerMalePersons');
	$providerFemalePersons = get_field('providerFemalePersons');
	$providerInstruments = get_field_object('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
    $providerKaraoke = get_field_object('providerKaraoke');
?>
<ul class="single-provider-litems">
    <li><?php echo $translations['numberOfMembers'][$tLang]; ?>:
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
            <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> - <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </li>
    <?php if($providerMalePersons): ?>
        <li><?php echo $translations['maleVocals'][$tLang]; ?> <?php echo $providerMalePersons ?></li>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <li><?php echo $translations['femaleVocals'][$tLang]; ?> <?php echo $providerFemalePersons ?></li>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        } 
        echo '<li>' . implode(', ',  $providerInstrumentsLabels) . '</li>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<li>' . $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</li>'; ?>
    <?php if($providerKaraoke['value']) echo '<li>' . $translations['karaoke'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - DJ  */ ?>

<?php /*  Category specific - FOTO KABINE  */ ?>
<?php if($parent_category_slug == 'foto-kabine' || $parent_category_slug == 'photo-cabins'): ?>
<?php endif; ?>
<?php /*  Category specific - FOTO KABINE  */ ?>

<?php /*  Category specific - FOTO, VIDEO  */ ?>
<?php if($parent_category_slug == 'foto-video' || $parent_category_slug == 'photo-video'): ?>
<?php 
    $providerFoto = get_field_object('providerFoto');
	$providerVideo = get_field_object('providerVideo');
?>
<ul class="single-provider-litems">
    <?php if($providerFoto['value']) echo '<li>'. $translations['photographyOtherServices'][$tLang] . '</li>'; ?>
    <?php if($providerVideo['value']) echo '<li>'. $translations['videoRecordingEditing'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - FOTO, VIDEO  */ ?>

<?php /*  Category specific - GLAZBA ZA CRKVU  */ ?>
<?php if($parent_category_slug == 'glazba-za-crkvu' || $parent_category_slug == 'church-music'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
    $providerMalePersons = get_field('providerMalePersons');
    $providerFemalePersons = get_field('providerFemalePersons');
    $providerInstrumentsObjects = get_field_object('providerInstruments');
    $providerInstruments = get_field('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
?>
<ul class="single-provider-litems">
    <li><?php echo $translations['numberOfMembers'][$tLang]; ?>:
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
            <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </li>
    <?php if($providerMalePersons): ?>
        <li><?php echo $translations['maleVocals'][$tLang]; ?> <?php echo $providerMalePersons ?></li>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <li><?php echo $translations['femaleVocals'][$tLang]; ?> <?php echo $providerFemalePersons ?></li>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
        if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<li>' . implode(', ',  $providerInstrumentsLabels) . '</li>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<li>' . $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - GLAZBA ZA CRKVU  */ ?>

<?php /*  Category specific - HOTELI  */ ?>
<?php if($parent_category_slug == 'hoteli' || $parent_category_slug == 'hotels'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

    $providerParking = get_field_object('providerParking');
    $providerPrivateParking = get_field_object('providerPrivateParking');
    $providerFreeParking = get_field_object('providerFreeParking');
    $providerWiFi = get_field_object('providerWiFi');
    $providerAirCondition = get_field_object('providerAirCondition');
    $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
    $providerWC = get_field_object('providerWC');
    $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
    $providerFloor = get_field_object('providerFloor');
    $providerHasElevator = get_field_object('providerHasElevator');
    $providerView = get_field_object('providerView');
    $providerTerrace = get_field_object('providerTerrace');
    $providerSmokingArea = get_field_object('providerSmokingArea');
    $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
    $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');

?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>

    <?php if($providerParking['value']) echo '<li>' . $translations['parking'][$tLang] . '</li>'; ?>
    <?php if($providerPrivateParking['value']) echo '<li>' . $translations['privateParking'][$tLang] . '</li>'; ?>
    <?php if($providerFreeParking['value']) echo '<li>' . $translations['freeParking'][$tLang] . '</li>'; ?>
    <?php if($providerWiFi['value']) echo '<li>' . $translations['wiFi'][$tLang] . '</li>'; ?>
    <?php if($providerAirCondition['value']) echo '<li>' . $translations['airCondition'][$tLang] . '</li>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<li>' . $translations['suitableForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerWC['value']) echo '<li>' . $translations['wc'][$tLang] . '</li>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<li>' . $translations['wcForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php //if($providerFloor['value']) echo '<li>' . $translations['floor'][$tLang] . '</li>'; ?>
    <?php if($providerHasElevator['value']) echo '<li>' . $translations['hasElevator'][$tLang] . '</li>'; ?>
    <?php if($providerView['value']) echo '<li>' . $translations['hasView'][$tLang] . '</li>'; ?>
    <?php if($providerTerrace['value']) echo '<li>' . $translations['hasTerrace'][$tLang] . '</li>'; ?>
    <?php if($providerSmokingArea['value']) echo '<li>' . $translations['smokingArea'][$tLang] . '</li>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<li>' . $translations['childrenFriendly'][$tLang] . '</li>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<li>' . $translations['accommodationAvailable'][$tLang] . '</li>'; ?>

</ul>
<?php endif; ?>
<?php /*  Category specific - HOTELI  */ ?>

<?php /*  Category specific - IZDVAJAMO  */ ?>
<?php if($parent_category_slug == 'izdvajamo' || $parent_category_slug == 'interesting'): ?>

<?php endif; ?>
<?php /*  Category specific - IZDVAJAMO  */ ?>

<?php /*  Category specific - MAKEUP NOKTI I TREPAVICE  */ ?>
<?php if($parent_category_slug == 'makeup-nokti-trepavice' || $parent_category_slug == 'makeup-nails-eyelashes'): ?>
<?php 
    $providerMakeUp = get_field_object('providerMakeUp');
	$providerNails = get_field_object('providerNails');
	$providerEyelashesTrue = get_field_object('providerEyelashes');
?>
<ul class="single-provider-litems">
    <?php if($providerMakeUp['value']) echo '<li>' . $translations['makeUp'][$tLang] . '</li>'; ?>
    <?php if($providerNails['value']) echo '<li>' . $translations['nails'][$tLang] . '</li>'; ?>
    <?php if($providerEyelashesTrue['value']) echo '<li>' . $translations['eyelashes'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - MAKEUP NOKTI I TREPAVICE  */ ?>

<?php /*  Category specific - MALI RESTORANI  */ ?>
<?php if($parent_category_slug == 'mali-restorani' || $parent_category_slug == 'small-restaurants'): ?>
<?php 
   $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
   $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

   $providerParking = get_field_object('providerParking');
   $providerPrivateParking = get_field_object('providerPrivateParking');
   $providerFreeParking = get_field_object('providerFreeParking');
   $providerWiFi = get_field_object('providerWiFi');
   $providerAirCondition = get_field_object('providerAirCondition');
   $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
   $providerWC = get_field_object('providerWC');
   $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
   $providerFloor = get_field_object('providerFloor');
   $providerHasElevator = get_field_object('providerHasElevator');
   $providerView = get_field_object('providerView');
   $providerTerrace = get_field_object('providerTerrace');
   $providerSmokingArea = get_field_object('providerSmokingArea');
   $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
   $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>

    <?php if($providerParking['value']) echo '<li>' . $translations['parking'][$tLang] . '</li>'; ?>
    <?php if($providerPrivateParking['value']) echo '<li>' . $translations['privateParking'][$tLang] . '</li>'; ?>
    <?php if($providerFreeParking['value']) echo '<li>' . $translations['freeParking'][$tLang] . '</li>'; ?>
    <?php if($providerWiFi['value']) echo '<li>' . $translations['wiFi'][$tLang] . '</li>'; ?>
    <?php if($providerAirCondition['value']) echo '<li>' . $translations['airCondition'][$tLang] . '</li>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<li>' . $translations['suitableForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerWC['value']) echo '<li>' . $translations['wc'][$tLang] . '</li>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<li>' . $translations['wcForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php //if($providerFloor['value']) echo '<li>' . $translations['floor'][$tLang] . '</li>'; ?>
    <?php if($providerHasElevator['value']) echo '<li>' . $translations['hasElevator'][$tLang] . '</li>'; ?>
    <?php if($providerView['value']) echo '<li>' . $translations['hasView'][$tLang] . '</li>'; ?>
    <?php if($providerTerrace['value']) echo '<li>' . $translations['hasTerrace'][$tLang] . '</li>'; ?>
    <?php if($providerSmokingArea['value']) echo '<li>' . $translations['smokingArea'][$tLang] . '</li>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<li>' . $translations['childrenFriendly'][$tLang] . '</li>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<li>' . $translations['accommodationAvailable'][$tLang] . '</li>'; ?>

</ul>
<?php endif; ?>
<?php /*  Category specific - MALI RESTORANI  */ ?>

<?php /*  Category specific - MUŠKA ODIJELA I DODACI  */ ?>
<?php if($parent_category_slug == 'muska-odijela-i-dodaci' || $parent_category_slug == 'mens-suits-and-accessories'): ?>
<?php 
    $providerMaleSuits = get_field_object('providerMaleSuits');
	$providerMaleSuitsRental = get_field_object('providerMaleSuitsRental');
	$providerMaleAcessories = get_field_object('providerMaleAcessories');
?>
<ul class="single-provider-litems">
    <?php if($providerMaleSuits['value']) echo '<li>' . $translations['maleSuits'][$tLang] . '</li>'; ?>
    <?php if($providerMaleSuitsRental['value']) echo '<li>' . $translations['maleSuitsRental'][$tLang] . '</li>'; ?>
    <?php if($providerMaleAcessories['value']) echo '<li>' . $translations['maleAccessories'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - MUŠKA ODIJELA I DODACI  */ ?>

<?php /*  Category specific - NAJAM KUĆE ZA PROSLAVE  */ ?>
<?php if($parent_category_slug == 'najam-kuce-za-proslave' || $parent_category_slug == 'rent-celebration-houses'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');

    $providerParking = get_field_object('providerParking');
    $providerPrivateParking = get_field_object('providerPrivateParking');
    $providerFreeParking = get_field_object('providerFreeParking');
    $providerWiFi = get_field_object('providerWiFi');
    $providerAirCondition = get_field_object('providerAirCondition');
    $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
    $providerWC = get_field_object('providerWC');
    $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');

    $providerPool = get_field_object('providerPool');
    $providerHeatedPool = get_field_object('providerHeatedPool');
    $providerBilliards = get_field_object('providerBilliards');
    $providerDarts = get_field_object('providerDarts');
    $providerTableTennis = get_field_object('providerTableTennis');
    $providerTableSoccer = get_field_object('providerTableSoccer');
    $providerCards = get_field_object('providerCards');
    $providerFootball = get_field_object('providerFootball');
    $providerBasketball = get_field_object('providerBasketball');
    $providerTennis = get_field_object('providerTennis');
    $providerBadminton = get_field_object('providerBadminton');
    $providerBalote = get_field_object('providerBalote');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>

    <?php if($providerParking['value']) echo '<li>' . $translations['parking'][$tLang] . '</li>'; ?>
    <?php if($providerPrivateParking['value']) echo '<li>' . $translations['privateParking'][$tLang] . '</li>'; ?>
    <?php if($providerFreeParking['value']) echo '<li>' . $translations['freeParking'][$tLang] . '</li>'; ?>
    <?php if($providerWiFi['value']) echo '<li>' . $translations['wiFi'][$tLang] . '</li>'; ?>
    <?php if($providerAirCondition['value']) echo '<li>' . $translations['airCondition'][$tLang] . '</li>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<li>' . $translations['suitableForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerWC['value']) echo '<li>' . $translations['wc'][$tLang] . '</li>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<li>' . $translations['wcForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerPool['value']) echo '<li>' . $translations['pool'][$tLang] . '</li>'; ?>
    <?php if($providerHeatedPool['value']) echo '<li>' . $translations['heatedPool'][$tLang] . '</li>'; ?>
    <?php if($providerBilliards['value']) echo '<li>' . $translations['billiards'][$tLang] . '</li>'; ?>
    <?php if($providerDarts['value']) echo '<li>' . $translations['darts'][$tLang] . '</li>'; ?>
    <?php if($providerTableTennis['value']) echo '<li>' . $translations['tableTennis'][$tLang] . '</li>'; ?>
    <?php if($providerTableSoccer['value']) echo '<li>' . $translations['tableFootball'][$tLang] . '</li>'; ?>
    <?php if($providerCards['value']) echo '<li>' . $translations['cards'][$tLang] . '</li>'; ?>
    <?php if($providerFootball['value']) echo '<li>' . $translations['football'][$tLang] . '</li>'; ?>
    <?php if($providerBasketball['value']) echo '<li>' . $translations['basketball'][$tLang] . '</li>'; ?>
    <?php if($providerTennis['value']) echo '<li>' . $translations['tennis'][$tLang] . '</li>'; ?>
    <?php if($providerBadminton['value']) echo '<li>' . $translations['badminton'][$tLang] . '</li>'; ?>
    <?php if($providerBalote['value']) echo '<li>' . $translations['boules'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - NAJAM KUĆE ZA PROSLAVE  */ ?>

<?php /*  Category specific - NAJAM ŠATORA  */ ?>
<?php if($parent_category_slug == 'najam-satora' || $parent_category_slug == 'rent-a-tent'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>
</ul>
<?php endif; ?>
<?php /*  Category specific - NAJAM ŠATORA  */ ?>

<?php /*  Category specific - POKLONI  */ ?>
<?php if($parent_category_slug == 'pokloni' || $parent_category_slug == 'gifts'): ?>
<?php 
    $providerGiftList = get_field_object('providerGiftList');
	$providerGiftCoupon = get_field_object('providerGiftCoupon');
?>
<ul class="single-provider-litems">
    <?php if($providerGiftList['value']) echo '<li>' . $translations['giftList'][$tLang] . '</li>'; ?>
    <?php if($providerGiftCoupon['value']) echo '<li>' . $translations['giftCoupon'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - POKLONI  */ ?>

<?php /*  Category specific - POZIVNICE, ZAHVALNICE, POPIS GOSTIJU  */ ?>
<?php if($parent_category_slug == 'pozivnice-zahvalnice-popis-gostiju' || $parent_category_slug == 'invitations-letter-of-thanks-and-guest-lists'): ?>
<?php 
    $providerInvitations = get_field_object('providerInvitations');
	$providerThankYouNotes = get_field_object('providerThankYouNotes');
	$providerGuestLists = get_field_object('providerGuestLists');
?>
<ul class="single-provider-litems">
    <?php if($providerInvitations['value']) echo '<li>' . $translations['invitations'][$tLang] . '</li>'; ?>
    <?php if($providerThankYouNotes['value']) echo '<li>' . $translations['thankYouNotes'][$tLang] . '</li>'; ?>
    <?php if($providerGuestLists['value']) echo '<li>' . $translations['guestLists'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - POZIVNICE, ZAHVALNICE, POPIS GOSTIJU  */ ?>

<?php /*  Category specific - RASVJETA, RAZGLAS, EFEKTI  */ ?>
<?php if($parent_category_slug == 'rasvjeta-razglas-efekti' || $parent_category_slug == 'lighting-soundsystem-effects'): ?>
<?php 
    $providerLighting = get_field_object('providerLighting');
	$providerSoundSystem = get_field_object('providerSoundSystem');
	$providerEffects = get_field_object('providerEffects');
?>
<ul class="single-provider-litems">
    <?php if($providerLighting['value']) echo '<li>' . $translations['lighting'][$tLang] . '</li>'; ?>
    <?php if($providerSoundSystem['value']) echo '<li>' . $translations['soundSystem'][$tLang] . '</li>'; ?>
    <?php if($providerEffects['value']) echo '<li>' . $translations['effects'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - RASVJETA, RAZGLAS, EFEKTI  */ ?>

<?php /*  Category specific - RENT A LIMO  */ ?>
<?php if($parent_category_slug == 'najam-limuzina' || $parent_category_slug == 'rent-a-limo'): ?>
<?php endif; ?>
<?php /*  Category specific - RENT A LIMO  */ ?>

<?php /*  Category specific - RENT A OLDTIMER  */ ?>
<?php if($parent_category_slug == 'najam-oldtimera' || $parent_category_slug == 'rent-a-oldtimer'): ?>
<?php endif; ?>
<?php /*  Category specific - RENT A OLDTIMER  */ ?>

<?php /*  Category specific - RESTORANI I SALE  */ ?>
<?php if($parent_category_slug == 'restorani-sale' || $parent_category_slug == 'restaurants-and-wedding-venues'): ?>
<?php 
     $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
     $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
  
     $providerParking = get_field_object('providerParking');
     $providerPrivateParking = get_field_object('providerPrivateParking');
     $providerFreeParking = get_field_object('providerFreeParking');
     $providerWiFi = get_field_object('providerWiFi');
     $providerAirCondition = get_field_object('providerAirCondition');
     $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
     $providerWC = get_field_object('providerWC');
     $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
     $providerFloor = get_field_object('providerFloor');
     $providerHasElevator = get_field_object('providerHasElevator');
     $providerView = get_field_object('providerView');
     $providerTerrace = get_field_object('providerTerrace');
     $providerSmokingArea = get_field_object('providerSmokingArea');
     $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
     $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>

    <?php if($providerParking['value']) echo '<li>' . $translations['parking'][$tLang] . '</li>'; ?>
    <?php if($providerPrivateParking['value']) echo '<li>' . $translations['privateParking'][$tLang] . '</li>'; ?>
    <?php if($providerFreeParking['value']) echo '<li>' . $translations['freeParking'][$tLang] . '</li>'; ?>
    <?php if($providerWiFi['value']) echo '<li>' . $translations['wiFi'][$tLang] . '</li>'; ?>
    <?php if($providerAirCondition['value']) echo '<li>' . $translations['airCondition'][$tLang] . '</li>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<li>' . $translations['suitableForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerWC['value']) echo '<li>' . $translations['wc'][$tLang] . '</li>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<li>' . $translations['wcForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php //if($providerFloor['value']) echo '<li>' . $translations['floor'][$tLang] . '</li>'; ?>
    <?php if($providerHasElevator['value']) echo '<li>' . $translations['hasElevator'][$tLang] . '</li>'; ?>
    <?php if($providerView['value']) echo '<li>' . $translations['hasView'][$tLang] . '</li>'; ?>
    <?php if($providerTerrace['value']) echo '<li>' . $translations['hasTerrace'][$tLang] . '</li>'; ?>
    <?php if($providerSmokingArea['value']) echo '<li>' . $translations['smokingArea'][$tLang] . '</li>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<li>' . $translations['childrenFriendly'][$tLang] . '</li>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<li>' . $translations['accommodationAvailable'][$tLang] . '</li>'; ?>

</ul>
<?php endif; ?>
<?php /*  Category specific - RESTORANI I SALE  */ ?>

<?php /*  Category specific - RESTORANI KONOBE DJEVOJAČKE MOMAČKE VEČERI  */ ?>
<?php if($parent_category_slug == 'restorani-konobe-djevojacke-momacke-veceri' || $parent_category_slug == 'restaurants-taverns-bachelorette-and-bachelor-party'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
	$providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
	$providerLiveMusic = get_field_object('providerLiveMusic');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>
    <?php if($providerLiveMusic['value']) echo '<li>' . $translations['liveMusic'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - RESTORANI KONOBE DJEVOJAČKE MOMAČKE VEČERI  */ ?>

<?php /*  Category specific - SALONI LJEPOTE I ZDRAVLJA  */ ?>
<?php if($parent_category_slug == 'saloni-ljepote-zdravlja' || $parent_category_slug == 'beauty-and-health-salons'): ?>
<?php 

	$providerHealthServices = get_field_object('providerHealthServices');
	$providerBeautyServices = get_field_object('providerBeautyServices');
?>
<ul class="single-provider-litems">
    <?php if($providerBeautyServices['value']) echo '<li>' . $translations['beautySalon'][$tLang] . '</li>'; ?>
    <?php if($providerHealthServices['value']) echo '<li>' . $translations['healthSalon'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - SALONI LJEPOTE I ZDRAVLJA  */ ?>

<?php /*  Category specific - SELFIE MIRROR  */ ?>
<?php if($parent_category_slug == 'najam-selfie-mirror' || $parent_category_slug == 'selfie-mirror'): ?>
<?php endif; ?>
<?php /*  Category specific - SELFIE MIRROR  */ ?>

<?php /*  Category specific - SMJEŠTAJ  */ ?>
<?php if($parent_category_slug == 'smjestaj' || $parent_category_slug == 'accommodation'): ?>
<?php 
     $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
     $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
  
     $providerParking = get_field_object('providerParking');
     $providerPrivateParking = get_field_object('providerPrivateParking');
     $providerFreeParking = get_field_object('providerFreeParking');
     $providerWiFi = get_field_object('providerWiFi');
     $providerAirCondition = get_field_object('providerAirCondition');
     $providerSuitableForDisabledPeople = get_field_object('providerSuitableForDisabledPeople');
     $providerWC = get_field_object('providerWC');
     $providerWCForDisabledPeople = get_field_object('providerWCForDisabledPeople');
     $providerFloor = get_field_object('providerFloor');
     $providerHasElevator = get_field_object('providerHasElevator');
     $providerView = get_field_object('providerView');
     $providerTerrace = get_field_object('providerTerrace');
     $providerSmokingArea = get_field_object('providerSmokingArea');
     $providerChildrenFriendly = get_field_object('providerChildrenFriendly');
     $providerAccommodationAvailable = get_field_object('providerAccommodationAvailable');
?>
<ul class="single-provider-litems">
<li>
    <?php echo $translations['capacityPersons'][$tLang]; ?>:
        <?php 
            if($providerNmbrPersonsTo > 0){

                if($providerNmbrPersonsFrom == $providerNmbrPersonsTo){
                    echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                }else{
                    if($providerNmbrPersonsFrom == '' || $providerNmbrPersonsFrom == 0){
                        echo $translations['upTo'][$tLang].' '.$providerNmbrPersonsTo;
                    }else{
                        echo $providerNmbrPersonsFrom.' - '.$providerNmbrPersonsTo;
                    }
                }

            }else{
                echo $translations['onInquiry'][$tLang];
            }
        ?>
    </li>

    <?php if($providerParking['value']) echo '<li>' . $translations['parking'][$tLang] . '</li>'; ?>
    <?php if($providerPrivateParking['value']) echo '<li>' . $translations['privateParking'][$tLang] . '</li>'; ?>
    <?php if($providerFreeParking['value']) echo '<li>' . $translations['freeParking'][$tLang] . '</li>'; ?>
    <?php if($providerWiFi['value']) echo '<li>' . $translations['wiFi'][$tLang] . '</li>'; ?>
    <?php if($providerAirCondition['value']) echo '<li>' . $translations['airCondition'][$tLang] . '</li>'; ?>
    <?php if($providerSuitableForDisabledPeople['value']) echo '<li>' . $translations['suitableForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php if($providerWC['value']) echo '<li>' . $translations['wc'][$tLang] . '</li>'; ?>
    <?php if($providerWCForDisabledPeople['value']) echo '<li>' . $translations['wcForDisabledPeople'][$tLang] . '</li>'; ?>
    <?php //if($providerFloor['value']) echo '<li>' . $translations['floor'][$tLang] . '</li>'; ?>
    <?php if($providerHasElevator['value']) echo '<li>' . $translations['hasElevator'][$tLang] . '</li>'; ?>
    <?php if($providerView['value']) echo '<li>' . $translations['hasView'][$tLang] . '</li>'; ?>
    <?php if($providerTerrace['value']) echo '<li>' . $translations['hasTerrace'][$tLang] . '</li>'; ?>
    <?php if($providerSmokingArea['value']) echo '<li>' . $translations['smokingArea'][$tLang] . '</li>'; ?>
    <?php if($providerChildrenFriendly['value']) echo '<li>' . $translations['childrenFriendly'][$tLang] . '</li>'; ?>
    <?php if($providerAccommodationAvailable['value']) echo '<li>' . $translations['accommodationAvailable'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - SMJEŠTAJ  */ ?>

<?php /*  Category specific - TAMBURAŠI  */ ?>
<?php if($parent_category_slug == 'tamburasi' || $parent_category_slug == 'tambouras'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
    $providerMalePersons = get_field('providerMalePersons');
    $providerFemalePersons = get_field('providerFemalePersons');
    $providerInstrumentsObjects = get_field_object('providerInstruments');
    $providerInstruments = get_field('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
?>
<ul class="single-provider-litems">
    <li><?php echo $translations['numberOfMembers'][$tLang]; ?>:
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
            <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </li>
    <?php if($providerMalePersons): ?>
        <li><?php echo $translations['maleVocals'][$tLang]; ?> <?php echo $providerMalePersons ?></li>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <li><?php echo $translations['femaleVocals'][$tLang]; ?> <?php echo $providerFemalePersons ?></li>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<li>' . implode(', ',  $providerInstrumentsLabels) . '</li>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<li>' . $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - TAMBURAŠI  */ ?>

<?php /*  Category specific - TAXI TRANSFERI  */ ?>
<?php if($parent_category_slug == 'taxi-transferi' || $parent_category_slug == 'taxi-transfers'): ?>
<?php endif; ?>
<?php /*  Category specific - TAXI TRANSFERI  */ ?>

<?php /*  Category specific - TEČAJ PLESA  */ ?>
<?php if($parent_category_slug == 'skola-plesa' || $parent_category_slug == 'dance-lessons'): ?>
<?php 
   $providerFirstDance = get_field_object('providerFirstDance');
   $providerDanceSchool = get_field_object('providerDanceSchool');
   $providerAvailableDances = get_field_object('providerAvailableDances');
?>
<ul class="single-provider-litems">
    <?php if($providerFirstDance['value']) echo '<li>' . $translations['firstDance'][$tLang] . '</li>'; ?>
    <?php if($providerDanceSchool['value']) echo '<li>' . $translations['danceSchool'][$tLang] . '</li>'; ?>
    <?php if($providerAvailableDances['value']) echo '<li>' . $translations['availableDances'][$tLang] . ': '. $providerAvailableDances['value'] .'</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - TEČAJ PLESA  */ ?>

<?php /*  Category specific - TORTE, KOLAČI  */ ?>
<?php if($parent_category_slug == 'torte-kolaci' || $parent_category_slug == 'cakes'): ?>
<?php 
   $providerWeddingCakes = get_field_object('providerWeddingCakes');
   $providerCakes = get_field_object('providerCakes');
   $providerCookies = get_field_object('providerCookies');
   $providerHomeMadeCakesCookies = get_field_object('providerHomeMadeCakesCookies');
?>
<ul class="single-provider-litems">
    <?php if($providerWeddingCakes['value']) echo '<li>' . $translations['weddingCakes'][$tLang] . '</li>'; ?>
    <?php if($providerCakes['value']) echo '<li>' . $translations['cakes'][$tLang] . '</li>'; ?>
    <?php if($providerCookies['value']) echo '<li>' . $translations['cookies'][$tLang] . '</li>'; ?>
    <?php if($providerHomeMadeCakesCookies['value']) echo '<li>' . $translations['homeMadeCakesCookies'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - TORTE, KOLAČI  */ ?>

<?php /*  Category specific - TRIO, DUO HARMONIKA  */ ?>
<?php if($parent_category_slug == 'trio-duo-harmonika' || $parent_category_slug == 'trio-duo-accordion'): ?>
<?php 
    $providerNmbrPersonsFrom = get_field('providerNmbrPersonsFrom');
    $providerNmbrPersonsTo = get_field('providerNmbrPersonsTo');
    $providerMalePersons = get_field('providerMalePersons');
    $providerFemalePersons = get_field('providerFemalePersons');
    $providerInstrumentsObjects = get_field_object('providerInstruments');
    $providerInstruments = get_field('providerInstruments');
    $providerSoundSystemIncluded = get_field_object('providerSoundSystemIncluded');
    $providerTrio = get_field_object('providerTrio');
	$providerDuo = get_field_object('providerDuo');
	$providerAccordion = get_field_object('providerAccordion');
?>
<ul class="single-provider-litems">
    <?php if($providerTrio['value'])	echo '<li>' . $translations['trio'][$tLang] . '</li>'; ?>
    <?php if($providerDuo['value'])	echo '<li>' . $translations['duo'][$tLang] . '</li>'; ?>
    <?php if($providerAccordion['value'])	echo '<li>' . $translations['accordion'][$tLang] . '</li>'; ?>
    <li><?php echo $translations['numberOfMembers'][$tLang]; ?>:
        <?php if($providerNmbrPersonsFrom == $providerNmbrPersonsTo): ?>
            <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsFrom ?>
        <?php else: ?>
            <?php echo $providerNmbrPersonsFrom ?> <?php echo $translations['upTo'][$tLang]; ?> <?php echo $providerNmbrPersonsTo ?>
        <?php endif; ?>
    </li>
    <?php if($providerMalePersons): ?>
        <li><?php echo $translations['maleVocals'][$tLang]; ?> <?php echo $providerMalePersons ?></li>
    <?php endif; ?>
    <?php if($providerFemalePersons): ?>
        <li><?php echo $translations['femaleVocals'][$tLang]; ?> <?php echo $providerFemalePersons ?></li>
    <?php endif; ?>
    <?php 
    $providerInstrumentsLabels = array();
    $values = get_field('providerInstruments');
    if($values){
        foreach ($providerInstruments as $value) {
            $providerInstrumentsLabels[] = $providerInstrumentsObjects['choices'][ $value ];
        }
        echo '<li>' . implode(', ',  $providerInstrumentsLabels) . '</li>'; 
    }
    ?>
	<?php if($providerSoundSystemIncluded['value']) echo '<li>' . $translations['priceIncludes'][$tLang] .': '. $translations['soundSystem'][$tLang] . '</li>'; ?>
</ul>

<?php endif; ?>
<?php /*  Category specific - TRIO, DUO HARMONIKA  */ ?>

<?php /*  Category specific - VJENČANICE I DODACI  */ ?>
<?php if($parent_category_slug == 'vjencanice-i-dodaci' || $parent_category_slug == 'wedding-dresses-and-accessories'): ?>
<?php 
   $providerWeddingDresses = get_field_object('providerWeddingDresses');
   $providerWeddingDressesRental = get_field_object('providerWeddingDressesRental');
   $providerWeddingAccessories = get_field_object('providerWeddingAccessories');
?>
<ul class="single-provider-litems">
    <?php if($providerWeddingDresses['value']) echo '<li>' . $translations['weddingDresses'][$tLang] . '</li>'; ?>
    <?php if($providerWeddingDressesRental['value']) echo '<li>' . $translations['weddingDressesRentals'][$tLang] . '</li>'; ?>
    <?php if($providerWeddingAccessories['value']) echo '<li>' . $translations['weddingDressesAccessories'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - VJENČANICE I DODACI   */ ?>

<?php /*  Category specific - WELLNESS SPA  */ ?>
<?php if($parent_category_slug == 'wellness-spa-usluge' || $parent_category_slug == 'wellness-spa'): ?>
<?php 
   $providerWellness = get_field_object('providerWellness');
   $providerSpa = get_field_object('providerSpa');
?>
<ul class="single-provider-litems">
    <?php if($providerWellness['value']) echo '<li>' . $translations['wellness'][$tLang] . '</li>'; ?>
    <?php if($providerSpa['value'])	echo '<li>' . $translations['spa'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - WELLNESS SPA  */ ?>

<?php /*  Category specific - ZARUČNIČKI TEČAJ  */ ?>
<?php if($parent_category_slug == 'zarucnicki-tecajevi' || $parent_category_slug == 'engagement-courses'): ?>
<?php endif; ?>
<?php /*  Category specific - ZARUČNIČKI TEČAJ  */ ?>

<?php /*  Category specific - Zaručničo prstenje, vere  */ ?>
<?php if($parent_category_slug == 'zarucnicko-prstenje-vere' || $parent_category_slug == 'jewelry-engagement-rings'): ?>
<?php 
   $providerEngagementRings = get_field_object('providerEngagementRings');
   $providerWeddingRings = get_field_object('providerWeddingRings');
   $providerDiamondsGemstones = get_field_object('providerDiamondsGemstones');
   $providerSilverJewelry = get_field_object('providerSilverJewelry');
   $providerGoldJewelry = get_field_object('providerGoldJewelry');
?>

<ul class="single-provider-litems">
    <?php if($providerEngagementRings['value'])	echo '<li>' . $translations['engagementRings'][$tLang] . '</li>'; ?>
    <?php if($providerWeddingRings['value'])	echo '<li>' . $translations['weddingRings'][$tLang] . '</li>'; ?>
    <?php if($providerDiamondsGemstones['value'])	echo '<li>' . $translations['diamondsGemstones'][$tLang] . '</li>'; ?>
    <?php if($providerGoldJewelry['value'])	echo '<li>' . $translations['goldJewelry'][$tLang] . '</li>'; ?>
    <?php if($providerSilverJewelry['value'])	echo '<li>' . $translations['silverJewelry'][$tLang] . '</li>'; ?>
</ul>
<?php endif; ?>
<?php /*  Category specific - Zaručničo prstenje, vere  */ ?>

