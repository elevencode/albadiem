<?php 
    if(have_rows('social')):
        while(have_rows('social')) : the_row();
            if(get_sub_field('showProviderSocial')): ?>

                <div class="single-content-section-styled">
                    <?php if(get_field('shortName')): ?>
                        <h2><?php _e('Društvene mreže', 'albadiem'); ?> <?php echo get_field('shortName') ?></h2>
                    <?php endif; ?>
                    <hr>
                    <p>
                        <?php 
                            if(get_sub_field('providerSocialDesc')):
                                echo get_sub_field('providerSocialDesc');
                            else:
                                echo get_field('providerSocialDynamicDesc', 'option');
                            endif;
                        ?>
                    </p>

                    <div class="row">
                        <?php if(get_sub_field('providerFacebookUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerFacebookUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerInstagramUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerInstagramUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerPinterestUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerPinterestUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerTwitterUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerTwitterUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerLinedinUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerLinedinUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerYoutubeUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerYoutubeUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                        <?php if(get_sub_field('providerVimeoUrl')): ?>
                            <div class="col-md-1">
                                <span class="single-provider-location-cta-web">
                                    <a href="<?php echo get_sub_field('providerVimeoUrl'); ?>"><span class="fa fa-globe"></span></a>
                                </span>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php
            endif;
        endwhile;
    endif;
?>
