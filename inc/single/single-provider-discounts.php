<!-- Single Provider Discounts

================================================== -->


<div id="single-provider-discounts-container">

	<div class="pos-relative">

		<div id="single-provider-discounts">


				<?php if( have_rows('providerSpecialOffers') ): ?>
				<?php while( have_rows('providerSpecialOffers') ): the_row(); 
					// vars
					$providerSpecialOfferPublished = get_sub_field('providerSpecialOfferPublished');
					$providerSpecialOfferOrder = get_sub_field('providerSpecialOfferOrder');
					$providerSpecialOfferName = get_sub_field('providerSpecialOfferName');
					$providerSpecialOfferDesc = get_sub_field('providerSpecialOfferDesc');
					$providerSpecialOfferDateFrom = get_sub_field('providerSpecialOfferDateFrom');
					$providerSpecialOfferDateTo = get_sub_field('providerSpecialOfferDateTo');
					$providerSpecialOfferOldPrice = get_sub_field('providerSpecialOfferOldPrice');
					$providerSpecialOfferNewPrice = get_sub_field('providerSpecialOfferNewPrice');
					$providerSpecialOfferDiscountPercent = get_sub_field('providerSpecialOfferDiscountPercent');
					$providerSpecialOfferImgPublished = get_sub_field('providerSpecialOfferImgPublished');
					$providerSpecialOfferImgOriginalUrl = get_sub_field('providerSpecialOfferImgOriginalUrl');
					$providerSpecialOfferImgDesktopUrl = get_sub_field('providerSpecialOfferImgDesktopUrl');
					$providerSpecialOfferImgTabletUrl = get_sub_field('providerSpecialOfferImgTabletUrl');
					$providerSpecialOfferImgMobileUrl = get_sub_field('providerSpecialOfferImgMobileUrl');
					$providerSpecialOfferImgAlt = get_sub_field('providerSpecialOfferImgAlt');
					$providerSpecialOfferImgDesc = get_sub_field('providerSpecialOfferImgDesc');
					$providerSpecialOfferImgWidth = get_sub_field('providerSpecialOfferImgWidth');
					$providerSpecialOfferImgHeight = get_sub_field('providerSpecialOfferImgHeight');
					$providerSpecialOfferImgRatio = get_sub_field('providerSpecialOfferImgRatio');
					$providerSpecialOfferImgClass = get_sub_field('providerSpecialOfferImgClass');
					$providerSpecialOfferImgQueryStringVersion = get_sub_field('providerSpecialOfferImgQueryStringVersion');

				if($providerSpecialOfferPublished):
				?>	
				<div class="single-provider-discount-item" data-id="">

					<div class="single-provider-discount-box">

						<div class="single-provider-discount-img" 
							data-mobile-src="<?php echo $providerSpecialOfferImgMobileUrl ?>" 
							data-tablet-src="<?php echo $providerSpecialOfferImgTabletUrl ?>" 
							data-desktop-src="<?php echo $providerSpecialOfferImgDesktopUrl ?>" 
							data-width="<?php echo $providerSpecialOfferImgWidth ?>" 
							data-height="<?php echo $providerSpecialOfferImgHeight ?>" 
							data-ratio="<?php echo $providerSpecialOfferImgRatio ?>" 
							data-class="<?php echo $providerSpecialOfferImgClass ?>" 
							data-alt="<?php echo $providerSpecialOfferImgAlt ?>" 
							data-title="<?php echo $providerSpecialOfferName ?>">

							<img src="<?php echo $providerSpecialOfferImgOriginalUrl; ?>" 
							alt="<?php echo $providerSpecialOfferImgAlt; ?>"
							height="<?php echo $providerSpecialOfferImgHeight; ?>"
							width="<?php echo $providerSpecialOfferImgWidth; ?>" />

							<div class="badges">

								<div class="discount-badge">-<?php echo $providerSpecialOfferDiscountPercent; ?>%</div>

							</div>

							<div class="single-provider-discount-prices">

								<span class="single-provider-discount-new-price"><?php echo number_format((float)$providerSpecialOfferOldPrice , 2, ',', '.'); ?> kn</span>

								<span class="single-provider-discount-old-price"><?php echo number_format((float)$providerSpecialOfferNewPrice , 2, ',', '.'); ?> kn</span>

							</div>

							<div class="single-provider-discount-date">

								<?php echo $providerSpecialOfferDateFrom . ' - ' .  $providerSpecialOfferDateTo; ?>

							</div>

						</div>

						<div class="single-provider-discount-title"><?php echo $providerSpecialOfferDiscountPercent; ?>% popusta na ukupnu cijenu</div>	

						<div class="single-provider-discount-desc"><?php echo $providerSpecialOfferDesc; ?></div>	

						<div class="single-provider-discount-cta">

							<button class="btn scrollToContactForm" type="button">Iskoristi odmah</button>

						</div>

					</div>

				</div>
				<?php
					endif; 
					endwhile;
					endif; 
				?>
		</div>
	</div>
</div>

<!-- Single Provider Discounts / End -->