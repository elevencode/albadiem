<!-- Single Slider

================================================== -->
<?php 
if(have_rows('images')): ?>

	<div id="single-slider-holder">

		<?php while(have_rows('images')) : the_row(); ?>

			<?php if(get_sub_field('showProviderImages')): ?>
				<div id="single-slider" class="carousel lazy-load-slider">				

					<?php 
						if(have_rows('providerImages')):
							while(have_rows('providerImages')) : the_row();
								
									$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
									$providerImgDesktopUrl = get_sub_field('providerImgDesktopUrl');
									$providerImgTabletUrl = get_sub_field('providerImgTabletUrl');
									$providerImgMobileUrl = get_sub_field('providerImgMobileUrl');
									$providerImgWidth = get_sub_field('providerImgWidth');
									$providerImgHeight = get_sub_field('providerImgHeight');
									$providerImgAlt = get_sub_field('providerImgAlt');
									$providerImgDesc = get_sub_field('providerImgDesc');
									$providerImgOriginalUrl = get_sub_field('providerImgOriginalUrl');
									$providerImgTargetContainer = get_sub_field('providerImgTargetContainer');
									$providerImgRatio = get_sub_field('providerImgRatio');
									$providerImgClass = get_sub_field('providerImgClass');
									$providerImgQueryStringVersion = get_sub_field('providerImgQueryStringVersion');
									
									if(get_sub_field('providerImgPublished') && $providerImgRatio < 1.6): ?>

										<div class="carousel-cell" data-flickity-bg-lazyload="<?php echo $providerImgOriginalUrl; ?>" style="width:<?php echo $providerImgWidth; ?>px; height:<?php echo $providerImgHeight; ?>px;">
										
											<noscript><img src="<?php echo $providerImgOriginalUrl; ?>" alt="<?php echo $providerImgAlt; ?>" /></noscript>
										
											</div>			

										<?php
									endif;
							endwhile;
						endif;
					?>
					<?php 
						if(have_rows('videos')): $counter = 0; 
							while(have_rows('videos')) : the_row();
								if(have_rows('providerVideos')):
									while(have_rows('providerVideos')) : the_row();
											
											$providerVideoUrl = get_sub_field('providerVideoUrl');
											$providerVideoWidth = get_sub_field('providerVideoWidth');
											$providerVideoHeight = get_sub_field('providerVideoHeight');
											$providerVideoDesc = get_sub_field('providerVideoDesc');
											$providerVideoOrder = get_sub_field('providerVideoOrder');
											$providerVideoTargetContainer = get_sub_field('providerVideoTargetContainer');
											$providerVideoRatio = get_sub_field('providerVideoRatio');
											$providerVideoClass = get_sub_field('providerVideoClass');

											$providerVideoWidth = '640';
											$providerVideoHeight = '480';
											$providerVideoRatio = '1.33';


											$yt_rx = '/^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/';
											$has_match_youtube = preg_match($yt_rx, $providerVideoUrl, $yt_matches);


											$vm_rx = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
											$has_match_vimeo = preg_match($vm_rx, $providerVideoUrl, $vm_matches);

											//Then we want the video id which is:
											if($has_match_youtube) {
												$video_id = $yt_matches[5]; 
												$type = 'youtube';
											}
											elseif($has_match_vimeo) {
												$video_id = $vm_matches[5];
												$type = 'vimeo';
											}
											else {
												$video_id = 0;
												$type = 'none';
											}

											$data['video_id'] = $video_id;
											$data['video_type'] = $type;

											//var_dump($data);


										if(get_sub_field('providerVideoPublished')): ?>
													
												<div class="carousel-cell iframeSingleProviderVideoPopUp" 
													<?php if($data['video_type'] == 'youtube'): ?>

													data-flickity-bg-lazyload="https://img.youtube.com/vi/<?php echo $data['video_id']; ?>/hqdefault.jpg"
													<?php elseif($data['video_type'] == 'vimeo'): ?>
													data-flickity-bg-lazyload="https://i.vimeocdn.com/video/<?php echo $data['video_id']; ?>.jpg?mw=640&mh=480"
													<?php else: ?>
													data-flickity-bg-lazyload=""
													<?php endif; ?>
													style="width: <?php echo $providerVideoWidth; ?>px;
													height:<?php echo $providerVideoHeight; ?>px; " 
													data-title="<?php echo $providerVideoDesc; ?>" 
													data-src="<?php echo $providerVideoUrl; ?>">

													<div class="center-v-h text-center">
															<div id="video_<?php echo $counter; ?>" 
															class="openIframeSingleProviderVideoPopUp">
															<span class="fa fa-play"></span></div>
													</div>
												</div>
											
												<?php $counter++; ?>

											<?php
										endif;
									endwhile;
								endif;
							endwhile;
						endif;
					?>
				</div>
				<div class="single-slider-box">

					<div class="container pos-relative">

						<div class="single-slider-box-holder">

								<div class="single-title-holder">	

									<h1><?php the_title(); ?></h1>

								</div>

						</div>

					</div>

				</div>

			<?php endif; ?>

		<?php endwhile; ?>

	</div>

<?php endif; ?>
<!-- Single Slider / End -->