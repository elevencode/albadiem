<!-- Single Provider All Reviews

================================================== -->



<div id="single-provider-reviews-container">

	<div class="pos-relative">
	

		<div id="single-provider-reviews">

				<?php 
					if(have_rows('providerReviews')):
						while(have_rows('providerReviews')) : the_row();
				?>
				<?php 
					if(get_sub_field('providerReviewPublished') 
					&& get_sub_field('providerReviewApproved')
					&& !get_sub_field('providerReviewMarkedAsSpam')): ?>

				<?php  
	
					$providerReviewOrder = get_sub_field('providerReviewOrder');
					$providerReviewApproved = get_sub_field('providerReviewApproved');
					$providerReviewMarkedAsSpam = get_sub_field('providerReviewMarkedAsSpam');

					$tmp_dt = new DateTime(get_sub_field('providerReviewDate'));
					$providerReviewDate = $tmp_dt->format("d.m.Y.");	

					$providerReviewConsumationBeginDate = get_sub_field('providerReviewConsumationBeginDate');
					$providerReviewConsumationEndDate = get_sub_field('providerReviewConsumationEndDate');
					$providerReviewRaterName = ucwords(mb_strtolower(get_sub_field('providerReviewRaterName')))." ";
					$providerReviewRaterLastName = mb_strtoupper(mb_substr(get_sub_field('providerReviewRaterLastName'),0,1)).". "; 
					$providerReviewLang = get_sub_field('providerReviewLang');

					$length = strlen(get_sub_field('providerReviewPositiveComment'));
					/*
					$providerReviewPositiveCommentFull = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 100, $length - 100));
					$providerReviewNegativeCommentFull = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 100, $length - 100));

					$providerReviewPositiveCommentPartial = trim(mb_substr(get_sub_field('providerReviewPositiveComment'), 0, 100));
					$providerReviewNegativeCommentPartial = trim(mb_substr(get_sub_field('providerReviewNegativeComment'), 0, 100));
					*/

					$providerReviewPositiveCommentFull = trim(get_sub_field('providerReviewPositiveComment'));
					$providerReviewNegativeCommentFull = trim(get_sub_field('providerReviewNegativeComment'));

					$providerReviewPositiveCommentPartial = truncWords(trim(get_sub_field('providerReviewPositiveComment')), 20);
					$providerReviewNegativeCommentPartial = truncWords(trim(get_sub_field('providerReviewNegativeComment')), 20);

					$providerReviewRating = get_sub_field('providerReviewRating');
				?>

				<div class="review-container review-container-item" data-id="<?php echo $providerReviewOrder; ?>">
					<div class="review">
						<div class="review-testimonial">
							<div class="review-testimonial-pos">
								<span class="review-testimonial-partial">
									
									<?php echo $providerReviewPositiveCommentPartial; ?>
								</span>

								<span class="3dots">...</span>

								<span class="review-testimonial-full hide">
									
									<?php echo $providerReviewPositiveCommentFull; ?>
								</span>
							</div>

							<div class="review-testimonial-neg">
								<span class="review-testimonial-partial">
									
									<?php echo $providerReviewNegativeCommentPartial; ?>
								</span>
								<span class="3dots">...</span>
								<span class="review-testimonial-full hide">
									
									<?php echo $providerReviewNegativeCommentFull; ?>
								</span>
							</div>
							<div class="review-date"><?php echo $providerReviewDate; ?></div>
							<div class="review-expand-full"><span class="review-expand-full-trigger"><?php _e('više', 'albadiem'); ?> <span class="fa fa-expand"></span></span></div>
						</div>
						<div class="reviewer-provider-box">
							<div class="rating-box pos-relative text-center">
								<span class="center-v-h"><span class="rating"><?php echo $providerReviewRating; ?></span> <small>/5</small></span>
							</div>
							<div class="reviewer-provider">
								<div class="reviewer"><?php echo $providerReviewRaterName . $providerReviewRaterLastName; ?></div>
							</div>
							<div class="cf"></div>
						</div>
					</div>
				</div>
			
			<?php 
				endif; 
				endwhile;
				endif;
			?>

			<?php if(false): ?>
			<div class="review-container review-container-item" data-id="1000">

				<div class="single-provider-reviews-showall-box">

					<div class="single-provider-reviews-showall">

						<button name="showAllProviderReviews" data-provider-id="" id="showAllProviderReviews" class="btn" type="button"><?php _e('Prikaži sve ocjene i komentare', 'albadiem'); ?></button>

					</div>

				</div>

			</div>
			<?php endif; ?>

		</div>

	</div>

</div>

<!-- Single Provider All Reviews / End -->