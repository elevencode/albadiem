<!-- Single Provider Locations
================================================== -->
<div id="single-provider-locations-container">

	<div class="pos-relative">

		<div id="single-provider-locations">

			<?php if( have_rows('locationsSubfield') ): ?>
				<?php while( have_rows('locationsSubfield') ) : the_row(); 

					$providerLocationPublished =  get_sub_field('providerLocationPublished');
					$providerLocationOrder =  get_sub_field('providerLocationOrder');
					$providerLocationName =  get_sub_field('providerLocationName');
					$providerLocationCityId =  get_sub_field('providerLocationCityId');

					$tmp = get_sub_field_object('providerLocationCityName');
					$value = $tmp['value'];
					$providerLocationCityName = $tmp['choices'][ $value ];

					$tmp = get_sub_field_object('providerLocationBigCityName');
					$value = $tmp['value'];
					$providerLocationBigCityName = $tmp['choices'][ $value ];

					$providerLocationPhone =  get_sub_field('providerLocationPhone');
					$providerLocationAddress =  get_sub_field('providerLocationAddress');
					$providerLocationMobilePhone =  get_sub_field('providerLocationMobilePhone');
					$providerLocationWeb =  get_sub_field('providerLocationWeb');
					$providerLocationEmail =  get_sub_field('providerLocationEmail');
					$providerLocationFacebookUrl =  get_sub_field('providerLocationFacebookUrl');
					$providerLocationInstagramUrl =  get_sub_field('providerLocationInstagramUrl');

					$providerLocationLat =  get_sub_field('providerLocationLat');
					$providerLocationLon =  get_sub_field('providerLocationLon');
				?>

				<?php if($providerLocationPublished): ?>
				<div class="single-provider-location-item" data-id="">

					<div class="single-provider-location-box">
							
						<h4>
							<?php 
							if($providerLocationName):
								echo $providerLocationName; 
							endif; ?>
						</h4>

						<div class="single-provider-location-address">
							<?php if($providerLocationAddress): ?> 
								<?php echo $providerLocationAddress; ?> <br />
								<?php echo $providerLocationCityName; ?> <br />
								<?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> <br />
								<button class="btn showMapPopPup" type="button" data-location-name="<?php echo $providerLocationName; ?>" data-lat="<?php echo $providerLocationLat; ?>" data-lon="<?php echo $providerLocationLon; ?>"><span class="fa fa-plus-circle"></span> prikaži kartu</button>
						<?php else: ?>

						<?php endif; ?>
						</div>

						<?php if(false): ?>
						<div class="single-provider-location-whours">
							<?php 
								// No working hours localization 
								$noWorkingHour = __("Ne radimo");
								?>
							<ul>			
								<?php if(get_sub_field('providerLocationwhoursMonAmFrom') && get_sub_field('providerLocationwhoursMonPmFrom')): ?>
									<li><span>Pon:</span>
										<?php echo get_sub_field('providerLocationwhoursMonAmFrom') . '-' . get_sub_field('providerLocationwhoursMonAmTo') . '  ' . get_sub_field('providerLocationwhoursMonPmFrom') . '-' . get_sub_field('providerLocationwhoursMonPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursMonAmFrom')): ?>
									<li><span>Pon:</span>
										<?php echo get_sub_field('providerLocationwhoursMonAmFrom') . '-' . get_sub_field('providerLocationwhoursMonPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Pon:</span> <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationwhoursTueAmFrom') && get_sub_field('providerLocationwhoursTueAmTo' ) && get_sub_field('providerLocationwhoursTuePmFrom') && get_sub_field('providerLocationwhoursTuePmTo')): ?>
									<li><span>Uto:</span>
										<?php echo get_sub_field('providerLocationwhoursTueAmFrom') . '-' . get_sub_field('providerLocationwhoursTueAmTo') . ' ' . get_sub_field('providerLocationwhoursTuePmFrom') . '-' . get_sub_field('providerLocationwhoursTuePmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursTueAmFrom') && get_sub_field('providerLocationwhoursTuePmTo')): ?>
									<li><span>Uto:</span>
										<?php echo get_sub_field('providerLocationwhoursTueAmFrom') . '-' . get_sub_field('providerLocationwhoursTuePmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Uto: </span>  <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationwhoursWedAmFrom') && get_sub_field('providerLocationwhoursWedAmTo' ) && get_sub_field('providerLocationwhoursWedPmFrom') && get_sub_field('providerLocationwhoursWedPmTo')): ?>
									<li><span>Sri:</span>
										<?php echo get_sub_field('providerLocationwhoursWedAmFrom') . '-' . get_sub_field('providerLocationwhoursWedAmTo') . ' ' . get_sub_field('providerLocationwhoursWedPmFrom') . '-' . get_sub_field('providerLocationwhoursWedPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursWedAmFrom') && get_sub_field('providerLocationwhoursWedPmTo')): ?>
									<li><span>Sri:</span>
										<?php echo get_sub_field('providerLocationwhoursWedAmFrom') . '-' . get_sub_field('providerLocationwhoursWedPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Sri:</span> <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationwhoursThuAmFrom') && get_sub_field('providerLocationwhoursThuAmTo' ) && get_sub_field('providerLocationwhoursThuPmFrom') && get_sub_field('providerLocationwhoursThuPmTo')): ?>
									<li><span>Čet:</span>
										<?php echo get_sub_field('providerLocationwhoursThuAmFrom') . '-' . get_sub_field('providerLocationwhoursThuAmTo') . '  ' . get_sub_field('providerLocationwhoursThuPmFrom') . '-' . get_sub_field('providerLocationwhoursThuPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursThuAmFrom') && get_sub_field('providerLocationwhoursThuPmTo')): ?>
									<li><span>Čet:</span>
										<?php echo get_sub_field('providerLocationwhoursThuAmFrom') . '-' . get_sub_field('providerLocationwhoursThuPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Čet:</span> <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationwhoursFriAmFrom') && get_sub_field('providerLocationwhoursFriAmTo' ) && get_sub_field('providerLocationwhoursFriPmFrom') && get_sub_field('providerLocationwhoursFriPmTo')): ?>
									<li><span>Pet:</span>
										<?php echo get_sub_field('providerLocationwhoursFriAmFrom') . '-' . get_sub_field('providerLocationwhoursFriAmTo') . '  ' . get_sub_field('providerLocationwhoursFriPmFrom') . '-' . get_sub_field('providerLocationwhoursFriPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursFriAmFrom') && get_sub_field('providerLocationwhoursFriPmTo')): ?>
									<li><span>Pet:</span>
										<?php echo get_sub_field('providerLocationwhoursFriAmFrom') . '-' . get_sub_field('providerLocationwhoursFriPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Pet:</span></li>
								<?php endif; ?>
								
								<?php if(get_sub_field('providerLocationwhoursSatAmFrom') && get_sub_field('providerLocationwhoursSatAmTo' ) && get_sub_field('providerLocationwhoursSatPmFrom') && get_sub_field('providerLocationwhoursSatPmTo')): ?>
									<li><span>Sub:</span>
										<?php echo get_sub_field('providerLocationwhoursSatAmFrom') . '-' . get_sub_field('providerLocationwhoursSatAmTo') . '  ' . get_sub_field('providerLocationwhoursSatPmFrom') . '-' . get_sub_field('providerLocationwhoursSatPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursSatAmFrom') && get_sub_field('providerLocationwhoursSatPmTo')): ?>
									<li><span>Sub:</span>
										<?php echo get_sub_field('providerLocationwhoursSatAmFrom') . '-' . get_sub_field('providerLocationwhoursSatPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Sub:</span> <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationwhoursSunAmFrom') && get_sub_field('providerLocationwhoursSunAmTo' ) && get_sub_field('providerLocationwhoursSunPmFrom') && get_sub_field('providerLocationwhoursSunPmTo')): ?>
									<li><span>Ned:</span>
										<?php echo get_sub_field('providerLocationwhoursSunAmFrom') . '-' . get_sub_field('providerLocationwhoursSunAmTo') . '  ' . get_sub_field('providerLocationwhoursSunPmFrom') . '-' . get_sub_field('providerLocationwhoursSunPmTo'); ?>
									</li>
								<?php elseif(get_sub_field('providerLocationwhoursSunAmFrom') && get_sub_field('providerLocationwhoursSunPmTo')): ?>
									<li><span>Ned:</span>
										<?php echo get_sub_field('providerLocationwhoursSunAmFrom') . '-' . get_sub_field('providerLocationwhoursSunPmTo'); ?>
									</li>
								<?php else: ?>
									<li><span>Ned:</span> <?php echo $noWorkingHour; ?></li>
								<?php endif; ?>

								<?php if(get_sub_field('providerLocationworkingHoursNote')): ?>
									<li><?php echo get_sub_field('providerLocationworkingHoursNote'); ?></li>
								<?php endif; ?>
							</ul>

						</div>
						<?php endif; ?>
						<?php if(false): ?>
							<div class="single-provider-location-cta">

								<?php if($providerLocationMobilePhone): ?>
									<span class="single-provider-location-cta-phone">
										<a href="tel:<?php echo $providerLocationMobilePhone; ?>"><span class="fa fa-phone"></span></a>
									</span>
								<?php endif; ?>
								<?php if($providerLocationWeb): ?>
								
									<span class="single-provider-location-cta-web">
										<a href="<?php echo $providerLocationWeb; ?>"><span class="fa fa-globe"></span></a>
									</span>

								<?php endif; ?>
								<?php if($providerLocationFacebookUrl): ?>
									<span class="single-provider-location-cta-fb">

										<a href="<?php echo $providerLocationFacebookUrl; ?>"><span class="fa fa-facebook"></span></a>

									</span>
								<?php endif; ?>
								<?php if($providerLocationInstagramUrl): ?>
									<span class="single-provider-location-cta-fb">

										<a href="<?php echo $providerLocationInstagramUrl; ?>"><span class="fa fa-facebook"></span></a>

									</span>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					</div>

				</div>
				<?php endif; ?>
				<?php endwhile; ?>
				<?php endif; ?>

		</div>

	</div>

</div>
<!-- Single Provider Locations / End -->