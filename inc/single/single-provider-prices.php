<!-- Single Provider Prices

================================================== -->
<div class="single-provider-prices-box">
    <?php if( have_rows('priceListItem') ): ?>
	<table class="single-provider-prices">
		<thead>
			<tr>
				<th>Opis</th>
				<th>Cijena</th>
			</tr>
		</thead>
		
		<tbody>
            <?php while( have_rows('priceListItem') ): the_row(); 

            // vars
            $providerPriceListPublished = get_sub_field('providerPriceListPublished');
            $providerPriceListItemDescription = get_sub_field('providerPriceListItemDescription');
            $providerPriceListOrder = get_sub_field('providerPriceListOrder');
            $providerPriceListItemFrom = get_sub_field('providerPriceListItemFrom');
            $providerPriceListItemTo = get_sub_field('providerPriceListItemTo');

            ?>
            <?php if($providerPriceListPublished): ?>
            <tr>
                <td><?php echo $providerPriceListItemDescription ?></td>
                <td><?php echo number_format((float)$providerPriceListItemFrom , 2, ',', '.') . ' - ' . number_format((float)$providerPriceListItemTo , 2, ',', '.'); ?> HRK</td>
            </tr>
            <?php endif; ?>
            <?php endwhile; ?>
        </tbody>    
	</table>
    <?php endif; ?>
</div> 