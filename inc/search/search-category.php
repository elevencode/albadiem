<!-- Section Banner Search================================================== -->
<section id="banner-search-container">	
  <div id="banner-search" class="container-fluid no-padding bg-img" data-mobile-src="img/bg/vjencanje-par-mobile.jpg" data-tablet-src="img/bg/vjencanje-par-tablet.jpg" data-desktop-src="img/bg/vjencanje-par.jpg" data-width="1920" data-height="1280" data-ratio="1.5" data-class="" data-alt="Pronađi sve za svoje vjenčanje iz snova na jednom mjestu." data-title="">						
    <div id="szv-search" class="container">			
      <div class="row">				
        <div id="banner-search-fields">                    
          <div class="padding-10px">                        
          <h1><?php echo $qobj->name ?></h1>                        
          <?php include_once('search.php'); ?>                    
          </div>				
        </div>			
      </div>		
    </div>	
  </div>	
</section><!-- Section Banner Search / End -->