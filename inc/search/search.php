<?php

    /**
     * @var $countries WP_Term[]
     */

    global $szvData;

    $countries = get_terms([
        'taxonomy' => 'drzave',
        'hide_empty' => false,
    ]);

    ?>

    <?php

    $argsa = array(
        'parent' => 0,
        'hide_empty' => false
    );
    /**
     * @var $categories WP_Term[]
     */
    $categories = get_categories($argsa);

    if($szvData['wpLangCode'] == 'hr'){
        $countryAllValue = 'vjencanje';
    }else if($szvData['wpLangCode'] == 'en'){
        $countryAllValue = 'wedding';
    }

    ?>

    <!-- Category -->

    <div class="banner-search-box">

        <div class="banner-search-category">

            <div class="banner-search-field-title"><?php _e('Kategorija', 'albadiem'); ?></div>

            <select id="select-category" name="select-category" class="main-search-select">

                <?php if(strpos($szvData['canonical'], '/vjencanje-') !== false || strpos($szvData['canonical'], '/wedding-') !== false): ?>
               
                    <option value="<?php echo $countryAllValue; ?>" selected="selected"><?php _e('Sve', 'albadiem'); ?></option>
                
                <?php else: ?>

                    <option value="<?php echo $countryAllValue; ?>"><?php _e('Sve', 'albadiem'); ?></option>

                <?php endif; ?>

                <?php

                    $excludeCat = [
                        'uncategorized', 
                        'vjencanje', 
                        'wedding', 
                        'tamburasi', 
                        'tambouras', 
                        'mali-restorani', 
                        'small-restaurants', 
                        'izdvajamo',
                        'interesting',
                        'zarucnicki-tecajevi',
                        'engagement-courses'
                    ];

                    $favoriteCat = [
                        'restorani-sale', 
                        'restaurants-and-wedding-venues', 
                        'bands', 
                        'bendovi', 
                        'dj', 
                        'dj-izvodaci', 
                        'photo-video',
                        'foto-video'
                    ];

                    $favoriteCategories = '';
                    $otherCategories = '';

                    foreach ($categories as $category) {
                ?>
                    <?php

                        if(!in_array($category->slug, $excludeCat)){

                            if(strpos($szvData['canonical'], $category->slug) !== false){

                                if(in_array($category->slug, $favoriteCat)){
                                    $favoriteCategories .= '<option value="'.$category->slug.'" selected="selected">'.$category->name.'</option>';
                                }else{
                                    $otherCategories .= '<option value="'.$category->slug.'" selected="selected">'.$category->name.'</option>';
                                }

                            }else{
                                
                                if(in_array($category->slug, $favoriteCat)){
                                    $favoriteCategories .= '<option value="'.$category->slug.'">'.$category->name.'</option>';
                                }else{
                                    $otherCategories .= '<option value="'.$category->slug.'">'.$category->name.'</option>';
                                }

                            }

                        }

                    ?>

                <?php }; ?>

                <?php

                    echo '<optgroup label="'.$szvData['translations']['mostSearched'][$szvData['wpLangCode']].'">';
                    echo $favoriteCategories;
                    echo '</optgroup>';
                    echo '<optgroup label="'.$szvData['translations']['other'][$szvData['wpLangCode']].'">';
                    echo $otherCategories;
                    echo '</optgroup>';

                ?>

            </select>

        </div>

    </div>

    <!-- Country -->

    <div class="banner-search-box">

        <div class="banner-search-country">

            <div class="banner-search-field-title"><?php _e('Država','albadiem'); ?></div>

            <select id="select-country" name="select-country" class="main-search-select">

                <?php if(false): ?>
                <option value="all"><?php _e('Sve', 'albadiem'); ?></option>
                <?php endif; ?>
                <option value="all" selected="selected"><?php _e('Hrvatska', 'albadiem'); ?></option>

                <?php
                /*
                foreach ($countries as $country)
                {
                    ?>
                    <option value="<?php echo $country->slug; ?>"><?php echo $country->name; ?></option>
                    <?php
                }
                */
                ?>
            </select>

        </div>

    </div>

    <?php

    /**
     * @var $cities WP_Term[]
     */
    $cities = get_terms([
        'taxonomy' => 'gradovi',
        'hide_empty' => false,
    ]);

    ?>

    <!-- Town -->

    <div class="banner-search-box">

        <div class="banner-search-town">

            <div class="banner-search-field-title"><?php _e('Grad i šira okolica', 'albadiem'); ?></div>

            <select id="select-city" name="select-city" class="main-search-select">

                <option value="all" selected="selected"><?php _e('Svi', 'albadiem'); ?></option>

                <optgroup label="<?php _e('Uskoro', 'albadiem'); ?>">
                    <?php
                    foreach ($cities as $city)
                    {
                        
                        ?>

                        <option value="<?php echo $city->slug; ?>" disabled="disabled"><?php echo $city->name; ?> (<?php _e('u izradi', 'albadiem'); ?>)</option>
                        <?php
                        
                    }
                    ?>
                </optgroup>

            </select>

        </div>

    </div>


    <!-- Banner Search Button -->

    <div class="banner-search-button-box">

        <div class="banner-search-button">

            <button id="banner-search-trigger" class="button banner-search-trigger-home"><i class="fa fa-search"></i><span>&nbsp;<?php _e('Pretraži', 'albadiem'); ?></span></button>

        </div>

    </div>

    <div class="cf"></div>



    <p><?php _e('Pronađi brzo i jednostavno sve za vjenčanje u Hrvatskoj na jednom mjestu!', 'albadiem'); ?></p>