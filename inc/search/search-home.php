<!-- Section Banner Search

================================================== -->

<?php

	global $szvData;

	$bannerSearchDesktopImg = STATIC_CDN_URL.'/img/bg/banner-search/'.$szvData['translations']['weddingSlug'][$szvData['wpLangCode']].'.jpg';
	$bannerSearchTabletImg = STATIC_CDN_URL.'/img/bg/banner-search/'.$szvData['translations']['weddingSlug'][$szvData['wpLangCode']].'-tablet.jpg';
	$bannerSearchMobileImg = STATIC_CDN_URL.'/img/bg/banner-search/'.$szvData['translations']['weddingSlug'][$szvData['wpLangCode']].'-tablet.jpg';

?>

<section id="banner-search-container">

	<div id="banner-search" class="container-fluid no-padding lazy-load-trigger-bg-img" data-bg="" data-mobile-src="<?php echo $bannerSearchMobileImg; ?>" data-tablet-src="<?php echo $bannerSearchTabletImg; ?>" data-desktop-src="<?php echo $bannerSearchDesktopImg; ?>" data-width="1920" data-height="1280" data-ratio="1.5">

		<div id="szv-search" class="container">

			<div class="row">

				<div id="banner-search-fields">

					<div class="padding-10px">

						<h1><?php _e('Sve Za Vjenčanje', 'albadiem'); ?></h1>

            <?php include_once('search.php'); ?>

					</div>

				</div>

			</div>

		</div>

	</div>

	<noscript>
  	<img src="<?php echo $bannerSearchDesktopImg; ?>" alt="<?php echo $szvData['translations']['wedding'][$szvData['wpLangCode']]; ?>" />
	</noscript>

</section>

<!-- Section Banner Search / End -->