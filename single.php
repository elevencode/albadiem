<?php
/*
Template Name: Single provider template
Template Post Type: post, product, provider 
*/
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package albadiem
 */

global $szvData;

$szvData['hasSearch'] = false;
$szvData['isCategoryListing'] = false;
$szvData['isProvider'] = true;

get_header();
get_template_part( 'inc/layouts/header' );

$online_szv_user_id =  get_field('online_szv_user_id');
$online_szv_service_id =  get_field('online_szv_service_id');
$online_szv_service_lang_id =  get_field('online_szv_service_lang_id');


$wp_post_id =  get_the_ID();

$category = get_the_category(); 

$shortNameDynamicText = get_the_title();
if(get_field('shortName')){
	$shortNameDynamicText = get_field('shortName');
}

$category_primary_name = getPrimaryCategoryByPostId(get_the_ID(), 'name');
$category_primary_id = getPrimaryCategoryByPostId(get_the_ID(), 'id');

?>
	<?php require_once 'inc/single/single-slider.php'; ?>

	<!-- Main Content
		================================================== -->
		<main id="content" role="main">
			<div id="single-provider-box" class="single-provider-box container-fluid pos-relative">
        
        <?php require_once 'inc/layouts/breadcrumb-single-provider.php'; ?>

				<div class="container">
					<div class="row no-gutters" data-sticky-container>
						<div class="col-lg-8 order-lg-0 order-1">
							<div class="single-content-section padding-5px">
								<?php if(get_field('desc')): ?>
									<div class="single-intro-desc expand-box">

										<?php echo get_field('desc'); ?>
										<?php echo '<p>'.get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true).'</p>';  ?>

										<div class="expand-box-overlay">
											<span class="expand-box-trigger">
											<span class="fa fa-angle-down"></span> 
												<?php _e('Prikaži više', 'albadiem'); ?>
											</span>
										</div>	
									</div>
									<?php else: ?>
									<div class="single-intro-desc expand-box">

										<?php //echo the_field('dynamicDesc', 'options'); ?>
										<?php echo '<p>'.get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true).'</p>';  ?>
										<?php if($szvData['wpLangCode'] == 'hr'): ?>
											<p>Želiš odabrati samo najbolje i bezbrižno uživati u pripremama za svoje vjenčanje iz snova?</p>
											<p>Odaberi <strong><?php echo $shortNameDynamicText; ?></strong> i kontaktiraj nas bez ikakvih obaveza, a mi ćemo se potruditi pripremiti ponudu koja će te oduševiti.</p>
										<?php elseif($szvData['wpLangCode'] == 'en'): ?>
											<p>Looking to find only the best and enjoy organizing your dream wedding without worries?</p>
											<p>Choose <strong><?php echo $shortNameDynamicText; ?></strong>, contact us without any obligation and we will give our best to prepare an offer that you can't resist.</p>
										<?php endif; ?>							

										<div class="expand-box-overlay">
											<span class="expand-box-trigger">
											<span class="fa fa-angle-down"></span> 
											<?php _e('Prikaži više', 'albadiem'); ?>
											</span>
										</div>	
									</div>
								<?php endif; ?>
							</div>	

							<div class="single-content-section-styled">
								
								<?php if(get_field('shortName')): ?>

									<h2><?php echo get_field('shortName'). ' Info';  ?></h2>

								<?php endif; ?>

								<hr>
								
									<?php
										
										$categories = get_the_category();
										$category_id = $categories[2]->cat_ID;
										//$category_id_primary = $categories[0]->name;
										//$category_id_primary = $categories[0]->slug;
										$category_id_primary = getPrimaryCategoryByPostId(get_the_ID(), 'slug');
										$parent_category_slug = $category_id_primary;
										
										/*
										echo category_description($category_id);
										*/
									?>
								
								
								<?php if(false): ?>
								<p class="margin-bottom-5px"><strong><?php _e('Informacije o usluzi:', 'albadiem'); ?></strong></p>
								<?php endif; ?>

								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<p>Zašto odabrati baš <strong><?php echo $shortNameDynamicText; ?></strong> na svom vjenčanju? Profesionalni pristup, pažnja koju pridajemo i najmanjim detaljima, kvaliteta usluge... samo su neki od razloga.</p>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<p>Why choose <strong><?php echo $shortNameDynamicText; ?></strong> for your wedding? The professional approach, attention we give to the smallest details, the quality of service... are just some of the reasons.</p>
								<?php endif; ?>							


								<?php require_once 'inc/single/single-provider-category.php'; ?>

							</div>
						
							<div class="single-content-section-styled">
								
								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<h2>Posebne ponude, akcije, popusti...</h2>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<h2>Special offers, discounts,...</h2>
								<?php endif; ?>		

								<hr>

								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<p>Trenutno nema aktivnih posebnih ponuda, akcija i popusta.</p>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<p>There are currently no active special offers, promotions and discounts.</p>
								<?php endif; ?>		

								<?php if(false): ?>
										<?php 
											if(have_rows('specialOffers')):
												while(have_rows('specialOffers')) : the_row();
													if(get_sub_field('providerSpecialOffersDesc')):  ?>
														
															<?php echo get_sub_field('providerSpecialOffersDesc'); ?>
														
												<?php 
													else: 
												?>
														
															<?php echo get_field('providerSpecialOffersDynamicDesc', 'options'); ?>
														
											<?php 	
													endif; 
													
													if(get_sub_field('showProviderSpecialOffers')):
														require_once 'inc/single/single-provider-discounts.php';  
													endif;

												endwhile; 
											endif; 
										?>
								<?php endif; ?>
							</div>

							<div class="single-content-section-styled">

								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<h2>Cjenik - Cijene usluga</h2>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<h2>Price list - service prices</h2>
								<?php endif; ?>		

								<hr>

								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<p>Trenutno nema aktivnih cjenika.<br />Za detalje vezane uz cijene <a href="#contact">pošaljite neobvezujući upit</a> ili nazovite.</p>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<p>There are no active price lists currently.<br />For all the details about prices <a href="#contact">send us inquiry without any obligation</a> or call us.</p>
								<?php endif; ?>		

								<?php if(false): ?>
									<?php 
										
										if(have_rows('prices')):
											while(have_rows('prices')) : the_row(); 
												if(get_sub_field('providerPricesDesc')): ?>
													<?php echo get_sub_field('providerPricesDesc'); ?>
										
												<?php else: ?>

													<?php echo get_field('providerPricesDynamicDesc', 'options'); ?>
										
											<?php 

												endif;
												
												require_once 'inc/single/single-provider-prices.php'; 
											
											endwhile;
										endif;
									?>
								<?php endif; ?>
							</div>
							
							<?php 
								if(have_rows('locations')):
									while(have_rows('locations')) : the_row(); 
										if(get_sub_field('showProviderLocations')): ?>
											<div class="single-content-section-styled">
												
												<?php if($szvData['wpLangCode'] == 'hr'): ?>
													<h2>Lokacije</h2>
												<?php elseif($szvData['wpLangCode'] == 'en'): ?>
													<h2>Locations</h2>
												<?php endif; ?>		
												
												<hr>
											
												<?php if($szvData['wpLangCode'] == 'hr'): ?>
													<p>Gdje mogu pronaći <strong><?php echo $shortNameDynamicText; ?></strong>?</p>
												<?php elseif($szvData['wpLangCode'] == 'en'): ?>
													<p>Where can I find <strong><?php echo $shortNameDynamicText; ?></strong>?</p>
												<?php endif; ?>	

												<?php 
													
													if(false){
														if(get_sub_field('providerLocationsDesc')){
															echo get_sub_field('providerLocationsDesc');
														}else{
															get_field('providerLocationsDynamicDesc', 'options');
														}
													}
														
													require_once 'inc/single/single-provider-locations.php'; 
												?>
											
											</div>
											<?php 
										endif; 
									endwhile;
								endif;
							?>


							<div id="reviews"></div>
							<div class="single-content-section-styled">
								
								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<h2><?php echo $shortNameDynamicText; ?> - ocjene, komentari & iskustva</h2>
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<h2><?php echo $shortNameDynamicText; ?> - reviews, rating & comments</h2>
								<?php endif; ?>

									<hr>

								<?php 
									if(have_rows('reviews')): ?>
										<?php while(have_rows('reviews')) : the_row();
											
											if(get_sub_field('showProviderReviews')): ?>
											
												

												<p><?php _e('Pročitajte komentare i iskustva korisnika, te saznajte kako su korisnici ocjenili uslugu.', 'albadiem'); ?></p>

												<?php	require_once 'inc/single/single-provider-reviews.php'; ?>
											
												<?php else: ?> 

													<?php if($szvData['wpLangCode'] == 'hr'): ?>
														<p>Trenutno nema aktivnih ocjena i komentara za <em><?php echo $shortNameDynamicText; ?></em>.</p>
													<?php elseif($szvData['wpLangCode'] == 'en'): ?>
														<p>There are no active reviews and comments currently for <em><?php echo $shortNameDynamicText; ?></em>.</p>
													<?php endif; ?>	

												<?php endif; ?>	
										<?php endwhile;
									else:
								?>
									
								<?php endif; ?>

								<?php if(false): ?>
								<p class="leave-comment-box"><button name="leaveReview" id="leaveReview" class="btn" type="button"><?php _e('Ostavi svoj komentar', 'albadiem'); ?></button></p>
								<?php endif; ?>
							</div>
						
							<?php if(false): ?>
							<div id="socialBox">
								<?php require_once 'inc/single/single-provider-social.php'; ?>
							</div>
							<?php endif; ?>

						
						</div>

					
						<div class="col-lg-4 order-lg-1 order-0">
							<div id="single-affix-cta-mobile-placeholder" class="hide"></div>
							<div id="single-affix-cta-mobile" class="single-affix-cta-mobile" data-margin-top="0" data-sticky-class="is-sticky">
								<div class="single-affix-cta-box">
									<div class="row no-gutters">
										<div class="col-5">
											<span class="single-affix-cta-phone">
																															
												<?php 

													$providerContact = getProviderContacts(get_the_ID());
													//var_dump($providerContact);
													$shortNamePhone = get_field('shortName');

												?>
												
					
													<?php if(isset($providerContact['callPhone']) && !isset($providerContact['callMobilePhone'])): ?>	
														<span class="single-provider-cta-phone-box">
															<a data-href="<?php echo $providerContact['callPhone']; ?>" class="single-provider-cta-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNamePhone; ?>" data-wp-id="<?php echo $wp_post_id; ?>">
																<span class="fa fa-plus-circle"></span> <?php if(false): ?>	<span class="fa fa-phone"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callPhone']); ?>
															</a>
														</span>
													<?php endif; ?> 
													<?php if(isset($providerContact['callMobilePhone'])): ?>	
														<span class="single-provider-cta-phone-box">
															<a data-href="<?php echo $providerContact['callMobilePhone']; ?>" class="single-provider-cta-mobile-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNamePhone; ?>" data-wp-id="<?php echo $wp_post_id; ?>">
																<span class="fa fa-plus-circle"></span> <?php if(false): ?>	<span class="fa fa-mobile"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callMobilePhone']); ?>
															</a>
														</span>
													<?php endif; ?>
																		

											</span>
										</div>
										<div class="col-7 text-right">
											<span class="single-affix-cta-contact">
												<button class="btn scrollToContactForm single-info-cta-button" type="button">
													<?php echo $szvData['translations']['sendInquiry'][$szvData['wpLangCode']]; ?>
												</button>
											</span>
										</div>
									</div>
								</div>
							</div>

							<div id="single-info-cta-holder" data-margin-top="0" data-sticky-for="991" data-sticky-class="is-sticky">
								<div class="single-info-cta-box pos-relative">
									<div class="single-info-cta-title pos-relative">
										<?php if(get_field('shortName')): ?>
										<?php echo get_field('shortName') ?>
										<?php endif; ?>

										<?php $shortNameFav = get_field('shortName'); ?>

										<div class="add-to-favorites">
											<div class="add-to-favorites-trigger" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNameFav; ?>">												
											</div>
											<span class="fa fa-plus favorites-change-icon" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNameFav; ?>"></span>
										</div>
									</div>

									<?php 
										if( have_rows('logo') ):
											while( have_rows('logo') ): the_row(); 
												$providerLogoOriginalUrl = get_sub_field('providerLogoOriginalUrl');
												$providerLogoOriginalDesktopUrl = get_sub_field('providerLogoOriginalDesktopUrl');
												$providerLogoOriginalTabletUrl = get_sub_field('providerLogoOriginalTabletUrl');
												$providerLogoOriginalMobileUrl = get_sub_field('providerLogoOriginalMobileUrl');
												$providerLogoAlt = get_sub_field('providerLogoAlt');
												$providerLogoDesc = get_sub_field('providerLogoDesc');
												$providerLogoWidth = get_sub_field('providerLogoWidth');
												$providerLogoHeight = get_sub_field('providerLogoHeight');
												$providerLogoRatio = get_sub_field('providerLogoRatio');
												$providerLogoClass = get_sub_field('providerLogoClass'); 
																					
												if(get_sub_field('providerHasLogoImg')): ?>
													<div class="single-info-cta-logo">
														<img class="lazy-load-trigger-img" 
														alt="<?php echo $providerLogoAlt; ?> Logo" 
														data-mobile-src="<?php echo $providerLogoOriginalMobileUrl; ?>" 
														data-tablet-src="<?php echo $providerLogoOriginalTabletUrl; ?>" 
														data-desktop-src="<?php echo $providerLogoOriginalDesktopUrl; ?>" 
														data-width="<?php echo $providerLogoWidth ?>" 
														data-height="<?php echo $providerLogoHeight ?>" 
														data-ratio="<?php echo $providerLogoRatio ?>" />
													</div>
													<noscript>
														<img src="<?php echo $providerLogoOriginalDesktopUrl; ?>" alt="<?php echo $providerLogoAlt; ?> Logo" />
													</noscript>
									<?php 
												endif;
											endwhile;
										endif; 
									?>	
									
									<?php
										$providerReviews = getAllSingleProviderReviews(get_the_id());
									?>
										
										
											

									<?php if($providerReviews['ratingNmbr'] > 0): ?>
										<div class="single-info-cta-reviews">
											<a href="#reviews" class="scrollToSingleProviderReviews">
												<span class="rating"><?php echo $providerReviews['avgRating']; ?></span> 
												<small>/5</small>&nbsp;&nbsp;
												<br><?php echo $szvData['translations']['read'][$szvData['wpLangCode']]; ?>
												<br><?php echo $szvData['translations']['comments'][$szvData['wpLangCode']]; ?>
											</a>
										</div>
									<?php else: ?>
										<?php if(false): ?>
											<div class="single-info-cta-reviews">
												<a href="#reviews" class="scrollToSingleProviderReviews">
													<span class="rating"></span> 
													&nbsp;&nbsp;
													<br><?php echo $szvData['translations']['no'][$szvData['wpLangCode']]; ?>
													<br><?php echo $szvData['translations']['reviews'][$szvData['wpLangCode']]; ?>
												</a>
											</div>
										<?php endif; ?>
									<?php endif; ?>

									<div class="cf"></div>
									<ul>
										<li class="single-info-cta-location">
			
										<?php
											// Display top parent category
										
											//echo $category[0]->cat_name;
											//var_dump($category);

											echo get_field('categorySingularNameForPosts', 'category_'.$category[0]->term_id);

											echo ' '.strtolower($szvData['translations']['for'][$szvData['wpLangCode']]).' ';

											echo strtolower($szvData['translations']['weddings'][$szvData['wpLangCode']]);
				
											// Display ACF city name 
											$fieldCityName = get_field_object('providerCityName');
											$valueCityName = $fieldCityName['value'];
											$labelCityName = $fieldCityName['choices'][ $valueCityName ];
											//echo $labelCityName;
									
											// Display ACF Big city name 
											$fieldBigCityName = get_field_object('providerBigCityName');
											$valueBigCityName = $fieldBigCityName['value'];
											$labelBigCityName = $fieldBigCityName['choices'][ $valueBigCityName ];
											//echo $labelBigCityName;

											if($labelCityName == $labelBigCityName){
												if($szvData['wpLangCode'] == 'hr'){
													echo ' '.$labelBigCityName; 
												}else{
													echo ' '.$labelBigCityName.' ('.$szvData['translations']['croatia'][$szvData['wpLangCode']].')';
												}
											}else{
												if($szvData['wpLangCode'] == 'hr'){
													echo ' '.$labelCityName.' ('.$labelBigCityName.')'; 
												}else{
													echo ' '.$labelCityName.' ('.$labelBigCityName.' - '.$szvData['translations']['croatia'][$szvData['wpLangCode']].')'; 
												}
											}
											
											echo '.';
											
										?>

										</li>

										<?php

										// Display Prices in info box
										if( have_rows('prices') ):
											while( have_rows('prices') ): the_row(); 
												if(get_sub_field('showGeneralPrice')): ?>
													<li class="single-info-cta-price">
														<span>
															<?php echo $szvData['translations']['price'][$szvData['wpLangCode']]; ?>:
														</span>
														<?php
														if(get_sub_field('providerGeneralPriceForm') && get_sub_field('providerGeneralPriceTo')){
															echo get_sub_field('providerGeneralPriceForm').' HRK - '.get_sub_field('providerGeneralPriceTo').' HRK ';
														}else{

															if(get_sub_field('providerGeneralPriceForm')){
																echo ' '.$szvData['translations']['from'][$szvData['wpLangCode']].' '.get_sub_field('providerGeneralPriceForm').' HRK ';
															}else if(get_sub_field('providerGeneralPriceTo')){
																echo ' '.$szvData['translations']['upTo'][$szvData['wpLangCode']].' '.get_sub_field('providerGeneralPriceTo').' HRK ';
															}

														}
														if(get_sub_field('providerGeneralPriceDescription')){
															echo get_sub_field('providerGeneralPriceDescription');
														}
														?>
													</li>
												<?php else: ?>
													<li class="single-info-cta-price"><span><?php echo $szvData['translations']['price'][$szvData['wpLangCode']]; ?>:</span> <?php echo $szvData['translations']['onInquiry'][$szvData['wpLangCode']]; ?></li>	
												<?php endif;
										 	endwhile;
										endif; 
										?>

										<?php // Display Address ?>
										<?php if(get_field('providerMainAddress') && (get_field('providerMainAddress') != $labelBigCityName)): ?>
											<li class="single-info-cta-address">
												<span>
													<?php echo $szvData['translations']['address'][$szvData['wpLangCode']]; ?>:
												</span>
												<?php 
												
													if($szvData['wpLangCode'] == 'en' && (strpos(strtolower(get_field('providerMainAddress')), 'hrvatska') !== false)){
														$tmpAddress = get_field('providerMainAddress');
														$tmpAddress = str_ireplace("hrvatska","Croatia", $tmpAddress);
														echo $tmpAddress;
													}else{
														echo get_field('providerMainAddress'); 
													}
													
													if (strpos(strtolower(get_field('providerMainAddress')), strtolower($labelCityName)) !== false) {
														
													}else{
														echo ', '.$labelCityName;
													}
	
												?>
											</li>
										<?php endif; ?>

										<?php // Display Contact Person ?>
										<?php if(get_field('providerShowContactPerson')): ?>
											<li class="single-info-cta-person">
												<span>
													<?php echo $szvData['translations']['contactPerson'][$szvData['wpLangCode']]; ?>:
												</span>
												<?php echo get_field('providerContactPersonName') . ' ' . get_field('providerContactPersonSurname'); ?>
											</li>
									<?php
										endif; 
									?>

									</ul>
									<div id="single-affix-cta" class="single-affix-cta">
										<div class="single-affix-cta-box">
											<div class="row no-gutters">

											<div class="col-5">
												<?php if(isset($providerContact['callPhone']) && !isset($providerContact['callMobilePhone'])): ?>	
													<span class="single-provider-cta-phone-box">
														<a data-href="<?php echo $providerContact['callPhone']; ?>" class="single-provider-cta-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNamePhone; ?>" data-wp-id="<?php echo $wp_post_id; ?>">
															<span class="fa fa-plus-circle"></span> <?php if(false): ?><span class="fa fa-phone"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callPhone']); ?>
														</a>
													</span>
												<?php endif; ?> 
												<?php if(isset($providerContact['callMobilePhone'])): ?>	
													<span class="single-provider-cta-phone-box">
														<a data-href="<?php echo $providerContact['callMobilePhone']; ?>" class="single-provider-cta-mobile-phone" data-online-u="<?php echo $online_szv_user_id; ?>" data-online-s="<?php echo $online_szv_service_id; ?>" data-online-sl="<?php echo $online_szv_service_lang_id; ?>" data-short-name="<?php echo $shortNamePhone; ?>" data-wp-id="<?php echo $wp_post_id; ?>">
															<span class="fa fa-plus-circle"></span> <?php if(false): ?><span class="fa fa-mobile"></span> <?php endif; ?><?php echo str_replace("+385","0",$providerContact['callMobilePhone']); ?>
														</a>
													</span>
												<?php endif; ?>
											</div>							


												<div class="col-7 text-right">
													<span class="single-affix-cta-contact">
														<button class="btn scrollToContactForm single-info-cta-button" type="button">
															<?php echo $szvData['translations']['sendInquiry'][$szvData['wpLangCode']]; ?>
														</button>
													</span>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	



					<div class="row no-gutters">
						<div class="col-lg-8">
							<div id="contact"></div>
							<div class="single-content-section-styled">
								<h2><?php _e('Kontaktiraj nas bez obaveza', 'albadiem'); ?></h2>
								<hr>
								<?php require_once 'inc/form/contact-form-'.$szvData['wpLangCode'].'.php'; ?>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</main>
		<!-- Main Content / End -->
		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>

<?php

get_template_part( 'inc/layouts/footer' ); 
