<?php

global $szvData;

$szvData['hasSearch'] = true;
$szvData['isCategoryListing'] = true;
$szvData['isProvider'] = false;

get_header(); 

get_template_part( 'inc/layouts/header' ); ?>



	<!--[if lt IE 8]>

		<p class="browserupgrade">Koristite <strong>zastarjeli</strong> preglednik. Molimo <a href="http://browsehappy.com/">nadogradite preglednik</a> kako biste mogli koristiti stranicu.</p>

	<![endif]-->

	<?php $qobj = get_queried_object(); ?>
	<?php $qobj_id = get_queried_object_id(); ?>

	<!-- Wrapper -->

	<div id="wrapper">

		<?php require_once 'inc/advertising/top-bar.php'; ?>
		
		<?php require_once 'inc/search/search-category.php'; ?>

		<!-- Main Content

		================================================== -->

		<main id="content" role="main" class="homepage">

			<div id="homepage-intro" class="container-fluid">

				<div class="container">

          <?php 
          
            //var_dump($qobj);
            //var_dump($qobj_id);
            
          ?>

					<?php require_once 'inc/layouts/breadcrumb.php'; ?>

					<div class="row">

						<?php if(!is_paged()): ?>
						<div class="category-text-intro expand-box">

							<h2><?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?></h2>

							<hr />
	
							<?php if(false): ?>
							<div class="subcategory-links text-left">

								<div class="row justify-content-md-center">

									<div class="col-12 col-xl-auto">

										<ul class="weddings-country-offer-list">
								
											<?php
													//preparing an array to hold later info
													$grandchildren_ids = [];
													//getting the child categories of parent 116
													$args = array(
													'orderby' => 'name',
													'parent' => $qobj_id,
													'taxonomy' => 'category',
													'hide_empty' => 0,
													);
													$categories =  get_categories($args);

													foreach ( $categories as $category ) {
														//setting up the args where the parents are the child categories
														$grandchildrenargs = array(
														'parent' => $category->term_id,
														'taxonomy' => 'category',
														'hide_empty' => 0
														);
														$grandchildrencategories =  get_categories($grandchildrenargs);
														
														foreach ( $grandchildrencategories as $grandchildrencategory ) {
														//getting the grandchildren ids or whatever else is needed and populating the array
															$grandchildren_ids[] = $grandchildrencategory->name;
															echo '<li><a href="' . get_category_link( $grandchildrencategory->term_id ) . '">' . $grandchildrencategory->name . '</a></li>';
														}
													}
											?>
										</ul>
									</div>
								</div>
							</div>
							<?php endif; ?>

							<!-- END NEW -->

							<div class="row">

								<div class="col-lg-4 no-padding">
									<div class="why-us-box">									
										<div class="category-why-us-box-icon"><span class="fa fa-comment"></span></div>
										<div class="category-why-us-box-title"><?php _e('Odaberi samo najbolje', 'albadiem'); ?> </div>
										<div class="why-us-box-desc"><?php _e('Uz ocjene i komentare pronađi najbolju uslugu.' , 'albadiem'); ?></div>
									</div>
								</div>

								<div class="col-lg-4 no-padding">
									<div class="why-us-box">
										<div class="category-why-us-box-icon"><span class="fa fa-envelope"></span></div>
										<div class="category-why-us-box-title"><?php _e('Direktni kontakt', 'albadiem'); ?> </div>
										<div class="category-why-us-box-desc"><?php _e('Kontaktiraj pružatelje usluga direktno, BEZ PROVIZIJE.', 'albadiem'); ?></div>
									</div>
								</div>

								<div class="col-lg-4 no-padding">
									<div class="why-us-box">
										<div class="category-why-us-box-icon"><span class="fa fa-user-plus"></span></div>
										<div class="category-why-us-box-title"><?php _e('Multi Upit', 'albadiem'); ?></div>
										<div class="category-why-us-box-desc"><?php _e('Pošalji multi upit odjednom više pružatelja usluga.', 'albadiem'); ?></div>
									</div>
								</div>

							</div>

							<?php 
							
								//echo category_description( $qobj->cat_ID ); 
								
							?>

							<div class="category-dynamic-desc">
								<?php if($szvData['wpLangCode'] == 'hr'): ?>
									<p>Želiš saznati cijene, dostupnost termina i dobiti detaljnu ponudu?</p>
									<p><?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - brzo i jednostavno pronađi samo najbolje za svoje vjenčanje iz snova u Hrvatskoj.
									Pošalji informativni upit ili nazovi bez obveze odmah.</p> 
									<p>Kako odabrati samo najbolje? Sve Za Vjenčanje - SZV Portal nudi mogućnost pregleda ocjena i komentara, slanja upita pružateljima usluga direktno, BEZ PROVIZIJE. <?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - odaberi pružatelje usluga i pošalji im multi upit.</p> 
								<?php elseif($szvData['wpLangCode'] == 'en'): ?>
									<p>Want to get prices, availability and a detailed quote?</p>
									<p><?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - quickly and easily find the best for your dream wedding in Croatia. Send an inquiry or call directly without any obligation.</p> 
									<p>How to choose only the best? All For Wedding - SZV Portal offers the possibility to browse reviews and comments, contact service providers directly, WITHOUT COMMISSION. <?php echo $qobj->name ?> <?php echo $szvData['translations']['croatia'][$szvData['wpLangCode']]; ?> - choose your favorite service providers and send them a multi inquiry.</p> 
								<?php endif; ?>
							</div>

							<div class="expand-box-overlay"><span class="expand-box-trigger"><span class="fa fa-angle-down"></span> <?php _e('Prikaži više', 'albadiem') ?></span></div>

						</div>
						<?php endif; ?>	

					</div>

				</div>

			

			</div>


			<?php if(!is_paged()): ?>
				<?php require_once 'inc/top-3-providers-latest-comment.php'; ?>
			<?php endif; ?>
			

			<div id="listing"></div>
      

			<?php //require_once 'inc/category/category-advertised-listing.php'; ?>		

			<?php require_once 'inc/category/category-main-listing.php'; ?>					

			

		</main>

		<!-- Main Content / End -->

		

		<?php require_once 'inc/cta/cta-create-profile-fw.php'; ?>

	</div>	

<?php get_template_part( 'inc/layouts/footer' );  ?>



